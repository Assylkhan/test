<?php
/**
 * Created by PhpStorm.
 * User: pomidorcheg
 * Date: 22.10.2014
 * Time: 0:19
 */

class PaymentWidget extends CWidget {

    public $id;

    public function run() {
        $this->render('payment', array(
            'btn_id' => $this->id
        ));
    }
}