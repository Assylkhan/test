<?php
/**
 * Created by PhpStorm.
 * User: pomidorcheg
 * Date: 16.10.2014
 * Time: 1:59
 */
?>

<script>
    $( document ).ready(function() {
        // Всплывающее окошко для пополнения баланса
        $("#<?= $btn_id ?>").on("click", function(){
            $('#<?= $btn_id ?>_modal').modal('show');
            return false;
        });

        document.getElementById('summ').onkeypress = function(e) {
            e = e || event;

            if (e.ctrlKey || e.altKey || e.metaKey) return;

            var chr = getChar(e);

            // с null надо осторожно в неравенствах,
            // т.к. например null >= '0' => true
            // на всякий случай лучше вынести проверку chr == null отдельно
            if (chr == null) return;

            if (chr < '0' || chr > '9') {
                return false;
            }
        }

        $("#topUp").click(function() {
            if ($("#summ").val() == '') {
                $("#topup_form").css('display', 'block').html('Не указана сумма пополнения');
                $("#summ").addClass('input-error');
                return false;
            } else {
                $("#topup_form").css('display', 'none').html('');
                $("#summ").removeClass('input-error');
                return true;
            }
        });
    });



    function getChar(event) {
        if (event.which == null) {
            if (event.keyCode < 32) return null;
            return String.fromCharCode(event.keyCode) // IE
        }

        if (event.which!=0 && event.charCode!=0) {
            if (event.which < 32) return null;
            return String.fromCharCode(event.which)   // остальные
        }

        return null; // специальная клавиша
    }

</script>

<!-- Прописываем чутка частненьких стилей -->
<style>
    #<?= $btn_id ?>_modal .footer {
        text-align: right;
    }
</style>

<div class="modal fade" id="<?= $btn_id ?>_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <!--                <h4 class="modal-title">Modal title</h4>-->
            </div>
            <div class="modal-body">
                <form action="/user/topUp" method="post">
                    <div class="modal-row">
                        <div class="span6">
                            <div class="header-block">
                                <h6>Личный кабинет - Название предприятия</h6>
                            </div>
                            <div class="header-block">
                                <h1>Пополнить баланс</h1>
                            </div>
                            <div class="message">
                                <span class="balance_error" id="topup_form"></span>
                                <label for="summ" class="reg-form-label">Сумма пополнения</label>
                                <input type="text" name="summ" id="summ" class="reg-form-input">
                            </div>
                            <div class="footer">
                                <input type="submit" class="hover-green" id="topUp" value="пополнить">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!--            <div class="modal-footer"></div>-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->