<?php
/**
 * Created by PhpStorm.
 * User: payment
 * Date: 22.10.2014
 * Time: 0:22
 */
?>

<script>
    $( document ).ready(function() {
        // Всплывающее окошко для пополнения баланса
        $("#<?= $btn_id ?>").on("click", function(){
            $('#<?= $btn_id ?>_modal').modal('show');
            return false;
        });

        $(".card").click(function() {
            $("#card_payment").submit();
        });

        $(".balance").click(function() {
            $("#balance_payment").submit();
        });
    });
</script>

<div class="modal fade" id="<?= $btn_id ?>_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <!--                <h4 class="modal-title">Modal title</h4>-->
            </div>
            <div class="modal-body">
                <div class="modal-row">
                    <div class="span6">
                        <div class="header-block">
                            <h6>Личный кабинет - Название предприятия</h6>
                        </div>
                        <div class="header-block">
                            <h1>Способы оплаты</h1>
                        </div>
                        <div class="message">
                            <form action="/user/cardPayment" method="post" id="card_payment">
                                <div class="payment-block card" id="textinside"></div>
                            </form>
                            <form action="/user/balancePayment" method="post" id="balance_payment">
                                <div class="payment-block balance" id="textinside"></div>
                            </form>
                            <div class="payment-block no-right"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--            <div class="modal-footer"></div>-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->