<?php
/**
 * Created by PhpStorm.
 * User: pomidorcheg
 * Date: 16.10.2014
 * Time: 1:58
 */

class BalanceWidget extends CWidget
{
    public $id;
    public $merchantId;

    public function run()
    {
        if (!$this->id) throw new CException('Не передан ID кнопки вызова окна');
//        if (!$this->merchantId) throw new CException('Не передан merchantId клиента');

        $this->render('balance', array(
            'btn_id' => $this->id
        ));
    }
}