<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $userid
 * @property string $email
 * @property string $createdate
 * @property integer $userroleid
 * @property string $password
 * @property string $nickname
 * @property string $activateddate
 *
 * The followings are the available model relations:
 * @property Cards[] $cards
 * @property UserNotifications[] $userNotifications
 * @property UserRoles $userrole
 */
class Users extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('email, userroleid, password', 'required',
                'message' => 'Поле {attribute} обязательно.'),
            array('userroleid', 'numerical', 'integerOnly' => true),
            array('email', 'length', 'max' => 50),
            array('password', 'length', 'max' => 100),
            array('nickname', 'length', 'max' => 25),
           /* array('nickname', 'unique',
                'caseSensitive' => false,
                'allowEmpty' => true,
                'message' => 'Поле {attribute} - {value} занято.'
            ),*/
            array('activateddate', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('userid, email, createdate, userroleid, nickname', 'safe', 'on' => 'search'),
            array('createdate', 'default',
                'value' => new CDbExpression('NOW()'),
                'setOnEmpty' => false, 'on' => 'insert'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'cards' => array(self::HAS_MANY, 'Cards', 'userid'),
            'userNotifications' => array(self::HAS_MANY, 'UserNotifications', 'userid'),
            'userrole' => array(self::BELONGS_TO, 'UserRoles', 'userroleid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'userid' => 'Пользователь',
            'email' => 'Почта',
            'createdate' => 'Дата создания',
            'userroleid' => 'Роль',
            'password' => 'Пароль',
            'nickname' => 'Имя пользователя',
            'activateddate' => 'Activateddate',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('userid', $this->userid);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('createdate', $this->createdate, true);
        $criteria->compare('userroleid', $this->userroleid);
        //$criteria->compare('password',$this->password,true);
        $criteria->compare('nickname', $this->nickname, true);
        $criteria->compare('activateddate', $this->activateddate, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchAdmin()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('userid', $this->userid);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('createdate', $this->createdate, true);
        $criteria->compare('userroleid', $this->userroleid);
        //$criteria->compare('password',$this->password,true);
        $criteria->compare('nickname', $this->nickname, true);
        $criteria2 = new CDbCriteria;
        $criteria2->addInCondition('userroleid', array('1', '2'), 'OR');
        $criteria->mergeWith($criteria2);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Checks if the given password is correct.
     * @param string the password to be validated
     * @return boolean whether the password is valid
     */
    public function validatePassword($password)
    {
        return CPasswordHelper::verifyPassword($password, $this->password);
    }

    /**
     * Generates the password hash.
     * @param string password
     * @return string hash
     */
    public function hashPassword($password)
    {
        return CPasswordHelper::hashPassword($password);
    }

    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $this->password = $this->hashPassword($this->password);
            } else {
                $oldPass = Users::model()->findByPk($this->userid);
                if ($oldPass->password != $this->password)
                    $this->password = $this->hashPassword($this->password);
            }
            return true;
        } else
            return false;
    }

    public function emailIsFree($email)
    {
        return $this->find('email=:email', array(':email' => $email));
    }

    public function getRoleName()
    {
        $item = $this->userrole;
        return $item ? $item->name : '';
    }


    public function sendMailActivation()
    {
        if($this->email == 'test@in4smart.kz')
		{
			return;
		}

        Yii::import('ext.YiiMailer.YiiMailer');
        $host = Yii::app()->params['host'];
        $content = "
		Пожалуйста, сохраните это сообщение. Параметры вашей учетной записи таковы:<br>
		Имя пользователя:" . $this->nickname . "<br>
		Пароль:<br>
		Ваша учётная запись ещё не активна. Перейдите по указанной ссылке для активации Вашего аккаунта: <a href='http://{$host}/site/ActivateUser?user={$this->email}'>Активировать </a><br>
        Не забывайте свой пароль: он хранится в нашей базе в зашифрованном виде, и мы не сможем вам его выслать. Если вы всё же забудете пароль, то сможете запросить новый, который, будет необходимо активировать таким же образом, как и вашу учётную запись.<br>
        Спасибо за то, что зарегистрировались на нашем сайте in4smart.kz<br>
        <h4>С Уважением, команда Info Smart!</h4>";

        $mail = new YiiMailer('contact', array('message' => $content, 'name' => '', 'description' => 'Уведомление об активации'));

        $mail->setFrom(Yii::app()->params['mailingAddress']);
        $mail->setSubject('Добро пожаловать на сайт in4smart.kz!');
        $mail->setTo($this->email);

        if (!$mail->send()) {
            throw new EmailException('dont send error');
        }
    }

    public function sendNotification($content, $subject, $description)
    {
		if($this->email == 'test@in4smart.kz')
		{
			return;
		}

		Yii::import('ext.YiiMailer.YiiMailer');
        $mail = new YiiMailer('contact', array('message' => $content, 'name' => '', 'description' => $description));

        $mail->setFrom(Yii::app()->params['mailingAddress']);
        $mail->setSubject('BCatalog: ' . $subject);
        $mail->setTo($this->email);

        if (!$mail->send()) {
            throw new EmailException('dont send error');
        }
    }
}
