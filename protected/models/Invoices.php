<?php

/**
 * This is the model class for table "invoices".
 *
 * The followings are the available columns in table 'invoices':
 * @property integer $invoiceid
 * @property integer $orderid
 * @property string $filename
 * @property integer $sum
 * @property string $created
 *
 * The followings are the available model relations:
 * @property ActiveInvoices[] $activeInvoices
 * @property Orders $order
 * @property Payments[] $payments
 */
class Invoices extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'invoices';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('orderid, sum', 'required'),
            array('orderid, sum, discount', 'numerical', 'integerOnly' => true),
            array('filename', 'length', 'max' => 60),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('invoiceid, orderid, filename, sum, created, discount', 'safe', 'on' => 'search'),
            array('created', 'default',
                'value' => new CDbExpression('NOW()'),
                'setOnEmpty' => false, 'on' => 'insert'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'activeInvoices' => array(self::HAS_MANY, 'ActiveInvoices', 'invoiceid'),
            'order' => array(self::BELONGS_TO, 'Orders', 'orderid'),
            'payments' => array(self::HAS_MANY, 'Payments', 'invoiceid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'invoiceid' => 'Invoiceid',
            'orderid' => 'Orderid',
            'filename' => 'Filename',
            'sum' => 'Sum',
            'created' => 'Created',
            'discount' => 'Discount',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('invoiceid', $this->invoiceid);
        $criteria->compare('orderid', $this->orderid);
        $criteria->compare('filename', $this->filename, true);
        $criteria->compare('sum', $this->sum);
        $criteria->compare('created', $this->created, true);
        $criteria->compare('discount',$this->discount);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Invoices the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getActiveOrderId()
    {
        //активный заказ бывает только 1
        $v = $this->activeInvoices;
        return $v[0]->orderid;
    }

    public function createPdf()
    {
        // найти активную счет фактуру
        $activeInvoiceId = $this->getActiveOrderId();
        $activeOrder = Orders::model()->findByPk($activeInvoiceId);
        $details = $activeOrder->orderDetails;
        $fullcost = 0;
        foreach ($details as $d) {
            $psd = $d->paidservicedetaild0;
            $ps = $psd->paidservice;
            if($ps->paidserviceid == '777'){
                $fullcost = 1;
            }
        }

        $rd = array();
        // взять сумму и услуги
        foreach ($details as $d) {
            $psd = $d->paidservicedetaild0;
            $ps = $psd->paidservice;
            $rd[] = array(
                'typeid'=>$ps->paidserviceid,
                'price'=>$psd->cost,
                'name' => $ps->name . ($d->paidservicedetaild0->paidserviceid == '777' ? ' №'.$d->paidservicedetaild0->paidservicedetaild:''),
                'cost' => $d->order->card->getCalcPackageCost(
                    $d->months,
                    ($psd->paidserviceid == '4' ? count($d->order->card->getPrivateCard()->proposals) : $psd->count),
                    $psd->paidservicedetaild,
                    $fullcost,
                    $d->months
                ),
                'count' => $psd->count,
                'months'=> $d->months,
                'balance'=> $this->order->card->getPackageBalance($psd->paidserviceid));
        }

        $org = $activeOrder->card;
        return $this->generatePdf($org, $rd, $this->invoiceid);
    }

    private function generatePdf($org, $data, $id)
    {
        date_default_timezone_set('UTC');
        Yii::app()->setLanguage('ru');
        $orgReq = ' БИН / ИИН ' . $org->iinbin . ', ' . $org->orgtype->name . ' "' . $org->orgname . '", Республика Казахстан, г.' . $org->city->name . ', ул.' . $org->street . ', д. №' . $org->housenumber;

        $addStr = '';
        $counter = 1;
        $sum = 0;

        foreach ($data as $v) {
//            if($v['typeid'] == '4'){
//                $itersum = ($v['cost'] * $v['months'] * $v['count']) - $v['balance'];
//            } else {
//                $itersum = ($v['cost'] * $v['months']) - $v['balance'];
//            }
            $itersum = floor($v['cost']) - floor($v['balance']);
            if($v['typeid'] == '777'){
                $v['name'] = $v['name']. " <ul><li>1 рубрика</li><li>25 слов</li><li>Контакты и Размещение на карте</li><li>Лого или картинка - 1 шт.</li><li>Размещение на мобильном приложении и на веб сайте</li></ul>";
            }
            $addStr .= "<tr>
                                <td>{$counter}</td>
                                <td>{$v['name']}</td>
                                <td>{$v['count']} шт.</td>
                                <td>{$v['months']} мес.</td>
                                <td>{$v['price']} тг.</td>
                                <td>{$itersum}</td>
                            </tr>";
            $counter++;
            if($itersum > 0) {
                $sum += floor($itersum);
            }

        }
        $nowdate = new DateTime();
        $rusnum = new RussianNumbers();
        $sumstr = $rusnum->Propis($sum);
        $html = '
                <!doctype html>
                <html>
                    <body>
                        <div style="padding-left:150px;">
                            Внимание! Оплата данного счета означает согласие с условиями поставки товара. Уведомление об оплате обязательно, в противном случае не гарантируется наличие товара на складе. Товар отпускается по факту  прихода денег на р/с Поставщика, самовывозом, при наличии доверенности и документов удостоверяющих личность.
                        </div>
                        <br>
                        <div style="padding-left:50px; padding-right:50px;">
                        <table border="1">
                            <tr>
                                <td>Бенефициар:

                                Товарищество с ограниченной ответственностью "SmartSellingGroup"
                                БИН: 130740025214</td>
                                <td>ИИК
                                KZ838560000006042278</td>
                                <td>Кбе
                                17</td>
                            </tr>

                            <tr>
                                <td>Банк бенефициара:
                                АО "Банк ЦентрКредит"</td>
                                <td>БИК</td>
                                <td>Код назначения платежа 851</td>
                            </tr>
                        </table>
                        <h1><u>Счет на оплату № ' . $id . ' от ' . Yii::app()->dateFormatter->format("d MMMM y", $nowdate->format('d MMMM y')) . ' г.</u></h1>

                        <table style="width:700px;">
                            <tr>
                                <td>Поставщик:</td>
                                <td> БИН / ИИН 130740025214,Товарищество с ограниченной ответственностью "SmartSellingGroup", 010000, Республика Казахстан, г. Астана, ул. Байсеитовой, д. № 8, кв. 93</td>>
                            </tr> <br>
                            <tr>
                                <td>Покупатель:</td>
                                <td> ' . $orgReq . '</td>
                            </tr>
                            <tr>
                                <td>Договор:</td>
                                <td>Без договора</td>
                            </tr>
                        </table> <br>

                        <table border="1">
                            <tr>
                                <td>№</td>
                                <td>Наименование</td>
                                <td>Кол-во</td>
                                <td>Срок</td>
                                <td>Цена</td>
                                <td>Сумма</td>
                            </tr>
                            ' . $addStr . '
                        </table><br>
                        <div style="text-align:right; padding-right:100px;">
                        Итого:  ' . $sum . ' тг<br>
                        Без налога (НДС)
                        </div> <br>
                        Всего наименований  на сумму: ' . $sum . ' тенге 00 тиын <br>
                        Всего к оплате: '. $sum .' тенге 00 тиын<br>
                        Всего к оплате (прописью): '. $sumstr .' тенге ноль тиын<br><br>

                        Исполнитель ___________________________________ /__________/


                        </div>
                    </body>
                </html>';

        $mpdf = Yii::app()->ePdf->mpdf();

        $mpdf->SetDisplayMode('fullpage');

        $mpdf->WriteHTML($html);
        //$mpdf->Output('invoice.pdf', 'I');

        $path = Yii::app()->params['invoicesFilesPath'];
        $mpdf->Output($path . $id . '.pdf', 'F');
        return true;
    }

    public function sendInvoice($email, $orgname, $userContent)
    {

		if($email == 'test@in4smart.kz')
		{
			return;
		}

		$path = Yii::app()->params['invoicesFilesPath'];
        $fileName = $this->invoiceid . '.pdf';
        $fullPath = $path . $fileName;

        Yii::import('ext.YiiMailer.YiiMailer');
        $content = "Здравствуйте, «" . $orgname . "» !<br>

Благодарим, Вас за размещение на нашем информационном справочнике «Info

Smart». Мы получили Вашу заявку и просим сделать оплату за выбранные Вами услуги в 

удобном для Вас порядке. Это может быть оплата через карту или через банк.

После произведения оплаты Ваша компания будет автоматически внесена в 

информационный справочник «Info Smart». <nr>

Мы будем рады видеть Вас на нашем информационном справочнике, «Info Smart». <br>

Надеемся на плодотворное сотрудничество! <br>

С уважением Info Smart!<br><hr>";

        $content .= 'От модератора: ' . $userContent;


        $mail = new YiiMailer('contact', array('message' => $content, 'name' => '', 'description' => 'Счет на оплату'));

        $mail->setFrom(Yii::app()->params['mailingAddress']);
        $mail->setSubject('Сервис Info Smart: Счет на оплату');
        $mail->setTo($email);
        $mail->addAttachment($fullPath);

        if (!$mail->send()) {
            throw new EmailException('dont send error');
        }

    }
}
class RussianNumbers
{
    /**
     * @static Число прописью
     * @param $num int Целое число
     * @return string
     */
    public static function Propis($num)
    {
        // Все варианты написания чисел прописью от 0 до 999 скомпануем в один небольшой массив
        $m = array(
            '0'=>array('0'=>'ноль'),
            '1'=>array('0'=>'-', '1'=>'один', '2'=>'два', '3'=>'три', '4'=>'четыре', '5'=>'пять', '6'=>'шесть', '7'=>'семь', '8'=>'восемь', '9'=>'девять'),
            '2'=>array('0'=>'десять', '1'=>'одиннадцать', '2'=>'двенадцать', '3'=>'тринадцать', '4'=>'четырнадцать', '5'=>'пятнадцать', '6'=>'шестнадцать', '7'=>'семнадцать', '8'=>'восемнадцать', '9'=>'девятнадцать'),
            '3'=>array('0'=>'-', '1'=>'-', '2'=>'двадцать', '3'=>'тридцать', '4'=>'сорок', '5'=>'пятьдесят', '6'=>'шестьдесят', '7'=>'семьдесят', '8'=>'восемьдесят', '9'=>'девяносто'),
            '4'=>array('0'=>'-', '1'=>'сто', '2'=>'двести', '3'=>'триста', '4'=>'четыреста', '5'=>'пятьсот', '6'=>'шестьсот', '7'=>'семьсот', '8'=>'восемьсот', '9'=>'девятьсот'),
            '5'=>array('0'=>'-', '1'=>'одна', '2'=>'две')
        );
//        $m = [
//            ['ноль'],
//            ['-', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'],
//            ['десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать'],
//            ['-', '-', 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто'],
//            ['-', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот'],
//            ['-', 'одна', 'две']
//        ];

        // Все варианты написания разрядов прописью скомпануем в один небольшой массив
        $r = array(
            '0'=>array('0'=>'...ллион', '1'=>'', '2'=>'а', '3'=>'ов'), // используется для всех неизвестно больших разрядов
            '1'=>array('0'=>'тысяч', '1'=>'а', '2'=>'и', '3'=>''),
            '2'=>array('0'=>'миллион', '1'=>'', '2'=>'а', '3'=>'ов'),
            '3'=>array('0'=>'миллиард', '1'=>'', '2'=>'а', '3'=>'ов'),
            '4'=>array('0'=>'триллион', '1'=>'', '2'=>'а', '3'=>'ов'),
            '5'=>array('0'=>'квадриллион', '1'=>'', '2'=>'а', '3'=>'ов'),
            '6'=>array('0'=>'квинтиллион', '1'=>'', '2'=>'а', '3'=>'ов')
            // ,array(... список можно продолжить
        );

        if ($num == 0) return $m[0][0]; # Если число ноль, сразу сообщить об этом и выйти
        $o = array(); # Сюда записываем все получаемые результаты преобразования

        # Разложим исходное число на несколько трехзначных чисел и каждое полученное такое число обработаем отдельно
        foreach (array_reverse(str_split(str_pad($num, ceil(strlen($num) / 3) * 3, '0', STR_PAD_LEFT), 3)) as $k => $p) {
            $o[$k] = array();

            # Алгоритм, преобразующий трехзначное число в строку прописью
            foreach ($n = str_split($p) as $kk => $pp)
                if (!$pp) continue; else
                    switch ($kk) {
                        case 0:
                            $o[$k][] = $m[4][$pp];
                            break;
                        case 1:
                            if ($pp == 1) {
                                $o[$k][] = $m[2][$n[2]];
                                break 2;
                            } else$o[$k][] = $m[3][$pp];
                            break;
                        case 2:
                            if (($k == 1) && ($pp <= 2)) {
                                $o[$k][] = $m[5][$pp];
                            } else {
                                $o[$k][] = $m[1][$pp];
                            }
                            break;
                    }
            $p *= 1;
            if (!$r[$k]) $r[$k] = reset($r);

            # Алгоритм, добавляющий разряд, учитывающий окончание русского языка
            if ($p && $k)switch (true) {
                case preg_match("/^[1]$|^\d*[0,2-9][1]$/", $p):
                    $o[$k][] = $r[$k][0] . $r[$k][1];
                    break;
                case preg_match("/^[2-4]$|\d*[0,2-9][2-4]$/", $p):
                    $o[$k][] = $r[$k][0] . $r[$k][2];
                    break;
                default:
                    $o[$k][] = $r[$k][0] . $r[$k][3];
                    break;
            }
            $o[$k] = implode(' ', $o[$k]);
        }

        return implode(' ', array_reverse($o));
    }
}


