<?php

/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property integer $orderid
 * @property integer $orderstatusid
 * @property integer $cardid
 *
 * The followings are the available model relations:
 * @property ActiveInvoices $activeInvoices
 * @property ActiveOrders[] $activeOrders
 * @property Invoices[] $invoices
 * @property OrderDetails[] $orderDetails
 * @property Cards $card
 * @property OrderStatuses $orderstatus
 */
class Orders extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('orderstatusid, cardid', 'required'),
			array('orderstatusid, cardid, months', 'numerical', 'integerOnly'=>true),
            array('moderationdate, publicationdate, createdate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('orderid, orderstatusid, cardid, moderationdate, publicationdate, createdate, months', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'activeInvoices' => array(self::HAS_ONE, 'ActiveInvoices', 'orderid'),
			'activeOrders' => array(self::HAS_MANY, 'ActiveOrders', 'orderid'),
			'invoices' => array(self::HAS_MANY, 'Invoices', 'orderid'),
			'orderDetails' => array(self::HAS_MANY, 'OrderDetails', 'orderid'),
			'card' => array(self::BELONGS_TO, 'Cards', 'cardid'),
			'orderstatus' => array(self::BELONGS_TO, 'OrderStatuses', 'orderstatusid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'orderid' => 'Orderid',
			'orderstatusid' => 'Orderstatusid',
			'cardid' => 'Cardid',
            'moderationdate' => 'Moderationdate',
            'publicationdate' => 'publicationdate',
            'createdate' => 'Createdate',
			'months' => 'Months',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('orderid',$this->orderid);
		$criteria->compare('orderstatusid',$this->orderstatusid);
		$criteria->compare('cardid',$this->cardid);
        $criteria->compare('moderationdate',$this->moderationdate,true);
        $criteria->compare('publicationdate',$this->publicationdate,true);
        $criteria->compare('createdate',$this->createdate,true);
        $criteria->compare('months',$this->months);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Orders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getActiveInvoiceId()
	{
		if(isset($this->activeInvoices))
			return $this->activeInvoices->invoiceid;
		return null;
	}

	public function setStatusPaid()
	{
        date_default_timezone_set('UTC');
		$this->orderstatusid = 1;//оплачено
        if($this->moderationdate == null){
            $this->moderationdate = new CDbExpression('NOW()');
        }
        if($this->publicationdate == null) {
            $this->publicationdate = new CDbExpression('NOW()');
        }
	}

	public function setStatusInvoicing()
	{
		$this->orderstatusid = 2;//ждет оплаты
        if($this->moderationdate == null){
            $this->moderationdate = new CDbExpression('NOW()');
        }
	}

	public function setStatusAdd()
	{
		$this->orderstatusid = 3;//добавлено
	}

    public function setStatusCancel()
    {
        $this->orderstatusid = 4;//no moderation
    }

	public function getSum()
	{
		$sum = 0;
		$details = $this->orderDetails;
        $discount = 1;
        foreach($details as $v){
            if($v->months == 12 && $v->paidservicedetaild0->paidserviceid == 777){
                $discount = 0.85;
            }
        }

		foreach($details as $v)
		{
			$sum += $v->paidservicedetaild0->cost * $v->months;
		}

		return $sum * $discount;
	}
}
