<?php

/**
 * This is the model class for table "messengers".
 *
 * The followings are the available columns in table 'messengers':
 * @property integer $messengerid
 * @property integer $cardid
 * @property integer $imessengertypeid
 * @property string $value
 *
 * The followings are the available model relations:
 * @property ImessengerTypes $imessengertype
 * @property Cards $card
 */
class Messengers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'messengers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cardid, imessengertypeid, value', 'required'),
			array('cardid, imessengertypeid', 'numerical', 'integerOnly'=>true),
			array('value', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('messengerid, cardid, imessengertypeid, value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'imessengertype' => array(self::BELONGS_TO, 'ImessengerTypes', 'imessengertypeid'),
			'card' => array(self::BELONGS_TO, 'Cards', 'cardid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'messengerid' => 'Мессенджер',
			'cardid' => 'Cardid',
			'imessengertypeid' => 'Тип',
			'value' => 'Мессенджер',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('messengerid',$this->messengerid);
		$criteria->compare('cardid',$this->cardid);
		$criteria->compare('imessengertypeid',$this->imessengertypeid);
		$criteria->compare('value',$this->value,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Messengers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function toString()
	{
		$type = $this->imessengertype;

		$add = '';
		if($type)
			$add =  $type->name.' - ';
		return $add.$this->value;
	}
}
