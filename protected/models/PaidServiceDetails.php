<?php

/**
 * This is the model class for table "paid_service_details".
 *
 * The followings are the available columns in table 'paid_service_details':
 * @property integer $paidservicedetaild
 * @property integer $paidserviceid
 * @property integer $cost
 * @property integer $count
 *
 * The followings are the available model relations:
 * @property Cards[] $cards
 * @property Cards[] $cards1
 * @property OrderDetails[] $orderDetails
 * @property PaidServices $paidservice
 */
class PaidServiceDetails extends CActiveRecord
{

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'paid_service_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('paidserviceid, cost, count', 'required'),
			array('paidserviceid, cost, count', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('paidservicedetaild, paidserviceid, cost, count', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cards' => array(self::HAS_MANY, 'Cards', 'keywordcountid'),
			'cards1' => array(self::HAS_MANY, 'Cards', 'rubriccountid'),
			'orderDetails' => array(self::HAS_MANY, 'OrderDetails', 'paidservicedetaild'),
			'paidservice' => array(self::BELONGS_TO, 'PaidServices', 'paidserviceid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'paidservicedetaild' => 'Paidservicedetaild',
			'paidserviceid' => 'Paidserviceid',
			'cost' => 'Cost',
			'count' => 'Count',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('paidservicedetaild',$this->paidservicedetaild);
		$criteria->compare('paidserviceid',$this->paidserviceid);
		$criteria->compare('cost',$this->cost);
		$criteria->compare('count',$this->count);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PaidServiceDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function behaviors() {
		return array(
			'EJsonBehavior'=>array(
				'class'=>'application.behaviors.EJsonBehavior'
			),
		);
	}
}
