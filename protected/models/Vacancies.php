<?php

/**
 * This is the model class for table "vacancies".
 *
 * The followings are the available columns in table 'vacancies':
 * @property integer $vacancyid
 * @property integer $cardid
 * @property integer $publicationstatusid
 * @property string $name
 * @property string $content
 * @property string $startpublic
 * @property string $endpublic
 * @property string $createdate
 * @property string $updatedate
 *
 * The followings are the available model relations:
 * @property Cards $card
 * @property PublicationStatuses $publicationstatus
 */
class Vacancies extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vacancies';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cardid, publicationstatusid, startpublic, endpublic, createdate', 'required'),
			array('cardid, publicationstatusid, fresh', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>50),
			array('updatedate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('vacancyid, cardid, publicationstatusid, name, content, startpublic, endpublic, createdate, updatedate, fresh', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'card' => array(self::BELONGS_TO, 'Cards', 'cardid'),
			'publicationstatus' => array(self::BELONGS_TO, 'PublicationStatuses', 'publicationstatusid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'vacancyid' => 'Vacancyid',
			'cardid' => 'Cardid',
			'publicationstatusid' => 'Publicationstatusid',
			'name' => 'Name',
			'content' => 'Content',
			'startpublic' => 'Startpublic',
			'endpublic' => 'Endpublic',
			'createdate' => 'Createdate',
			'updatedate' => 'Updatedate',
            'fresh' => 'Fresh',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('vacancyid',$this->vacancyid);
		$criteria->compare('cardid',$this->cardid);
		$criteria->compare('publicationstatusid',$this->publicationstatusid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('startpublic',$this->startpublic,true);
		$criteria->compare('endpublic',$this->endpublic,true);
		$criteria->compare('createdate',$this->createdate,true);
		$criteria->compare('updatedate',$this->updatedate,true);
        $criteria->compare('fresh',$this->fresh);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Vacancies the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
