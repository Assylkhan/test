<?php

/**
 * This is the model class for table "payments".
 *
 * The followings are the available columns in table 'payments':
 * @property integer $paymentid
 * @property integer $paymenttypeid
 * @property string $checknumber
 * @property string $filename
 * @property integer $sum
 * @property integer $invoiceid
 *
 * The followings are the available model relations:
 * @property Invoices $invoice
 * @property PaymentTypes $paymenttype
 */
class Payments extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payments';
	}

	public $image;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('paymenttypeid, sum, invoiceid', 'required'),
			array('paymenttypeid, sum, invoiceid', 'numerical', 'integerOnly'=>true),
			array('filename', 'length', 'max'=>70),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('paymentid,  paymenttypeid, checknumber, filename, sum, invoiceid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'invoice' => array(self::BELONGS_TO, 'Invoices', 'invoiceid'),
			'paymenttype' => array(self::BELONGS_TO, 'PaymentTypes', 'paymenttypeid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'paymentid' => 'Paymentid',

			'paymenttypeid' => 'Paymenttypeid',
			'checknumber' => 'Checknumber',
			'filename' => 'Filename',
			'sum' => 'Sum',
			'invoiceid' => 'Invoiceid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('paymentid',$this->paymentid);
		$criteria->compare('paymenttypeid',$this->paymenttypeid);
		$criteria->compare('checknumber',$this->checknumber,true);
		$criteria->compare('filename',$this->filename,true);
		$criteria->compare('sum',$this->sum);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Payments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function setPaidOnSite()//перевод на счет
	{
		$this->paymenttypeid = 2;
	}
}
