<?php

/**
 * This is the model class for table "selection_top".
 *
 * The followings are the available columns in table 'selection_top':
 * @property integer $selectiontopid
 * @property integer $cardid
 * @property string $date_start
 * @property string $date_stop
 *
 * The followings are the available model relations:
 * @property Cards $card
 */
class SelectionTop extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'selection_top';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cardid, date_start, date_stop', 'required'),
			array('cardid', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('selectiontopid, cardid, date_start, date_stop', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'card' => array(self::BELONGS_TO, 'Cards', 'cardid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'selectiontopid' => 'Selectiontopid',
			'cardid' => 'Cardid',
			'date_start' => 'Date Start',
			'date_stop' => 'Date Stop',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('selectiontopid',$this->selectiontopid);
		$criteria->compare('cardid',$this->cardid);
		$criteria->compare('date_start',$this->date_start,true);
		$criteria->compare('date_stop',$this->date_stop,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SelectionTop the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
