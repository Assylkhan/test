<?php

/**
 * This is the model class for table "news".
 *
 * The followings are the available columns in table 'news':
 * @property integer $newsid
 * @property integer $publicationstatusid
 * @property string $title
 * @property string $content
 * @property string $shortcontent
 * @property string $createdate
 * @property string $photo
 * @property integer $cityid
 * @property string $photosize
 * @property string $phototype
 *
 * The followings are the available model relations:
 * @property Cities $city
 * @property PublicationStatuses $publicationstatus
 */
class News extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'news';
	}

    public $image;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, content, shortcontent, filename, publicationstatusid', 'required',
                'message'=>'Поле {attribute} обязательно.'),
			array('publicationstatusid, cityid', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>250),
			array('filename', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('newsid, publicationstatusid, title, content, shortcontent, createdate, cityid', 'safe', 'on'=>'search'),
            array('createdate','default',
                'value'=>new CDbExpression('NOW()'),
                'setOnEmpty'=>false,'on'=>'insert'),
            array('publicationstatusid','default',
                'value'=>new CDbExpression('1'),
                'setOnEmpty'=>new CDbExpression('1'),'on'=>'insert'),
//            array('image', 'file', 'types'=>'jpg, gif, png'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'city' => array(self::BELONGS_TO, 'Cities', 'cityid'),
			'publicationstatus' => array(self::BELONGS_TO, 'PublicationStatuses', 'publicationstatusid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'image' => 'Фото',
			'filename' => 'Фото',
			'newsid' => 'Идентификатор',
			'publicationstatusid' => 'Статус',
			'title' => 'Название',
			'content' => 'Контент',
			'shortcontent' => 'Описание',
			'createdate' => 'Дата создания',
            'photo' => 'Фото',
            'cityid' => 'Город',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('newsid',$this->newsid);
		$criteria->compare('publicationstatusid',$this->publicationstatusid);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('shortcontent',$this->shortcontent,true);
		$criteria->compare('createdate',$this->createdate,true);
        $criteria->compare('cityid',$this->cityid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeDelete() {
        if($this->filename){
            unlink(Yii::app()->params['uploadNewsImagesPath'].$this->filename);
        }
        return parent::beforeDelete();
    }

    public function getStatusName(){
        $item = $this->publicationstatus;
        return $item ? $item->name : '';
    }
}
