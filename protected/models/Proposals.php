<?php

/**
 * This is the model class for table "proposals".
 *
 * The followings are the available columns in table 'proposals':
 * @property integer $proposalsid
 * @property integer $cardid
 * @property integer $publicationstatusid
 * @property string $name
 * @property string $content
 * @property string $startpublic
 * @property string $endpublic
 * @property string $createdate
 * @property string $updatedate
 * @property integer $version
 * @property integer $rubricid
 *
 * The followings are the available model relations:
 * @property Rubrics $rubric
 * @property Cards $card
 * @property PublicationStatuses $publicationstatus
 */
class Proposals extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'proposals';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('cardid, publicationstatusid, startpublic, endpublic, createdate', 'required'),
            array('cardid, publicationstatusid, version, rubricid, fresh', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>50),
			array('image', 'length', 'max'=>100),
			array('updatedate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('proposalsid, cardid, publicationstatusid, name, content, startpublic, endpublic, createdate, updatedate, version, rubricid, image, fresh', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rubric' => array(self::BELONGS_TO, 'Rubrics', 'rubricid'),
			'card' => array(self::BELONGS_TO, 'Cards', 'cardid'),
			'publicationstatus' => array(self::BELONGS_TO, 'PublicationStatuses', 'publicationstatusid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'proposalsid' => 'Proposalsid',
			'cardid' => 'Cardid',
			'publicationstatusid' => 'Publicationstatusid',
			'name' => 'Name',
			'content' => 'Content',
			'startpublic' => 'Startpublic',
			'endpublic' => 'Endpublic',
			'createdate' => 'Createdate',
			'updatedate' => 'Updatedate',
			'version' => 'Version',
			'rubricid' => 'Rubricid',
            'image' => 'Image',
            'fresh' => 'Fresh',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('proposalsid',$this->proposalsid);
		$criteria->compare('cardid',$this->cardid);
		$criteria->compare('publicationstatusid',$this->publicationstatusid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('startpublic',$this->startpublic,true);
		$criteria->compare('endpublic',$this->endpublic,true);
		$criteria->compare('createdate',$this->createdate,true);
		$criteria->compare('updatedate',$this->updatedate,true);
		$criteria->compare('version',$this->version);
		$criteria->compare('rubricid',$this->rubricid);
        $criteria->compare('image',$this->image,true);
        $criteria->compare('fresh',$this->fresh);


        return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Proposals the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function isPublish()
    {
        date_default_timezone_set('UTC');
        $now = new DateTime();
        $orderdatestart = new DateTime($this->startpublic);
        $orderdateend = new DateTime($this->endpublic);
        $intervalstart = $orderdatestart->diff($now);
        $intervalend = $now->diff($orderdateend);
        if (!$intervalstart->invert && !$intervalend->invert) {
            return true;
        }
        return false;
    }
}
