<?php

/**
 * This is the model class for table "rubrics".
 *
 * The followings are the available columns in table 'rubrics':
 * @property integer $rubricid
 * @property integer $parent_rubricid
 * @property string $name
 * @property integer $rubrictypeid
 * @property integer $rubricgroupid
 * @property string $filename
 *
 * The followings are the available model relations:
 * @property Proposals[] $proposals
 * @property Cards[] $cards
 * @property Rubrics $parentRubric
 * @property Rubrics[] $rubrics
 * @property RubricTypes $rubrictype
 * @property RubricGroups $rubricgroup
 */
class Rubrics extends CActiveRecord
{
    public $image;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rubrics';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('parent_rubricid, rubrictypeid, rubricgroupid, paidgroupid', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>50),
            array('filename', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('rubricid, parent_rubricid, name, rubrictypeid, rubricgroupid, paidgroupid, filename', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'proposals' => array(self::HAS_MANY, 'Proposals', 'rubricid'),
			'cards' => array(self::MANY_MANY, 'Cards', 'ref_rubrics_cards(rubricid, cardid)'),
			'parentRubric' => array(self::BELONGS_TO, 'Rubrics', 'parent_rubricid'),
			'rubrics' => array(self::HAS_MANY, 'Rubrics', 'parent_rubricid'),
			'rubrictype' => array(self::BELONGS_TO, 'RubricTypes', 'rubrictypeid'),
			'rubricgroup' => array(self::BELONGS_TO, 'RubricGroups', 'rubricgroupid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'rubricid' => 'Рубрика',
			'parent_rubricid' => 'Родительская рубрика',
			'name' => 'Название',
			'rubrictypeid' => 'Группа',
			'rubricgroupid' => 'Тип',
            'paidgroupid' => 'Категория базового пакета',
            'image' => 'Изображение',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('rubricid',$this->rubricid);
		$criteria->compare('parent_rubricid',$this->parent_rubricid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('rubrictypeid',$this->rubrictypeid);
		$criteria->compare('rubricgroupid',$this->rubricgroupid);
        $criteria->compare('paidgroupid',$this->paidgroupid);
        $criteria->compare('filename',$this->filename,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Rubrics the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeDelete() {
        if($this->filename){
            unlink(Yii::app()->params['rubricsPath'].$this->filename);
        }
        return parent::beforeDelete();
    }

    protected function beforeSave()
    {
        if(parent::beforeSave())
        {
            $this->rubrictypeid=$this->parent_rubricid ? 2 : 1;
            return true;
        }
        else
            return false;
    }
}
