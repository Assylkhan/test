<?php

/**
 * This is the model class for table "telephones".
 *
 * The followings are the available columns in table 'telephones':
 * @property integer $telephoneid
 * @property integer $cardid
 * @property integer $value
 *
 * The followings are the available model relations:
 * @property Cards $card
 */
class Telephones extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'telephones';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cardid, value', 'required'),
			array('cardid', 'numerical', 'integerOnly'=>true),
            array('value', 'length', 'max'=>16),
            array('note', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('telephoneid, cardid, value, note', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'card' => array(self::BELONGS_TO, 'Cards', 'cardid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'telephoneid' => 'Телефон',
			'cardid' => 'Cardid',
			'value' => 'Телефон',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('telephoneid',$this->telephoneid);
		$criteria->compare('cardid',$this->cardid);
		$criteria->compare('value',$this->value);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Telephones the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static $kzMobileCodes = array(700, 701, 702, 703, 704, 705, 706, 707, 708, 709, 747, 750, 751, 760, 761, 762, 763, 764, 771, 775, 776, 777, 778);

	public function toString()
	{
		$code = substr($this->value, 1, 3);
		if (in_array($code, self::$kzMobileCodes))
		{
			return substr($this->value,0,1). ' ' .  substr($this->value,1,3). ' ' .  substr($this->value,4,  strlen((string)$this->value));
		}
		else
		{
			return substr($this->value,0,1). ' ' .  substr($this->value,1,4). ' ' .  substr($this->value,5,  strlen((string)$this->value));
		}

	}
}
