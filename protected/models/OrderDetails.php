<?php

/**
 * This is the model class for table "order_details".
 *
 * The followings are the available columns in table 'order_details':
 * @property integer $orderdetailid
 * @property integer $orderid
 * @property integer $paidservicedetaild
 * @property string $note
 * @property integer $months
 * @property string $moderationdate
 * @property string $publicationdate
 *
 * The followings are the available model relations:
 * @property PaidServiceDetails $paidservicedetaild0
 * @property Orders $order
 */
class OrderDetails extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'order_details';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('orderid', 'required'),
            array('orderid, paidservicedetaild, months', 'numerical', 'integerOnly'=>true),
            array('note', 'length', 'max'=>250),
            array('moderationdate, publicationdate', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('orderdetailid, orderid, paidservicedetaild, note, months, moderationdate, publicationdate', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'paidservicedetaild0' => array(self::BELONGS_TO, 'PaidServiceDetails', 'paidservicedetaild'),
            'order' => array(self::BELONGS_TO, 'Orders', 'orderid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'orderdetailid' => 'Orderdetailid',
            'orderid' => 'Orderid',
            'paidservicedetaild' => 'Paidservicedetaild',
            'note' => 'Note',
            'months' => 'Months',
            'moderationdate' => 'Moderationdate',
            'publicationdate' => 'Publicationdate',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('orderdetailid',$this->orderdetailid);
        $criteria->compare('orderid',$this->orderid);
        $criteria->compare('paidservicedetaild',$this->paidservicedetaild);
        $criteria->compare('note',$this->note,true);
        $criteria->compare('months',$this->months);
        $criteria->compare('moderationdate',$this->moderationdate,true);
        $criteria->compare('publicationdate',$this->publicationdate,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return OrderDetails the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}