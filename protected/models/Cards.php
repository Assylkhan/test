<?php

/**
 * This is the model class for table "cards".
 *
 * The followings are the available columns in table 'cards':
 * @property integer $cardid
 * @property integer $parentcardid
 * @property integer $userid
 * @property integer $orgtypeid
 * @property integer $keywordcountid
 * @property integer $rubriccountid
 * @property integer $regionid
 * @property integer $cityid
 * @property integer $cardstatusid
 * @property string $orgname
 * @property string $logo
 * @property string $street
 * @property string $housenumber
 * @property string $office
 * @property string $websiteurl
 * @property integer $ispublic
 * @property string $createdate
 * @property string $updatedate
 * @property string $juridicaladdress
 * @property string $iinbin
 * @property string $bik
 * @property string $iik
 * @property string $kbe
 * @property string $bankname
 * @property string $orgindex
 * @property string $workschedule
 * @property string $busnums
 * @property string $updatecontactname
 * @property string $updatecontactphone
 * @property string $accountantname
 * @property string $accountantphone
 * @property string $description
 *
 * The followings are the available model relations:
 * @property ActiveOrders[] $activeOrders
 * @property CardPhotos[] $cardPhotoses
 * @property Cards $parentcard
 * @property Cards[] $cards
 * @property CardStatuses $cardstatus
 * @property Cities $city
 * @property OrgTypes $orgtype
 * @property PaidServiceDetails $keywordcount
 * @property Regions $region
 * @property PaidServiceDetails $rubriccount
 * @property Users $user
 * @property Emails[] $emails
 * @property MapCordinates[] $mapCordinates
 * @property Messengers[] $messengers
 * @property Orders[] $orders
 * @property Proposals[] $proposals
 * @property Keywords[] $keywords
 * @property Rubrics[] $rubrics
 * @property SelectionTop[] $selectionTops
 * @property ShowStatistics[] $showStatistics
 * @property Telephones[] $telephones
 * @property Vacancies[] $vacancies
 * @property RefRubricsCards[] refrubrics
 * @property RefKeywordCard[] refkeywords
 */
class Cards extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cards';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
            array( 'userid, orgtypeid, keywordcountid, rubriccountid, regionid, cityid, cardstatusid,  orgname,  street, housenumber, ispublic, createdate, juridicaladdress, iinbin, bik, iik, kbe, bankname', 'required', 'on' => 'hasNormalUser'),
            array( 'userid, orgtypeid, keywordcountid, rubriccountid, cityid, cardstatusid,  orgname, ispublic, createdate', 'required', 'on' => 'hasTestUser'),

            array('parentcardid, userid, orgtypeid, keywordcountid, rubriccountid, regionid, cityid, cardstatusid, ispublic, main, top, photoscountid, months, rubricid, topmonths, mainmonths', 'numerical', 'integerOnly'=>true),// updatecontactphone, accountantphone
			array('orgname', 'length', 'max'=>100),
			array('logo', 'length', 'max'=>45),
			array('street, juridicaladdress, bankname, orgindex, busnums, updatecontactname, accountantname', 'length', 'max'=>50),
			array('housenumber, office, bik, iik', 'length', 'max'=>20),
			array('websiteurl', 'length', 'max'=>25),
			array('iinbin', 'length', 'max'=>12),
			array('kbe', 'length', 'max'=>4),
			array('logo, updatedate, description, updatecontactphone, accountantphone, moderationdate, publicationdate', 'safe'),
			array('updatecontactphone, accountantphone', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('cardid, parentcardid, userid, orgtypeid, keywordcountid, rubriccountid, regionid, cityid, cardstatusid, orgname,description, logo, street, housenumber, office, websiteurl, ispublic, createdate, updatedate, juridicaladdress, iinbin, bik, iik, kbe, bankname, orgindex, workschedule, busnums, updatecontactname, updatecontactphone, accountantname, accountantphone, main, top, photoscountid, months, moderationdate, publicationdate, rubricid, topmonths, mainmonths', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'activeOrders' => array(self::HAS_MANY, 'ActiveOrders', 'cardid'),
			'cardPhotoses' => array(self::HAS_MANY, 'CardPhotos', 'cardid'),
            'rubric' => array(self::BELONGS_TO, 'Rubrics', 'rubricid'),
			'parentcard' => array(self::BELONGS_TO, 'Cards', 'parentcardid'),
			'cards' => array(self::HAS_MANY, 'Cards', 'parentcardid'),
			'cardstatus' => array(self::BELONGS_TO, 'CardStatuses', 'cardstatusid'),
			'city' => array(self::BELONGS_TO, 'Cities', 'cityid'),
			'orgtype' => array(self::BELONGS_TO, 'OrgTypes', 'orgtypeid'),
			'keywordcount' => array(self::BELONGS_TO, 'PaidServiceDetails', 'keywordcountid'),
			'region' => array(self::BELONGS_TO, 'Regions', 'regionid'),
			'rubriccount' => array(self::BELONGS_TO, 'PaidServiceDetails', 'rubriccountid'),
			'user' => array(self::BELONGS_TO, 'Users', 'userid'),
			'emails' => array(self::HAS_MANY, 'Emails', 'cardid'),
			'mapCordinates' => array(self::HAS_MANY, 'MapCordinates', 'cardid'),
			'messengers' => array(self::HAS_MANY, 'Messengers', 'cardid'),
			'orders' => array(self::HAS_MANY, 'Orders', 'cardid'),
			'proposals' => array(self::HAS_MANY, 'Proposals', 'cardid'),
			'keywords' => array(self::MANY_MANY, 'Keywords', 'ref_keyword_card(cardid, keywordid)'),
			'rubrics' => array(self::MANY_MANY, 'Rubrics', 'ref_rubrics_cards(cardid, rubricid)'),
			'selectionTops' => array(self::HAS_MANY, 'SelectionTop', 'cardid'),
			'showStatistics' => array(self::HAS_MANY, 'ShowStatistics', 'cardid'),
			'telephones' => array(self::HAS_MANY, 'Telephones', 'cardid'),
			'vacancies' => array(self::HAS_MANY, 'Vacancies', 'cardid'),
			'refrubrics' => array(self::HAS_MANY, 'RefRubricsCards', 'cardid'),
			'refkeywords' => array(self::HAS_MANY, 'RefKeywordCard', 'cardid'),
            'statistics' => array(self::HAS_MANY, 'Statistics', 'cardid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cardid' => 'ИД Карточки',
			'parentcardid' => 'ИД карточки родителя',
			'userid' => 'Пользователь',
			'orgtypeid' => 'Тип организации',
			'keywordcountid' => 'Количество ключевых слов',
			'rubriccountid' => 'Количество рубрик',
			'regionid' => 'Регион',
			'cityid' => 'Город',
			'cardstatusid' => 'Статус карточки',
			'orgname' => 'Название организации',
			'logo' => 'Логотип',
			'street' => 'Улица',
			'housenumber' => 'Номер дома',
			'office' => 'Офис',
			'websiteurl' => 'Сайт',
			'ispublic' => 'Ispublic',
			'createdate' => 'Дата создания',
			'updatedate' => 'Updatedate',
			'juridicaladdress' => 'Юр. адрес',
			'iinbin' => 'ИИН/БИН',
			'bik' => 'БИК',
			'iik' => 'ИИК',
			'kbe' => 'Кбе',
			'bankname' => 'Название банка',
			'orgindex' => 'Индекс',
			'workschedule' => 'Расписание',
			'busnums' => 'Транспорт',
			'updatecontactname' => 'Ответственный за обновление',
			'updatecontactphone' => 'Телефон ответств.',
			'accountantname' => 'Бухгалтер',
			'accountantphone' => 'Телефон бух',
			'description' => 'Описание',
			'orderStatus' => 'Статус заказа',
			'publicCardId'=> 'ИД публичной карточки',
            'main' => 'Main',
			'top' => 'Top',
			'photoscountid' => 'Photoscountid',
            'months' => 'Months',
            'moderationdate' => 'Moderationdate',
            'publicationdate' => 'Publicationdate',
            'moderationdate' => 'Moderationdate',
			'rubricid' => 'Rubricid',
            'topmonths' => 'Topmonths',
            'mainmonths' => 'Mainmonths',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($selectFields = false)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		if($selectFields)
		{
			$criteria->select = $selectFields;
		}

		$criteria->compare('cardid',$this->cardid);
		$criteria->compare('parentcardid',$this->parentcardid);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('orgtypeid',$this->orgtypeid);
		$criteria->compare('keywordcountid',$this->keywordcountid);
		$criteria->compare('rubriccountid',$this->rubriccountid);
		$criteria->compare('regionid',$this->regionid);
		$criteria->compare('cityid',$this->cityid);
		$criteria->compare('cardstatusid',$this->cardstatusid);
		$criteria->compare('orgname',$this->orgname,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('street',$this->street,true);
		$criteria->compare('housenumber',$this->housenumber,true);
		$criteria->compare('office',$this->office,true);
		$criteria->compare('websiteurl',$this->websiteurl,true);
		$criteria->compare('ispublic',$this->ispublic);
		$criteria->compare('createdate',$this->createdate,true);
		$criteria->compare('updatedate',$this->updatedate,true);
		$criteria->compare('juridicaladdress',$this->juridicaladdress,true);
		$criteria->compare('iinbin',$this->iinbin,true);
		$criteria->compare('bik',$this->bik,true);
		$criteria->compare('iik',$this->iik,true);
		$criteria->compare('kbe',$this->kbe,true);
		$criteria->compare('bankname',$this->bankname,true);
		$criteria->compare('orgindex',$this->orgindex,true);
		$criteria->compare('workschedule',$this->workschedule,true);
		$criteria->compare('busnums',$this->busnums,true);
		$criteria->compare('updatecontactname',$this->updatecontactname,true);
		$criteria->compare('updatecontactphone',$this->updatecontactphone,true);
		$criteria->compare('accountantname',$this->accountantname,true);
		$criteria->compare('accountantphone',$this->accountantphone,true);
		$criteria->compare('description',$this->description,true);
        $criteria->compare('main',$this->main);
        $criteria->compare('top',$this->top);
        $criteria->compare('photoscountid',$this->photoscountid);
        $criteria->compare('months',$this->months);
        $criteria->compare('moderationdate',$this->moderationdate,true);
        $criteria->compare('publicationdate',$this->publicationdate,true);
        $criteria->compare('rubricid',$this->rubricid);
        $criteria->compare('topmonths',$this->topmonths);
        $criteria->compare('mainmonths',$this->mainmonths);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cards the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if(!$this->isNewRecord)
			{
				$this->updatedate = new CDbExpression('NOW()');
			}
			return true;
		}
		else
			return false;
	}

	public function addressToString()
	{
		return 'г.'. $this->city->name. ' Ул.'.$this->street.' дом. '.$this->housenumber.' '.($this->office ? ' офис '. $this->office: '');
	}

	public function getBranchNameAndId()
	{
		$result = array();
		$fls = $this->cards;
		foreach($fls as $v)
		{
			$result[] = array('cardid'=>$v->cardid, 'name'=> $v->orgtype->name.' '. $v->orgname);
		}

		$publicCardId = $this->getPublicCardId();
		if($publicCardId != $this->cardid && $publicCardId)// private card
		{
			$fls = Cards::model()->findByPk($publicCardId)->cards;
			foreach($fls as $v)
			{
				$result[] = array('cardid'=>$v->cardid, 'name'=> $v->orgtype->name.' '. $v->orgname);
			}
		}

		return $result;
	}

	public function orgNameToString()
	{
		return $this->orgtype->name.' '. $this->orgname;
	}

	public function getKeyWords()
	{
		$result = array();
		$publicCardId = $this->getPublicCardId();
		$kws = $this->keywords;
		foreach($kws as $v)
			$result[] = $v->value;

		if($publicCardId != $this->cardid && $publicCardId)// private card
		{
			$kws = Cards::model()->findByPk($publicCardId)->keywords;
			foreach($kws as $v)
				$result[] = $v->value;
		}

		return $result;
	}

	public function getSubRubricsName()
	{
		$result = array();

		$publicCardId = $this->getPublicCardId();
		$rubrics = $this->rubrics;
		foreach($rubrics as $v)
		{
			if($v->parent_rubricid)
				$result[] = array('rubric'=> $v->parentRubric->name, 'subrubric'=> $v->name);
		}

		if($publicCardId != $this->cardid && $publicCardId)// private card
		{
			$rubrics = Cards::model()->findByPk($publicCardId)->rubrics;
			foreach($rubrics as $v)
			{
				if($v->parent_rubricid)
					$result[] = array('rubric'=> $v->parentRubric->name, 'subrubric'=> $v->name);
			}
		}

		return $result;
	}

	public function getTelephones()
	{
		$result = array();
		$publicCardId = $this->getPublicCardId();

		foreach ($this->telephones as $v) {
			//$result[] = $v->toString();
            if ($v->note != '')
            {
            $result[] = $v->value.'('.$v->note.')';
            }
            else
            {
            $result[] = $v->value;
            }
            }


		if($publicCardId != $this->cardid && $publicCardId)// private card
		{
			$telephones = Cards::model()->findByPk($publicCardId)->telephones;
			foreach ($telephones as $v) {
				//$result[] = $v->toString();
             if($v->note != '')
                  {
               $result[] = $v->value.'('.$v->note.')';
                 }
                else
                {
                $result[] = $v->value;
                    }
			}
		}
		 return $result;

	}

	public function getMessengers()
	{
		$result = array();
		$publicCardId = $this->getPublicCardId();

		foreach ($this->messengers as $v) {
			$result[] = $v->toString();
		}

		if($publicCardId != $this->cardid && $publicCardId)// private card
		{
			$messengers = Cards::model()->findByPk($publicCardId)->messengers;
			foreach ($messengers as $v) {
				$result[] = $v->toString();
			}
		}
		return $result;

	}



	public function getPublicCardId()
	{
		$result = Cards::model()->findByAttributes(array('userid'=>$this->userid, 'ispublic'=>'1'));
        if($result){
            return $result->cardid;
        } else {
            return null;
        }
	}

	public function getPrivateCardId()
	{
		$result = Cards::model()->findByAttributes(array('userid'=>$this->userid, 'ispublic'=>'0'));
        if($result){
            return $result->cardid;
        } else {
            return null;
        }
	}

	public function getActiveOrderId()
	{
		//активный заказ бывает только 1
		$v = $this->activeOrders;
		return $v[0]->orderid;
	}

    public function approveCard(){
        $this->transferPrivateCardFieldValues($this->getPublicCard());
    }

    public function getPublicCard()
    {
        $currentId = $this->cardid;
        $publicId = $this->getPublicCardId();

        if (!$publicId) //если  публичное еще не существует, надо создать// то есть только добавленный
        {
            $new = new Cards();
            $new->ispublic = 1;
            $new->setIsNewRecord(true);
//            if ($new->save()) {
                if ($this->transferPrivateCardFieldValues($new))
                    return $new;
//            }
        } else if ($currentId != $publicId) //если публичная есть
        {
            $publicCard = Cards::model()->findByAttributes(array('cardid' => $publicId, 'ispublic' => 1));
            //if ($this->transferPrivateCardFieldValues($publicCard))
            return $publicCard;
        }
    }

    public function getPrivateCard()
    {
        $cardid = $this->getPrivateCardId();
        $card = Cards::model()->findByAttributes(array('cardid' => $cardid, 'ispublic' => 0));
        return $card;

    }

    private function transferPrivateCardFieldValues($card)
    {
        date_default_timezone_set('UTC');
//        $card = Cards::model()->findByAttributes(array('cardid' => $id, 'ispublic' => 1));
        if ($card) {
            $card->userid = $this->userid;
            $card->ispublic = 1;
            $card->parentcardid = $this->parentcardid;
            $card->orgtypeid = $this->orgtypeid;
            $card->keywordcountid = $this->keywordcountid;
            $card->rubriccountid = $this->rubriccountid;
            $card->photoscountid = $this->photoscountid;
            $card->regionid = $this->regionid;
            $card->cityid = $this->cityid;
            $card->cardstatusid = 1; // на модерации
            $card->orgname = $this->orgname;
            $card->description = $this->description;
            $card->street = $this->street;
            $card->housenumber = $this->housenumber;
            $card->office = $this->office;
            $card->websiteurl = $this->websiteurl;
            $card->juridicaladdress = $this->juridicaladdress;
            $card->iinbin = $this->iinbin;
            $card->bik = $this->bik;
            $card->iik = $this->iik;
            $card->kbe = $this->kbe;
            $card->bankname = $this->bankname;
            $card->orgindex = $this->orgindex;
            $card->createdate = $this->createdate;
            $card->updatedate = new CDbExpression('NOW()');
            $card->workschedule = $this->workschedule;
            $card->busnums = $this->busnums;
            $card->months = $this->months;
            $card->topmonths = $this->topmonths;
            $card->rubricid = $this->rubricid;
            $card->updatecontactname = $this->updatecontactname;
            $card->updatecontactphone = $this->updatecontactphone;
            $card->accountantname = $this->accountantname;
            $card->accountantphone = $this->accountantphone;

            $mapCorPrivate = MapCordinates::model()->findByAttributes(array('cardid' => $card->cardid));
            if($mapCorPrivate) {
                $mapCor = new MapCordinates();
                $mapCor->cardid = $card->cardid;
                $mapCor->valuex = $mapCorPrivate->valuex;
                $mapCor->valuey = $mapCorPrivate->valuey;
                $mapCor->save();
            }

            //emails
            $emails = Emails::model()->findAllByAttributes(array('cardid' => $card->cardid));
            foreach ($emails as $i) {
                $i->delete();
            }
            foreach ($this->emails as $i) {
                $email = new Emails();

                $email->cardid = $card->cardid;
                $email->value = $i->value;
                $email->save();
            }

            //telephones
            $telephones = Telephones::model()->findAllByAttributes(array('cardid' => $card->cardid));
            foreach ($telephones as $i) {
                $i->delete();
            }
            foreach ($this->telephones as $i) {
                $telephone = new Telephones();

               /* $telephone->cardid = $card->cardid;
                $telephone->value = $i->value;*/
                $telephone->cardid = $card->cardid;
                $telephone->value = $i->value;
                $telephone->note = $i->note;
                $telephone->phonetypeid = $i->phonetypeid;
               // $telephone->phonetypeid = 1;
                $telephone->save();
            }

            //messengers
            $messengers = Messengers::model()->findAllByAttributes(array('cardid' => $card->cardid));
            foreach ($messengers as $i) {
                $i->delete();
            }
            foreach ($this->messengers as $i) {
                $messenger = new Messengers();

                $messenger->cardid = $card->cardid;
                $messenger->value = $i->value;
               // $messenger->imessengertypeid = $i->typeid;
                $messenger->imessengertypeid = $i->imessengertypeid;
                $messenger->save();
            }

            //rubrics
            $rubrics = RefRubricsCards::model()->findAllByAttributes(array('cardid' => $card->cardid));
            foreach ($rubrics as $i) {
                $i->delete();
            }
            foreach ($this->rubrics as $i) {
                $rubric = new RefRubricsCards();
                $rubric->cardid = $card->cardid;
                $rubric->rubricid = $i->rubricid;
                $rubric->save();
            }

            //keywords

            $keywords = RefKeywordCard::model()->findAllByAttributes(array('cardid' => $card->cardid));
            foreach ($keywords as $i) {
                $i->delete();
            }
            foreach ($this->keywords as $i) {

                $keyword = new RefKeywordCard();
                $keyword->cardid = $card->cardid;
                $keyword->keywordid = $i->keywordid;
                $keyword->save();
            }

            //photos
            $photos = CardPhotos::model()->findAllByAttributes(array('cardid' => $card->cardid));
            foreach ($photos as $i) {
                $i->delete();
            }
            foreach ($this->cardPhotoses as $i) {
                $newname = '';
                if ($i->photo) {
                    $info = pathinfo($i->photo);
                    $ext = $info['extension']; // get the extension of the file
                    $newname = $this->guid() . '.' . $ext;
                    copy(Yii::app()->params['orgPhotosPath'] . $i->photo, Yii::app()->params['orgPhotosPath'] . $newname);
                }
                $photo = new CardPhotos();
                $photo->cardid = $card->cardid;
                $photo->createdate = $i->createdate;
                $photo->photo = $newname;
                $photo->save();


            }

            //proposals
                $proposals = Proposals::model()->findAllByAttributes(array('cardid' => $card->cardid));
                foreach ($proposals as $i) {
                    $i->delete();
                }
                foreach ($this->proposals as $i) {
                    $newname = '';
                    if ($i->image) {
                        $info = pathinfo($i->image);
                        $ext = $info['extension']; // get the extension of the file
                        $newname = $this->guid() . '.' . $ext;
                        try {
                            copy(Yii::app()->params['proposalsPath'] . $i->image, Yii::app()->params['proposalsPath'] . $newname);
                        }
                        catch(Exception $ex){

                        }
                    }
                    $proposal = new Proposals();
                    $proposal->createdate = $i->createdate;;
                    $proposal->image = $newname;
                    $proposal->cardid = $card->cardid;
                    $proposal->publicationstatusid = 1;
                    $proposal->name = $i->name;
                    $proposal->content = $i->content;
                    $proposal->startpublic = $i->startpublic;
                    $proposal->endpublic = $i->endpublic;
                    $proposal->version = 0;
                    $proposal->rubricid = $i->rubricid;
                    $proposal->fresh = 0;
                    $proposal->updatedate = new CDbExpression('NOW()');
                    $proposal->save();

                }

            //vacancies
            $vacancies = Vacancies::model()->findAllByAttributes(array('cardid' => $card->cardid));
            foreach ($vacancies as $i) {
                $i->delete();
            }
            foreach ($this->vacancies as $i) {
                $vacancy = new Vacancies();
                $vacancy->createdate = $i->createdate;
                $vacancy->cardid = $card->cardid;
                $vacancy->publicationstatusid = 1;
                $vacancy->name = $i->name;
                $vacancy->content = $i->content;
                $vacancy->startpublic = $i->startpublic;
                $vacancy->endpublic = $i->endpublic;
                $vacancy->fresh = false;
                $vacancy->updatedate = $i->updatedate;
                $vacancy->save();

            }

            $newname = '';
            if ($this->logo) {
                $info = pathinfo($this->logo);
                $ext = $info['extension']; // get the extension of the file
                $newname = $this->guid() . '.' . $ext;
                $fullPathd = Yii::app()->params['orgLogosPath'] . $newname;
                $fullPaths = Yii::app()->params['orgLogosPath'] . $this->logo;
                copy($fullPaths, $fullPathd);
            }
            $card->logo = $newname;

            if ($card->validate()) {
                if (!$card->save()) {
                    throw new Exception('error in transfer');
                }

                return true;
            } else {
                $err = $card->getErrors();
                throw new Exception(CJSON::encode($err));
            }

        } else {
            throw new Exception('error in start transfer');
        }
    }

	public function setStatusPublished()
    {
        $this->cardstatusid = 1;
        if ($this->moderationdate == null) {
            $this->moderationdate = new CDbExpression('NOW()');
        }
        $this->publicationdate = new CDbExpression('NOW()');
        if ($this->validate()) {
            if (!$this->save()) {
                $err = $this->getErrors();
                throw new Exception(CJSON::encode($err));
            }
        } else {
            $err = $this->getErrors();
            throw new Exception(CJSON::encode($err));
        }

    }

	public function setStatusUnPublished()
	{
		$this->cardstatusid = 2;
		$this->save();

		$publicCarId = $this->getPublicCardId();

		if(isset($publicCarId))
		{
			$publicCard = Cards::model()->findByPk($publicCarId);
			if(isset($publicCard))
			{
				$publicCard->cardstatusid = 2;
				$publicCard->save();
			}
		}
	}

	public function setStatusAdjustmentReq()
	{
		//требует коректировки
		$this->cardstatusid = 4;
		if(!$this->save())
		{
			throw new Exception('error in setPublished');
		}
	}

	public function setStatusModeration()
	{
		$this->cardstatusid = 3;
        $this->moderationdate = new CDbExpression('NOW()');
		if(!$this->save())
		{
			throw new Exception('error in setPublished');
		}
	}

	public function getUserEmail()
	{
		return isset($this->user->email) ? $this->user->email : '';
	}

	public function getOrderStatus()
	{
		$v = $this->activeOrders;
		if($v)
		{
			return $v[0]->order->orderstatus->name;
		}
		else
			return ' ';
	}

	public function getMapCordinateX()
	{
		$publicCardId = $this->getPublicCardId();

		if(isset($publicCardId))
		{
			if($publicCardId != $this->cardid)
			{
				$card = Cards::model()->findByPk($publicCardId);
				return isset($card->mapCordinates[0]->valuex) ? $card->mapCordinates[0]->valuex : '';
			}
		}
		return isset($this->mapCordinates[0]->valuex) ? $this->mapCordinates[0]->valuex : '';
	}

	public function getMapCordinateY()
	{
		$publicCardId = $this->getPublicCardId();

		if(isset($publicCardId))
		{
			if($publicCardId != $this->cardid)
			{
				$card = Cards::model()->findByPk($publicCardId);
				return isset($card->mapCordinates[0]->valuey) ? $card->mapCordinates[0]->valuey : '';
			}
		}
		return isset($this->mapCordinates[0]->valuey) ? $this->mapCordinates[0]->valuey : '';
	}

	public function getLogoName()
	{
		if($this->logo)
		{
			$fileName = $this->logo ;
			return $fileName;
		}
		return '';
	}

	public function deleteFiles()
	{

	}

	public function getFirstSubRubInfo()
	{
		$subRubs = $this->refrubrics;

		if(count($subRubs)==0)
		{
			$publicCardId = $this->getPublicCardId();
			if($publicCardId)
			{
				$card = Cards::model()->findByPk($publicCardId);
				$subsubrics= $card->refrubrics;
				if(count($subsubrics)>0)
					$firstSubRub = $subsubrics[0];
			}
		}
		else
		{
			$firstSubRub = $this->refrubrics[0];
		}

		if (isset($firstSubRub)) {
			$rubric = Rubrics::model()->findByPk($firstSubRub->rubricid);
			return array('parent' => $rubric->parentRubric->name,
				'sub' => $rubric->name);
		} else
			return array('parent' => '', 'sub' => '');
	}

    public function getCityName()
    {
        $item = $this->city;
        return $item ? $item->name : '';
    }

	public function haveProposals()
	{
		$result = false;
		if(count($this->proposals)>0)
			$result = true;
		else
			$result = false;

		$publicCardId = $this->getPublicCardId();
		if(isset($publicCardId) && $publicCardId != $this->cardid && $result == false)
		{
			$publicCard = Cards::model()->findByPk($publicCardId);
			if(count($publicCard->proposals)>0)
				$result = true;
			else
				$result = false;
		}

		return $result;

	}

	public function haveOrgPhotos()
	{
		if(count($this->cardPhotoses)>0)
			return true;
		else
			return false;
	}

	public function getActiveVacancies()
	{
		$result = array();
		foreach($this->vacancies as $v)
		{
			if( $v->endpublic >= date("Y-m-d") && $v->fresh == false)// && $v->publicationstatusid ==1
				$result[]  = array('name'=>$v->name, 'content'=>$v->content);
		}

		return $result;
	}

	public function getActiveProposals()
	{
		$result = array();
		foreach($this->proposals as $v)
		{
			if( $v->endpublic >= date("Y-m-d") && $v->fresh == false  )//&& $v->publicationstatusid == 1
			$result[]  = array('name'=>$v->name, 'content'=>$v->content, 'image'=> isset($v->image) ?
				(Yii::app()->request->getBaseUrl(true)) . ('/images/proposal/' . $v->image) :
				(Yii::app()->request->getBaseUrl(true)) . ('/images/nophoto.jpg' )
			);
		}

		$publicCardId = $this->getPublicCardId();
		if($this->cardid != $publicCardId)
		{
			$publicCard = Cards::model()->findByPk($publicCardId);
			foreach($publicCard->proposals as $v)
			{
				if( $v->endpublic >= date("Y-m-d") && $v->fresh == false )//&& $v->publicationstatusid ==1
				$result[]  = array('name'=>$v->name, 'content'=>$v->content, 'image'=> isset($v->image) ?
					(Yii::app()->request->getBaseUrl(true)) . ('/images/proposal/' . $v->image) :
					(Yii::app()->request->getBaseUrl(true)) . ('/images/nophoto.jpg' )
				);
			}
		}

		return $result;
	}

    public function isBasePackageRun()
    {
        return $this->isPackageRun('777');
    }

    public function isPackageRun($id){
        date_default_timezone_set('UTC');
        $orders = Orders::model()->findAllByAttributes(array('cardid'=>$this->cardid, 'orderstatusid' => 1), array('order'=>'orderid desc'));
        array_reverse($orders);
        foreach($orders as $order) {
            foreach($order->orderDetails as $od) {
                if($od->paidservicedetaild0->paidserviceid == $id) {
                    $orderdatenow = new DateTime();
                    $orderdatefinish = new DateTime($od->publicationdate);
                    $orderdatefinish->add(new DateInterval('P' . ($od->months ? $od->months : '1') . 'M'));
                    $interval = $orderdatenow->diff($orderdatefinish);
                    if (!$interval->invert) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function isTopRun(){
        return $this->isPackageRun('3');
    }

    public function getBasePackageExpiredDate(){
        return $this->getPackageExpiredDate('777');
    }

    public function getTopExpiredDate(){
        return $this->getPackageExpiredDate('3');
    }

    public function getPackageExpiredDate($id){
        date_default_timezone_set('UTC');
        $orders = Orders::model()->findAllByAttributes(array('cardid'=>$this->cardid, 'orderstatusid' => 1), array('order'=>'orderid desc'));
        array_reverse($orders);
        foreach($orders as $order) {
            foreach($order->orderDetails as $od) {
                if($od->paidservicedetaild0->paidserviceid == $id) {
                    $orderdatefinish = new DateTime($od->publicationdate);
                    $orderdatefinish->add(new DateInterval('P' . ($od->months ? $od->months : '1') . 'M'));
                    return $orderdatefinish;
                }
            }
        }
        return null;
    }

    public function getBasePackageStartDate(){
        return $this->getPackageStartDate('777');
    }

    public function getTopStartDate(){
        return $this->getPackageStartDate('3');
    }

    public function getPackageStartDate($id){
        date_default_timezone_set('UTC');
        $orders = Orders::model()->findAllByAttributes(array('cardid'=>$this->cardid, 'orderstatusid' => 1), array('order'=>'orderid desc'));
        array_reverse($orders);
        foreach($orders as $order) {
            foreach($order->orderDetails as $od) {
                if($od->paidservicedetaild0->paidserviceid == $id) {
                    $orderdatefinish = new DateTime($od->publicationdate);
//                    $orderdatefinish->add(new DateInterval('P' . ($od->months ? $od->months : '1') . 'M'));
                    return $orderdatefinish;
                }
            }
        }
        return null;
    }

    public function getBasePackageIntervalExpiredDate(){
        return $this->getPackageIntervalExpiredDate('777');
    }

    public function getTopIntervalExpiredDate(){
        return $this->getPackageIntervalExpiredDate('3');
    }

    public function getPackageIntervalExpiredDate($id)
    {
        date_default_timezone_set('UTC');
        $orderdatenow = new DateTime();
        $orderdatefinish = $this->getPackageExpiredDate($id);
        $interval = $orderdatenow->diff($orderdatefinish);
        if (!$interval->invert) {
            return $interval->days;
        }
        return 0;
    }

    public function getBasePackageInterval(){
        return $this->getPackageInterval('777');
    }

    public function getTopInterval(){
        return $this->getPackageInterval('3');
    }

    public function getPackageInterval($id)
    {
        date_default_timezone_set('UTC');
        $orderdatestart = $this->getPackageStartDate($id);
        $orderdatefinish = $this->getPackageExpiredDate($id);
        $interval = $orderdatestart->diff($orderdatefinish);
        if (!$interval->invert) {
            return $interval->days;
        }
        return 0;
    }

    public function getPackageBalance($id)
    {
        date_default_timezone_set('UTC');

        $balance = 0;

        $orders = Orders::model()->findAllByAttributes(array('cardid' => $this->cardid, 'orderstatusid' => 1), array('order' => 'orderid desc'));
        foreach ($orders as $order) {
            foreach ($order->orderDetails as $od) {
                if ($od->paidservicedetaild0->paidserviceid == $id) {
                    $datetime = new DateTime();
                    $orderdatestart = new DateTime($od->publicationdate);
                    $orderdatefinish = new DateTime($od->publicationdate);
                    $orderdatefinish->add(new DateInterval('P' . ($od->months ? $od->months : '1') . 'M'));
                    $intervalnow = $datetime->diff($orderdatefinish);
                    $interval = $orderdatestart->diff($orderdatefinish);
                    $count = 1;//$od->paidservicedetaild0->count;
                    if ($id == '4') {
                        $count = count($this->getPublicCard()->proposals);

                        foreach ($this->getPublicCard()->proposals as $p) {

                            $startpublic = $p->startpublic;
                            $endpublic = $p->endpublic;
                            if ($startpublic && $endpublic && $startpublic != '' && $endpublic != '') {
                                $start = new DateTime($startpublic);
                                $end = new DateTime($endpublic);
                                $timeDiff = $start->diff($end);
                                $balance += $timeDiff->days * floor($od->paidservicedetaild0->cost / 30);
                            }
                        }
                        return $balance;

                    }

                    if (!$intervalnow->invert && !$interval->invert) {
                        $daycost = ($od->paidservicedetaild0->cost * $od->months * $count / $interval->days);
                        $balance = floor($daycost * $intervalnow->days);
                        return $balance;
                    }
                }
            }
        }

        return $balance;
    }

    public function getCalcPackageCost($months, $count, $paidservicedetailid, $fullcost, $topmonths = 0){
        date_default_timezone_set('UTC');
        $paidservicedetail = PaidServiceDetails::model()->findByAttributes(array('paidservicedetaild'=>$paidservicedetailid));
        $paidservice = PaidServices::model()->findByAttributes(array('paidserviceid'=>$paidservicedetail->paidserviceid));
        if($paidservice->paidserviceid == '3'){
            $finishbase = $this->getBasePackageExpiredDate();
            $now = new DateTime();
            $starttop = new DateTime();
            $finishtop = $starttop->add(new DateInterval('P' . $topmonths . 'M'));
            $intervaltop = $finishtop->diff($finishbase);
            $intervaltopreal = $now->diff($finishtop);
            if (!$intervaltop->invert) {
                $months = $topmonths;
            } else {
                $interval = $this->getBasePackageIntervalExpiredDate();
                $months = $topmonths * $interval / $intervaltopreal->days;
            }
        } else {
            if (!$fullcost) {
                $interval = $this->getBasePackageIntervalExpiredDate();
                $intervalfull = $this->getBasePackageInterval();
                if ($interval > 0) {
                    $months = $months * $interval / $intervalfull;
                } else {
                    $months = $months;
                }
            }
        }
        $count = $count;
        switch ($paidservice->paidserviceid) {
            case "4":
                $cost = 0;
                foreach ($this->proposals as $p) {
                    $startpublic = $p->startpublic;
                    $endpublic = $p->endpublic;
                    if ($startpublic && $endpublic && $startpublic != '' && $endpublic != '') {
                        $start = new DateTime($startpublic);
                        $end = new DateTime($endpublic);
                        $timeDiff = $start->diff($end);
                        $cost += $timeDiff->days * floor($paidservicedetail->cost / 30);
                    }
                }
                return $cost;
                break;
            default:
                $count = 1;
                break;
        }

        if($months > 0){
            return $months * $paidservicedetail->cost * $count;
        }
        return 0;
    }

    public function getPackageCost($orderdetailid, $fullcost = null){
        date_default_timezone_set('UTC');

        $orderdetail = OrderDetails::model()->findByAttributes(array('orderdetailid'=>$orderdetailid));
        $paidservicedetailid = $orderdetail->paidservicedetaild0->paidserviceid;

        $months = 1;
        if($fullcost == null) {
            $fullcost = false;
            foreach ($orderdetail->order->orderDetails as $od) {
                if ($od->paidservicedetaild0->paidserviceid == '777') {
                    $fullcost = true;
                    break;
                }
            }
        }
        if($fullcost){
            $months = $orderdetail->months;
        } else {
            $interval = $this->getBasePackageIntervalExpiredDate();
            if($interval > 0){
                $months = $interval / 30;
            } else {
                $months = $orderdetail->months;
            }
        }
        $count = 1;
        switch ($orderdetail->paidservicedetaild0->paidserviceid) {
            case "4":
                $count = count($orderdetail->order->card->proposals);
                break;
            default:
                $count = $orderdetail->paidservicedetaild0->count;
                break;
        }

        if($orderdetail->months > 0){
            return $months * $orderdetail->paidservicedetaild0->cost * $count;
        }
        return 0;
    }

    function guid()
    {
        if (function_exists('uuid_create')) {
            return uuid_create();
        } else {
            mt_srand((double)microtime() * 10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = // "{"
                substr($charid, 0, 8) . $hyphen
                . substr($charid, 8, 4) . $hyphen
                . substr($charid, 12, 4) . $hyphen
                . substr($charid, 16, 4) . $hyphen
                . substr($charid, 20, 12);// "}"
            return $uuid;
        }
    }
}
