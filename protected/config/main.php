<?php

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Info Smart',
	'defaultController'=>'site',

	'preload'=>array('log'),

	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.components.exception.*'
	),

//	'modules'=>array(
//		'gii'=>array(
//			'class'=>'system.gii.GiiModule',
//			'password'=>'123',
//			'ipFilters'=>array('127.0.0.1','::1'),
//		),
//	),

	'components'=>array(
		'urlManager'=>array(
			'showScriptName' => false,  // что бы не цеплялся index.php к ссылкам
			'urlFormat'=>'path',
			'rules'=>array(
				// '<url>/post/'=>'post/view',
			)
		),

		'user'=>array(
			'loginUrl'=>array('site/index?login=true'),
			'class' => 'WebUser',
		),

		'db'=>array(
			'connectionString' => 'mysql:host=127.0.0.1;port=3306;dbname=bcatalog;',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
//              Database=bcatalog;Data Source=ap-cdbr-azure-east-c.cloudapp.net;User Id=bdf2af727a973a;Password=6d09a1e7
        /*'db'=>array(
            'connectionString' => 'mysql:host=ap-cdbr-azure-east-c.cloudapp.net;port=3306;dbname=bcatalog;',
            'emulatePrepare' => true,
            'username' => 'bdf2af727a973a',
            'password' => '6d09a1e7',
            'charset' => 'utf8',
        ),*/
/*        'db' => array(
            'class' => 'system.db.CDbConnection',
            'connectionString' => 'mysql:host=in4smarttest.cloudapp.net;port=3306;dbname=bcatalog;',
			'emulatePrepare' => true,
			'username' => 'in4smartguy',
			'password' => 'qazwsx123!',
        ),
		//база для тех поддержки
		//оригинальная строка на всякий случай: Database=cdb_c224b459b7;Data Source=ap-cdbr-azure-east-c.cloudapp.net;User Id=b9d9cdd767c502;Password=521b3b6c
		'techdb'=>array(
			'connectionString' => 'mysql:host=ap-cdbr-azure-east-c.cloudapp.net;port=3306;dbname=cdb_c224b459b7;',
			'emulatePrepare' => true,
			'username' => 'b9d9cdd767c502',
			'password' => '521b3b6c',
			'charset' => 'utf8',
		),
        'sphinx' => array(
            'class' => 'system.db.CDbConnection',
            'connectionString' => 'mysql:host=in4smarttest.cloudapp.net;port=9306;dbname=bcatalog;',
			'emulatePrepare' => true,
			'username' => 'in4smartguy',
			'password' => 'qazwsx123!',
        ),*/
//        'sphinx' => array(
//            'class' => 'system.db.CDbConnection',
//            'connectionString' => 'mysql:host=in4smart.cloudapp.net;port=9306;dbname=bcatalog;',
//            'emulatePrepare' => true,
//            'username' => 'in4smartguy',
//            'password' => 'qazwsx123!',
//        ),
		'errorHandler'=>array(
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),

		'ePdf' => array(
			'class'         => 'ext.yii-pdf.EYiiPdf',
			'params'        => array(
				'mpdf'     => array(
					'librarySourcePath' => 'application.vendor.mpdf.*',
					'constants'         => array(
						'_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
					),
					'class'=>'mpdf', // the literal class filename to be loaded from the vendors folder
					/*'defaultParams'     => array( // More info: http://mpdf1.com/manual/index.php?tid=184
						'mode'              => '', //  This parameter specifies the mode of the new document.
						'format'            => 'A4', // format A4, A5, ...
						'default_font_size' => 0, // Sets the default document font size in points (pt)
						'default_font'      => '', // Sets the default font-family for the new document.
						'mgl'               => 15, // margin_left. Sets the page margins for the new document.
						'mgr'               => 15, // margin_right
						'mgt'               => 16, // margin_top
						'mgb'               => 16, // margin_bottom
						'mgh'               => 9, // margin_header
						'mgf'               => 9, // margin_footer
						'orientation'       => 'P', // landscape or portrait orientation
					)*/
				)

			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'host' => 'in4smarttest.azurewebsites.net',
		'adminEmail'=>'webmaster@example.com',
		'mailingAddress' => 'service@in4smart.kz',
		'uploadNewsImagesPath'=>dirname(__FILE__).'/../../images/news/',
		'uploadNewsImagesUrl'=>'/images/news/',
		'invoicesFilesPath'=>dirname(__FILE__).'/../../storage/invoices/',
		'paymentsFilesPath'=>dirname(__FILE__).'/../../storage/payments/',
		'orgPhotosPath'=>dirname(__FILE__).'/../../images/orgphotos/',
		'orgLogosPath'=>dirname(__FILE__).'/../../images/orglogos/',
		'proposalsPath'=>dirname(__FILE__).'/../../images/proposal/',
        'rubricsPath'=>dirname(__FILE__).'/../../images/rubrics/',
        'citiesPath'=>dirname(__FILE__).'/../../images/cities/',
        'messengersPath'=>dirname(__FILE__).'/../../images/messengers/',
	),



);