
<div class="row deals center-content">
    <div class="span12">
        <h1>Предложения</h1>
        <div id="news">
            <?php foreach($proposals as $post): ?>
                <div class="deal">
                    <a href="/proposals/view/id/<?php echo $post->proposalsid ?>">
                        <img src="<?php echo (Yii::app()->request->getBaseUrl(true)).('/images/proposal/'.$post->image); ?>" alt="" title="">
                        <div class="ptext">
                            <span class="caption"><?php echo $post->name; ?></span>
                            <p class="text"><?php echo $post->content; ?></p>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
        <?php $this->widget('ext.yiinfinite-scroll.YiinfiniteScroller', array(
            'contentSelector' => '#news',
            'itemSelector' => 'div.deal',
            'loadingText' => 'Загрузка...',
            'donetext' => 'Конец',
            'pages' => $pages,
        )); ?>
        <div class="clr"></div>
    </div>
</div>