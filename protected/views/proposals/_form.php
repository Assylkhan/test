<?php
/* @var $this ProposalsController */
/* @var $model Proposals */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'proposals-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'cardid'); ?>
		<?php echo $form->textField($model,'cardid'); ?>
		<?php echo $form->error($model,'cardid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'publicationstatusid'); ?>
		<?php echo $form->textField($model,'publicationstatusid'); ?>
		<?php echo $form->error($model,'publicationstatusid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'content'); ?>
		<?php echo $form->textField($model,'content'); ?>
		<?php echo $form->error($model,'content'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'startpublic'); ?>
		<?php echo $form->textField($model,'startpublic'); ?>
		<?php echo $form->error($model,'startpublic'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'endpublic'); ?>
		<?php echo $form->textField($model,'endpublic'); ?>
		<?php echo $form->error($model,'endpublic'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'createdate'); ?>
		<?php echo $form->textField($model,'createdate'); ?>
		<?php echo $form->error($model,'createdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updatedate'); ?>
		<?php echo $form->textField($model,'updatedate'); ?>
		<?php echo $form->error($model,'updatedate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'version'); ?>
		<?php echo $form->textField($model,'version'); ?>
		<?php echo $form->error($model,'version'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rubricid'); ?>
		<?php echo $form->textField($model,'rubricid'); ?>
		<?php echo $form->error($model,'rubricid'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->