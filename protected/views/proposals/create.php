<?php
/* @var $this ProposalsController */
/* @var $model Proposals */

$this->breadcrumbs=array(
	'Proposals'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Proposals', 'url'=>array('index')),
	array('label'=>'Manage Proposals', 'url'=>array('admin')),
);
?>

<h1>Create Proposals</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>