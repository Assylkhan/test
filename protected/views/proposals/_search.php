<?php
/* @var $this ProposalsController */
/* @var $model Proposals */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'proposalsid'); ?>
		<?php echo $form->textField($model,'proposalsid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cardid'); ?>
		<?php echo $form->textField($model,'cardid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'publicationstatusid'); ?>
		<?php echo $form->textField($model,'publicationstatusid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'content'); ?>
		<?php echo $form->textField($model,'content'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'startpublic'); ?>
		<?php echo $form->textField($model,'startpublic'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'endpublic'); ?>
		<?php echo $form->textField($model,'endpublic'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'createdate'); ?>
		<?php echo $form->textField($model,'createdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updatedate'); ?>
		<?php echo $form->textField($model,'updatedate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'version'); ?>
		<?php echo $form->textField($model,'version'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rubricid'); ?>
		<?php echo $form->textField($model,'rubricid'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->