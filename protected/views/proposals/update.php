<?php
/* @var $this ProposalsController */
/* @var $model Proposals */

$this->breadcrumbs=array(
	'Proposals'=>array('index'),
	$model->name=>array('view','id'=>$model->proposalsid),
	'Update',
);

$this->menu=array(
	array('label'=>'List Proposals', 'url'=>array('index')),
	array('label'=>'Create Proposals', 'url'=>array('create')),
	array('label'=>'View Proposals', 'url'=>array('view', 'id'=>$model->proposalsid)),
	array('label'=>'Manage Proposals', 'url'=>array('admin')),
);
?>

<h1>Update Proposals <?php echo $model->proposalsid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>