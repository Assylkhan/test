<?php
/* @var $this ProposalsController */
/* @var $model Proposals */

$this->breadcrumbs=array(
	'Proposals'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Proposals', 'url'=>array('index')),
	array('label'=>'Create Proposals', 'url'=>array('create')),
	array('label'=>'Update Proposals', 'url'=>array('update', 'id'=>$model->proposalsid)),
	array('label'=>'Delete Proposals', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->proposalsid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Proposals', 'url'=>array('admin')),
);
?>

<h1>View Proposals #<?php echo $model->proposalsid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'proposalsid',
		'cardid',
		'publicationstatusid',
		'name',
		'content',
		'startpublic',
		'endpublic',
		'createdate',
		'updatedate',
		'version',
		'rubricid',
	),
)); ?>
