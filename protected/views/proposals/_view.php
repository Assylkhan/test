<?php
/* @var $this ProposalsController */
/* @var $data Proposals */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('proposalsid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->proposalsid), array('view', 'id'=>$data->proposalsid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cardid')); ?>:</b>
	<?php echo CHtml::encode($data->cardid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('publicationstatusid')); ?>:</b>
	<?php echo CHtml::encode($data->publicationstatusid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content')); ?>:</b>
	<?php echo CHtml::encode($data->content); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('startpublic')); ?>:</b>
	<?php echo CHtml::encode($data->startpublic); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('endpublic')); ?>:</b>
	<?php echo CHtml::encode($data->endpublic); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('createdate')); ?>:</b>
	<?php echo CHtml::encode($data->createdate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updatedate')); ?>:</b>
	<?php echo CHtml::encode($data->updatedate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('version')); ?>:</b>
	<?php echo CHtml::encode($data->version); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rubricid')); ?>:</b>
	<?php echo CHtml::encode($data->rubricid); ?>
	<br />

	*/ ?>

</div>