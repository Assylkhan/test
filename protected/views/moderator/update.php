<?php
/* @var $this ModeratorController */
/* @var $model Cards */

$this->breadcrumbs=array(
	'Cards'=>array('index'),
	$model->cardid=>array('view','id'=>$model->cardid),
	'Update',
);

$this->menu=array(
	array('label'=>'List Cards', 'url'=>array('index')),
	array('label'=>'Create Cards', 'url'=>array('create')),
	array('label'=>'View Cards', 'url'=>array('view', 'id'=>$model->cardid)),
	array('label'=>'Manage Cards', 'url'=>array('admin')),
);
?>

<h1>Update Cards <?php echo $model->cardid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>