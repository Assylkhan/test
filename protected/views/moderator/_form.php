<?php
/* @var $this ModeratorController */
/* @var $model Cards */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cards-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'parentcardid'); ?>
		<?php echo $form->textField($model,'parentcardid'); ?>
		<?php echo $form->error($model,'parentcardid'); ?>
	</div>



	<div class="row">
		<?php echo $form->labelEx($model,'userid'); ?>
		<?php echo $form->textField($model,'userid'); ?>
		<?php echo $form->error($model,'userid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'orgtypeid'); ?>
		<?php echo $form->textField($model,'orgtypeid'); ?>
		<?php echo $form->error($model,'orgtypeid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'keywordcountid'); ?>
		<?php echo $form->textField($model,'keywordcountid'); ?>
		<?php echo $form->error($model,'keywordcountid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rubriccountid'); ?>
		<?php echo $form->textField($model,'rubriccountid'); ?>
		<?php echo $form->error($model,'rubriccountid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'regionid'); ?>
		<?php echo $form->textField($model,'regionid'); ?>
		<?php echo $form->error($model,'regionid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cityid'); ?>
		<?php echo $form->textField($model,'cityid'); ?>
		<?php echo $form->error($model,'cityid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cardstatusid'); ?>
		<?php echo $form->textField($model,'cardstatusid'); ?>
		<?php echo $form->error($model,'cardstatusid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'orgname'); ?>
		<?php echo $form->textField($model,'orgname',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'orgname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'logo'); ?>
		<?php echo $form->textField($model,'logo'); ?>
		<?php echo $form->error($model,'logo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'street'); ?>
		<?php echo $form->textField($model,'street',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'street'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'housenumber'); ?>
		<?php echo $form->textField($model,'housenumber',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'housenumber'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'office'); ?>
		<?php echo $form->textField($model,'office',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'office'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'websiteurl'); ?>
		<?php echo $form->textField($model,'websiteurl',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'websiteurl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ispublic'); ?>
		<?php echo $form->textField($model,'ispublic'); ?>
		<?php echo $form->error($model,'ispublic'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'createdate'); ?>
		<?php echo $form->textField($model,'createdate'); ?>
		<?php echo $form->error($model,'createdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updatedate'); ?>
		<?php echo $form->textField($model,'updatedate'); ?>
		<?php echo $form->error($model,'updatedate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'juridicaladdress'); ?>
		<?php echo $form->textField($model,'juridicaladdress',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'juridicaladdress'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'iinbin'); ?>
		<?php echo $form->textField($model,'iinbin',array('size'=>13,'maxlength'=>13)); ?>
		<?php echo $form->error($model,'iinbin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bik'); ?>
		<?php echo $form->textField($model,'bik',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'bik'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'iik'); ?>
		<?php echo $form->textField($model,'iik',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'iik'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kbe'); ?>
		<?php echo $form->textField($model,'kbe',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'kbe'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bankname'); ?>
		<?php echo $form->textField($model,'bankname',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'bankname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'orgindex'); ?>
		<?php echo $form->textField($model,'orgindex',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'orgindex'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'workschedule'); ?>
		<?php echo $form->textArea($model,'workschedule',array('rows'=>6)); ?>
		<?php echo $form->error($model,'workschedule'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'busnums'); ?>
		<?php echo $form->textField($model,'busnums',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'busnums'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updatecontactname'); ?>
		<?php echo $form->textField($model,'updatecontactname',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'updatecontactname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updatecontactphone'); ?>
		<?php echo $form->textField($model,'updatecontactphone'); ?>
		<?php echo $form->error($model,'updatecontactphone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'accountantname'); ?>
		<?php echo $form->textField($model,'accountantname',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'accountantname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'accountantphone'); ?>
		<?php echo $form->textField($model,'accountantphone'); ?>
		<?php echo $form->error($model,'accountantphone'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->