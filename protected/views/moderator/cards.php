<script src="/frontend/js/cards/functions.js"></script>
<link rel="stylesheet" href="/frontend/css/forms.css">

<script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>



<?php
/* @var $this ModeratorController */
/* @var $model Cards */


$this->breadcrumbs=array(
	'Cards'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Cards', 'url'=>array('index')),
	array('label'=>'Create Cards', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#cards-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div id="content-block">
	<div class="row">
		<div class="span12">
			<div class="main-row">
				<h1 class="adminh">Карточки</h1>

			</div>
			<div class="button-row">
				<button class="hover-green" onclick="location.href='/site/registration'">новая</button>
				<button class="hover-green not-active" onclick="setNotPublished();false;">снять с публикации</button>
			</div>

		</div>
	</div>
</div>

<?php /*echo CHtml::link('Advanced Search','#',array('class'=>'search-button'));*/ ?>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cards-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'summaryText'=>'Отображение {start}-{end} из  {page} результатов',
	'columns'=>array(
		array(
			'class' => 'CCheckBoxColumn',
			'id'=>'selectedItems',
			//'selectableRows' => 2,
			'checkBoxHtmlOptions' => array(
				//'name' => 'ids[]',
			),
		),
		'cardid',
		array(
			'name'=>'publicCardId',
			'type' => 'raw',
			'value' => '$data->getPublicCardId()',
		),
		array(
			'name'=>'userid',
			'value'=>'$data->user->email'
		),
		array(
			'name'=>'orgtypeid',
			'value'=>'$data->orgtype->name',//OrgTypes::model('orgtypeid = :t', array(':t'=>  1)),//
			'filter' => CHtml::listData(OrgTypes::model()->findAll(), 'orgtypeid', 'name'),
			'htmlOptions'=>array('width'=>'20px'),
		),
		//'keywordcountid',
		//'rubriccountid',
		array(
			'name'=>'regionid',
			'value'=>'$data->region ? $data->region->name : ""',
			'filter' => CHtml::listData(Regions::model()->findAll(), 'regionid', 'name'),
		),
		array(
			'name'=>'cityid',
			'value'=>'$data->GetCityName()',
			'filter' => CHtml::listData(Cities::model()->findAll(), 'cityid', 'name'),
		),

		array(
			'name'=>'cardstatusid',
			'value'=>'$data->cardstatus->name',
			'filter' => CHtml::listData(CardStatuses::model()->findAll(), 'cardstatusid', 'name'),
		),

		array(
			'name'=>'orderStatus',
			'type' => 'raw',
			'value' => '$data->getOrderStatus()',
		),

		'orgname',
		//'logo',
		//'street',
		//'housenumber',
		//'office',
		//'websiteurl',
		//'ispublic',
		'createdate',
		//'updatedate',
		//'juridicaladdress',
		'iinbin',
		//'bik',
		//'iik',
		//'kbe',
		//'bankname',
		//'orgindex',
		//'workschedule',
		//'busnums',
		'updatecontactname',
		'updatecontactphone',
		'accountantname',
		'accountantphone',
		array(
			'class' => 'CButtonColumn',
			'template' => '{delete}',
			'buttons' => array(
				'delete' => array('imageUrl'=>Yii::app()->request->baseUrl.'/images/delete-16.png',
				),
			),
			'deleteConfirmation'=>"Вы уверены, что хотите удалить этот элемент?",
			'deleteButtonOptions' => array(
				'rel' => 'tooltip',
				'data-toggle' => 'tooltip',
				'title' => 'Удалить'),
		),

	),
)); ?>



<div class="modal fade" id="general_modal">
</div><!-- /.modal -->