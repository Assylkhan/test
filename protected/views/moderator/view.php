<?php
/* @var $this ModeratorController */
/* @var $model Cards */

$this->breadcrumbs=array(
	'Cards'=>array('index'),
	$model->cardid,
);

$this->menu=array(
	array('label'=>'List Cards', 'url'=>array('index')),
	array('label'=>'Create Cards', 'url'=>array('create')),
	array('label'=>'Update Cards', 'url'=>array('update', 'id'=>$model->cardid)),
	array('label'=>'Delete Cards', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->cardid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Cards', 'url'=>array('admin')),
);
?>

<h1>View Cards #<?php echo $model->cardid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'cardid',
		'parentcardid',

		'userid',
		'orgtypeid',
		'keywordcountid',
		'rubriccountid',
		'regionid',
		'cityid',
		'cardstatusid',
		'orgname',
'description',
		'logo',
		'street',
		'housenumber',
		'office',
		'websiteurl',
		'ispublic',
		'createdate',
		'updatedate',
		'juridicaladdress',
		'iinbin',
		'bik',
		'iik',
		'kbe',
		'bankname',
		'orgindex',
		'workschedule',
		'busnums',
		'updatecontactname',
		'updatecontactphone',
		'accountantname',
		'accountantphone',
	),
)); ?>
