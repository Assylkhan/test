<?php
/* @var $this ModeratorController */
/* @var $model Cards */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'cardid'); ?>
		<?php echo $form->textField($model,'cardid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'parentcardid'); ?>
		<?php echo $form->textField($model,'parentcardid'); ?>
	</div>



	<div class="row">
		<?php echo $form->label($model,'userid'); ?>
		<?php echo $form->textField($model,'userid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'orgtypeid'); ?>
		<?php echo $form->textField($model,'orgtypeid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'keywordcountid'); ?>
		<?php echo $form->textField($model,'keywordcountid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rubriccountid'); ?>
		<?php echo $form->textField($model,'rubriccountid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'regionid'); ?>
		<?php echo $form->textField($model,'regionid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cityid'); ?>
		<?php echo $form->textField($model,'cityid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cardstatusid'); ?>
		<?php echo $form->textField($model,'cardstatusid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'orgname'); ?>
		<?php echo $form->textField($model,'orgname',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'logo'); ?>
		<?php echo $form->textField($model,'logo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'street'); ?>
		<?php echo $form->textField($model,'street',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'housenumber'); ?>
		<?php echo $form->textField($model,'housenumber',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'office'); ?>
		<?php echo $form->textField($model,'office',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'websiteurl'); ?>
		<?php echo $form->textField($model,'websiteurl',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ispublic'); ?>
		<?php echo $form->textField($model,'ispublic'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'createdate'); ?>
		<?php echo $form->textField($model,'createdate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updatedate'); ?>
		<?php echo $form->textField($model,'updatedate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'juridicaladdress'); ?>
		<?php echo $form->textField($model,'juridicaladdress',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'iinbin'); ?>
		<?php echo $form->textField($model,'iinbin',array('size'=>13,'maxlength'=>13)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bik'); ?>
		<?php echo $form->textField($model,'bik',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'iik'); ?>
		<?php echo $form->textField($model,'iik',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kbe'); ?>
		<?php echo $form->textField($model,'kbe',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bankname'); ?>
		<?php echo $form->textField($model,'bankname',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'orgindex'); ?>
		<?php echo $form->textField($model,'orgindex',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'workschedule'); ?>
		<?php echo $form->textField($model,'workschedule',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'busnums'); ?>
		<?php echo $form->textField($model,'busnums',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updatecontactname'); ?>
		<?php echo $form->textField($model,'updatecontactname',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updatecontactphone'); ?>
		<?php echo $form->textField($model,'updatecontactphone'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'accountantname'); ?>
		<?php echo $form->textField($model,'accountantname',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'accountantphone'); ?>
		<?php echo $form->textField($model,'accountantphone'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->