<?php
/* @var $this ModeratorController */
/* @var $data Cards */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('cardid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->cardid), array('view', 'id'=>$data->cardid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parentcardid')); ?>:</b>
	<?php echo CHtml::encode($data->parentcardid); ?>
	<br />



	<b><?php echo CHtml::encode($data->getAttributeLabel('userid')); ?>:</b>
	<?php echo CHtml::encode($data->userid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('orgtypeid')); ?>:</b>
	<?php echo CHtml::encode($data->orgtypeid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keywordcountid')); ?>:</b>
	<?php echo CHtml::encode($data->keywordcountid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rubriccountid')); ?>:</b>
	<?php echo CHtml::encode($data->rubriccountid); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('regionid')); ?>:</b>
	<?php echo CHtml::encode($data->regionid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cityid')); ?>:</b>
	<?php echo CHtml::encode($data->cityid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cardstatusid')); ?>:</b>
	<?php echo CHtml::encode($data->cardstatusid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('orgname')); ?>:</b>
	<?php echo CHtml::encode($data->orgname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('logo')); ?>:</b>
	<?php echo CHtml::encode($data->logo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('street')); ?>:</b>
	<?php echo CHtml::encode($data->street); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('housenumber')); ?>:</b>
	<?php echo CHtml::encode($data->housenumber); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('office')); ?>:</b>
	<?php echo CHtml::encode($data->office); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('websiteurl')); ?>:</b>
	<?php echo CHtml::encode($data->websiteurl); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ispublic')); ?>:</b>
	<?php echo CHtml::encode($data->ispublic); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('createdate')); ?>:</b>
	<?php echo CHtml::encode($data->createdate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updatedate')); ?>:</b>
	<?php echo CHtml::encode($data->updatedate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('juridicaladdress')); ?>:</b>
	<?php echo CHtml::encode($data->juridicaladdress); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('iinbin')); ?>:</b>
	<?php echo CHtml::encode($data->iinbin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bik')); ?>:</b>
	<?php echo CHtml::encode($data->bik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('iik')); ?>:</b>
	<?php echo CHtml::encode($data->iik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kbe')); ?>:</b>
	<?php echo CHtml::encode($data->kbe); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bankname')); ?>:</b>
	<?php echo CHtml::encode($data->bankname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('orgindex')); ?>:</b>
	<?php echo CHtml::encode($data->orgindex); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('workschedule')); ?>:</b>
	<?php echo CHtml::encode($data->workschedule); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('busnums')); ?>:</b>
	<?php echo CHtml::encode($data->busnums); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updatecontactname')); ?>:</b>
	<?php echo CHtml::encode($data->updatecontactname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updatecontactphone')); ?>:</b>
	<?php echo CHtml::encode($data->updatecontactphone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('accountantname')); ?>:</b>
	<?php echo CHtml::encode($data->accountantname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('accountantphone')); ?>:</b>
	<?php echo CHtml::encode($data->accountantphone); ?>
	<br />

	*/ ?>

</div>