<div class="row center-content settings">
    <div class="span12">
        <div class="main-row">
            <h1 class="adminh">Настройки сайта</h1>
        </div>
        <table class="addnew-table">
            <tbody>

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'rubrics-form',
                    'action'=>Yii::app()->createUrl('settings/createrubrics'),
                    'enableAjaxValidation'=>false,
                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                )); ?>

                    <tr>
                        <td>
                            <label for="name">Добавить рубрику:</label>
                        </td>
                        <td>
                            <?php echo $form->errorSummary($rubrics); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo $form->labelEx($rubrics,'image'); ?>

                        </td>
                        <td>
                            <?php echo $form->fileField($rubrics,'image'); ?>
                            <?php echo $form->error($rubrics,'image'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo $form->labelEx($rubrics,'name'); ?>
                            
                        </td>
                        <td>
                            <?php echo $form->textField($rubrics,'name',array('class'=>'registration-input')); ?>
                            <?php echo $form->error($rubrics,'name'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo $form->labelEx($rubrics,'parent_rubricid'); ?>
                            
                        </td>
                        <td>
                            <?php echo $form->dropDownList($rubrics,'parent_rubricid', CHtml::listData(Rubrics::model()->findAllByAttributes(array('rubrictypeid'=>'1'),array('order' => 'name')),'rubricid','name'), array('class'=>'registration-select-big','empty'=>'Главная'));?>
                            <?php echo $form->error($rubrics,'parent_rubricid'); ?>

                        </td>
                    </tr>
                    <tr id="rub-tr">
                        <td>
                            <?php echo $form->labelEx($rubrics,'rubricgroupid'); ?>

                        </td>
                        <td>
                            <?php echo $form->dropDownList($rubrics,'rubricgroupid', CHtml::listData(RubricGroups::model()->findAll(array('order' => 'name')),'rubricgroupid','name'), array('class'=>'registration-select-big'));?>
                            <?php echo $form->error($rubrics,'rubricgroupid'); ?>

                        </td>
                    </tr>
                    <tr>
                        <td>

                        </td>
                        <td>
                            <div class="select-buttons">
                                <?php echo CHtml::submitButton('Добавить', array('class'=>'create-button')); ?>
                            </div>

                        </td>
                    </tr>

                <?php $this->endWidget(); ?>

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'d-rubrics-form',
                    'action'=>Yii::app()->createUrl('settings/deleterubrics'),
                    'enableAjaxValidation'=>false,
                )); ?>

                    <tr>
                        <td>
                            <label for="name">Удалить рубрику:</label>
                        </td>
                        <td>
                            <?php echo $form->errorSummary($rubrics); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo $form->labelEx($rubrics,'rubricid'); ?>
                            
                        </td>
                        <td>
                            <?php echo $form->dropDownList($rubrics,'rubricid', CHtml::listData(Rubrics::model()->findAll(array('order' => 'name')),'rubricid','name'), array('class'=>'registration-select-big'));?>
                            <?php echo $form->error($rubrics,'rubricid'); ?>
                            <div class="select-buttons">
                                <?php echo CHtml::submitButton('Удалить', array('class'=>'create-button')); ?>
                            </div>
                        </td>
                    </tr>

                <?php $this->endWidget(); ?>

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'cities-form',
                    'action'=>Yii::app()->createUrl('settings/createcities'),
                    'enableAjaxValidation'=>false,
                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                )); ?>

                <tr>
                    <td>
                        <label for="name">Добавить город:</label>
                    </td>
                    <td>
                        <?php echo $form->errorSummary($cities); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $form->labelEx($cities,'image'); ?>

                    </td>
                    <td>
                        <?php echo $form->fileField($cities,'image'); ?>
                        <?php echo $form->error($cities,'image'); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $form->labelEx($cities,'name'); ?>
                        
                    </td>
                    <td>
                        <?php echo $form->textField($cities,'name',array('class'=>'registration-input')); ?>
                        <?php echo $form->error($cities,'name'); ?>
                        <div class="select-buttons">
                            <?php echo CHtml::submitButton('Добавить', array('class'=>'create-button')); ?>
                        </div>
                    </td>
                </tr>

                <?php $this->endWidget(); ?>

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'d-cities-form',
                    'action'=>Yii::app()->createUrl('settings/deletecities'),
                    'enableAjaxValidation'=>false,
                )); ?>

                <tr>
                    <td>
                        <label for="name">Удалить город:</label>
                    </td>
                    <td>
                        <?php echo $form->errorSummary($cities); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $form->labelEx($cities,'cityid'); ?>
                        
                    </td>
                    <td>
                        <?php echo $form->dropDownList($cities,'cityid', CHtml::listData(Cities::model()->findAll(array('order' => 'name')),'cityid','name'), array('class'=>'registration-select-big'));?>
                        <?php echo $form->error($cities,'cityid'); ?>
                        <div class="select-buttons">
                            <?php echo CHtml::submitButton('Удалить', array('class'=>'create-button')); ?>
                        </div>
                    </td>
                </tr>

                <?php $this->endWidget(); ?>

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'regions-form',
                    'action'=>Yii::app()->createUrl('settings/createregions'),
                    'enableAjaxValidation'=>false,
                )); ?>

                <tr>
                    <td>
                        <label for="name">Добавить регион:</label>
                    </td>
                    <td>
                        <?php echo $form->errorSummary($regions); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $form->labelEx($regions,'name'); ?>
                        
                    </td>
                    <td>
                        <?php echo $form->textField($regions,'name',array('class'=>'registration-input')); ?>
                        <?php echo $form->error($regions,'name'); ?>
                        <div class="select-buttons">
                            <?php echo CHtml::submitButton('Добавить', array('class'=>'create-button')); ?>
                        </div>
                    </td>
                </tr>

                <?php $this->endWidget(); ?>

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'d-regions-form',
                    'action'=>Yii::app()->createUrl('settings/deleteregions'),
                    'enableAjaxValidation'=>false,
                )); ?>

                <tr>
                    <td>
                        <label for="name">Удалить регион:</label>
                    </td>
                    <td>
                        <?php echo $form->errorSummary($regions); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $form->labelEx($regions,'regionid'); ?>
                        
                    </td>
                    <td>
                        <?php echo $form->dropDownList($regions,'regionid', CHtml::listData(Regions::model()->findAll(array('order' => 'name')),'regionid','name'), array('class'=>'registration-select-big'));?>
                        <?php echo $form->error($regions,'regionid'); ?>
                        <div class="select-buttons">
                            <?php echo CHtml::submitButton('Удалить', array('class'=>'create-button')); ?>
                        </div>
                    </td>
                </tr>

                <?php $this->endWidget(); ?>

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'mes-form',
                    'action'=>Yii::app()->createUrl('settings/createimessengertypes'),
                    'enableAjaxValidation'=>false,
                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                )); ?>

                <tr>
                    <td>
                        <label for="name">Добавить мессенджер:</label>
                    </td>
                    <td>
                        <?php echo $form->errorSummary($imessengertypes); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $form->labelEx($imessengertypes,'image'); ?>

                    </td>
                    <td>
                        <?php echo $form->fileField($imessengertypes,'image'); ?>
                        <?php echo $form->error($imessengertypes,'image'); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $form->labelEx($imessengertypes,'name'); ?>
                        
                    </td>
                    <td>
                        <?php echo $form->textField($imessengertypes,'name',array('class'=>'registration-input')); ?>
                        <?php echo $form->error($imessengertypes,'name'); ?>
                        <div class="select-buttons">
                            <?php echo CHtml::submitButton('Добавить', array('class'=>'create-button')); ?>
                        </div>
                    </td>
                </tr>

                <?php $this->endWidget(); ?>

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'d-mes-form',
                    'action'=>Yii::app()->createUrl('settings/deleteimessengertypes'),
                    'enableAjaxValidation'=>false,
                )); ?>

                <tr>
                    <td>
                        <label for="name">Удалить мессенджер:</label>
                    </td>
                    <td>
                        <?php echo $form->errorSummary($imessengertypes); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $form->labelEx($imessengertypes,'imessengertypeid'); ?>
                        
                    </td>
                    <td>
                        <?php echo $form->dropDownList($imessengertypes,'imessengertypeid', CHtml::listData(ImessengerTypes::model()->findAll(array('order' => 'name')),'imessengertypeid','name'), array('class'=>'registration-select-big'));?>
                        <?php echo $form->error($imessengertypes,'rimessengertypeid'); ?>
                        <div class="select-buttons">
                            <?php echo CHtml::submitButton('Удалить', array('class'=>'create-button')); ?>
                        </div>
                    </td>
                </tr>

                <?php $this->endWidget(); ?>
            </tbody>
        </table>
    </div>
</div>
<?php
Yii::app()->clientScript->registerScript('settings', "
    (function(){
        $('#Rubrics_parent_rubricid').on('change', function(e){
            $('#Rubrics_rubricgroupid').prop( 'disabled', (e.target.value != null && e.target.value != '') );
            if(e.target.value != null && e.target.value != '') {
                $('#Rubrics_rubricgroupid').val( '' );
            }
            $('#rub-tr').css( 'visibility', (e.target.value != null && e.target.value != '')? 'hidden':'visible' );
        });
    })();
");
?>