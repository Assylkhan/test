<div class="row seasrch-result center-content">
<div class="span12 advanced-search">
    <div class="span6">
        <div class="search-results">
            <span class="result-label">Поиск:</span>
            <span class="search-param">"<?php echo $search;?>"</span>
            <span class="result-count">(предприятий:<?php echo $total;?>)</span>
        </div>
    </div>

</div>
</div>

<div class="row seasrch-result center-content">
    <div class="span12">
        <div class="chargeable-unit">

            <?php foreach($top as $card): ?>
                <div class="unit">
                    <a href="#" data-id="<?php echo $card->cardid ?>" class="name"><?php echo $card->orgname ?></a>
                    <span class="category">
                        <?php foreach($card->rubrics as $rubric): ?>
                            <?php echo $rubric->name.' '; ?>
                        <?php endforeach; ?>
                    </span>
                    <span class="phones">
                        <?php foreach($card->telephones as $telephone): ?>
                            <?php echo $telephone->value.' '; ?>
                        <?php endforeach; ?>
                    </span>
                    <span class="address">
                        <?php
                        $citycard = null;
                        if($card->cityid) {
                            $citycard = Cities::model()->findByAttributes(array('cityid' => $card->cityid));
                        }
                        echo implode(', ', array(
                                'city'=>$citycard != null?$citycard->name:'',
                                'street'=>$card->street,
                                'house'=>$card->housenumber,
                                'office'=>$card->office
                            )
                        );
                        ?>
                    </span>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="span12">
        <div id="cards" class="free-block">
            <?php foreach($cards as $card): ?>
                <div class="unit">
                    <a href="#" data-id="<?php echo $card->cardid ?>" class="name"><?php echo $card->orgname ?></a>
                    <span class="category">
                        <?php foreach($card->rubrics as $rubric): ?>
                            <?php echo $rubric->name.' '; ?>
                        <?php endforeach; ?>
                    </span>
                    <span class="phones">
                        <?php foreach($card->telephones as $telephone): ?>
                            <?php echo $telephone->value.' '; ?>
                        <?php endforeach; ?>
                    </span>
                    <span class="address">
                        <?php
                        $citycard = null;
                        if($card->cityid) {
                            $citycard = Cities::model()->findByAttributes(array('cityid' => $card->cityid));
                        }
                        echo implode(', ', array(
                                'city'=>$citycard != null?$citycard->name:'',
                                'street'=>$card->street,
                                'house'=>$card->housenumber,
                                'office'=>$card->office
                            )
                        );
                        ?>
                    </span>
                </div>
            <?php endforeach; ?>
        </div>
        <?php $this->widget('ext.yiinfinite-scroll.YiinfiniteScroller', array(
            'contentSelector' => '#cards',
            'itemSelector' => 'div.unit',
            'loadingText' => 'Загрузка...',
            'donetext' => 'Конец',
            'pages' => $pages,
        )); ?>
    </div>
</div>
<div class="wrap"></div>

<?php $this->renderPartial('/cards/_card'); ?>