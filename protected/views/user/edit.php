<?php Yii::app()->setLanguage('ru'); ?>
<?php
Yii::app()->getClientScript()->registerCoreScript( 'jquery.ui' );
?>

<link rel="stylesheet" href="/libs/ioncache-Tag-Handler/css/jquery.taghandler.css">
<script src="/libs/ioncache-Tag-Handler/js/jquery.taghandler.min.js"></script>

<script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<link rel="stylesheet" href="/frontend/css/site/style.css">

<div class="row registration center-content">
<div class="span12">
    <h1>Личный кабинет - <span>Редактирование карточки</span></h1>
    <button class="preview-button hover-green" data-id="<?php echo $card->cardid ?>">просмотр карточки</button>
</div>

<div class="span12 personal-menu">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#tab-general" id="general" role="tab" data-toggle="tab">Общая информация</a></li>
        <li><a href="#tab-onmap" id="onmap" role="tab" data-toggle="tab">На карте</a></li>
        <li class="proposals-tab"><a href="#tab-padeals" id="deals" role="tab" data-toggle="tab">Предложения</a></li>
        <li class="photos-tab"><a href="#tab-foto" id="foto" role="tab" data-toggle="tab">Фото</a></li>
        <li><a href="#tab-job" id="job" role="tab" data-toggle="tab">Вакансии</a></li>
    </ul>
</div>

<div class="tab-content">

    <div class="tab-pane active" id="tab-general">
        <div class="span12 registration2">
            <div class="span6 morespan">
                <table class="formtable">
                    <tbody>
                    <?php
                    Yii::app()->setLanguage('ru');
                    date_default_timezone_set('UTC');
                    if ($card->getBasePackageExpiredDate()) {
                            echo '<tr><td>Текущая публикация до:</td><td>'.// $card->getBasePackageExpiredDate()->format('d/M/Y').
                                Yii::app()->dateFormatter->format("d MMMM y", strtotime($card->getBasePackageExpiredDate()->format('Y/m/d'))) .
                                '</td></tr>';
                            }
                    ?>

                    <tr>
                        <td>
                            <label for="orgname" >Название организации</label>

                        </td>
                        <td>
                            <input name="orgname" id="orgname" type="text" class="registration-input card-editor" value="<?php echo $card->orgname; ?>"/>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="orgtypeid" >Тип организации</label>

                        </td>
                        <td>
                            <?php echo CHtml::dropDownList('orgtypeid',$card->orgtypeid, CHtml::listData(OrgTypes::model()->findAll(array('order' => 'name')),'orgtypeid','name'), array('class'=>'registration-select card-editor'));?>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="basepackage" >Базовый пакет</label>

                        </td>
                        <td>
                            <input type="checkbox" name="basepackage" id="basepackage" class="card-editor"/>
                        </td>
                    </tr>
                    <tr class="rubric-tr basepackage">
                        <td>
                            <label for="main-rubrics-selector">Главная рубрика</label>

                        </td>
                        <td>
                            <?php
                            $p_id = 0;
                            $mainrubric = Rubrics::model()->findByAttributes(array('rubricid' => $card->rubricid));
                            if($mainrubric){

                                $p_id = $mainrubric->parentRubric->rubricid;
                            }
                                echo CHtml::dropDownList('rubric', $p_id ? $p_id:0, CHtml::listData(Rubrics::model()->findAllByAttributes(array('rubrictypeid'=>'1'),array('order' => 'name')), 'rubricid', 'name'),  array('class'=>'registration-select', 'id'=>'main-rubrics-selector'));
                            ?>

                        </td>
                    </tr>
                    <tr class="subrubric-tr basepackage">
                        <td>
                            <label for="">Главная подрубрика</label>
                        </td>
                        <td>
                            <?php
                                $p_id = 0;
                                $mainrubric = Rubrics::model()->findByAttributes(array('rubricid' => $card->rubricid));
                                if($mainrubric){

                                    $p_id = $mainrubric->parentRubric->rubricid;
                                }
                                echo CHtml::dropDownList('rubricid', $card->rubricid, CHtml::listData(Rubrics::model()->findAllByAttributes(array('parent_rubricid'=>$p_id),array('order' => 'name')), 'rubricid', 'name'),  array('class'=>'registration-select card-editor', 'id'=>'rubricid'));
                            ?>
                        </td>
                    </tr>
                    <tr class="basepackage">
                        <td>
                            <label for="months" >Срок публикации</label>

                        </td>
                        <td>
                            <select id="months" class="registration-select card-editor months-editor">
                                <option value="2" <?php if($card->months == 2) echo "selected";?>>2</option>
                                <option value="3" <?php if($card->months == 3) echo "selected";?>>3</option>
                                <option value="4" <?php if($card->months == 4) echo "selected";?>>4</option>
                                <option value="5" <?php if($card->months == 5) echo "selected";?>>5</option>
                                <option value="6" <?php if($card->months == 6) echo "selected";?>>6</option>
                                <option value="7" <?php if($card->months == 7) echo "selected";?>>7</option>
                                <option value="8" <?php if($card->months == 8) echo "selected";?>>8</option>
                                <option value="9" <?php if($card->months == 9) echo "selected";?>>9</option>
                                <option value="10" <?php if($card->months == 10) echo "selected";?>>10</option>
                                <option value="11" <?php if($card->months == 11) echo "selected";?>>11</option>
                                <option value="12" <?php if($card->months == 12) echo "selected";?>>12</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="logo" >Лого</label>

                        </td>
                        <td>
                            <input name="logo" id="logo" type="file"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="description" >Описание</label>

                        </td>
                        <td>
                            <textarea id="description" name="description" class="registration-textarea card-editor"><?php echo $card->description ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="keywordspackage" >Ключевые слова</label>

                        </td>
                        <td>
                            <input type="checkbox" name="keywordspackage" id="keywordspackage" class="card-editor"/>
                        </td>
                    </tr>
                    <tr class="keywords">
                        <td>
                            <label for="keywordcountid" >Количество ключевых слов</label>
                        </td>
                        <td>
                            <?php echo CHtml::dropDownList('keywordcountid', $card->keywordcountid, CHtml::listData(PaidServiceDetails::model()->findAll('paidserviceid = 1'), 'paidservicedetaild', 'count'), array('class'=>'registration-select card-editor', 'value'=>$card->keywordcountid));?>
                            
                        </td>
                    </tr>
                    <tr class="keywords">
                        <td>
                            <label for="keywords">Ключевые слова:</label>
                        </td>
                        <td>
                            <div class="tagHandler " id="keywords"><ul id="ajax_no_initial_tag_handler" class="tagHandlerContainer"><li class="tagInput"><input class="tagInputField ui-autocomplete-input" type="text" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true"></li></ul></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="rubricspackage" >Дополнительные рубрики</label>

                        </td>
                        <td>
                            <input type="checkbox" name="rubricspackage" id="rubricspackage" class="card-editor"/>
                        </td>
                    </tr>
                    <tr class="rubrics">
                        <td>
                            <label for="rubriccountid" >Количество рубрик</label>

                        </td>
                        <td>
                            <?php echo CHtml::dropDownList('rubriccountid',$card->rubriccountid, CHtml::listData(PaidServiceDetails::model()->findAllByAttributes(array('paidserviceid'=>'2'), 'paidservicedetaild <> :paidservicedetaild', array(':paidservicedetaild'=>11)), 'paidservicedetaild', 'count'), array('class'=>'registration-select card-editor', 'value'=>$card->rubriccountid));?>

                        </td>
                    </tr>
                    <tr class="rubric-tr rubrics">
                        <td>
                            <label for="rubrics-selector">Рубрика</label>

                        </td>
                        <td>
                            <?php echo CHtml::dropDownList('rubric', 'rubricid', CHtml::listData(Rubrics::model()->findAllByAttributes(array('rubrictypeid'=>'1'),array('order' => 'name')), 'rubricid', 'name'),  array('class'=>'registration-select', 'id'=>'rubrics-selector'));?>

                        </td>
                    </tr>
                    <tr class="subrubric-tr rubrics">
                        <td>
                            <label for="">Подрубрика</label>
                        </td>
                        <td>
                            <select id="subrubrics-selector" class="registration-select">
                            </select>
                        </td>
                    </tr>
                    <tr class="rubrics">
                        <td></td>
                        <td>
                            <input type="button" class="hover-green add-rubric" value="добавить рубрику"/>
                        </td>
                    </tr>
                    <tr class="rubrics">
                        <td></td>
                        <td>
                            <div id="card-rubrics"></div>
                        </td>
                    </tr>
<!--                    <tr>-->
<!--                        <td>-->
<!--                            <label for="proposalspackage" >Предложения</label>-->
<!---->
<!--                        </td>-->
<!--                        <td>-->
<!--                            <input type="checkbox" name="proposalspackage" id="proposalspackage" class="card-editor"/>-->
<!--                        </td>-->
<!--                    </tr>-->
                    <tr>
                        <td>
                            <label for="photospackage" >Фото</label>

                        </td>
                        <td>
                            <input type="checkbox" name="photospackage" id="photospackage" class="card-editor"/>
                        </td>
                    </tr>
                    <tr class="photos">
                        <td>
                            <label for="photoscountid" >Количество фото</label>
                        </td>
                        <td>
                            <?php echo CHtml::dropDownList('photoscountid',$card->photoscountid, CHtml::listData(PaidServiceDetails::model()->findAll('paidserviceid = 6'), 'paidservicedetaild', 'count'), array('class'=>'registration-select card-editor'));?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="top" >Выделение в топ</label>

                        </td>
                        <td>
                            <input type="checkbox" name="top" id="top" class="card-editor" <?php if($card->top) echo "checked"; ?>"/>
                        </td>
                    </tr>
                    <tr class="top">
                        <td>
                            <label for="topmonths" >Срок публикации в топе</label>

                        </td>
                        <td>
                            <select id="topmonths" class="registration-select card-editor months-editor">
                                <option value="1" <?php if($card->months == 1) echo "selected";?>>1</option>
                                <option value="2" <?php if($card->months == 2) echo "selected";?>>2</option>
                                <option value="3" <?php if($card->months == 3) echo "selected";?>>3</option>
                                <option value="4" <?php if($card->months == 4) echo "selected";?>>4</option>
                                <option value="5" <?php if($card->months == 5) echo "selected";?>>5</option>
                                <option value="6" <?php if($card->months == 6) echo "selected";?>>6</option>
                                <option value="7" <?php if($card->months == 7) echo "selected";?>>7</option>
                                <option value="8" <?php if($card->months == 8) echo "selected";?>>8</option>
                                <option value="9" <?php if($card->months == 9) echo "selected";?>>9</option>
                                <option value="10" <?php if($card->months == 10) echo "selected";?>>10</option>
                                <option value="11" <?php if($card->months == 11) echo "selected";?>>11</option>
                                <option value="12" <?php if($card->months == 12) echo "selected";?>>12</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="main" >В новости</label>

                        </td>
                        <td>
                            <input type="checkbox" name="main" id="main" class="card-editor" <?php if($card->main) echo "checked"; ?>/>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label for="parentcardid" >ID родительской организации</label>

                        </td>
                        <td>
                            <input type="text" name="parentcardid" id="parentcardid" class="registration-input card-editor" value="<?php echo $card->parentcardid ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="telephone" >Телефон</label>
                        </td>
                        <td>
                            <?php
                            $this->widget('CMaskedTextField', array(
//                                'model' => $telephone,
                                'attribute' => 'value',
                                'id' =>  'telephone',
                                'name' => 'telephone',
                                'mask' => ' 9(999)9999999',
                                'htmlOptions' => array('class'=>'registration-input')
                            ));
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="button" class="hover-green add-telephone edit-button" value="добавить телефон"/>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <div id="card-telephones"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="email" >Почта</label>

                        </td>
                        <td>
                            <input id="email" name="email" value=""/>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="button" class="hover-green add-email edit-button" value="добавить email"/>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <div id="card-emails"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="imessengertypeid" >Мессенджер</label>

                        </td>
                        <td>
                            <?php echo CHtml::dropDownList('imessengertypeid','imessengertypeid', CHtml::listData(ImessengerTypes::model()->findAll(), 'imessengertypeid', 'name'), array('class'=>'registration-select'));?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="messenger" ></label>

                        </td>
                        <td>
                            <input id="messenger" name="messenger" value=""/>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="button" class="hover-green add-messenger edit-button" value="добавить мессенджер"/>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <div id="card-messengers"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="orgindex" >Индекс</label>

                        </td>
                        <td>
                            <input id="orgindex" type="text" class="registration-input card-editor" value="<?php echo $card->orgindex ?>"/>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="regionid" >Регион</label>

                        </td>
                        <td>
                            <?php echo CHtml::dropDownList('regionid',$card->regionid, CHtml::listData(Regions::model()->findAll(), 'regionid', 'name'), array('class'=>'registration-select card-editor'));?>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="cityid" >Город</label>

                        </td>
                        <td>
                            <?php echo CHtml::dropDownList('cityid',$card->cityid, CHtml::listData(Cities::model()->findAll(), 'cityid', 'name'), array('class'=>'registration-select card-editor'));?>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="street" >Улица</label>

                        </td>
                        <td>
                            <input id="street" type="text" class="registration-input card-editor" value="<?php echo $card->street ?>"/>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="housenumber" >Номер дома</label>

                        </td>
                        <td>
                            <input id="housenumber" type="text" class="registration-input card-editor" value="<?php echo $card->housenumber ?>"/>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="office" >Офис</label>

                        </td>
                        <td>
                            <input id="office" type="text" class="registration-input card-editor" value="<?php echo $card->office ?>"/>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="websiteurl" >Сайт</label>

                        </td>
                        <td>
                            <input id="websiteurl" type="text" class="registration-input card-editor" value="<?php echo $card->websiteurl ?>"/>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="updatecontactphone" >Ответственный за обновление</label>

                        </td>
                        <td>
                            <?php
                            $this->widget('CMaskedTextField', array(
                                'model' => $card,
                                'attribute' => 'updatecontactphone',
                                'id' =>  'updatecontactphone',
                                'name' => 'updatecontactphone',
                                'mask' => ' 9(999)9999999',
                                'htmlOptions' => array('class'=>'registration-input')
                            ));
                            ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="updatecontactname" >Телефон ответственного</label>

                        </td>
                        <td>
                            <input id="updatecontactname" type="text" class="registration-input card-editor" value="<?php echo $card->updatecontactname ?>"/>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="housenumber" >Номер дома</label>

                        </td>
                        <td>
                            <input id="housenumber" type="text" class="registration-input card-editor" value="<?php echo $card->housenumber ?>"/>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="accountantphone" >Телефон бухгалтера</label>

                        </td>
                        <td>
                            <?php
                            $this->widget('CMaskedTextField', array(
                                'model' => $card,
                                'attribute' => 'accountantphone',
                                'id' =>  'accountantphone',
                                'name' => 'accountantphone',
                                'mask' => ' 9(999)9999999',
                                'htmlOptions' => array('class'=>'registration-input')
                            ));
                            ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="accountantname" >Бухгалтер</label>

                        </td>
                        <td>
                            <input id="accountantname" type="text" class="registration-input card-editor" value="<?php echo $card->accountantname ?>"/>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="workschedule" >Расписание</label>

                        </td>
                        <td>
                            <input id="workschedule" type="text" class="registration-input card-editor" value="<?php echo $card->workschedule ?>"/>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="busnums" >Транспорт</label>

                        </td>
                        <td>
                            <input id="busnums" type="text" class="registration-input card-editor" value="<?php echo $card->busnums ?>"/>
                            
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label for="juridicaladdress" >Юридический адрес</label>

                        </td>
                        <td>
                            <input id="juridicaladdress" type="text" class="registration-input card-editor" value="<?php echo $card->juridicaladdress ?>"/>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="iinbin" >ИИН/БИН</label>

                        </td>
                        <td>
                            <input id="iinbin" type="text" class="registration-input card-editor" value="<?php echo $card->iinbin ?>"/>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="bik" >БИК</label>

                        </td>
                        <td>
                            <input id="bik" type="text" class="registration-input card-editor" value="<?php echo $card->bik ?>"/>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="iik" >ИИК</label>

                        </td>
                        <td>
                            <input id="iik" type="text" class="registration-input card-editor" value="<?php echo $card->iik ?>"/>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="kbe" >КБЕ</label>

                        </td>
                        <td>
                            <input id="kbe" type="text" class="registration-input card-editor" value="<?php echo $card->kbe ?>"/>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="bankname" >Банк</label>

                        </td>
                        <td>
                            <input id="bankname" type="text" class="registration-input card-editor" value="<?php echo $card->bankname ?>"/>
                            
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="span6 lessspan">
                <table class="paytable table-striped order-details">
                    <tbody>
                    <tr>
                        <td colspan="2">
                            <span class="pay-label">Сумма</span>
                            <span class="sum orderSum" ></span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="tab-pane" id="tab-onmap">
        <div class="span12 onmap">
            <div class="span6 lessspan">
                <h4 class="maptip">Отметьте на карте месторасположение с помощью мышки</h4>
                <button id="reset-map" class="hover-green">сбросить</button>
            </div>
            <div class="span6 morespan">
                <div id="map"></div>
            </div>
        </div>
    </div>

    <div class="tab-pane proposals-tab" id="tab-padeals">
        <div class="span12 registration2">
            <div class="span6 morespan">
                <div id="add-proposal">
                    <div class="add-block">
                        <button class="add-proposal add-button hover-green" onclick="createDeal();">добавить предложение</button>
                    </div>
                </div>
                <div id="card-proposals">
                </div>         
                <div class="add-block">
                    <span class="add-text">
                    <p><i>Здравствуйте, уважаемый клиент!</i></p>
                    <p>Если вашей компанией планируется проведение акции или скидки, мы предлагаем Вам, разместится в рубрике «Акции и Предложения».</p>
                    <p>В компании «Info Smart» работают профессиональные график дизайнеры и журналисты, которые смогут вам помочь сделать рекламу яркой и интересной.</p>
                    <p>Чтобы заказать услугу свяжитесь снами по <b>тел: 8 (7172) 61 62 61, 8 (7172) 30 79 42</b></p>
                    <p><i>Работая вместе, мы сможем достичь огромных высот!</i></p>
                    </span>
                </div>
            </div>  
            <div class="span6 lessspan">
                <table class="paytable table-striped order-details">
                    <tbody>
                    <tr>
                        <td colspan="2">
                            <span class="pay-label">Сумма</span>
                            <span class="sum orderSum" ></span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="tab-pane photo-tab" id="tab-foto">

        <div class="span12 foto">
            <div class="span6 morespan">
                <div id="add-photo">
                    <div class="add-block">
                        <span class="add-text">
                            Информация для пользователя (например, стоимость 1 предложения,
                            предложение от дизайнера компании)
                        </span>
<!--                        <button class="add-vacancy add-button hover-green" onclick="createVacancies();">добавить вакансию</button>-->
                        <div style="padding-top: 20px;padding-bottom: 20px;">
                            <input id="add-photo" class="add-photo" type="file" value="Выберите файлы" multiple/>
                        </div>
                    </div>
                </div>
                <div id="card-photos">
                </div>
            </div>
            <div class="span6 lessspan">
                <table class="paytable table-striped order-details">
                    <tbody>
                    <tr>
                        <td colspan="2">
                            <span class="pay-label">Сумма</span>
                            <span class="sum orderSum" ></span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="tab-pane" id="tab-job">
        <div class="span12 registration2">
            <div class="span6 morespan">
                <div id="add-vacancy">
                    <div class="add-block">
                        <span class="add-text">
                            Информация для пользователя (например, стоимость 1 предложения,
                            предложение от дизайнера компании)
                        </span>
                        <button class="add-vacancy add-button hover-green" onclick="createVacancies();">добавить вакансию</button>
                    </div>
                </div>
                <div id="card-vacancies">
                </div>
            </div>
            <div class="span6 lessspan">
                <table class="paytable table-striped order-details">
                    <tbody>
                    <tr>
                        <td colspan="2">
                            <span class="pay-label">Сумма</span>
                            <span class="sum orderSum" ></span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="span12 registration-buttons">
    <button class="cancel-edit cancel-step hover-green">отмена</button>
    <button class="save-edit next-step hover-green">сохранить и отправить на модерацию</button>
</div>

<script src="/frontend/js/user/onmap.js"></script>
</div>
<div class="wrap"></div>

<div class="modal fade" id="success_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="location.href='/site/'"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <!--                <h4 class="modal-title">Modal title</h4>-->
            </div>
            <div class="modal-body">
                <div class="modal-row">
                    <div class="span6">
                        <div class="message">
                            <span class="message-row">Ваш личный кабинет успешно сохранен!</span>
                        </div>
                        <button class="center-button hover-green" onclick="location.href='/site/'">на главную</button>
                    </div>
                </div>
            </div>
            <!--            <div class="modal-footer"></div>-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="error_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="location.href='/site/'"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <!--                <h4 class="modal-title">Modal title</h4>-->
            </div>
            <div class="modal-body">
                <div class="modal-row">
                    <div class="span6">
                        <div class="message">
                            <span class="message-row">Произошла ошибка при сохранении. Обратитесь к разработчику.</span>
                            <span class="message-row" id ="errormsg" style="color: red;font-weight: bold;"></span>
                        </div>
                        <button class="center-button hover-green" onclick="location.href='/site/'">на главную</button>
                    </div>
                </div>
            </div>
            <!--            <div class="modal-footer"></div>-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php $this->renderPartial('/cards/_card'); ?>

<script>
    /* Запускаем сразу после загрузки страницы */
    $(document).ready(function () {
        marginBlocks();

        // Читаем из куков текущий город
        var currentCity = readCookie("currentCity");
        currentCity = (currentCity != null) ? currentCity : $(".city-ul li").eq(0).text();
        $(".current-city").text(currentCity);

        $('.upbutton').click(function () {
            $('html, body').animate({scrollTop: 0}, 'slow');
        });

        $(".selectcity").click(function () {
            rotateBG();
            $(".city-ul").slideToggle();
        });

        $(".city-ul li").click(function () {
            var selectedCity = $(this).text();
            $(".current-city").text(selectedCity);
            // Записываем в куки текущий город
            writeCookie("currentCity", selectedCity, 7);
            rotateBG();
            $(".city-ul").slideToggle();
        });

        // Показываем лист контактов, которые можно добавить
        $(".contact-button").click(function () {
            $("#contactList").show();
        });

        // Добавляем поля для контакта, выбранного из списка
        $("#contactList").change(function () {
            var type = parseInt($("#contactList option:selected").val());
            $("#contactList").hide();

            var contactName = getName(type);
            var contactLabel = getLabel(type);
            var tr = document.createElement("tr");

            var td1 = document.createElement("td");
            var label = document.createElement("label");
            label.innerHTML = contactLabel;
            var span = document.createElement("span");
            span.setAttribute("class", "explanation");
            span.innerHTML = "пояснение к пункту";
            td1.appendChild(label);
            td1.appendChild(span);

            var td2 = document.createElement("td");
            var input = document.createElement("input");
            input.setAttribute("type", "text");
            input.setAttribute("name", contactName + "[]");
            input.setAttribute("class", "registration-input");
            td2.appendChild(input);

            tr.appendChild(td1);
            tr.appendChild(td2);
            $(".formtable tbody").append(tr);
        });

        // Добавляем рубрику / подрубрику
//        $(".add-rubric").click(function () {
//            var rubric = $(".rubric-tr").eq(0).clone();
//            var last = $(".subrubric-tr").length - 1;
//            $(rubric).insertAfter($(".subrubric-tr").eq(last));
//            var subrubric = $(".subrubric-tr").eq(0).clone();
//            var slast = $(".rubric-tr").length - 1;
//            $(subrubric).insertAfter($(".rubric-tr").eq(slast));
//        });
    });

    function rotateBG() {
        if ($(".city-arrow").is(".rotate-bg")) {
            $(".city-arrow").removeClass("rotate-bg");
        } else {
            $(".city-arrow").addClass("rotate-bg");
        }
    }

    function getName(type) {
        var name = "";

        switch (type) {
            case 1:
                name = "phone";
                break;
            case 2:
                name = "email";
                break;
            case 3:
                name = "site";
                break;
            case 4:
                name = "messenger";
                break;
        }

        return name;
    }

    function getLabel(type) {
        var label = "";

        switch (type) {
            case 1:
                label = "Телефон:";
                break;
            case 2:
                label = "Электронная почта:";
                break;
            case 3:
                label = "Сайт:";
                break;
            case 4:
                label = "Интернет мессенджер:";
                break;
        }

        return label;
    }

    /* Используем при изменении ширины окна в режиме live */
    $(window).resize(function () {
        $(".img-block").removeClass("no-right")
        marginBlocks()
    });

    /* Собственно, меняем отступы для крайних в строке картинок, чтобы все было хорошо, дети росли, старики не болели, закончилась война */
    function marginBlocks() {
        var blocks = $(".img-block");
        var length = blocks.length;
        var width = window.innerWidth;
        var num = width < 1440 ? 4 : 6;
        if (length > 0) {
            for (var i = num; i < length; i += num + 1) {
                if (blocks.eq(i)) {
                    blocks.eq(i).addClass("no-right");
                }
            }
        }
    }
</script>
<script>
    var expired = {
        base: <?php if($card->isBasePackageRun()){echo 'new Date("'.$card->getBasePackageExpiredDate()->format('Y/m/d').'")';} else{echo 0;}?>,
    }
    var first_rubrics =   [<?php foreach($card->rubrics as $rubric): ?>
                                <?php echo "{id:".($rubric->rubricid.",name:'".$rubric->name."'},");?>
                            <?php endforeach ?>];
    var first_keywords =   [<?php foreach($card->keywords as $keyword): ?>
                                <?php echo "{id:".($keyword->keywordid.",value:'".$keyword->value."'},");?>
                            <?php endforeach ?>];
    var first_telephones =   [<?php foreach($card->telephones as $item): ?>
                                <?php echo "{id:".($item->telephoneid.",value:'".$item->value."'},");?>
                            <?php endforeach ?>];
    var first_emails =     [<?php foreach($card->emails as $item): ?>
                                <?php echo "{id:".($item->emailid.",value:'".$item->value."'},");?>
                            <?php endforeach ?>];
    var first_messengers =     [<?php foreach($card->messengers as $item): ?>
                                <?php echo "{id:".$item->messengerid.",value:'".$item->value."',typename:'".$item->imessengertype->name."',typeid:".$item->imessengertype->imessengertypeid."},";?>
                            <?php endforeach ?>];
    var keywords_count =   <?php echo $card->keywordcount->count;?>;
    var rubrics_count =   <?php echo $card->rubriccount->count;?>;

    var first_vacancies =  [<?php foreach($card->vacancies as $item): ?>
                                <?php echo "{id:".$item->vacancyid.
                                ",name:'".$item->name.
                                "',content:'".trim(preg_replace('/\s+/', ' ', $item->content)).
                                "',endpublic:'".$item->endpublic.
                                "',startpublic:'".$item->startpublic."'},";?>
                            <?php endforeach ?>];

    var first_proposals =  [<?php foreach($card->proposals as $item): ?>
                                <?php echo "{id:".$item->proposalsid.
                                ",name:'".$item->name.
                                "',fresh:'".$item->fresh.
                                "',image:'".$item->image.
                                "',content:'".trim(preg_replace('/\s+/', ' ', $item->content)).
                                "',rubricid:'".$item->rubricid.
                                "',endpublic:'".$item->endpublic.
                                "',startpublic:'".$item->startpublic."'},";?>
                            <?php endforeach ?>];

    var first_photos = [<?php foreach($card->cardPhotoses as $item): ?>
                            <?php echo "{id:".$item->cardphotoid.
                            ",photo:'".$item->photo."'},";?>
                        <?php endforeach ?>];
    console.log(first_proposals);
    console.log(first_vacancies);
</script>
<script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script src="/frontend/js/user/routine.js"></script>