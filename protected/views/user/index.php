<?php Yii::app()->setLanguage('ru'); ?>
<link rel="stylesheet" href="/frontend/css/forms.css">
<div class="row application person-area center-content">
    <div class="span12">
        <div class="header-block">
            <h1>Личный кабинет - <span><?php echo $card->orgname ?></span></h1>
        </div>
    </div>
    <? if ($payStatus != ''): ?>
        <span class="payment-result"><?= $payStatus ?></span>
    <? endif; ?>
    <div class="span12">
        <div class="balance">
            <div class="content">
                <span>Баланс</span> <span class="bold-content"><?= number_format($balance, 0, '.', ' ') ?> тг</span>
            </div>
            <button class="recharge-button hover-green" id="balance_topup">пополнить счет</button>
        </div>
        <div class="orders-history">
            <div class="content">
                <span>История заказов</span>
            </div>
            <span class="result bold-content">+1</span>
        </div>
    </div>
    <div class="span12 business-card">
        <h3>Карточка предприятия</h3>
        <table class="info-table">
            <tbody>
            <tr>
                <td class="name">
                    <span class="main"><?php echo $card->orgname ?></span>
                    <span class="decription">(код карточки: <?php echo $card->cardid ?>)</span>
                    <!--                    <span class="description">--><?php //echo $card->cardstatus->name; ?><!--</span>-->
                </td>
                <td class="card-info">
                    <div class="info-date"><?php if($card->cardstatusid == 1){ echo 'опубликовано';}else{echo 'не опубликовано';} ?> <span><?php date_default_timezone_set('UTC');if($card->cardstatusid == 1){ echo ' ( '.Yii::app()->dateFormatter->format("d MMMM y",$card->publicationdate).' )';} ?></span></div>
                    <hr>
                    <div class="info-date"><?php if($card->cardstatusid == 3){ echo 'на модерации';}else{if($card->cardstatusid == 1){echo 'модерация пройдена';}else{echo 'модерация еще не пройдена';}} ?><span><?php date_default_timezone_set('UTC');if($card->cardstatusid == 1){echo ' ( '.Yii::app()->dateFormatter->format("d MMMM y",$card->moderationdate).' )';} ?></span></div>
                </td>

                <td class="publication-label">

                    <?php
                    if($card->cardstatusid == 1) {
                        echo '<span class="main">';
                        echo 'срок публикации';
                        echo '</span>';
                    }?>
                </td>
                <td class="publication-date">
                    <span class="info-date">
                        <?php
                        if($card->cardstatusid == 1){
                            $date = new DateTime($activeorder->order->publicationdate);
                            echo 'c  '.Yii::app()->dateFormatter->format("d MMMM y",strtotime($card->getBasePackageStartDate()->format('Y/m/d'))) . "\n";
                        }
                        ?>
                    </span>

                    <?php
                    if($card->cardstatusid == 1) {
                        echo '<hr>';
                    }
                    ?>
                    <span class="info-date">
                        <?php
                        Yii::app()->setLanguage('ru');
                        if($card->cardstatusid == 1) {
                            $date = new DateTime($activeorder->order->publicationdate);
                            $resdata = $date->add(new DateInterval('P'.($activeorder->order->months?$activeorder->order->months:'1').'M'));
                            echo 'до '.Yii::app()->dateFormatter->format("d MMMM y",strtotime($card->getBasePackageExpiredDate()->format('Y/m/d'))) . "\n";
                        }?>
                    </span>
                </td>
            </tr>
            <tr>
                <td class="name">
                    <span style="padding-left: 60px;">Статус: <?php echo $card->cardstatus->name; ?></span>
                </td>
            </tr>
            </tbody>
        </table>
        <ul class="menu">
            <li><a href="/user/edit">редактировать</a></li>
            <li><a id="orders-history-open" href="#">история заказов</a></li>
        </ul>
        <div class="span12">
            <?php
            if($activeorder != null&&$activeorder->order!=null&&$activeorder->order->activeInvoices!=null&&$activeorder->order->activeInvoices->invoice!=null&&$activeorder->order->activeInvoices->invoice->invoiceid!=null&&$activeorder->order->orderstatusid != 1) {
                echo '<div class="order">' .
                    '<div class="order-info">' .
                    '<span class="date">';
                echo Yii::app()->dateFormatter->format("d MMMM y", $activeorder->order->createdate) . "\n";
                echo '</span>' .
                    '</div>'.
                    '<div class="order-header">' .
                    '<span class="name">Заказ № ' . $activeorder->orderid . '</span> - <span class="status">' . $activeorder->order->orderstatus->name . '</span>' .
                    '</div>' .
                    '<div class="order-info">' .
                    '<p class="discription">';
                foreach ($activeorder->order->orderDetails as $od) {
                    echo $od->paidservicedetaild0->paidservice->name . '; ';
                }
                echo '</p>' .
                    '</div>'.
                    '<br>' .
                    '<div>'.
                    '<ul class="menu">'.
                    '<li><a href="#" id="payment_topup">оплатить картой</a></li>'.
                    '<li><a href="'.(Yii::app()->request->getBaseUrl(true)).'/user/getinvoice/'.$activeorder->order->activeInvoices->invoice->invoiceid.'.pdf'.'">счет на оплату</a></li>'.
                    '</ul>'.
                    '</div>'.
                    '</div>';
            }
            ?>
        </div>
    </div>
    <div class="span12 business-card" style="padding-top: 20px;;
    <?php
    if(!$card->isPackageRun('3') && !$card->isBasePackageRun()) { echo ';display: none;';}?>">
        <table class="info-table">
            <tbody>
            <tr>
                <td class="name">
                    <span class="main">
                    <?php
                    if($card->isPackageRun('3') && $card->isBasePackageRun()) {
                        echo 'Выделение карточки в топ';
                    }
                    ?>
                    </span>
                </td>
                <td class="card-info">

                </td>

                <td class="publication-label">

                    <?php
                    if($card->isPackageRun('3') && $card->isBasePackageRun()) {
                        echo '<span class="main">';
                        echo 'срок публикации';
                        echo '</span>';
                    }?>
                </td>
                <td class="publication-date">
                    <span class="info-date">
                        <?php
                        if($card->isPackageRun('3') && $card->isBasePackageRun()) {
                            echo 'c  '.Yii::app()->dateFormatter->format("d MMMM y",strtotime($card->getPackageStartDate('3')->format('Y/m/d'))) . "\n";
                        }
                        ?>
                    </span>

                    <?php
                    if($card->isPackageRun('3') && $card->isBasePackageRun()) {
                        echo '<hr>';
                    }
                    ?>
                    <span class="info-date">
                        <?php
                        Yii::app()->setLanguage('ru');
                        if($card->isPackageRun('3') && $card->isBasePackageRun()) {
                            echo 'до '.Yii::app()->dateFormatter->format("d MMMM y",strtotime($card->getPackageExpiredDate('3')->format('Y/m/d'))) . "\n";
                        }?>
                    </span>
                </td>
            </tr>
            </tbody>
        </table>

    </div>

    <div id="statistic" class="span12 statistic" data-id="<?php echo $publiccard ? $publiccard->cardid : ''; ?>" <?php if($publiccard == null){echo 'style="display: none;"';}; ?>>
        <div class="span12">
            <div class="statistic-block">
                <h4>Показы на сайте за</h4>
                <select id="site-stat-period" class="period" name="period">
                    <option value="1">год</option>
                    <option selected value="2">месяц</option>
                    <option value="3">неделя</option>
                </select>
                <div id="site-stat" class="schedule"></div>
            </div>
        </div>
        <div class="span12">
            <div class="statistic-block">
                <h4>Показы на мобильном за</h4>
                <select id="mobile-stat-period" class="period" name="period">
                    <option value="1">год</option>
                    <option selected value="2">месяц</option>
                    <option value="3">неделя</option>
                </select>
                <div id="mobile-stat" class="schedule"></div>
            </div>
        </div>
    </div>
</div>
<div class="wrap"></div>

<div class="modal fade" id="general_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <!--                <h4 class="modal-title">Modal title</h4>-->
            </div>
            <div class="modal-body">
                <div class="modal-row">
                    <div class="span6">
                        <h6>Личный кабинет - <?php echo $card->orgname;?></h6>
                        <div class="header-block">
                            <h1>История заказов</h1>
                        </div>
                        <div class="orders-list">
                            <?php
                            foreach(array_reverse($orders) as $order){
                                if($order->orderstatus->orderstatusid == 1) {
                                    echo '<div class="order">' .
                                        '<div class="order-info">' .
                                        '<span class="date">';
                                    date_default_timezone_set('UTC');
                                    echo Yii::app()->dateFormatter->format("d MMMM y", $order->createdate);
                                    echo '</span>' .
                                        '</div>' .
                                        '<div class="order-header">' .
                                        '<span class="name">Заказ № ' . $order->orderid . '</span> - <span class="status">' . $order->orderstatus->name . '</span>' .
                                        '</div>' .
                                        '<div class="order-info">' .

                                        '<p class="discription">';
                                    foreach ($order->orderDetails as $od) {
                                        echo $od->paidservicedetaild0->paidservice->name . '; ';
                                    }
                                    echo '</p>' .
                                        '</div>' .
                                        '</div>';
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <!--            <div class="modal-footer"></div>-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<? $this->widget('application.widgets.BalanceWidget', array(
    'id' => 'balance_topup',
    'merchantId' => '000000000000005'
)); ?>
<? $this->widget('application.widgets.PaymentWidget', array(
    'id' => 'payment_topup'
)); ?>

<script src="/frontend/js/crossfilter.min.js"></script>
<script src="/frontend/js/d3.min.js"></script>
<script src="/frontend/js/dimple.v2.1.0.min.js"></script>
<script>
    /* Запускаем сразу после загрузки страницы */
    $( document ).ready(function() {
        $("#orders-history-open").on("click", function(){
            $('#general_modal').modal('show');
            return false;
        });
        // Читаем из куков текущий город
        var currentCity = readCookie("currentCity");
        currentCity = (currentCity != null) ? currentCity : $(".city-ul li").eq(0).text();
        $(".current-city").text(currentCity);

        $('.upbutton').click(function(){
            $('html, body').animate({scrollTop:0}, 'slow');
        });

        $(".selectcity").click(function(){
            rotateBG();
            $(".city-ul").slideToggle();
        });

        $(".city-ul li").click(function(){
            var selectedCity = $( this).text();
            $(".current-city").text(selectedCity);
            // Записываем в куки текущий город
            writeCookie("currentCity", selectedCity, 7);
            rotateBG();
            $(".city-ul").slideToggle();
        });
    });

    function rotateBG() {
        if ( $(".city-arrow").is(".rotate-bg") ) {
            $(".city-arrow").removeClass("rotate-bg");
        } else {
            $(".city-arrow").addClass("rotate-bg");
        }
    }

</script>
<script src="/frontend/js/statistics.js"></script>