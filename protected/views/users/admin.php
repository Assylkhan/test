<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);
?>
<h1 class="adminh">Управление пользователями</h1>


<div class="button-row">
    <a href="create" class="button-style hover-green">Создать</a>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$model->searchAdmin(),
	'filter'=>$model,
	'summaryText'=>'Отображение {start}-{end} из  {page} результатов',
	'columns'=>array(
		'userid',
		'email',
        array(
            'name'=>'userroleid',
            'value'=>'$data->getRoleName()',
//            'value'=>'($data->publicationstatusid) ? $data->publicationstatusid : ""',
//            'filter' => CHtml::activeDropDownList($model, 'publicationstatusid', CHtml::listdata(PublicationStatuses::model()->findAll(), 'publicationstatusid', 'name')),
            'filter' => CHtml::listData(UserRoles::model()->findAll(), 'userroleid', 'name'),
        ),
		'nickname',
        array(
            'name'=>'createdate',
            'filter'=>false,
        ),
		array(
			'class'=>'CButtonColumn',
            'template' => '{update}&nbsp;{delete}',
			'deleteConfirmation'=>"Вы уверены, что хотите удалить этот элемент?",
			'buttons' => array(
				'delete' => array('imageUrl'=>Yii::app()->request->baseUrl.'/images/delete-16.png'),
				'update' => array('imageUrl'=>Yii::app()->request->baseUrl.'/images/update-16.png'),
			),
			'deleteButtonOptions' => array(
				'rel' => 'tooltip',
				'data-toggle' => 'tooltip',
				'title' => 'Удалить'),
			'updateButtonOptions'=> array(
				'rel' => 'tooltip',
				'data-toggle' => 'tooltip',
				'title' => 'Изменить'),
		),
	),
)); ?>
