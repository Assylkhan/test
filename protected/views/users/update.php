<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->userid=>array('view','id'=>$model->userid),
	'Update',
);
?>

<div class="row">
    <div class="span12">
        <div class="main-row">
            <h1 class="adminh">Администрация - <span>Обновить пользователя</span></h1>
            <!--            <a href="#" class="button-style preview hover-green">предпросмотр статьи</a>-->
        </div>
        <?php $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
</div>