<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'users-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'enableClientValidation'=>true,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    <p class="note">Поля, отмеченные звездочкой <span class="required">*</span> обязательны.</p>

<?php echo $form->errorSummary($model,'',''); ?>

    <table class="addnew-table">
        <tbody>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'nickname'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'nickname',array('size'=>25,'maxlength'=>25)); ?>
                <?php echo $form->error($model,'nickname'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'userroleid'); ?>
                
            </td>
            <td>
                <?php
                    $criteria=new CDbCriteria(); $criteria->addInCondition('userroleid',array(1,2));
                    $roles = UserRoles::model()->findAll($criteria);
                    echo $form->dropDownList($model,'userroleid', CHtml::listData($roles,'userroleid','name'),array('empty'=>'Выберите'));
                ?>
                <?php echo $form->error($model,'userroleid'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'email'); ?>
                
            </td>
            <td>
                <?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50)); ?>
                <?php echo $form->error($model,'email'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'password'); ?>
                
            </td>
            <td>
                <?php echo $form->passwordField($model,'password',array('size'=>50,'maxlength'=>50)); ?>
                <?php echo $form->error($model,'password'); ?>
            </td>
        </tr>

        <?php echo $form->hiddenField($model,'createdate'); ?>
        </tbody>
    </table>
    <div class="button-row">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('class'=>'create-button')); ?>
        <?php echo CHtml::linkButton('Отмена', array('class'=>'create-button', 'href'=>'/users/admin')); ?>
        <!--                <button class="create-button">создать</button>-->
<!--        <button class="cancel-button">отмена</button>-->
    </div>
<?php $this->endWidget(); ?>