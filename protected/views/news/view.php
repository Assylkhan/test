<?php Yii::app()->setLanguage('ru'); ?>

<div class="row deals center-content">
    <div class="span12 new">
        <div class="header-row">
            <h1><?php echo $model->title; ?></h1>
            <span class="public-date"><?php date_default_timezone_set('UTC');echo Yii::app()->dateFormatter->format("d MMM y",$model->createdate) ?></span>
        </div>
        <div class="new-content">
            <?php if($model->filename){ ?>
                <div class="img-block">
                    <img src="<?php echo (Yii::app()->request->getBaseUrl(true)).('/images/news/'.$model->filename); ?>" alt="New" title="New">
                </div>
            <?php } ?>
            <?php if($model->content){ ?>
                <p>
                    <?php echo $model->content; ?>
                </p>
            <?php } ?>
        </div>
    </div>
</div>