<?php
/* @var $this NewsController */
/* @var $model News */
?>

<div class="row">
    <div class="span12">
        <div class="main-row">
            <h1 class="adminh">Новости - <span>Редактировать новость</span></h1>
<!--            <a href="#" class="button-style preview hover-green">предпросмотр статьи</a>-->
        </div>
        <?php $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
</div>