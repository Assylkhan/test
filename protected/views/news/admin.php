<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs=array(
	'News'=>array('index'),
	'Manage',
); ?>
<h1 class="adminh">Управление новостями</h1>


<div class="button-row">
    <a href="create" class="button-style hover-green">Создать</a>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'news-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'summaryText'=>'Отображение {start}-{end} из  {page} результатов',
	'columns'=>array(
		'newsid',
        array(
            'name'=>'publicationstatusid',
            'value'=>'$data->getStatusName()',
//            'value'=>'($data->publicationstatusid) ? $data->publicationstatusid : ""',
//            'filter' => CHtml::activeDropDownList($model, 'publicationstatusid', CHtml::listdata(PublicationStatuses::model()->findAll(), 'publicationstatusid', 'name')),
            'filter' => CHtml::listData(PublicationStatuses::model()->findAll(), 'publicationstatusid', 'name'),
        ),
		'title',
		'shortcontent',
		array(
            'name'=>'createdate',
            'filter'=>false,
        ),
		array(
			'class' => 'CButtonColumn',
			'template' => ' {update} {view} {delete} ',
			'deleteConfirmation' => "Вы уверены, что хотите удалить этот элемент?",
			'deleteButtonOptions' => array(
				'rel' => 'tooltip',
				'data-toggle' => 'tooltip',
				'title' => 'Удалить'),
			'updateButtonOptions'=> array(
				'rel' => 'tooltip',
				'data-toggle' => 'tooltip',
				'title' => 'Изменить'),
			'viewButtonOptions'=> array(
				'rel' => 'tooltip',
				'data-toggle' => 'tooltip',
				'title' => 'Просмотр'),
			'buttons' => array(
				'delete' => array('imageUrl' => Yii::app()->request->baseUrl . '/images/delete-16.png'),
				'update' => array('imageUrl' => Yii::app()->request->baseUrl . '/images/update-16.png'),
				'view' => array('imageUrl' => Yii::app()->request->baseUrl . '/images/open_in-16.png',

				),
			),
			'htmlOptions'=>array('width'=>'80px'),
		),
	)
)); ?>
