
<div class="row deals center-content">
    <div class="span12">
        <h1>Новости</h1>
        <div id="news">
            <?php foreach($news as $post): ?>
                <div class="deal">
                    <a href="/news/view/id/<?php echo $post->newsid ?>">
                        <img src="<?php echo (Yii::app()->request->getBaseUrl(true)).('/images/news/'.$post->filename); ?>" alt="" title="">
                        <div class="ptext">
                            <span class="caption"><?php echo $post->title; ?></span>
                            <p class="text"><?php echo $post->shortcontent; ?></p>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
        <?php $this->widget('ext.yiinfinite-scroll.YiinfiniteScroller', array(
            'contentSelector' => '#news',
            'itemSelector' => 'div.deal',
            'loadingText' => 'Загрузка...',
            'donetext' => 'Конец',
            'pages' => $pages,
        )); ?>
        <div class="clr"></div>
    </div>
</div>