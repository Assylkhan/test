<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'news-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>true,
    'enableClientValidation'=>true,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<p class="note">Поля, отмеченные звездочкой <span class="required">*</span> обязательны.</p>

<?php echo $form->errorSummary($model,'',''); ?>

    <table class="addnew-table">
        <tbody>
        <tr>
            <td>
                <!--                            <label for="name">Название новости<span class="required">*</span>:</label>-->
                <?php echo $form->labelEx($model,'title'); ?>
                
            </td>
            <td>
                <!--                            <input name="name" id="name" class="registration-input">-->
                <?php echo $form->textField($model,'title',array('class'=>'registration-input', 'id'=>'name')); ?>
                <?php echo $form->error($model,'title'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'cityid'); ?>
                
            </td>
            <td>
                <?php echo $form->dropDownList($model,'cityid', CHtml::listData(Cities::model()->findAll(array('order' => 'name')),'cityid','name'),array('empty'=>'Выберите'));?>
                <?php echo $form->error($model,'cityid'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'publicationstatusid'); ?>
                
            </td>
            <td>

                <?php echo $form->dropDownList($model,'publicationstatusid', CHtml::listData(PublicationStatuses::model()->findAll(array('order' => 'name')),'publicationstatusid','name'));?>
                <!--                            <select name="status" class="status">-->
                <!--                                <option value="1">опубликовано</option>-->
                <!--                                <option value="2">отключено</option>-->
                <!--                            </select>-->

                <?php echo $form->error($model,'publicationstatusid'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'image'); ?>
                
            </td>
            <td>
                <?php echo $form->fileField($model,'image'); ?>
                <?php echo $form->error($model,'image'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <!--                            <label for="description">Описание компании<span class="required">*</span>:</label>-->
                <?php echo $form->labelEx($model,'shortcontent'); ?>
                
            </td>
            <td>
                <!--                            <textarea name="description" id="description" class="registration-textarea"></textarea>-->
                <?php echo $form->textArea($model,'shortcontent',array('class'=>'registration-textarea', 'id'=>'description')); ?>
                <?php echo $form->error($model,'shortcontent'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'content'); ?>
                
            </td>
            <td>
                <!--                            <textarea name="main-text" class="main-text"></textarea>-->
                <?php echo $form->textArea($model,'content',array('class'=>'main-text', 'id'=>'main-text')); ?>
                <?php echo $form->error($model,'content'); ?>
            </td>
        </tr>

        <?php echo $form->hiddenField($model,'createdate'); ?>
        </tbody>
    </table>
    <div class="button-row">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('class'=>'create-button')); ?>
        <?php echo CHtml::linkButton('Отмена', array('class'=>'create-button', 'href'=>'/news/admin')); ?>
        <!--                <button class="create-button">создать</button>-->
<!--        <button class="cancel-button">отмена</button>-->
    </div>
<?php $this->endWidget(); ?>