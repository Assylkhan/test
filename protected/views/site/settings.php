<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <script src="js/jquery-2.1.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.liquidcarousel.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="css/template.css">
    <link rel="stylesheet" href="css/modules.css">
    <link rel="stylesheet" href="css/screen.css">
    <link rel="stylesheet" href="css/admin.css">
</head>
<body>
<div id="content-block">
    <div class="row header center-content">
        <div class="span12">
            <div class="span6 p-relative lessspan">
                <div class="logo">
                    <a href="/">
                        <img src="images/logo.png" alt="INFOSMART" title="INFOSMART">
                    </a>
                </div>
            </div>
            <div class="span6 morespan">
                <ul class="admin-menu">
                    <li>
                        <a href="admin.php">пользователи</a>
                    </li>
                    <li>
                        <a href="cards.php">карточки</a>
                    </li>
                    <li>
                        <a href="news.php">новости</a>
                    </li>
                    <li>
                        <a href="#">статистика</a>
                    </li>
                    <li>
                        <a href="settings.php">найтроски сайта</a>
                    </li>
                    <li>
                        <a href="#">выйти</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="span12">
            <div class="main-row">
                <h1 class="adminh">Настройки сайта</h1>
            </div>
            <form action="#" method="post">
                <table class="addnew-table">
                    <tbody>
                        <tr>
                            <td>
                                <label for="name">Добавить рубрику:</label>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <label for="name">Название:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <input name="name" id="name" class="registration-input">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="rubric">Родительская рубрика:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <select name="rubric" id="rubric" class="registration-select-big">
                                    <option value="1">Главная</option>
                                    <option value="2">Не главная</option>
                                    <option value="3">Левая</option>
                                    <option value="4">Правая</option>
                                    <option value="5">Партия Мао</option>
                                    <option value="6">Свободу зайцам</option>
                                </select>
                                <div class="select-buttons">
                                    <button class="rubrics-button hover-green">добавить</button>
                                    <button class="rubrics-button hover-green">очистить</button>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="category">Удалить категорию:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <select name="category" id="category" class="registration-select-big">
                                    <option value="0">Выберите категорию</option>
                                    <option value="1">Категория 1</option>
                                    <option value="2">Категория 2</option>
                                    <option value="3">Категория 3</option>
                                    <option value="4">Категория 4</option>
                                    <option value="5">Категория 5</option>
                                    <option value="6">Категория 6</option>
                                </select>
                                <div class="select-buttons">
                                    <button class="rubrics-button hover-green">удалить</button>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="city">Добавить город:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <input name="city" id="city" class="registration-input">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="city-del">Удалить город:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <select name="city-del" id="city-del" class="registration-select-big">
                                    <option value="0">Выберите город</option>
                                    <option value="1">Город 1</option>
                                    <option value="2">Город 2</option>
                                    <option value="3">Город 3</option>
                                    <option value="4">Город 4</option>
                                    <option value="5">Город 5</option>
                                    <option value="6">Город 6</option>
                                </select>
                                <div class="select-buttons">
                                    <button class="rubrics-button hover-green">удалить</button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
<!--    <div class="wrap"></div>-->
</div>
</body>
</html>