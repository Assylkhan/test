<?php Yii::app()->getClientScript()->registerCoreScript('jquery.ui');
//Yii::app()->clientScript->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css')
?>
<!--ioncache-->

<link rel="stylesheet" href="/frontend/css/forms.css">
<link rel="stylesheet" href="/libs/ioncache-Tag-Handler/css/jquery.taghandler.css">
<script src="/libs/ioncache-Tag-Handler/js/jquery.taghandler.min.js"></script>

<script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script src="/frontend/js/site/onmap.js"></script>
<script src="/frontend/js/site/functions.js"></script>
<script src="/frontend/js/jquery.maskedinput-1.3.min.js"></script>
<link rel="stylesheet" href="/frontend/css/site/style.css">


<span id="basePackageCost" style="display: none;"><?php echo $basicPackage->cost; ?></span>

<!-- 11111111111111111-->
<div class="row registration center-content" id="registrationform1">
    <div class="span12">
        <h1>Регистрация</h1>
    </div>
    <div class="span12 registration-steps center-content">
        <div class="steps">
            <div class="active-step"><span class="step-number">1</span> шаг</div>
            <div class="upcoming-brench"></div>

            <div class="upcoming-step"><span class="step-number">2</span> шаг</div>
            <div class="upcoming-brench"></div>
            <div class="upcoming-step"><span class="step-number">3</span> шаг</div>
        </div>
    </div>
    <div class="span12 registration2">
        <!--	<div class="span6 morespan">-->
        <form action="#" method="post">
            <div class="reg-form">
                <div class="reg-row">
                    <label for="login">Логин (email)</label>
                    <input type="text" name="login" id="login">

                    <div class="description">
                        <span class="arrow"></span>
                        <span class="text" id="loginstatus"></span>
                    </div>
                </div>
                <div class="reg-row">
                    <label for="password1">пароль</label>
                    <input type="password" name="password1" id="password1">

                    <div class="description">
                        <span class="arrow"></span>
                        <span class="text">только латиница и цифры</span>
                    </div>
                </div>
                <div class="reg-row">
                    <label for="password2">Пароль повторно</label>
                    <input type="password" name="password2" id="password2">

                    <div class="description">
                        <span class="arrow"></span>
                        <span class="text" id="passwordstatus"></span>
                    </div>
                </div>
                <?php if (CCaptcha::checkRequirements()): ?>
                    <div class="reg-row">
                        <div class="captcha-block">
                            <?php $this->widget('CCaptcha', array("buttonLabel" => "<br />Сгенерировать другой")); ?>
                        </div>
                        <input type="text" name="captcha" id="captcha">

                        <div class="description">
                            <span class="arrow"></span>
                            <span class="text" id="captchastatus"></span>
                        </div>
                    </div>
                <?php endif; ?>

            </div>
        </form>
        <!--		</div>-->

    </div>
    <div class="span12 registration-buttons">
        <button class="cancel-step hover-green" onclick="location.href='/site/'"> отмена</button>
        <button class="next-step hover-green" onclick="go2Step(); false;">следующий шаг</button>
    </div>
</div>

<!-- 22222222222222222-->
<div class="row registration center-content reghidden" id="registrationform2">
<div class="span12">
    <h1>Регистрация</h1>
</div>
<div class="span12 registration-steps center-content">
    <div class="steps">
        <div class="passed-step"><span class="step-number">1</span> шаг</div>
        <div class="passed-brench"></div>
        <div class="active-step"><span class="step-number">2</span> шаг</div>
        <div class="upcoming-brench"></div>
        <div class="upcoming-step"><span class="step-number">3</span> шаг</div>
    </div>
</div>
<div class="span12 registration2">
<div class="span6 morespan">
<table class="formtable">
<tbody>
<tr>
    <td>
        <label for="companyName">Название организации<span class="required">*</span>:</label>
        <span class="explanation">пояснение к пункту</span>
    </td>
    <td>
        <input name="companyName" id="companyName" class="registration-input">
    </td>
</tr>
<tr>
    <td>
        <label for="ownership">Вид (форма) собственности:</label>
        <span class="explanation">пояснение к пункту</span>
    </td>
    <td>
        <?php echo CHtml::dropDownList('ownership', 'orgtypeid', CHtml::listData(OrgTypes::model()->findAll(), 'orgtypeid', 'name'), array('class' => 'registration-select'));?>

    </td>
</tr>
<tr>
    <td>
        <label for="logo">Логотип организации:</label>
        <span class="explanation">загружать рисунок не больше 200 кб</span>
    </td>
    <td>
        <input type="file" name="logo" id="logo">
    </td>
</tr>
<tr>
    <td>
        <label for="description">Описание компании:</label>
        <span class="explanation">пояснение к пункту</span>
    </td>
    <td>
        <textarea name="description" id="description" class="registration-textarea"></textarea>
    </td>
</tr>
<!----------------------------------------------------------------------->
<tr class="rubric-tr">
    <td>
        <label for="main-rubrics-selector">Главная рубрика</label>

    </td>
    <td>
        <?php echo CHtml::dropDownList('rubric', 'rubricid', CHtml::listData(Rubrics::model()->findAllByAttributes(array('rubrictypeid'=>'1'),array('order' => 'name')), 'rubricid', 'name'),  array('class'=>'registration-select', 'id'=>'main-rubrics-selector'));?>

    </td>
</tr>
<tr class="subrubric-tr">
    <td>
        <label for="">Главная подрубрика</label>
    </td>
    <td>
        <select id="rubricid" class="registration-select card-editor">
        </select>
    </td>
</tr>
<tr>
    <td>
        <label for="months" >Срок публикации</label>

    </td>
    <td>
        <select id="months" class="registration-select card-editor months-editor">
<!--            <option value="1">1</option>-->
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
        </select>
    </td>
</tr>
<!----------------------------------------------------------------------->
<!--<tr>
		<td>
			<label for="selectiontopday">Выделение в топ*:</label>
			<span class="explanation">Выберите количество дней</span>
		</td>
		<td>
			<?php /*echo CHtml::dropDownList('selectiontopday', 'paidservicedetaild', CHtml::listData(PaidServiceDetails::model()->findAll('paidserviceid = 3'), 'paidservicedetaild', 'count'),  array('class'=>'registration-select', 'empty'=>'Выберите'));*/?>
		</td>
	</tr>-->
<!---->

<tr>
    <td>
        <label for="rubricspackage" >Дополнительные рубрики</label>

    </td>
    <td>
        <input type="checkbox" name="rubricspackage" id="rubricspackage" class="card-editor"/>
    </td>
</tr>

<tr>
    <td>
        <label for="rubriccount">Количество рубрик*:</label>
        <span class="explanation">увеличения количество до:</span>
    </td>
    <td>
        <?php echo CHtml::dropDownList('rubriccount', 'paidservicedetaild', CHtml::listData(PaidServiceDetails::model()->findAll('paidserviceid = 2'), 'paidservicedetaild', 'count'), array('class' => 'registration-select', 'empty' => 'Выберите'));?>
    </td>
</tr>

<tr>
    <td>
        <label for="rubric">Рубрика<span class="required">*</span>:</label>
        <span class="explanation">пояснение к пункту</span>
    </td>
    <td>
        <?php echo CHtml::dropDownList('rubric', 'rubricid', CHtml::listData(Rubrics::model()->findAll('parent_rubricid IS NULL'), 'rubricid', 'name'), array('class' => 'registration-select', 'empty' => 'Выберите'));?>
    </td>
</tr>
<tr>
    <td>
        <label for="subrubric">Подубрика<span class="required">*</span>:</label>
        <span class="explanation">пояснение к пункту</span>
    </td>
    <td>
        <select name="subrubric" id="subrubric" class="registration-select">
        </select>
        <span class="hrefstyle" onclick="addSubRubricForSave();false;">Добавить</span>
        <br>

        <div id="subrubricAddItems"></div>
    </td>
</tr>
<!---->
<!--<tr>-->
<!--    <td>-->
<!--        <label for="keywordspackage" >Ключевые слова</label>-->
<!---->
<!--    </td>-->
<!--    <td>-->
<!--        <input type="checkbox" name="keywordspackage" id="keywordspackage" class="card-editor"/>-->
<!--    </td>-->
<!--</tr>-->
<!---->
<tr>
    <td>
        <label for="keynum">Количество ключевых слов*:</label>
        <span class="explanation">увеличения количество до:</span>
    </td>
    <td>
        <?php echo CHtml::dropDownList('keywordnum', 'paidservicedetaild', CHtml::listData(PaidServiceDetails::model()->findAll('paidserviceid = 1'), 'paidservicedetaild', 'count'), array('class' => 'registration-select', 'empty' => 'Выберите'));?>
    </td>
</tr>
<tr>
    <td>
        <label for="keywords">Ключевые слова:</label>
        <span class="explanation">пояснение к пункту</span>
    </td>
    <td>

        <div class="tagHandler " id="keywords">
            <ul id="ajax_no_initial_tag_handler" class="tagHandlerContainer">
                <li class="tagInput"><input class="tagInputField ui-autocomplete-input" type="text" autocomplete="off"
                                            role="textbox" aria-autocomplete="list" aria-haspopup="true"></li>
            </ul>
        </div>

    </td>
</tr>
<!---->
<tr>
    <td>
        <label for="top" >Выделение в топ</label>

    </td>
    <td>
        <input type="checkbox" name="top" id="top" class="card-editor""/>
    </td>
</tr>
<tr>
    <td>
        <label for="topmonths" >Срок публикации в топе</label>

    </td>
    <td>
        <select id="topmonths" class="registration-select card-editor months-editor">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
        </select>
    </td>
</tr>
<tr>
    <td>
        <label for="main" >В новости</label>

    </td>
    <td>
        <input type="checkbox" name="main" id="main" class="card-editor"/>
    </td>
</tr>

<!---->
<tr>
    <td>
        <label for="orgindex">Индекс<span class="required">*</span>:</label>
        <span class="explanation">пояснение к пункту</span>
    </td>
    <td>
        <input type="text" name="orgindex" id="orgindex" class="registration-input">
    </td>
</tr>
<tr>
    <td>
        <label for="region">Область<span class="required">*</span>:</label>
        <span class="explanation">пояснение к пункту</span>
    </td>
    <td>
        <?php echo CHtml::dropDownList('region', 'regionid', CHtml::listData(Regions::model()->findAll(), 'regionid', 'name'), array('class' => 'registration-select', 'empty' => 'Выберите'));?>
    </td>
</tr>
<tr>
    <td>
        <label for="city">Город<span class="required">*</span>:</label>
        <span class="explanation">пояснение к пункту</span>
    </td>
    <td>
        <?php echo CHtml::dropDownList('city', 'cityid', CHtml::listData(Cities::model()->findAll(), 'cityid', 'name'), array('class' => 'registration-select', 'empty' => 'Выберите'));?>
    </td>
</tr>
<tr>
    <td>
        <label for="street">Улица<span class="required">*</span>:</label>
        <span class="explanation">пояснение к пункту</span>
    </td>
    <td>
        <input type="text" name="street" id="street" class="registration-input">
    </td>
</tr>
<tr>
    <td>
        <label for="house">Дом<span class="required">*</span>:</label>
        <span class="explanation">пояснение к пункту</span>
    </td>
    <td>
        <input type="text" name="house" id="house" class="registration-input">
    </td>
</tr>
<tr>
    <td>
        <label for="office">Офис:</label>
        <span class="explanation">пояснение к пункту</span>
    </td>
    <td>
        <input type="text" name="office" id="office" class="registration-input">
    </td>
</tr>
<tr>
    <td>
        <label for="office">Отметить на карте:</label>
        <span class="explanation">пояснение к пункту</span>
    </td>
    <td>
        <div id="map2" style="width: 805; height: 500px;background-color: #ffffff;"></div>
    </td>
</tr>
<tr>
    <td>
        <label for="phone">Телефон<span class="required">*</span>:</label>
        <span class="explanation">пояснение к пункту</span>

    </td>
    <td>
         <?php echo CHtml::dropDownList('phone_type', 'phonetypeid', CHtml::listData(PhoneTypes::model()->findAll(), 'phonetypeid', 'name'), array('class' => 'registration-select', 'empty' => 'Выберите'/*, 'onchange'=>'changeMask();'*/));?>
        <input name="phone" id="phone" class="registration-input" style="width: 177px;">
        <input name="note" id="note" class="registration-input" placeholder="Примечание">
        <span class="hrefstyle" onclick="addTelephoneForSave();false;">Добавить</span>
        <br>

        <div id="telephonesAddItems"></div>
    </td>
</tr>
<tr>
    <td>
        <label for="email">Электронная почта<span class="required">*</span>:</label>
        <span class="explanation">пояснение к пункту</span>
    </td>
    <td>
        <input type="text" name="email" id="email" class="registration-input" style="width: 177px;">
        <span class="hrefstyle" onclick="addEmailForSave();false;">Добавить</span>
        <br>

        <div id="emailsAddItems"></div>
    </td>
</tr>
<tr>
    <td>
        <label for="site">Сайт:</label>
        <span class="explanation">пояснение к пункту</span>
    </td>
    <td>
        <input type="text" name="site" id="site" class="registration-input">
    </td>
</tr>
<tr>
    <td>
        <label for="messenger">Интернет мессенджер:</label>
        <span class="explanation">пояснение к пункту</span>
    </td>
    <td>
        <?php echo CHtml::dropDownList('messenger_type', 'imessengertypeid', CHtml::listData(ImessengerTypes::model()->findAll(), 'imessengertypeid', 'name'), array('class' => 'registration-select', 'empty' => 'Выберите'));?>
        <input type="text" name="messenger" id="messenger" class="registration-input-small"
               style="width: 179px;margin-top: 9px;">
        <span class="hrefstyle" onclick="addMessengerForSave();false;">Добавить</span>
        <br>

        <div id="messengersAddItems"></div>
    </td>
</tr>

<tr>
    <td>
        <label for="updatecontact">Ответственный сотрудник для обновления карточки<span
                class="required"></span>:</label>
        <span class="explanation">пояснение к пункту</span>
    </td>
    <td>
        <input type="text" name="updatecontact" id="updatecontact" class="registration-input">
    </td>
</tr>
<tr>
    <td>
        <label for="updatecontactphone">Телефон<span class="required"></span>:</label>
        <span class="explanation">телефон сотрудника</span>
    </td>
    <td>
        <?php
        $this->widget('CMaskedTextField', array(
            'id' => 'updatecontactphone',
            'name' => 'updatecontactphone',
            'mask' => ' 9(999)9 999999',
            'htmlOptions' => array('size' => 20)
        ));
        ?>
    </td>
</tr>
<tr>
    <td>
        <label for="accountant">Бухгалтер<span class="required"></span>:</label>
        <span class="explanation">пояснение к пункту</span>
    </td>
    <td>
        <input type="text" name="accountant" id="accountant" class="registration-input">
    </td>
</tr>
<tr>
    <td>
        <label for="accountantphone">Телефон<span class="required"></span>:</label>
        <span class="explanation">Телефон бухгалтера</span>
    </td>
    <td>

        <?php
        $this->widget('CMaskedTextField', array(
            'id' => 'accountantphone',
            'name' => 'accountantphone',
            'mask' => ' 9(999)9 999999',
            'htmlOptions' => array('size' => 20)
        ));
        ?>
    </td>
</tr>

<tr>
    <td>
        <label for="workschedule">Время работы<span class="required"></span>:</label>
        <span class="explanation">пояснение к пункту</span>
    </td>
    <td>
      <?php /*  <input name="workschedule" id="workschedule" class="registration-input"> */?>
		<textarea name="workschedule" id="workschedule" class="registration-textarea"></textarea>

    </td>
</tr>
<tr>
    <td>
        <label for="busnums">Проезд автобусами<span class="required"></span>:</label>
        <span class="explanation">пояснение к пункту</span>
    </td>
    <td>
        <input name="busnums" id="busnums" class="registration-input">
    </td>
</tr>

<tr>
    <td>
        <label for="parentorgid">ID родительской организации<span class="required"></span>:</label>
        <span class="explanation">пояснение к пункту</span>
    </td>
    <td>
        <input name="parentorgid" id="parentorgid" class="registration-input">
        <span id='parentorgiderror' style="color: red;"> </span>
    </td>
</tr>


</tbody>
</table>
<!--div class="add-info">
    <span class="add-label">Дополнительные поля информации</span>
    <button class="add-open hover-green">раскрыть</button>
</div-->
</div>
<div class="span6 lessspan">
    <table class="paytable table-striped order-details" id="orderDetails">
        <tbody>
        <tr>
            <td colspan="2">
                <span class="pay-label">Сумма</span>
                <span class="sum orderSum">0 тг</span>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</div>
<div class="span12 registration-buttons">
    <button class="cancel-step hover-green" onclick="location.href='/site/'">отмена</button>
    <button class="next-step hover-green" onclick="go3Step(); false;">следующий шаг</button>
</div>
</div>

<!-- 33333333333333333-->

<div class="row registration center-content reghidden" id="registrationform3">
    <div class="span12">
        <h1>Формирование заявки</h1>
    </div>
    <div class="span12 registration-steps center-content">
        <div class="steps">
            <div class="passed-step"><span class="step-number">1</span> шаг</div>
            <div class="passed-brench"></div>
            <div class="passed-step"><span class="step-number">2</span> шаг</div>
            <div class="passed-brench"></div>
            <div class="active-step"><span class="step-number">3</span> шаг</div>
        </div>
    </div>
    <div class="span12 registration3">
        <h2>Заказ <!--№ 656415354--></h2>

        <table class="order-table table-striped" id="finishOrder">
            <tbody>
            <tr>
                <td>Итого</td>
                <td class="sum orderSum"></td>
                <td class=""></td>
                <td class=""></td>
            </tr>
            </tbody>
        </table>


        <div class="span6 morespan">
            <table class="formtable">
                <tbody>
                <tr>
                    <td>
                        <label for="uAddress">Юридический адрес<span class="required">*</span>:</label>
                        <span class="explanation">пояснение к пункту</span>
                    </td>
                    <td>
                        <input name="uAddress" id="uAddress" class="registration-input">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="iinBin">ИИН/БИН<span class="required">*</span>:</label>
                        <span class="explanation">пояснение к пункту</span>
                    </td>
                    <td>
                        <input name="iinBin" id="iinBin" class="registration-input">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="bik">БИК<span class="required">*</span>:</label>
                        <span class="explanation">пояснение к пункту</span>
                    </td>
                    <td>
                        <input name="bik" id="bik" class="registration-input">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="iik">ИИК<span class="required">*</span>:</label>
                        <span class="explanation">пояснение к пункту</span>
                    </td>
                    <td>
                        <input name="iik" id="iik" class="registration-input">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="kbe">Кбе<span class="required">*</span>:</label>
                        <span class="explanation">пояснение к пункту</span>
                    </td>
                    <td>
                        <input name="kbe" id="kbe" class="registration-input">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="bankName">Название банка<span class="required">*</span>:</label>
                        <span class="explanation">пояснение к пункту</span>
                    </td>
                    <td>
                        <input name="bankName" id="bankName" class="registration-input">
                    </td>
                </tr>

                </tbody>
            </table>
        </div>
        <div class="span6 lessspan"></div>
    </div>
    <div class="span12 moderation-button">
        <button class="send-application hover-green" onclick="sendToModeration();false;">отправить заявку на модерацию
        </button>
    </div>
</div>

<div class="modal fade" id="general_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="location.href='/site/'"><span
                        aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <!--                <h4 class="modal-title">Modal title</h4>-->
            </div>
            <div class="modal-body">
                <div class="modal-row">
                    <div class="span6">
                        <div class="message">
                            <span class="message-row"><b>Поздравляем!</b></span>
                            <span class="message-row">Вы зарегистрировались в информационном справочнике Info Smart, теперь необходмо активировать Вашу учетную запись!</span>
                            <span class="message-row">На Ваш e-mail через несколько минут будет выслано письмо с ключом для активации. Дождитесь его получения и подтвердите регистрацию.</span>
                        </div>
                        <button class="center-button hover-green" onclick="location.href='/site/'">на главную</button>
                    </div>
                </div>
            </div>
            <!--            <div class="modal-footer"></div>-->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="general_modal2">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="location.href='/site/'"><span
                        aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <!--                <h4 class="modal-title">Modal title</h4>-->
            </div>
            <div class="modal-body">
                <div class="modal-row">
                    <div class="span6">
                        <div class="message">
                            <span class="message-row">Произошла ошибка при сохранении. Обратитесь к разработчику.</span>
                            <span class="message-row" id="errormsg" style="color: red;font-weight: bold;"></span>
                        </div>
                        <button class="center-button hover-green" onclick="location.href='/site/'">на главную</button>
                    </div>
                </div>
            </div>
            <!--            <div class="modal-footer"></div>-->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->


