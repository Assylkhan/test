<div class="row news">
    <?php
    $lastnews = 0;
    $lastprs = 0;
    while ($lastnews < count($news) || $lastprs < count($proposals)) {

        if ($lastnews > count($news) && $lastprs > count($proposals)) {
            break;
        }
        if (count($news) < $lastnews) {
            if (count($proposals) > $lastprs) {
                echo    '<div class="newsblock">' .
                    '<div class="newscontent big" data-type="proposal" data-id="' . $proposals[$lastprs]->cardid . '">' .
                    '<div class="img" style="background-image: url(\'' . ('/images/news/' . $news[$lastnews]->filename) . '\');background-size: cover;background-repeat: no-repeat;background-position:center;width: 100%;height: 300px;"></div>' .
                    '<div class="ptext">' .
                    '<span class="caption">' . $proposals[$lastprs]->name . '</span>' .
                    '</div>' .
                    '</div>' .
                    '</div>';
                $lastprs++;
            }
        } else {
            if (count($news) > $lastnews) {
                echo    '<div class="newsblock">' .
                    '<div class="newscontent big" data-type="news" data-id="' . $news[$lastnews]->newsid . '">' .
                    '<div class="img" style="background-image: url(\'' . ('/images/news/' . $news[$lastnews]->filename) . '\');background-size: cover;background-repeat: no-repeat;background-position:center;width: 100%;height: 300px;"></div>' .

                    '<div class="ptext">' .
                    '<span class="caption">' . $news[$lastnews]->title . '</span>' .
                    '</div>' .
                    '</div>' .
                    '</div>';
                $lastnews++;
            }
        }

        $block = '<div class="newsblock">';
        if ($lastnews > count($news) && $lastprs > count($proposals)) {
            break;
        }
        if (count($proposals) <= $lastprs) {
            if (count($news) > $lastnews) {
                $block = $block .
                    '<div class="newscontent" data-type="news" data-id="' . $news[$lastnews]->newsid . '">' .
                    '<div class="img" style="background-image: url(\'' . ('/images/news/' . $news[$lastnews]->filename) . '\');background-size: cover;background-repeat: no-repeat;background-position:center;width: 100%;height: 150px;"></div>' .

                    '<div class="ptext">' .
                    '<span class="caption">' . $news[$lastnews]->title . '</span>' .
                    '</div>' .
                    '</div>';
                $lastnews++;
            }
        } else {
            if (count($proposals) > $lastprs) {
                $block = $block .
                    '<div class="newscontent" data-type="proposal" data-id="' . $proposals[$lastprs]->cardid . '">' .
                    '<div class="img" style="background-image: url(\'' . ('/images/proposal/' . $proposals[$lastprs]->image) . '\');background-size: cover;background-repeat: no-repeat;background-position:center;width: 100%;height: 150px;"></div>' .
                    '<div class="ptext">' .
                    '<span class="caption">' . $proposals[$lastprs]->name . '</span>' .
                    '</div>' .
                    '</div>';
                $lastprs++;
            }
        }

        if ($lastnews > count($news) && $lastprs > count($proposals)) {
            break;
        }
        if (count($proposals) <= $lastprs) {
            if (count($news) > $lastnews) {
                $block = $block .
                    '<div class="newscontent" data-type="news" data-id="' . $news[$lastnews]->newsid . '">' .
                    '<div class="img" style="background-image: url(\'' . ('/images/news/' . $news[$lastnews]->filename) . '\');background-size: cover;background-repeat: no-repeat;background-position:center;width: 100%;height: 150px;"></div>' .

                    '<div class="ptext">' .
                    '<span class="caption">' . $news[$lastnews]->title . '</span>' .
                    '</div>' .
                    '</div>';
                $lastnews++;
            }
        } else {
            if (count($proposals) > $lastprs) {
                $block = $block .
                    '<div class="newscontent" data-type="proposal" data-id="' . $proposals[$lastprs]->cardid . '">' .
                    '<div class="img" style="background-image: url(\'' . ('/images/proposal/' . $proposals[$lastprs]->image) . '\');background-size: cover;background-repeat: no-repeat;background-position:center;width: 100%;height: 150px;"></div>' .
                    '<div class="ptext">' .
                    '<span class="caption">' . $proposals[$lastprs]->name . '</span>' .
                    '</div>' .
                    '</div>';
                $lastprs++;
            }
        }
        $block = $block . '</div>';
        echo $block;
    }
    ?>
    <div class="clr"></div>
</div>
<div class="row headings center-content">
    <div class="span12">
        <div class="heading">
            <?php
            $count = count($main_rubrics);
            $step = ceil($count / 4);
            $pos = 0;
            ?>
            <?php foreach (array_slice($main_rubrics, $pos, $step) as $main_rubric): ?>

                <table class="headengtable table-striped">
                    <thead>
                    <tr>
                        <th colspan="2">
                            <div class="rubric-icon">
                                <img src="<?php echo (Yii::app()->request->getBaseUrl(true)).('/images/rubrics/'.$main_rubric->filename); ?>" alt="+" title="+">
                            </div>
                            <a href="/cards/index/id/<?php echo $main_rubric->rubricid; ?>">
                                <?php echo $main_rubric->name; ?>
                            </a>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($main_rubric->rubrics as $rubric): ?>
                        <tr>
                            <td class="name"><a
                                    href="/cards/index/id/<?php echo $rubric->rubricid; ?>"><?php echo $rubric->name; ?></a>
                            </td>
                            <td class="count">
                                <?php
                                $count = Yii::app()->db->createCommand()
                                    ->select('count(*) as value')
                                    ->from('cards c')
                                    ->leftJoin('ref_rubrics_cards r', 'r.cardid=c.cardid')
                                    ->where('c.ispublic=true && r.rubricid=:rubricid', array(':rubricid' => $rubric->rubricid))
                                    ->queryRow();
                                //                        $count = RefRubricsCards::model()->countByAttributes(array('rubricid'=>$rubric->rubricid));
                                echo $count['value'] > 0 ? $count['value'] : '';
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endforeach; ?>
        </div>
        <div class="headingmargin"></div>
        <div class="heading">
            <?php foreach (array_slice($main_rubrics, $pos + $step, $step) as $main_rubric): ?>
                <table class="headengtable table-striped">
                    <thead>
                    <tr>
                        <th colspan="2">
                            <div class="rubric-icon">
                                <img src="<?php echo (Yii::app()->request->getBaseUrl(true)).('/images/rubrics/'.$main_rubric->filename); ?>" alt="+" title="+">
                            </div>
                            <a href="/cards/index/id/<?php echo $main_rubric->rubricid; ?>">
                                <?php echo $main_rubric->name; ?>
                            </a>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($main_rubric->rubrics as $rubric): ?>
                        <tr>
                            <td class="name"><a
                                    href="/cards/index/id/<?php echo $rubric->rubricid; ?>"><?php echo $rubric->name; ?></a>
                            </td>
                            <td class="count">
                                <?php
                                $count = Yii::app()->db->createCommand()
                                    ->select('count(*) as value')
                                    ->from('cards c')
                                    ->leftJoin('ref_rubrics_cards r', 'r.cardid=c.cardid')
                                    ->where('c.ispublic=true && r.rubricid=:rubricid', array(':rubricid' => $rubric->rubricid))
                                    ->queryRow();
                                //                        $count = RefRubricsCards::model()->countByAttributes(array('rubricid'=>$rubric->rubricid));
                                echo $count['value'] > 0 ? $count['value'] : '';
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endforeach; ?>
        </div>
        <div class="headingmargin"></div>
        <div class="heading">
            <?php foreach (array_slice($main_rubrics, $pos + 2 * $step, $step) as $main_rubric): ?>
                <table class="headengtable table-striped">
                    <thead>
                    <tr>
                        <th colspan="2">
                            <div class="rubric-icon">
                                <img src="<?php echo (Yii::app()->request->getBaseUrl(true)).('/images/rubrics/'.$main_rubric->filename); ?>" alt="+" title="+">
                            </div>
                            <a href="/cards/index/id/<?php echo $main_rubric->rubricid; ?>">
                                <?php echo $main_rubric->name; ?>
                            </a>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($main_rubric->rubrics as $rubric): ?>
                        <tr>

                            <td class="name"><a
                                    href="/cards/index/id/<?php echo $rubric->rubricid; ?>"><?php echo $rubric->name; ?></a>
                            </td>
                            <td class="count">
                                <?php
                                $count = Yii::app()->db->createCommand()
                                    ->select('count(*) as value')
                                    ->from('cards c')
                                    ->leftJoin('ref_rubrics_cards r', 'r.cardid=c.cardid')
                                    ->where('c.ispublic=true && r.rubricid=:rubricid', array(':rubricid' => $rubric->rubricid))
                                    ->queryRow();
                                //                        $count = RefRubricsCards::model()->countByAttributes(array('rubricid'=>$rubric->rubricid));
                                echo $count['value'] > 0 ? $count['value'] : '';
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endforeach; ?>
        </div>
        <div class="headingmargin"></div>
        <div class="heading">
            <?php foreach (array_slice($main_rubrics, $pos + 3 * $step, $step) as $main_rubric): ?>
                <table class="headengtable table-striped">
                    <thead>
                    <tr>
                        <th colspan="2">
                            <div class="rubric-icon">
                                <img src="<?php echo (Yii::app()->request->getBaseUrl(true)).('/images/rubrics/'.$main_rubric->filename); ?>" alt="+" title="+">
                            </div>
                            <a href="/cards/index/id/<?php echo $main_rubric->rubricid; ?>">
                                <?php echo $main_rubric->name; ?>
                            </a>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($main_rubric->rubrics as $rubric): ?>
                        <tr>
                            <td class="name">
                                <a href="/cards/index/id/<?php echo $rubric->rubricid; ?>">
                                    <?php echo $rubric->name; ?>
                                </a>
                            </td>
                            <td class="count">
                                <?php
                                $count = Yii::app()->db->createCommand()
                                    ->select('count(*) as value')
                                    ->from('cards c')
                                    ->leftJoin('ref_rubrics_cards r', 'r.cardid=c.cardid')
                                    ->where('c.ispublic=true && r.rubricid=:rubricid', array(':rubricid' => $rubric->rubricid))
                                    ->queryRow();
                                echo $count['value'] > 0 ? $count['value'] : '';
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php $this->renderPartial('/cards/_card'); ?>
<script>
    (function () {
        $(".newscontent").on('click', function (e) {
            console.log(e);
            var type = $(e.currentTarget).attr('data-type');
            var id = $(e.currentTarget).attr('data-id');
            if (type === "news") {
                window.location = "/news/view/id/" + id;
            } else {
                ajaxmodal('general', id);
            }
        })
    })();
</script>

<div id="livezilla_tracking" style="display:none"></div>
<script type="text/javascript">
    var script = document.createElement("script");
    script.async = true;
    script.type = "text/javascript";
    var src = "http://in4smart.azurewebsites.net/livezilla/server.php?a=36ba7&request=track&output=jcrpt&ovlp=MjI_&ovlc=IzczYmUyOA__&ovlct=I2ZmZmZmZg__&eca=MQ__&ecw=Mjg1&ech=OTU_&ecmb=Mjk_&echt=0JXRgdGC0Ywg0LLQvtC,0YDQvtGB0Ys,&echst=0JfQsNC00LDQudGC0LUg0LjRhSDQv9GA0Y,QvNC-INGB0LXQudGH0LDRgSE_&ecoht=0JXRgdGC0Ywg0LLQvtC,0YDQvtGB0Ys,&ecohst=0J3QsNC,0LjRiNC40YLQtSDQvdCw0Lwg0YHQvtC-0LHRidC10L3QuNC1IQ__&ecfs=I0YwRkZENQ__&ecfe=I0QzRjI5OQ__&echc=IzZFQTMwQw__&ecslw=Mg__&ecsgs=IzY1OUYyQQ__&ecsge=IzY1OUYyQQ__&nse=" + Math.random();
    setTimeout("script.src=src;document.getElementById('livezilla_tracking').appendChild(script)", 1);</script>
<noscript><h1><img
            src="http://in4smart.azurewebsites.net/livezilla/server.php?a=36ba7&amp;request=track&amp;output=nojcrpt"
            width="0" height="0" style="visibility:hidden;" alt=""></h1>
</noscript>
