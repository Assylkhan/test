<div class="row">
    <div class="span12">
        <div class="main-row">
            <h1 class="adminh">Новости - <span>Добавить новость</span></h1>
            <a href="#" class="button-style preview hover-green">предпросмотр статьи</a>
        </div>
        <form action="#" method="post">
            <table class="addnew-table">
                <tbody>
                    <tr>
                        <td>
                            <label for="name">Название новости<span class="required">*</span>:</label>
                            <span class="explanation">пояснение к пункту</span>
                        </td>
                        <td>
                            <input name="name" id="name" class="registration-input">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="city">Город<span class="required">*</span>:</label>
                            <span class="explanation">пояснение к пункту</span>
                        </td>
                        <td>
                            <select name="city" id="city" class="registration-select">
                                <option value="1">Город1</option>
                                <option value="2">Город2</option>
                                <option value="3">Город3</option>
                                <option value="4">Город4</option>
                                <option value="5">Город5</option>
                                <option value="6">Город6</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="date1">Дата публикации<span class="required">*</span>:</label>
                            <span class="explanation">пояснение к пункту</span>
                        </td>
                        <td>
                            <input name="date1" id="date1" class="registration-input-date">
                            <input name="date2" id="date2" class="registration-input-date">
                            <select name="status" class="status">
                                <option value="1">опубликовано</option>
                                <option value="2">отключено</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="img">Картинка<span class="required">*</span>:</label>
                            <span class="explanation">пояснение к пункту</span>
                        </td>
                        <td>
                            <input type="file" name="img" id="img">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="description">Описание компании<span class="required">*</span>:</label>
                            <span class="explanation">пояснение к пункту</span>
                        </td>
                        <td>
                            <textarea name="description" id="description" class="registration-textarea"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <textarea name="main-text" class="main-text"></textarea>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="button-row">
                <button class="create-button">создать</button>
                <button class="cancel-button">отмена</button>
            </div>
        </form>
    </div>
</div>