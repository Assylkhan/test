<?php Yii::app()->setLanguage('ru'); ?>
<?php Yii::app()->getClientScript()->registerCoreScript( 'jquery.ui' );?>

<!doctype html>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title></title>
        <link rel="stylesheet" href="/frontend/css/bootstrap.min.css">
        <link rel="stylesheet" href="/frontend/css/bootstrap-responsive.css">
        <link rel="stylesheet" href="/frontend/css/template.css">
        <link rel="stylesheet" href="/frontend/css/modules.css">
        <link rel="stylesheet" href="/frontend/css/screen.css">
		<script src="/frontend/js/site/common.js"></script>
    </head>
    <body>
        <div id="content-block">
            <div class="row header center-content">
                <div class="span12">
                    <div class="span6 p-relative lessspan">
                        <div class="logo">
                            <a href="/">
                                <img src="/images/logo.png" alt="INFOSMART" title="INFOSMART">
                            </a>
                        </div>
                        <div class="citywidget">
                            <div class="cityname">Астана</div>
                            <span class="selectcity">выберите город</span>
                        </div>
                        <div class="infowidget">
                            <span class="weather-day">+18°<span class="dotes">...</span>+20°</span>
                            <span class="weather-night">+4°<span class="dotes">...</span>+6°</span>
                            <span class="dollar">185.5<span class="depreciation"></span></span>
                            <span class="euro">250.2<span class="growth-rate"></span></span>
                            <span class="rub">5.4<span class="depreciation"></span></span>
                            <div class="clr"></div>
                        </div>
                    </div>
                    <div class="span6 morespan">
                        <ul class="mainmenu">
                            <li>
                                <a class="deals hover-green" href="/proposals/index">предложения</a>
                            </li>

                            <li>
                                <a class="news hover-green" href="/news/index">новости</a>
                            </li>
                            <?php
                            if (Yii::app()->user->role == 'moderator') {
                                echo '<li>
                                <a class="personal-area hover-green" href="/moderator/cards">Модерация</a>
                                </li>';

                            } else if (Yii::app()->user->role == 'admin') {
                                echo '<li>
                                <a class="personal-area hover-green" href="/settings">Настройки</a>
                                </li>';
                            }
							else if(Yii::app()->user->role == 'client') {
								echo '<li>
                                <a class="personal-area hover-green" href="/user/index">Личный кабинет</a>
                                </li>';
							}
							else {
                                echo '<li>
                                <a class="personal-area hover-green" href="#" onclick="showLoginWindow();false;">личный кабинет</a>
                                </li>';
                            }
                            ?>


							<?php
							if (!Yii::app()->user->isGuest) {?>
							<li>
								<a class="map" href="/site/logout">Выход</a>
							</li>
							<?php }?>



                        </ul>
                    </div>
                </div>
            </div>
            <div class="row search center-content">
                <div class="span12">
                    <div class="span6 searchdiv">
                        <div class="searchform">
                            <form action="<?php echo Yii::app()->createUrl('search/index'); ?>" method="get">
                                <input type="text" name="search" placeholder="Введите ключевое слово" class="searchinput">
                                <input type="submit" class="searchbutton">

                            </form>
                            <div class="clr"></div>
                        </div>
                    </div>
                    <div class="span6 adddiv">
                        <div class="addblock">
                            <a href="<?php echo Yii::app()->createUrl('site/registration'); ?>">Добавить компанию</a>
                        </div>
                    </div>
                </div>
            </div>

            <?php  echo $content;?>

            <div class="wrap"></div>

        </div>
		<div class="row footer">
			<span class="upbutton">наверх</span>
			<ul class="footermenu">
				<li>
					<a href="../../../frontend/aboutserv.php">О сервисе</a>
				</li>
				<li>
					<a href="../../../frontend/aboutus.php">О нас</a>
				</li>
				<li>
					<a href="../../../frontend/aboutlicense.php">Пользовательское соглашение</a>
				</li>
				<li>
					<a href="../../../frontend/aboutmob.php">Мобильное приложение</a>
				</li>
			</ul>
			<span class="footer-copirights">©2014 ТОО "Smart Selling Group". Контакты для вопросов размещения на сайте:</span>
			<span class="footer-email">email: info@in4smart.kz</span>
			<span class="footer-phone">тел: +7 (7172) 61 62 61</span>

		</div>

        <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
        <script src="/frontend/js/bootstrap.min.js"></script>
        <script src="/frontend/js/bootstrap-hover-dropdown.min.js"></script>
        <script src="/frontend/js/jquery.liquidcarousel.js"></script>
        <script src="/frontend/js/cookie.js"></script>
        <script>
            /* Запускаем сразу после загрузки страницы */
            $( document ).ready(function() {
                newsBlocks();
                // Читаем из куков текущий город
                var currentCity = readCookie("currentCity");
                currentCity = (currentCity != null) ? currentCity : $(".city-ul li").eq(0).text();
                $(".current-city").text(currentCity);

                $('.upbutton').click(function(){
                    $('html, body').animate({scrollTop:0}, 'slow');
                });

                $(".selectcity").click(function(){
                    rotateBG();
                    $(".city-ul").slideToggle();
                });

                $(".city-ul li").click(function(){
                    var selectedCity = $( this).text();
                    $(".current-city").text(selectedCity);
                    // Записываем в куки текущий город
                    writeCookie("currentCity", selectedCity, 7);
                    rotateBG();
                    $(".city-ul").slideToggle();
                });
            });

            function rotateBG() {
                if ( $(".city-arrow").is(".rotate-bg") ) {
                    $(".city-arrow").removeClass("rotate-bg");
                } else {
                    $(".city-arrow").addClass("rotate-bg");
                }
            }

            /* Используем при изменении ширины окна в режиме live */
            $( window ).resize(function() {
                newsBlocks();
            });

            function newsBlocks() {
                var blocks = $(".newsblock");
                var width = window.innerWidth;
                var blockCount = parseInt(width/320);
                // Высчитываем длину блока в процентах
                var newWidth = 100/blockCount;
                // Переводим проценты в пиксели
                var newWidthPX = 1920/100/newWidth;
                // Если длина блока в пикселях менее минимально необходимой,
                // уменьшаем кол-во блоков и пересчитываем проценты
                if (newWidthPX < 320) {
                    blockCount--;
                    newWidth = 100/blockCount;
                }
                // Задаем новую длинну для блоков
                for (var i = 0; i < blockCount; i++) {
                    blocks.eq(i).css("width", newWidth + "%").show();
                }
                // Скрываем лишние
                for (blockCount; blockCount < blocks.length; blockCount++) {
                    blocks.eq(blockCount).hide();
                }
            }
        </script>
    </body>

	<?php $this->widget('application.components.UserLoginWidget'); ?>

</html>
