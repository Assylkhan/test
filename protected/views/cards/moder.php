<span id = 'cardId' style="display: none;"><?php echo $card->cardid;?> </span>

<script src="/frontend/js/moderator/functions.js"></script>
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<!--                <h4 class="modal-title">Modal title</h4>-->
		</div>
		<div class="modal-body">
			<div class="modal-row">
				<div class="span6">
					<ul class="top-nav breadcrumbs">
						<?php	$rubInfo = $card->getFirstSubRubInfo();?>
						<li><a href="#">Главная</a></li>
						<li><a href="#"><?php echo $rubInfo['parent']?></a></li>
						<li><a href="#"><?php echo $rubInfo['sub']?></a></li>
					</ul>
					<h1><?php echo $card->orgNameToString(); ?></h1>
					<a href="#" class="button-style share">поделиться</a>
					<ul class="main-menu">
						<li><a href="#" onclick="ajaxmodal('general',<?php echo $card->cardid;?>); false;">общая информация</a></li>
						<li><a href="#" onclick="ajaxmodal('onmap',<?php echo $card->cardid;?>); false;">на карте</a></li>
						<?php if ($card->haveProposals()) { ?>
							<li><a href="#" onclick="ajaxmodal('deals',<?php echo $card->cardid; ?>); false;">предложения</a>	</li>
						<?php } ?>
						<?php if ($card->haveOrgPhotos()) { ?>
							<li><a href="#" onclick="ajaxmodal('foto',<?php echo $card->cardid; ?>); false;">фото</a>
							</li>
						<?php } ?>
						<li><a href="#" onclick="ajaxmodal('job',<?php echo $card->cardid;?>); false;">вакансии</a></li>
						<li><a href="#" onclick="ajaxmodal('details',<?php echo $card->cardid;?>); false;">реквизиты</a></li>
						<?php if ( Yii::app()->user->role==="moderator" && $card->cardstatusid != 1) {?>
							<li><a href="#" onclick="ajaxmodal('moder',<?php echo $card->cardid;?>); false;" class="active">модерация</a></li>
						<?php }?>
					</ul>
					<div class="span12 moderation">
						<h2>Добавить сообщение </h2><br><br>
						<textarea class="zh_message-body" name="message" id="zh_message"></textarea>
						<br><br>
						<label for="mail">
							<input type="checkbox" name="mail" id="zh_mail">
							копия на почту
						</label>
						<br><br>
                        <label for="payment">
                            <input type="radio" name="payment" id="zh_payment_none" checked>
                            <!--							<input type="checkbox" name="payment" id="zh_payment">-->
                            модерация пройдена
                        </label>
                        <br>
						<label for="payment">
                            <input type="radio" name="payment" id="zh_payment">
<!--							<input type="checkbox" name="payment" id="zh_payment">-->
							оплачено на месте, ввести номер чека
						</label>
                        <br>
						<label for="payment">
                            <input type="radio" name="payment" id="zh_payment2">
<!--							<input type="checkbox" name="payment" id="zh_payment">-->
							оплачено через банк, ввести номер платежа
						</label>
                        <br>
                        <br>
                        <div id="form-pay" style="display: none;">
                            <input type="text" id="checkNumber" class="zh_checkNumber">
                            <br>
                            <br>
                            Сумма   <input type="text" id="checkSum" class="zh_checkNumber" style="width: 100px;">
                            <br>
                            <br>
                            <div id="form-check" style="display: none;">
                                Скан файл квитанции (jpg, pdf, png не больше 2 мб)
                                <form id="frmImport" name="frmImport" method="post" enctype="multipart/form-data" >
                                    <br>
                                    <input id="fileImportData" name="importData" type="file" accept="image/*, application/pdf"/>
                                    <br>
                                </form>
                            </div>
                            <br>
                        </div>
                        <br>


						<div class="zh_button-row">
							<button class="zh_hover-green" onclick="approve(); false;">положительно</button>
							<button class="zh_hover-green" onclick="reject(); false;">отрицательно</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--            <div class="modal-footer"></div>-->
	</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->

<div id="editor">11</div>
