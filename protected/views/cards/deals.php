<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<!--                <h4 class="modal-title">Modal title</h4>-->
		</div>
		<div class="modal-body">
			<div class="modal-row">
				<div class="span6">
					<ul class="top-nav breadcrumbs">
						<?php	$rubInfo = $card->getFirstSubRubInfo();?>
						<li><a href="#">Главная</a></li>
						<li><a href="#"><?php echo $rubInfo['parent']?></a></li>
						<li><a href="#"><?php echo $rubInfo['sub']?></a></li>
					</ul>
					<h1><?php echo $card->orgNameToString(); ?></h1>
					<a href="#" class="button-style share">поделиться</a>
					<ul class="main-menu">
						<li><a href="#" onclick="ajaxmodal('general',<?php echo $card->cardid;?>); false;">общая информация</a></li>
						<li><a href="#" onclick="ajaxmodal('onmap',<?php echo $card->cardid;?>); false;">на карте</a></li>
						<?php if ($card->haveProposals()) { ?>
							<li><a href="#" onclick="ajaxmodal('deals',<?php echo $card->cardid; ?>); false;"class="active">предложения</a>	</li>
						<?php } ?>
						<?php if ($card->haveOrgPhotos()) { ?>
							<li><a href="#" onclick="ajaxmodal('foto',<?php echo $card->cardid; ?>); false;">фото</a>
							</li>
						<?php } ?>
						<li><a href="#" onclick="ajaxmodal('job',<?php echo $card->cardid;?>); false;">вакансии</a></li>
						<li><a href="#" onclick="ajaxmodal('details',<?php echo $card->cardid;?>); false;">реквизиты</a></li>
						<?php if ( Yii::app()->user->role==="moderator" && $card->cardstatusid != 1) {?>
							<li><a href="#" onclick="ajaxmodal('moder',<?php echo $card->cardid;?>); false;">модерация</a></li>
						<?php }?>
					</ul>
					<div class="header-block">

						<?php
						$vacs = $card->getActiveProposals();
						foreach($vacs as $v){
						?>
						<div class="deals-dialog">
                            	<div class="one_deal">
                                	<div class="deals-dialog-description">
                                		<div class="deals-dialog-name"><?php echo $v['name'];?></div>
                                		<div class="deals-dialog-text"><?php echo substr($v['content'],0, 5000);?></div>
                                    </div>
                            		<img class="deals-dialog-img" src="<?php echo $v['image'];?>" />
                                </div>
                        </div>

						<?php }?>
					</div>
				</div>
			</div>
		</div>
		<!--            <div class="modal-footer"></div>-->
	</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
