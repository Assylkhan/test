<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<!--                <h4 class="modal-title">Modal title</h4>-->
		</div>
		<div class="modal-body">
			<div class="modal-row">
				<div class="span6">
					<ul class="top-nav breadcrumbs">
						<?php	$rubInfo = $card->getFirstSubRubInfo();?>
						<li><a href="#">Главная</a></li>
						<li><a href="#"><?php echo $rubInfo['parent']?></a></li>
						<li><a href="#"><?php echo $rubInfo['sub']?></a></li>
					</ul>
					<h1><?php echo $card->orgNameToString(); ?></h1>
					<a href="#" class="button-style share">поделиться</a>
					<ul class="main-menu">
						<li><a href="#" onclick="ajaxmodal('general',<?php echo $card->cardid;?>); false;" class="active">общая информация</a></li>
						<li><a href="#" onclick="ajaxmodal('onmap',<?php echo $card->cardid;?>); false;">на карте</a></li>
						<?php if ($card->haveProposals()) { ?>
							<li><a href="#" onclick="ajaxmodal('deals',<?php echo $card->cardid; ?>); false;">предложения</a>	</li>
						<?php } ?>
						<?php if ($card->haveOrgPhotos()) { ?>
							<li><a href="#" onclick="ajaxmodal('foto',<?php echo $card->cardid; ?>); false;">фото</a>
							</li>
						<?php } ?>
						<li><a href="#" onclick="ajaxmodal('job',<?php echo $card->cardid;?>); false;">вакансии</a></li>
						<li><a href="#" onclick="ajaxmodal('details',<?php echo $card->cardid;?>); false;">реквизиты</a></li>
						<?php if ( Yii::app()->user->role==="moderator" && $card->cardstatusid != 1) {?>
						<li><a href="#" onclick="ajaxmodal('moder',<?php echo $card->cardid;?>); false;">модерация</a></li>
						<?php }?>
					</ul>
					<div class="span4">
						<div class="logo-place">
							<img src="<?php echo Yii::app()->request->getBaseUrl(true).'/images/orglogos/'.$card->getLogoName();?>" alt="<?php echo $card->orgname;?>" title="<?php echo $card->orgname;?>" height="130" width="450">
						</div>
						<div class="content-block">
							<h2>О компании</h2>
							<p>
								<?php echo  CHtml::encode(substr($card->description, 0, 5000).'...'); ?>
							</p>
						</div>
						<div class="content-block">
							<h2>Рубрики</h2>
							<?php foreach($card->getSubRubricsName() as $v) {?>
								<p><?php echo $v['rubric']?>, <?php echo $v['subrubric']?></p>
							<?php }?>

						</div>
						<div class="content-block">
							<h2>Ключевые слова</h2>
							<p>
							<?php
								echo implode(", ", $card->getKeyWords());
							?>
							</p>
						</div>
					</div>
					<div class="span2">
						<div class="content-block">
							<h3>Основной адрес</h3>

							<span class="text-info">Адрес <?php if (!empty($card->cityid))  {echo $card->addressToString();} ?></span>
							<span class="text-info">Режим работы<?php echo $card->workschedule; ?></span>
							<span class="text-info">Сайт <a href="http://<?php echo $card->websiteurl; ?>"> <?php echo $card->websiteurl; ?></a></span>

							<?php foreach($card->getTelephones() as $v) {?>
								<span class="phones2"><?php echo $v; ?> </span>
							<?php }?>

							<?php foreach($card-> getMessengers() as $v) {?>
								<span class="text-info"><?php echo $v; ?></span>
							<?php }?>


						</div>
						<?php if(Yii::app()->user->role==="moderator") {?>
						<div class="content-block">
							<h3>Представители компании</h3>
							<span class="text-info">Ответственный за обновление <?php echo $card->updatecontactname; ?></span>
							<span class="text-info">Телефон <?php echo $card->updatecontactphone; ?></span>
							<span class="text-info">Бухгалтер <?php echo $card->accountantname; ?></span>
							<span class="text-info">Телефон <?php echo $card->accountantphone; ?></span>

						</div>
						<?php }?>

						<div class="content-block">
							<h3>Филиалы</h3>
							<?php foreach($card->getBranchNameAndId() as $v) {?>
								<span class="hrefstyle" onclick="ajaxmodal('general', <?php echo $v['cardid']; ?>); false;"><?php echo $v['name']; ?></span>
							<?php }?>

						</div>
					</div>
				</div>
			</div>
		</div>
		<!--            <div class="modal-footer"></div>-->
	</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
