<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<!--                <h4 class="modal-title">Modal title</h4>-->
		</div>
		<div class="modal-body">
			<div class="modal-row">
				<div class="span6">
					<ul class="top-nav breadcrumbs">
						<?php	$rubInfo = $card->getFirstSubRubInfo();?>
						<li><a href="#">Главная</a></li>
						<li><a href="#"><?php echo $rubInfo['parent']?></a></li>
						<li><a href="#"><?php echo $rubInfo['sub']?></a></li>
					</ul>
					<h1><?php echo $card->orgNameToString(); ?></h1>
					<a href="#" class="button-style share">поделиться</a>
					<ul class="main-menu">
						<li><a href="#" onclick="ajaxmodal('general',<?php echo $card->cardid;?>); false;">общая информация</a></li>
						<li><a href="#" onclick="ajaxmodal('onmap',<?php echo $card->cardid;?>); false;">на карте</a></li>
						<?php if ($card->haveProposals()) { ?>
							<li><a href="#" onclick="ajaxmodal('deals',<?php echo $card->cardid; ?>); false;">предложения</a>	</li>
						<?php } ?>
						<?php if ($card->haveOrgPhotos()) { ?>
							<li><a href="#" onclick="ajaxmodal('foto',<?php echo $card->cardid; ?>); false; " class="active">фото</a>
							</li>
						<?php } ?>
						<li><a href="#" onclick="ajaxmodal('job',<?php echo $card->cardid;?>); false;">вакансии</a></li>
						<li><a href="#" onclick="ajaxmodal('details',<?php echo $card->cardid;?>); false;">реквизиты</a></li>
						<?php if ( Yii::app()->user->role==="moderator" && $card->cardstatusid != 1) {?>
							<li><a href="#" onclick="ajaxmodal('moder',<?php echo $card->cardid;?>); false;">модерация</a></li>
						<?php }?>
					</ul>
					<div id="slider">
						<div id="mySlider1" class="carousel slide" data-ride="carousel" data-interval="false">
							<div class="carousel-inner">

								<?php

								$photos = $card->cardPhotoses;
								//$path = Yii::app()->params['orgPhotosPath'];

								$isFirst = true;

								foreach ($photos as $v) {
									if ($isFirst) {
										echo '<div class="item active">
									<div class="container">
										<img src="' . (Yii::app()->request->getBaseUrl(true)) . ('/images/orgphotos/' . $v->photo) . '">
									</div>
									</div>';
										$isFirst = false;
									} else {
										echo '<div class="item">
									<div class="container">
										<img src="' . (Yii::app()->request->getBaseUrl(true)) . ('/images/orgphotos/' . $v->photo) . '">
									</div>
									</div>';
									}
								}

								?>



							</div>
							<a class="left carousel-control" href="#mySlider1" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
							<a class="right carousel-control" href="#mySlider1" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
							<div id="carousel-index" style="text-align: center;">1/<?php echo count($card->cardPhotoses)?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--            <div class="modal-footer"></div>-->
	</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->

<script>
	// This triggers after each slide change
	$('#mySlider1').on('slid.bs.carousel', function () {
		var carouselData = $(this).data('bs.carousel');
		var currentIndex = carouselData.getActiveIndex();
		var total = carouselData.$items.length;

		// Now display this wherever you want
		var text = (currentIndex + 1) + "/" + total;
		$('#carousel-index').text(text);
	});
</script>

