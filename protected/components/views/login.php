
<div class="modal fade" id="general_modallogin">
	<div class="modal-dialog">>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<!--                <h4 class="modal-title">Modal title</h4>-->
			</div>
			<div class="modal-body">
				<div class="modal-row">
					<div class="span6 marginbottom">
						<!-- Nav tabs -->
						<?php $form=$this->beginWidget('CActiveForm', array(
							'id'=>'login-form',
							'enableClientValidation'=>true,
							//'enableAjaxValidation'  => true,

							'clientOptions'=>array(
								'validateOnSubmit'=>true,
								'afterValidate' => 'js:function(form, data, hasError) {
								auth(form, data, hasError);
							}'
							),
						)); ?>
						<ul class="nav nav-tabs" role="tablist">
							<li class="active"></li>
							<li ><a href="#" role="tab" data-toggle="tab" style="width: 864px;">Авторизация</a></li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">
							<div class="tab-pane active" id="auth">
								<div class="auth-error" id = "auth-error">
									<?php echo $form->error($model,'password'); ?>
									<?php echo $form->error($model,'email'); ?>

								</div>

								<!--<form action="#" method="post">-->
									<div class="reg-form">
										<div class="reg-row">


											<label for="login">Логин (email)</label>
											<?php echo $form->textField($model,'email'); ?>

											<!--<input type="text" name="login" id="login">-->
										</div>
										<div class="reg-row">
											<label for="password">Пароль</label>
											<!--<input type="password" name="password" id="password">-->
											<?php echo $form->passwordField($model,'password'); ?>
										</div>
										<div class="reg-row">
											<a href="#" class="remain-link">забыли пароль?</a>
										</div>
									</div>
								<!--</form>-->



							</div>
							<div class="tab-pane" id="registration">

							</div>
						</div>
						<?php echo CHtml::submitButton('Войти', array('id' => 'login', 'class' => 'big-next-button hover-green')); ?>
						<?php $this->endWidget(); ?>
					</div>
				</div>
			</div>
			<!--            <div class="modal-footer"></div>-->
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
