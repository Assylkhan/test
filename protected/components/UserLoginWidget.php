<?php

class UserLoginWidget extends CWidget
{
	public $title = 'Авторизация';
	public $visible = true;

	public function init()
	{
		if ($this->visible) {

		}
	}

	public function run()
	{
		if ($this->visible) {
			$this->renderContent();
		}
	}

	protected function renderContent()
	{
		$model = new LoginForm;

		//проверка ajax перенесена в site
	/*	// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form')
		{
			$errors = CActiveForm::validate($model);
			if ($errors != '[]')
			{
				echo $errors;
				Yii::app()->end();
			}
		}

		// collect user input data
		if (isset($_POST['LoginForm']))
		{
			$model->attributes = $_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login())
			{
				if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form')
				{
					echo CJSON::encode(array(
						'authenticated' => true,
						'redirectUrl' => Yii::app()->user->returnUrl,
						//"param" => "Any additional param"
					));
					Yii::app()->end();
				}
				$this->redirect(Yii::app()->user->returnUrl);
			}
		}*/
		// display the login form
		$this->render('login', array('model' => $model));

	}
}