<?php


class UserIdentity extends CUserIdentity
{
	protected $_id;

	public function authenticate()
	{

		$record = Users::model()->findByAttributes(array('email' => $this->username));
		//if($record)
		if ($record === null)
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		else {
			if (!$record->validatePassword($this->password))
				$this->errorCode = self::ERROR_PASSWORD_INVALID;
			else {
				$this->_id = $record->userid;

				$this->errorCode = self::ERROR_NONE;

				if($record->userroleid == 3 && !$record->activateddate)//не активирован
				{
					$this->errorCode = self::ERROR_USERNAME_INVALID;
				}
			}
		}

		return !$this->errorCode;
	}

	public function getId()
	{
		return $this->_id;
	}

}