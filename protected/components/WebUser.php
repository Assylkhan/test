<?php

class WebUser extends CWebUser
{
	private $_model = null;

	function getRole()
	{
		if ($user = $this->getModel()) {
			switch ($user->userroleid) {
				case 1:
					return 'admin';
					break;
				case 2:
					return 'moderator';
					break;
				case 3:
					return 'client';
					break;
				default:
					return 'guest';
					break;
			}
		}
	}

	private function getModel()
	{
		if (!$this->isGuest && $this->_model === null) {
			$this->_model = Users::model()->findByPk($this->id);
		}
		return $this->_model;
	}
}