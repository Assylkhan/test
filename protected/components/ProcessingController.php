<?php
/**
 * Created by PhpStorm.
 * User: Alexandr
 * Date: 17.10.2014
 * Time: 0:43
 */

class ProcessingController extends Controller {
    /**
     * Переопределяем для того,
     * чтобы получить ответ
     * из процессинговой системы
     */
    public function missingAction($actionID)
    {;
        $kostyl = $_SERVER["REQUEST_URI"];
        if (strpos($kostyl, "&customerReference=") !== false) {
            $kostyl = str_replace(array('&', '='), '/', $kostyl);
            header("Location: " . $kostyl);
        } else {
            throw new CHttpException(404,Yii::t('yii','The system is unable to find the requested action "{action}".',
                array('{action}'=>$actionID==''?$this->defaultAction:$actionID)));
        }
    }
} 