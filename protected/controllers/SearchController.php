<?php

class SearchController extends Controller  {

	public $layout = '//layouts/main';
	public function actionIndex()
	{
        if(isset($_GET['search']))
        {
            $search = $_GET['search'];
            $sSql = 'select * from bcatalog_cards where match('.Yii::app()->sphinx->quoteValue($search).')';

            $ids = Yii::app()->sphinx->createCommand($sSql)->queryColumn();

            $criteria=new CDbCriteria();
            $criteria->addInCondition('cardid',$ids);

            $total = count($ids);
            $pages = new CPagination($total);
            $pages->pageSize = 20;
            $pages->applyLimit($criteria);

            $cards = Cards::model()->findAll($criteria);
            shuffle($cards);
            $rescards = array();
            foreach($cards as $card) {
                $p_card = $card->getPrivateCard();
                if ($p_card) {
                    $check = $p_card->isBasePackageRun();
                    if($check){
                        array_push($rescards, $card);
                    }
                }
            }
            shuffle($rescards);

            $topcards = array();
            foreach($rescards as $card) {
                $p_card = $card->getPrivateCard();
                if ($p_card) {
                    $check = $p_card->isBasePackageRun() && $p_card->isPackageRun('3');
                    if ($check) {
                        array_push($topcards, $card);
                    }
                }
                if (count($topcards) >= 4) {
                    break;
                }
            }

            $this->render('index', array(
                'cards'=>$rescards,
                'top' => $topcards,
                'pages' => $pages,
                'total' => $total,
                'search' => $search
            ));
        } else{
            $this->redirect(array('index'));
        }


    }
}