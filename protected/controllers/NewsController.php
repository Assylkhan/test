<?php

class NewsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('admin','create', 'update', 'delete'),
				'expression' => 'Yii::app()->user->role==="moderator"'
			),
			array(
				'allow',
				'actions'=>array('index', 'view'),
				'users' => array('*'),
			),
			array(
				'deny',
				'users' => array('*'),
			)
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->layout='/layouts/main';
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new News;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['News']))
		{
            $folder=Yii::getPathOfAlias('webroot').'/images/news/';// folder for uploaded files

            $old=$model->filename;
            $model->attributes=$_POST['News'];

            $name       = $_FILES['News']['name']['image'];
            $filename   = pathinfo($name, PATHINFO_FILENAME);
            $ext        = pathinfo($name, PATHINFO_EXTENSION);

//            $newName        = $model->createdate."-".$filename.'.'.$ext;
            $newName        = $this->guid().'.'.$ext;

            $model->image = CUploadedFile::getInstance($model,'image');

            $fullImgSource = Yii::app()->params['uploadNewsImagesPath'].$newName;

            if($model->image===null||$model->image===''){
                $model->filename = $old;
            } else{
                $model->filename = $newName;
            }
            if($model->save()){
                if(!is_dir($folder)){
                    mkdir($folder);
                }
                if($model->image!==null&&$model->image!==''){
                    $model->image->saveAs($fullImgSource);
                }
                $this->redirect(array('view','id'=>$model->newsid));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));

    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['News']))
		{
            $folder=Yii::getPathOfAlias('webroot').'/images/news/';// folder for uploaded files

            $old=$model->filename;
            $model->attributes=$_POST['News'];

            $name       = $_FILES['News']['name']['image'];
            $filename   = pathinfo($name, PATHINFO_FILENAME);
            $ext        = pathinfo($name, PATHINFO_EXTENSION);

//            $newName        = $model->createdate."-".$filename.'.'.$ext;
            $newName        = $this->guid().'.'.$ext;

            $model->image = CUploadedFile::getInstance($model,'image');

            $fullImgSource = Yii::app()->params['uploadNewsImagesPath'].$newName;

            if($model->image===null||$model->image===''){
                $model->filename = $old;
            } else{
                $model->filename = $newName;
            }
            if($model->save()){
                if(!is_dir($folder)){
                    mkdir($folder);
                }
                if($model->image!==null&&$model->image!==''){
                    $model->image->saveAs($fullImgSource);
                }
                $this->redirect(array('view','id'=>$model->newsid));
            }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $this->layout='/layouts/main';
        $criteria = new CDbCriteria;
        $total = News::model()->count();

        $pages = new CPagination($total);
        $pages->pageSize = 20;
        $pages->applyLimit($criteria);

        $news = News::model()->findAll($criteria);

        $this->render('index', array(
            'news' => $news,
            'pages' => $pages,
        ));

//		$dataProvider=new CActiveDataProvider('News');
//		$this->render('index',array(
//			'dataProvider'=>$dataProvider,
//		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new News('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['News']))
			$model->attributes=$_GET['News'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return News the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=News::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param News $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='news-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    function guid(){
        if (function_exists('uuid_create')){
            $res = uuid_create();
            str_replace('}', '', $res);
            str_replace('{', '', $res);
            return $res;
        }else{
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = // "{"
                substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12);// "}"
            return $uuid;
        }
    }
}
