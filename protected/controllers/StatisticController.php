<?php

class StatisticController extends Controller {

	public function actionIndex()
	{
		$this->render('index');
	}

    public function actionPdf(){
        $var1="text";
        $bik = '1234567890';

        if(isset($_POST['var1'])) $var1=$_POST['var1'];

        $html = '
                <!doctype html>
                <html>
                    <body>
                        <div style="padding-left:150px;">
                            Внимание! Оплата данного счета означает согласие с условиями поставки товара. Уведомление об оплате обязательно, в противном случае не гарантируется наличие товара на складе. Товар отпускается по факту  прихода денег на р/с Поставщика, самовывозом, при наличии доверенности и документов удостоверяющих личность.
                        </div>
                        <br>
                        <div style="padding-left:50px; padding-right:50px;">
                        <table border="1">
                            <tr>
                                <td>Бенефициар:

                                Товарищество с ограниченной ответственностью "New ServiceArt"
                                БИН: 070540002006</td>
                                <td>ИИК
                                KZ689261501164206000</td>
                                <td>Кбе
                                17</td>
                            </tr>

                            <tr>
                                <td>Банк бенефициара:
                                АО Казкоммерцбанк</td>
                                <td>БИК</td>
                                <td>Код назначения платежа
                                859</td>
                            </tr>
                        </table>
                        <h1><u>Счет на оплату № 00000000002 от 6 июня 2014 г.</u></h1>

                        <table style="width:700px;">
                            <tr>
                                <td>Поставщик:</td>
                                <td> БИН / ИИН 070540002006,Товарищество с ограниченной ответственностью "New ServiceArt",Республика Казахстан, Сарыаркинский, Астана, Желтоксан, дом № 30\2, к.34</td>>
                            </tr> <br>
                            <tr>
                                <td>Покупатель:</td>
                                <td> БИН / ИИН 000740000728,Акционерное общество  "Национальные информационные технологии",010000, Республика Казахстан, г.Астана, ул.Орынбор,, дом № 8, корпус 16В</td>
                            </tr>
                            <tr>
                                <td>Договор:</td>
                                <td>Договор №2357 от 05.06.2014 г.</td>
                            </tr>
                        </table> <br>

                        <table border="1">
                            <tr>
                                <td>№</td>
                                <td>Наименование</td>
                                <td>Кол-во</td>
                                <td>Ед.</td>
                                <td>Цена</td>
                                <td>Сумма</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Услуги по сбору и анализу данных об элементах инфраструктуры ИК-инфраструктуры и формирование планов перехода на сервисную (аутсорсинговую) модель информатизации для ГО.</td>
                                <td>1</td>
                                <td>шт</td>
                                <td>6,024,000.00</td>
                                <td>6,024,000.00</td>
                            </tr>
                        </table><br>
                        <div style="text-align:right; padding-right:100px;">
                        Итого: <br>
                        Без налога (НДС)
                        </div> <br>
                        Всего наименований 1, на сумму 6 024 000,00 KZT <br>
                        Всего к оплате: Шесть миллионов двадцать четыре тысячи тенге 00 тиын<br><br>

                        Исполнитель ___________________________________ /Бухгалтер/


                        </div>
                    </body>
                </html>';

        $mpdf=Yii::app()->ePdf->mpdf();

        $mpdf->SetDisplayMode('fullpage');

        $mpdf->WriteHTML($html);
        $mpdf->Output('invoice.pdf', 'I');
        exit;
    }

    public function actionPdfTest(){
        $html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META http-equiv="X-UA-Compatible" content="IE=8">
<TITLE>Created by BCL easyConverter SDK 3 (HTML Version)</TITLE>
<META name="generator" content="BCL easyConverter SDK 3.0.60">
<STYLE type="text/css">

body {margin-top: 0px;margin-left: 0px;}

#page_1 {position:relative; overflow: hidden;margin: 95px 0px 305px 95px;padding: 0px;border: none;width: 736px;}

#page_1 #dimg1 {position:absolute;top:0px;left:0px;z-index:-1;width:638px;height:656px;}
#page_1 #dimg1 #img1 {width:638px;height:656px;}




.dclr {clear:both;float:none;height:1px;margin:0px;padding:0px;overflow:hidden;}

.ft0{font: 11px "Arial";line-height: 14px;}
.ft1{font: 11px "Gabriola";line-height: 18px;}
.ft2{font: bold 12px "Arial";line-height: 15px;}
.ft3{font: 1px "Arial";line-height: 1px;}
.ft4{font: bold 11px "Arial";line-height: 14px;}
.ft5{font: 12px "Gabriola";line-height: 15px;}
.ft6{font: 12px "Gabriola";line-height: 17px;}
.ft7{font: 12px "Arial";line-height: 15px;}
.ft8{font: 13px "Gabriola";line-height: 23px;}
.ft9{font: 13px "Arial";line-height: 16px;}
.ft10{font: bold 13px "Arial";line-height: 16px;}
.ft11{font: 11px "Gabriola";line-height: 16px;}
.ft12{font: 1px "Arial";line-height: 13px;}
.ft13{font: 11px "Arial";line-height: 13px;}
.ft14{font: 11px "Gabriola";line-height: 13px;}
.ft15{font: 1px "Arial";line-height: 12px;}
.ft16{font: 11px "Arial";line-height: 12px;}
.ft17{font: 11px "Gabriola";line-height: 12px;}
.ft18{font: 11px "Gabriola";line-height: 14px;}
.ft19{font: 1px "Arial";line-height: 5px;}
.ft20{font: 1px "Arial";line-height: 6px;}
.ft21{font: 1px "Arial";line-height: 11px;}

.p0{text-align: left;padding-left: 108px;padding-right: 105px;margin-top: 21px;margin-bottom: 0px;}
.p1{text-align: left;padding-left: 4px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p2{text-align: left;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p3{text-align: center;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p4{text-align: right;padding-right: 47px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p5{text-align: right;padding-right: 44px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p6{text-align: left;padding-left: 14px;margin-top: 84px;margin-bottom: 0px;}
.p7{text-align: left;padding-left: 89px;margin-top: 3px;margin-bottom: 0px;}
.p8{text-align: left;padding-left: 14px;margin-top: 25px;margin-bottom: 0px;}
.p9{text-align: left;padding-left: 89px;margin-top: 4px;margin-bottom: 0px;}
.p10{text-align: left;padding-left: 14px;margin-top: 32px;margin-bottom: 0px;}
.p11{text-align: right;padding-right: 15px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p12{text-align: left;padding-left: 2px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p13{text-align: right;padding-right: 3px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p14{text-align: right;padding-right: 5px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p15{text-align: right;padding-right: 4px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p16{text-align: left;padding-left: 1px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p17{text-align: left;padding-left: 3px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}

.td0{border-left: #515151 1px solid;border-right: #515151 1px solid;border-top: #515151 1px solid;padding: 0px;margin: 0px;width: 347px;vertical-align: bottom;}
.td1{border-right: #515151 1px solid;border-top: #515151 1px solid;padding: 0px;margin: 0px;width: 158px;vertical-align: bottom;}
.td2{border-right: #515151 1px solid;border-top: #515151 1px solid;padding: 0px;margin: 0px;width: 107px;vertical-align: bottom;}
.td3{border-left: #515151 1px solid;border-right: #515151 1px solid;padding: 0px;margin: 0px;width: 347px;vertical-align: bottom;}
.td4{border-right: #515151 1px solid;padding: 0px;margin: 0px;width: 158px;vertical-align: bottom;}
.td5{border-right: #515151 1px solid;padding: 0px;margin: 0px;width: 107px;vertical-align: bottom;}
.td6{border-left: #515151 1px solid;border-right: #515151 1px solid;border-bottom: #515151 1px solid;padding: 0px;margin: 0px;width: 347px;vertical-align: bottom;}
.td7{border-right: #515151 1px solid;border-bottom: #515151 1px solid;padding: 0px;margin: 0px;width: 158px;vertical-align: bottom;}
.td8{border-right: #515151 1px solid;border-bottom: #515151 1px solid;padding: 0px;margin: 0px;width: 107px;vertical-align: bottom;}
.td9{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 2px;vertical-align: bottom;background: #000000;}
.td10{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 36px;vertical-align: bottom;}
.td11{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 280px;vertical-align: bottom;}
.td12{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 58px;vertical-align: bottom;}
.td13{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 43px;vertical-align: bottom;}
.td14{border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 28px;vertical-align: bottom;}
.td15{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 67px;vertical-align: bottom;}
.td16{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 97px;vertical-align: bottom;}
.td17{padding: 0px;margin: 0px;width: 10px;vertical-align: bottom;}
.td18{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 2px;vertical-align: bottom;background: #000000;}
.td19{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 36px;vertical-align: bottom;}
.td20{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 280px;vertical-align: bottom;}
.td21{padding: 0px;margin: 0px;width: 47px;vertical-align: bottom;}
.td22{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 11px;vertical-align: bottom;}
.td23{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 43px;vertical-align: bottom;}
.td24{padding: 0px;margin: 0px;width: 28px;vertical-align: bottom;}
.td25{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 67px;vertical-align: bottom;}
.td26{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 97px;vertical-align: bottom;}
.td27{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 2px;vertical-align: bottom;background: #000000;}
.td28{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 36px;vertical-align: bottom;}
.td29{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 55px;vertical-align: bottom;}
.td30{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 225px;vertical-align: bottom;}
.td31{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 47px;vertical-align: bottom;}
.td32{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 11px;vertical-align: bottom;}
.td33{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 43px;vertical-align: bottom;}
.td34{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 28px;vertical-align: bottom;}
.td35{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 67px;vertical-align: bottom;}
.td36{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 97px;vertical-align: bottom;}
.td37{padding: 0px;margin: 0px;width: 3px;vertical-align: bottom;}
.td38{padding: 0px;margin: 0px;width: 37px;vertical-align: bottom;}
.td39{padding: 0px;margin: 0px;width: 55px;vertical-align: bottom;}
.td40{padding: 0px;margin: 0px;width: 226px;vertical-align: bottom;}
.td41{padding: 0px;margin: 0px;width: 12px;vertical-align: bottom;}
.td42{padding: 0px;margin: 0px;width: 44px;vertical-align: bottom;}
.td43{padding: 0px;margin: 0px;width: 68px;vertical-align: bottom;}
.td44{padding: 0px;margin: 0px;width: 98px;vertical-align: bottom;}
.td45{padding: 0px;margin: 0px;width: 140px;vertical-align: bottom;}
.td46{padding: 0px;margin: 0px;width: 449px;vertical-align: bottom;}
.td47{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 92px;vertical-align: bottom;}
.td48{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 226px;vertical-align: bottom;}
.td49{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 84px;vertical-align: bottom;}
.td50{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 68px;vertical-align: bottom;}
.td51{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 98px;vertical-align: bottom;}
.td52{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 10px;vertical-align: bottom;}
.td53{padding: 0px;margin: 0px;width: 92px;vertical-align: bottom;}
.td54{padding: 0px;margin: 0px;width: 84px;vertical-align: bottom;}

.tr0{height: 19px;}
.tr1{height: 18px;}
.tr2{height: 17px;}
.tr3{height: 15px;}
.tr4{height: 20px;}
.tr5{height: 21px;}
.tr6{height: 13px;}
.tr7{height: 12px;}
.tr8{height: 14px;}
.tr9{height: 5px;}
.tr10{height: 6px;}
.tr11{height: 27px;}
.tr12{height: 34px;}
.tr13{height: 11px;}
.tr14{height: 30px;}
.tr15{height: 29px;}

.t0{width: 615px;margin-left: 11px;margin-top: 72px;font: bold 12px "Arial";}
.t1{width: 628px;margin-left: 10px;margin-top: 15px;font: 11px "Arial";}

</STYLE>
</HEAD>

<BODY>
<DIV id="page_1">


<DIV class="dclr"></DIV>
<P class="p0 ft1">Внимание<SPAN class="ft0">! </SPAN>Оплата данного счета означает согласие с условиями поставки товара<SPAN class="ft0">. </SPAN>Уведомление об оплате обязательно<SPAN class="ft0">, </SPAN>в противном случае не гарантируется наличие товара на складе<SPAN class="ft0">. </SPAN>Товар отпускается по факту прихода денег на р<SPAN class="ft0">/</SPAN>с Поставщика<SPAN class="ft0">, </SPAN>самовывозом<SPAN class="ft0">, </SPAN>при наличии доверенности и документов удостоверяющих личность<SPAN class="ft0">.</SPAN></P>
<TABLE cellpadding=0 cellspacing=0 class="t0">
<TR>
	<TD class="tr0 td0"><P class="p1 ft2">Товарищество с ограниченной ответственностью "New</P></TD>
	<TD class="tr0 td1"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr0 td2"><P class="p2 ft3">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr1 td3"><P class="p1 ft2">ServiceArt"</P></TD>
	<TD class="tr1 td4"><P class="p3 ft4">KZ689261501164206000</P></TD>
	<TD class="tr1 td5"><P class="p4 ft2">17</P></TD>
</TR>
<TR>
	<TD class="tr2 td6"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr2 td7"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr2 td8"><P class="p2 ft3">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr3 td6"><P class="p1 ft5">АО Казкоммерцбанк</P></TD>
	<TD class="tr3 td7"><P class="p3 ft2">KZKOKZKZ</P></TD>
	<TD class="tr3 td8"><P class="p5 ft2">859</P></TD>
</TR>
</TABLE>
<P class="p6 ft2"><SPAN class="ft6">Поставщик</SPAN><SPAN class="ft7">: </SPAN>БИН / ИИН 070540002006,Товарищество с ограниченной ответственностью "New</P>
<P class="p7 ft2">ServiceArt",Республика Казахстан, Сарыаркинский, Астана, Желтоксан, дом № 30\2, к.34</P>
<P class="p8 ft2"><SPAN class="ft6">Покупатель</SPAN><SPAN class="ft7">: </SPAN>БИН / ИИН 000740000728,Акционерное общество "Национальные информационные</P>
<P class="p9 ft2">технологии",010000, Республика Казахстан, г.Астана, ул.Орынбор,, дом № 8, корпус 16В</P>
<P class="p10 ft10"><SPAN class="ft8">Договор</SPAN><SPAN class="ft9">: </SPAN>Договор №2357 от 05.06.2014 г.</P>
<TABLE cellpadding=0 cellspacing=0 class="t1">
<TR>
	<TD class="tr4 td9"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr4 td10"><P class="p11 ft0">1</P></TD>
	<TD colspan=2 class="tr4 td11"><P class="p12 ft11">Услуги по сбору и анализу данных об элементах</P></TD>
	<TD colspan=2 class="tr4 td12"><P class="p13 ft0">1,000</P></TD>
	<TD class="tr4 td13"><P class="p12 ft11">шт</P></TD>
	<TD class="tr4 td14"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr4 td15"><P class="p13 ft0">6 024 000,00</P></TD>
	<TD class="tr4 td16"><P class="p14 ft0">6 024 000,00</P></TD>
	<TD class="tr5 td17"><P class="p2 ft3">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr6 td18"><P class="p2 ft12">&nbsp;</P></TD>
	<TD class="tr6 td19"><P class="p2 ft12">&nbsp;</P></TD>
	<TD colspan=2 class="tr6 td20"><P class="p12 ft14">инфраструктуры <NOBR>ИК<SPAN class="ft13">-</SPAN>инфраструктуры</NOBR> и формирование</P></TD>
	<TD class="tr6 td21"><P class="p2 ft12">&nbsp;</P></TD>
	<TD class="tr6 td22"><P class="p2 ft12">&nbsp;</P></TD>
	<TD class="tr6 td23"><P class="p2 ft12">&nbsp;</P></TD>
	<TD class="tr6 td24"><P class="p2 ft12">&nbsp;</P></TD>
	<TD class="tr6 td25"><P class="p2 ft12">&nbsp;</P></TD>
	<TD class="tr6 td26"><P class="p2 ft12">&nbsp;</P></TD>
	<TD class="tr6 td17"><P class="p2 ft12">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr7 td18"><P class="p2 ft15">&nbsp;</P></TD>
	<TD class="tr7 td19"><P class="p2 ft15">&nbsp;</P></TD>
	<TD colspan=2 class="tr7 td20"><P class="p12 ft17">планов перехода на сервисную <SPAN class="ft16">(</SPAN>аутсорсинговую<SPAN class="ft16">)</SPAN></P></TD>
	<TD class="tr7 td21"><P class="p2 ft15">&nbsp;</P></TD>
	<TD class="tr7 td22"><P class="p2 ft15">&nbsp;</P></TD>
	<TD class="tr7 td23"><P class="p2 ft15">&nbsp;</P></TD>
	<TD class="tr7 td24"><P class="p2 ft15">&nbsp;</P></TD>
	<TD class="tr7 td25"><P class="p2 ft15">&nbsp;</P></TD>
	<TD class="tr7 td26"><P class="p2 ft15">&nbsp;</P></TD>
	<TD class="tr7 td17"><P class="p2 ft15">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr8 td18"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr8 td19"><P class="p2 ft3">&nbsp;</P></TD>
	<TD colspan=2 class="tr8 td20"><P class="p12 ft18">модель информатизации для ГО<SPAN class="ft0">.</SPAN></P></TD>
	<TD class="tr8 td21"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr8 td22"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr8 td23"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr8 td24"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr8 td25"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr8 td26"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr8 td17"><P class="p2 ft3">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr9 td27"><P class="p2 ft19">&nbsp;</P></TD>
	<TD class="tr9 td28"><P class="p2 ft19">&nbsp;</P></TD>
	<TD class="tr9 td29"><P class="p2 ft19">&nbsp;</P></TD>
	<TD class="tr9 td30"><P class="p2 ft19">&nbsp;</P></TD>
	<TD class="tr9 td31"><P class="p2 ft19">&nbsp;</P></TD>
	<TD class="tr9 td32"><P class="p2 ft19">&nbsp;</P></TD>
	<TD class="tr9 td33"><P class="p2 ft19">&nbsp;</P></TD>
	<TD class="tr9 td34"><P class="p2 ft19">&nbsp;</P></TD>
	<TD class="tr9 td35"><P class="p2 ft19">&nbsp;</P></TD>
	<TD class="tr9 td36"><P class="p2 ft19">&nbsp;</P></TD>
	<TD class="tr10 td17"><P class="p2 ft20">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr11 td37"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr11 td38"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr11 td39"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr11 td40"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr11 td21"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr11 td41"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr11 td42"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr11 td24"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr11 td43"><P class="p15 ft2">Итого:</P></TD>
	<TD class="tr11 td44"><P class="p15 ft2">6 024 000,00</P></TD>
	<TD class="tr11 td17"><P class="p2 ft3">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr1 td37"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr1 td38"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr1 td39"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr1 td40"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr1 td21"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr1 td41"><P class="p2 ft3">&nbsp;</P></TD>
	<TD colspan=3 class="tr1 td45"><P class="p15 ft2">Без налога (НДС)</P></TD>
	<TD class="tr1 td44"><P class="p15 ft2">-</P></TD>
	<TD class="tr1 td17"><P class="p2 ft3">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr12 td37"><P class="p2 ft3">&nbsp;</P></TD>
	<TD colspan=7 class="tr12 td46"><P class="p16 ft2">Всего к оплате: Шесть миллионов двадцать четыре тысячи тенге 00 тиын</P></TD>
	<TD class="tr12 td43"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr12 td44"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr12 td17"><P class="p2 ft3">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr7 td37"><P class="p2 ft15">&nbsp;</P></TD>
	<TD colspan=2 class="tr13 td47"><P class="p2 ft21">&nbsp;</P></TD>
	<TD class="tr13 td48"><P class="p2 ft21">&nbsp;</P></TD>
	<TD class="tr13 td31"><P class="p2 ft21">&nbsp;</P></TD>
	<TD colspan=3 class="tr13 td49"><P class="p2 ft21">&nbsp;</P></TD>
	<TD class="tr13 td50"><P class="p2 ft21">&nbsp;</P></TD>
	<TD class="tr13 td51"><P class="p2 ft21">&nbsp;</P></TD>
	<TD class="tr13 td52"><P class="p2 ft21">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr14 td37"><P class="p2 ft3">&nbsp;</P></TD>
	<TD colspan=2 class="tr14 td53"><P class="p16 ft2">Исполнитель</P></TD>
	<TD class="tr15 td48"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr15 td31"><P class="p2 ft3">&nbsp;</P></TD>
	<TD colspan=3 class="tr14 td54"><P class="p17 ft1"><SPAN class="ft0">/</SPAN>Бухгалтер<SPAN class="ft0">/</SPAN></P></TD>
	<TD class="tr14 td43"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr14 td44"><P class="p2 ft3">&nbsp;</P></TD>
	<TD class="tr14 td17"><P class="p2 ft3">&nbsp;</P></TD>
</TR>
</TABLE>
</DIV>
</BODY>
</HTML>
';


//==============================================================
//==============================================================
//==============================================================

        $mpdf=Yii::app()->ePdf->mpdf();

        $mpdf->SetDisplayMode('fullpage');

        $mpdf->WriteHTML($html);
        $mpdf->Output('invoice.pdf', 'I');
        exit;
    }
}