<?php

class CardsController extends Controller {

    public function actionIndex($id)
    {
        $this->layout='/layouts/main';
        $rubric = Rubrics::model()->findByAttributes(array('rubricid'=>$id));
        if($rubric&&$rubric->rubrictypeid == 2){
            $cardids = Yii::app()->db->createCommand()
                ->select('c.cardid')
                ->from('cards c')
                ->leftJoin('ref_rubrics_cards r', 'r.cardid=c.cardid')
                ->where('c.ispublic=true && r.rubricid=:rubricid', array(':rubricid'=>$rubric->rubricid))
                ->queryAll();
            $rescardids = array();
            foreach($cardids as $it){
                array_push($rescardids, $it['cardid']);
            }
            $cards = Cards::model()->findAllByPk($rescardids);
            shuffle($cards);

            $rescards = array();
            foreach($cards as $card) {
                $p_card = $card->getPrivateCard();
                if ($p_card) {
                    $check = $p_card->isBasePackageRun();
                    if($check){
                        array_push($rescards, $card);
                    }
                }
            }
            shuffle($rescards);

            $topcards = array();
            foreach($rescards as $card) {
                $p_card = $card->getPrivateCard();
                if ($p_card) {
                    $check = $p_card->isBasePackageRun() && $p_card->isPackageRun('3');
                    if ($check) {
                        array_push($topcards, $card);
                    }
                }
                if (count($topcards) >= 4) {
                    break;
                }
            }

            $this->render('index', array(
                'cards' => $rescards,
                'top' => $topcards,
                'rubric' => $rubric
            ));
        } else {
            $cardids = Yii::app()->db->createCommand()
                ->select('c.cardid')
                ->from('cards c')
                ->leftJoin('ref_rubrics_cards r', 'r.cardid=c.cardid')
                ->leftJoin('rubrics rb', 'rb.rubricid=r.rubricid')
                ->leftJoin('rubrics mrb', 'mrb.rubricid=rb.parent_rubricid')
                ->where('c.ispublic=true && mrb.rubricid=:rubricid', array(':rubricid'=>$rubric->rubricid))
                ->queryAll();
            $rescardids = array();
            foreach($cardids as $it){
                array_push($rescardids, $it['cardid']);
            }
            $cards = Cards::model()->findAllByPk($rescardids);
            shuffle($cards);
            $rescards = array();
            foreach($cards as $card) {
                $p_card = $card->getPrivateCard();
                if ($p_card) {
                    $check = $p_card->isBasePackageRun();
                    if($check){
                        array_push($rescards, $card);
                    }
                }
            }
            shuffle($rescards);

            $topcards = array();
            foreach($rescards as $card){
                $p_card = $card->getPrivateCard();
                if ($p_card) {
                    $check = $p_card->isBasePackageRun() && $p_card->isPackageRun('3');
                    if($check){
                        array_push($topcards, $card);
                    }
                }
                if(count($topcards) >= 4){
                    break;
                }
            }

            $this->render('index', array(
                'cards' => $rescards,
                'top' => $topcards,
                'rubric' => $rubric
            ));
        }

//        $this->render('index', array('cards'=>$cards))
    }

	public function actionGetCard()
	{
		if (Yii::app()->request->isAjaxRequest) {

			if(isset($_POST['pageId']) && isset($_POST['cardId']))
			{
				$cardId = $_POST['cardId'];
				$model = Cards::model()->with('telephones')->findByPk($cardId);

				switch($_POST['pageId'] ){
					case 'general':
                        $this->hitCard($_POST['cardId']);
						$body = $this->renderPartial('general', array(
							'card'=> $model
						), true, true);
						break;
					case 'onmap':
						$body = $this->renderPartial('onmap', array(
							'card'=> $model
						), true, true);
						break;
					case 'deals':
						$body = $this->renderPartial('deals', array(
							'card'=> $model
						), true, true);
						break;
					case 'foto':
						$body = $this->renderPartial('foto', array(
							'card'=> $model
						), true, true);
						break;
					case 'job':
						$body = $this->renderPartial('job', array(
							'card'=> $model
						), true, true);
						break;
					case 'details':
						$body = $this->renderPartial('details', array(
							'card'=> $model
						), true, true);
						break;
					case 'moder':
						$body = $this->renderPartial('moder', array(
							'card'=> $model
						), true, true);
						break;
				}
			}
			else
			{
				$body = 'pageId or cardId is null';
			}

			echo CJSON::encode(array(
				'header' => 'success',
				'body' => $body,
			));
			exit;
		} else
			throw new CHttpException('403', 'Forbidden access.');
	}


    public function actionGetTempCard()
    {
        if (Yii::app()->request->isAjaxRequest) {
            if(isset($_POST['pageId']) && isset($_POST['card']))
            {
                $model = $_POST['card'];

                switch($_POST['pageId'] ){
                    case 'general':
                        $body = $this->renderPartial('general', array(
                            'card'=> $model
                        ), true, true);
                        break;
                    case 'onmap':
                        $body = $this->renderPartial('onmap', array(
                            'card'=> $model
                        ), true, true);
                        break;
                    case 'deals':
                        $body = $this->renderPartial('deals', array(
                            'card'=> $model
                        ), true, true);
                        break;
                    case 'foto':
                        $body = $this->renderPartial('foto', array(
                            'card'=> $model
                        ), true, true);
                        break;
                    case 'job':
                        $body = $this->renderPartial('job', array(
                            'card'=> $model
                        ), true, true);
                        break;
                    case 'details':
                        $body = $this->renderPartial('details', array(
                            'card'=> $model
                        ), true, true);
                        break;
                    case 'moder':
                        $body = $this->renderPartial('moder', array(
                            'card'=> $model
                        ), true, true);
                        break;
                }
            }
            else
            {
                $body = 'pageId or cardId is null';
            }

            echo CJSON::encode(array(
                'header' => 'success',
                'body' => $body,
            ));
            exit;
        } else
            throw new CHttpException('403', 'Forbidden access.');
    }


    public function hitCard($id){
        date_default_timezone_set('UTC');
        $date = new DateTime();
        if($id){
            $card = Cards::model()->findByPk($id);
            if($card->ispublic) {
                $st = Statistics::model()->findByAttributes(array('cardid' => $id, 'date' => $date->format('d/m/y'), 'type' => 1));
                if ($st == null) {
                    $st = new Statistics();
                    $st->cardid = $id;
                    $st->date = $date->format('d/m/Y');
                    $st->type = 1;//desktop
                }
                $st->count++;
                $st->save();
            }
        }
    }

    public function actionGetStatistics(){
        if(isset($_POST['cardId'])){
            $id = $_POST['cardId'];
            if($id){
                $stsdesk = Statistics::model()->findAllByAttributes(array('cardid'=>$id, 'type'=>1));
                $stsmobi = Statistics::model()->findAllByAttributes(array('cardid'=>$id, 'type'=>2));
                $this->sendData(array('desktop'=>$stsdesk, 'mobile'=>$stsmobi));
            }
        }
        $this->sendErrorResponse('Статистика временно не работает!');
    }
}