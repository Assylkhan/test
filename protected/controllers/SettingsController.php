<?php

class SettingsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout='//layouts/admin';

	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'expression' => 'Yii::app()->user->role==="moderator"'
			),
			array(
				'deny',
				'users' => array('*'),
			)
		);
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $rubrics=new Rubrics;
        $cities=new Cities;
        $regions=new Regions();
        $imessengertypes=new ImessengerTypes();

        if(isset($_POST['Rubrics']))
        {
            $rubrics->attributes=$_POST['Rubrics'];
            if($rubrics->save())
                $this->refresh();
        }
        if(isset($_POST['Regions']))
        {
            $regions->attributes=$_POST['Regions'];
            if($regions->save())
                $this->refresh();
        }
        if(isset($_POST['Cities']))
        {
            $cities->attributes=$_POST['Cities'];
            if($cities->save())
                $this->refresh();
        }
        if(isset($_POST['ImessengerTypes']))
        {
            $imessengertypes->attributes=$_POST['ImessengerTypes'];
            if($imessengertypes->save())
                $this->refresh();
        }

        $this->render('index',array(
            'rubrics'=>$rubrics,
            'cities'=>$cities,
            'regions'=>$regions,
            'imessengertypes'=>$imessengertypes,
        ));
	}

	/**
	 * Performs the AJAX validation.
	 * @param News $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='news-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreateRubrics()
    {
        $model=new Rubrics;

        if(isset($_POST['Rubrics'])) {
            $folder = Yii::getPathOfAlias('webroot') . '/images/rubrics/';// folder for uploaded files
            $old = $model->filename;
            $model->attributes = $_POST['Rubrics'];
            $name = $_FILES['Rubrics']['name']['image'];
            $ext = pathinfo($name, PATHINFO_EXTENSION);
            $newName = $this->guid() . '.' . $ext;
            $model->image = CUploadedFile::getInstance($model, 'image');
            $fullImgSource = Yii::app()->params['rubricsPath'] . $newName;
            if ($model->image === null || $model->image === '') {
                $model->filename = $old;
            } else {
                $model->filename = $newName;
            }
            if ($model->save()) {
                if (!is_dir($folder)) {
                    mkdir($folder);
                }
                if ($model->image !== null && $model->image !== '') {
                    $model->image->saveAs($fullImgSource);
                }
            }
        }
        $rubrics=new Rubrics;
        $cities=new Cities;
        $regions=new Regions();
        $imessengertypes=new ImessengerTypes();

        $this->render('index',array(
            'rubrics'=>$rubrics,
            'cities'=>$cities,
            'regions'=>$regions,
            'imessengertypes'=>$imessengertypes,
        ));
    }
    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDeleteRubrics()
    {
        if(isset($_POST['Rubrics']))
        {
            $this->loadModelRubrics($_POST['Rubrics']['rubricid'])->delete();
        }

        $rubrics=new Rubrics;
        $cities=new Cities;
        $regions=new Regions();
        $imessengertypes=new ImessengerTypes();

        $this->render('index',array(
            'rubrics'=>$rubrics,
            'cities'=>$cities,
            'regions'=>$regions,
            'imessengertypes'=>$imessengertypes,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreateCities()
    {
        $model=new Cities;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if(isset($_POST['Cities'])) {
            $folder = Yii::getPathOfAlias('webroot') . '/images/cities/';// folder for uploaded files
            $old = $model->filename;
            $model->attributes = $_POST['Cities'];
            $name = $_FILES['Cities']['name']['image'];
            $ext = pathinfo($name, PATHINFO_EXTENSION);
            $newName = $this->guid() . '.' . $ext;
            $model->image = CUploadedFile::getInstance($model, 'image');
            $fullImgSource = Yii::app()->params['citiesPath'] . $newName;
            if ($model->image === null || $model->image === '') {
                $model->filename = $old;
            } else {
                $model->filename = $newName;
            }
            if ($model->save()) {
                if (!is_dir($folder)) {
                    mkdir($folder);
                }
                if ($model->image !== null && $model->image !== '') {
                    $model->image->saveAs($fullImgSource);
                }
            }
        }
        $rubrics=new Rubrics;
        $cities=new Cities;
        $regions=new Regions();
        $imessengertypes=new ImessengerTypes();

        $this->render('index',array(
            'rubrics'=>$rubrics,
            'cities'=>$cities,
            'regions'=>$regions,
            'imessengertypes'=>$imessengertypes,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDeleteCities()
    {
        if(isset($_POST['Cities']))
        {
            $this->loadModelCities($_POST['Cities']['cityid'])->delete();
        }

        $rubrics=new Rubrics;
        $cities=new Cities;
        $regions=new Regions();
        $imessengertypes=new ImessengerTypes();

        $this->render('index',array(
            'rubrics'=>$rubrics,
            'cities'=>$cities,
            'regions'=>$regions,
            'imessengertypes'=>$imessengertypes,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreateImessengerTypes()
    {
        $model=new ImessengerTypes;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if(isset($_POST['ImessengerTypes'])) {
            $folder = Yii::getPathOfAlias('webroot') . '/images/messenger/';// folder for uploaded files
            $old = $model->filename;
            $model->attributes = $_POST['ImessengerTypes'];
            $name = $_FILES['ImessengerTypes']['name']['image'];
            $ext = pathinfo($name, PATHINFO_EXTENSION);
            $newName = $this->guid() . '.' . $ext;
            $model->image = CUploadedFile::getInstance($model, 'image');
            $fullImgSource = Yii::app()->params['messengerPath'] . $newName;
            if ($model->image === null || $model->image === '') {
                $model->filename = $old;
            } else {
                $model->filename = $newName;
            }
            if ($model->save()) {
                if (!is_dir($folder)) {
                    mkdir($folder);
                }
                if ($model->image !== null && $model->image !== '') {
                    $model->image->saveAs($fullImgSource);
                }
            }
        }

        $rubrics=new Rubrics;
        $cities=new Cities;
        $regions=new Regions();
        $imessengertypes=new ImessengerTypes();

        $this->render('index',array(
            'rubrics'=>$rubrics,
            'cities'=>$cities,
            'regions'=>$regions,
            'imessengertypes'=>$imessengertypes,
        ));;
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDeleteImessengerTypes()
    {
        if(isset($_POST['ImessengerTypes']))
        {
            $this->loadModelImessengerTypes($_POST['ImessengerTypes']['imessengertypeid'])->delete();
        }

        $rubrics=new Rubrics;
        $cities=new Cities;
        $regions=new Regions();
        $imessengertypes=new ImessengerTypes();

        $this->render('index',array(
            'rubrics'=>$rubrics,
            'cities'=>$cities,
            'regions'=>$regions,
            'imessengertypes'=>$imessengertypes,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreateRegions()
    {
        $model=new Regions;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if(isset($_POST['Regions']))
        {
            $model->attributes=$_POST['Regions'];
            $model->save();
        }

        $rubrics=new Rubrics;
        $cities=new Cities;
        $regions=new Regions();
        $imessengertypes=new ImessengerTypes();

        $this->render('index',array(
            'rubrics'=>$rubrics,
            'cities'=>$cities,
            'regions'=>$regions,
            'imessengertypes'=>$imessengertypes,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDeleteRegions()
    {
        if(isset($_POST['Regions']))
        {
            $this->loadModelRegions($_POST['Regions']['regionid'])->delete();
        }

        $rubrics=new Rubrics;
        $cities=new Cities;
        $regions=new Regions();
        $imessengertypes=new ImessengerTypes();

        $this->render('index',array(
            'rubrics'=>$rubrics,
            'cities'=>$cities,
            'regions'=>$regions,
            'imessengertypes'=>$imessengertypes,
        ));
    }

    public function loadModelRubrics($id)
    {
        $model=Rubrics::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
    public function loadModelRegions($id)
    {
        $model=Regions::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
    public function loadModelCities($id)
    {
        $model=Cities::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
    public function loadModelImessengerTypes($id)
    {
        $model=ImessengerTypes::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
    function guid(){
        if (function_exists('uuid_create')){
            $res = uuid_create();
            str_replace('}', '', $res);
            str_replace('{', '', $res);
            return $res;
        }else{
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = // "{"
                substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12);// "}"
            return $uuid;
        }
    }
}
