<?php

class SiteController extends Controller
{
    public $layout = '//layouts/main';

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function rules()
    {
        return array(
            array('verifyCode', 'captcha', 'allowEmpty' => !extension_loaded('gd'))
        );
    }

    public function accessRules()
    {
        return array();
    }

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
                'testLimit' => '1'
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }


    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        $main_rubrics = Rubrics::model()->findAllByAttributes(array('rubrictypeid' => '1'), array('order' => 'name'));

        $news = News::model()->findAllByAttributes(array('publicationstatusid' => 1));
        $preproposals = Proposals::model()->findAll();
        $proposals = array();
        foreach($preproposals as $prep){
            if($prep->card->ispublic){
                $p_card =  $prep->card->getPrivateCard();
                if($p_card) {
                    $check = $p_card->isBasePackageRun() && $p_card->isPackageRun('4');
                    if($check) {
                        array_push($proposals, $prep);
                    }
                }
            }
        }
        shuffle($proposals);

        $this->render('index', array(
            'main_rubrics' => $main_rubrics,
            'news' => $news,
            'proposals' => $proposals
        ));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {

        $ff = Yii::app()->user->id;
        $userData = Users::model()->findByPk($ff);

        $this->render('contact');
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        $model = new LoginForm();

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    public function actionAjaxLogin()
    {
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            $errors = CActiveForm::validate($model);
            if ($errors != '[]') {
                echo $errors;
                Yii::app()->end();
            }
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
                    echo CJSON::encode(array(
                        'authenticated' => true,
                        'redirectUrl' => Yii::app()->user->returnUrl,
                        //"param" => "Any additional param"
                    ));
                    Yii::app()->end();
                }
                $this->redirect(Yii::app()->user->returnUrl);
            }
        }
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionDeals()
    {
        $this->render('deals');
    }


    public function actionNotice()
    {
        echo 'Сайт на тех обслуживании';
    }

    public function actionRegistration()
    {
        $basicPackage = PaidServiceDetails::model()->findByPk(777);
        $this->render('registration', array('basicPackage' => $basicPackage));
    }

    public function actionEmailIsFree()
    {
        $email = $_POST['email'];
        if ($email) {
            if ($email == 'test@in4smart.kz')
                $this->sendSuccessResponse('no');

            $email = Users::model()->emailIsFree($email);

            if ($email)
                $this->sendSuccessResponse('yes');
            else
                $this->sendSuccessResponse('no');
        } else
            $this->sendErrorResponse('email is null');
    }

    public function actionVerifyCaptcha()
    {
        $input = $_POST['input'];
        if ($input) {
            $captcha = Yii::app()->getController()->createAction("captcha");
            $code = $captcha->verifyCode;
            if ($input == $code)
                $this->sendSuccessResponse('true');
            else
                $this->sendSuccessResponse('false');
        } else
            $this->sendErrorResponse('verify code is null');
    }


    /*	private static $rubricTree = array();

        public static function getRubricTree() {
            if (empty(self::$rubricTree)) {
                $rows = Rubrics::model()->findAll('parent_rubricid IS NULL');
                foreach ($rows as $item) {
                    self::$rubricTree[] = self::getRubricItems($item);
                }
            }
            return self::$rubricTree;
        }

        private static function getRubricItems($modelRow) {

            if (!$modelRow)
                return;

            if (isset($modelRow->rubrics)) {
                $chump = self::getRubricItems($modelRow->rubrics);
                if ($chump != null)
                    $res = array('label' => $modelRow->name, 'items' => $chump, 'id' => $modelRow->rubricid);
                else
                    $res = array('label' => $modelRow->name, 'id' => $modelRow->rubricid);
                return $res;
            } else {
                if (is_array($modelRow)) {
                    $arr = array();
                    foreach ($modelRow as $leaves) {
                        $arr[] = self::getRubricItems($leaves);
                    }
                    return $arr;
                } else {
                    return array('label' => ($modelRow->name), 'id' => $modelRow->rubricid);
                }
            }
        }*/

    public function actionGetManySubRubrics()
    {
        $rubricsIds = $_POST['rubricsIds'];

        if ($rubricsIds) {
            $d = Rubrics::model()->findAllByPk($rubricsIds);
            $this->sendData($d);
        } else
            $this->sendErrorResponse('rubricsIds is null');
    }

    public function actionGetSubRubrics()
    {
        $rubricId = $_POST['rubricId'];

        if ($rubricId) {
            $d = Rubrics::model()->findAll("parent_rubricid = :rid", array(":rid" => $rubricId));
            $this->sendData($d);
        } else
            $this->sendErrorResponse('rubricId is null');
    }

    public function actionGetPaidServiceDetails()
    {
        date_default_timezone_set('UTC');
        $id = $_POST['paidservicedetaild'];
        if ($id) {
            $model = PaidServiceDetails::model()->with('paidservice')->findByPk($id);
//
            $this->sendDataWithRelations($model);
        } else
            $this->sendErrorResponse('paidserviceid is null');

    }

    public function actionGetBasisPaidServiceDetails()
    {
        date_default_timezone_set('UTC');
        $id = $_POST['rubricid'];
        if ($id) {
            $rubric = Rubrics::model()->findByPk($id);
            $model = PaidServiceDetails::model()->with('paidservice')->findByPk($rubric->paidgroupid);

            $this->sendDataWithRelations($model);
        } else
            $this->sendErrorResponse('paidserviceid is null');


    }

    public function actionGetCostPaidServiceDetails()
    {
        date_default_timezone_set('UTC');
        $id = $_POST['paidservicedetailid'];
        $months = $_POST['months'];
        $topmonths = $_POST['topmonths'];
        $count = $_POST['count'];
        $fullcost = $_POST['fullcost'];
        $balance = 0;
        if ($id) {
            $userid = Yii::app()->user->id;
            $user = Users::model()->findByPk($userid);
            if($userid && $user && ($user->userrole->userroleid == '3')) {
                $card = Cards::model()->findByAttributes(array('userid' => $userid, 'ispublic' => 0));

                $balance = $card->getCalcPackageCost($months, $count, $id, $fullcost, $topmonths);
            } else {
                date_default_timezone_set('UTC');
                $paidservicedetail = PaidServiceDetails::model()->findByAttributes(array('paidservicedetaild' => $id));
                $paidservice = PaidServices::model()->findByAttributes(array('paidserviceid' => $paidservicedetail->paidserviceid));

                if($paidservice->paidserviceid == '3') {
                    $startbase = new DateTime();
                    $finishbase = $startbase->add(new DateInterval('P' . $months . 'M'));
                    $now = new DateTime();
                    $starttop = new DateTime();
                    $finishtop = $starttop->add(new DateInterval('P' . $topmonths . 'M'));
                    $intervaltop = $finishtop->diff($finishbase);
                    $intervaltopreal = $now->diff($finishtop);
                    if (!$intervaltop->invert) {
                        $months = $topmonths;
                    } else {
                        $interval = $startbase->diff($finishbase);
                        $months = $topmonths * $interval->days / $intervaltopreal->days;
                    }
                }
                $count = $count;//$od->paidservicedetaild0->count;
                switch ($paidservice->paidserviceid) {
                    case "4":
                        $count = $count;
                        break;
                    default:
                        $count = 1;
                        break;
                }

                if ($months > 0) {
                    $balance = $months * $paidservicedetail->cost * $count;
                }

            }
            $this->sendData(array("cost" => $balance));

        } else
            $this->sendErrorResponse('paidserviceid is null');

    }

//    public function actionGetBalancePaidServiceDetails()
//    {
//        date_default_timezone_set('UTC');
//        $id = $_POST['paidservicedetaild'];
//        if ($id) {
//            $model = PaidServices::model()->findByPk($id);
//
//            $balance = $model->getBalance(Yii::app()->user->id);
//
//            $this->sendData(array("balance" => $balance));
//        } else
//            $this->sendErrorResponse('paidserviceid is null');
//
//    }
    public function actionGetBalancePaidServiceDetails()
    {
        date_default_timezone_set('UTC');
        $id = $_POST['paidservicedetaild'];
        if ($id) {
            $userid = Yii::app()->user->id;
            $card = Cards::model()->findByAttributes(array('userid' => $userid, 'ispublic' => 0));

            $balance = $card->getPackageBalance($id);

            $this->sendData(array("balance" => $balance));
        } else
            $this->sendErrorResponse('paidserviceid is null');

    }

    public function actionGetTags()
    {
        if (isset($_GET['q'])) {

            $criteria = new CDbCriteria(array(
                'limit' => 10
            ));

            $criteria->addSearchCondition('value', $_GET['q']);

            $tags = Keywords::model()->findAll($criteria);
        }
    }

    public function actionSaveEdit()
    {
        date_default_timezone_set('UTC');
        try {
            $data = CJSON::decode($_POST['data']);
            if ($data) {
                $id = Yii::app()->user->id;
                $card = Cards::model()->findByAttributes(array('userid' => $id, 'ispublic' => 0));

                $transaction = $card->getDbConnection()->beginTransaction();

                if (isset($data['parentcardid'])) {
                    $c = Cards::model()->findByPk($data['parentcardid']);
                    if (isset($c))
                        $card->parentcardid = $data['parentcardid'];
                }

                $card->orgtypeid = $data['orgtypeid'];
                $card->keywordcountid = $data['keywordcountid'];
                $card->rubriccountid = $data['rubriccountid'];
                $card->photoscountid = $data['photoscountid'];
                $card->regionid = $data['regionid'];
                $card->cityid = $data['cityid'];
                $card->cardstatusid = 3; // на модерации
                $card->orgname = $data['orgname'];
                $card->description = $data['description'];
                $card->street = $data['street'];
                $card->housenumber = $data['housenumber'];
                $card->office = $data['office'];
                $card->websiteurl = $data['websiteurl'];
                $card->juridicaladdress = $data['juridicaladdress'];
                $card->iinbin = $data['iinbin'];
                $card->bik = $data['bik'];
                $card->iik = $data['iik'];
                $card->kbe = $data['kbe'];
                $card->bankname = $data['bankname'];
                $card->orgindex = $data['orgindex'];
                $card->updatedate = new CDbExpression('NOW()');
                $card->workschedule = $data['workschedule'];
                $card->busnums = $data['busnums'];
                $card->months = $data['months'];
                $card->topmonths = $data['topmonths'];
                $card->rubricid = $data['rubricid'];

                $card->updatecontactname = $data['updatecontactname'];
                if ($data['updatecontactphone'])
                    $card->updatecontactphone = $data['updatecontactphone'];

                $card->accountantname = $data['accountantname'];
                if ($data['accountantphone'])
                    $card->accountantphone = $data['accountantphone'];

                if ($card->validate()) {
                    if ($card->save(false)) {
                        //map cordinates
                        if (isset($data['coordinates'])) {
                            $mapCor = MapCordinates::model()->findByAttributes(array('cardid' => $card->cardid));
                            if ($mapCor == null) {
                                $mapCor = new MapCordinates();
                            }
                            $mapCor->cardid = $card->cardid;
                            $mapCor->valuex = $data['coordinates'][0];
                            $mapCor->valuey = $data['coordinates'][1];
                            $mapCor->save();
                        }


                        //emails
                        $emails = Emails::model()->findAllByAttributes(array('cardid' => $card->cardid));
                        foreach ($emails as $i) {
                            $i->delete();
                        }
                        foreach ($data['emails'] as $i) {
                            $email = new Emails();

                            $email->cardid = $card->cardid;
                            $email->value = $i;
                            $email->save();
                        }

                        //telephones
                        $telephones = Telephones::model()->findAllByAttributes(array('cardid' => $card->cardid));
                        foreach ($telephones as $i) {
                            $i->delete();
                        }
                        foreach ($data['telephones'] as $i) {
                            $telephone = new Telephones();
                            $telephone->cardid = $card->cardid;
                            $telephone->value = $i['value'];
                            $telephone->note = $i['note'];
                            $telephone->phonetypeid = $i['phoneTypeId'];
                            $telephone->save();
                        }

                        //messengers
                        $messengers = Messengers::model()->findAllByAttributes(array('cardid' => $card->cardid));
                        foreach ($messengers as $i) {
                            $i->delete();
                        }
                        foreach ($data['messengers'] as $i) {
                            $messenger = new Messengers();

                            $messenger->cardid = $card->cardid;
                            $messenger->value = $i['value'];
                            //$messenger->imessengertypeid = $i['typeid'];
                            $messenger->imessengertypeid = $i['mesTypeId'];
                            $messenger->save();
                        }

                        //rubrics
                        $serviceRubric = PaidServiceDetails::model()->findByPk($data['rubriccountid']);
                        $maxAddCountSR = $serviceRubric->count;

                        if ($maxAddCountSR > 0) {
                            $addCount = 0;

                            $rubrics = RefRubricsCards::model()->findAllByAttributes(array('cardid' => $card->cardid));
                            foreach ($rubrics as $i) {
                                $i->delete();
                            }
                            foreach ($data['subrubricids'] as $i) {
                                $rubric = new RefRubricsCards();

                                $rubric->cardid = $card->cardid;
                                $rubric->rubricid = $i;
                                $rubric->save();

                                $addCount++;
                                if ($addCount >= $maxAddCountSR)
                                    break;
                            }
                        }

                        //keywords
                        $serviceKeyword = PaidServiceDetails::model()->findByPk($data['keywordcountid']);
                        $maxAddCountKW = $serviceKeyword->count;

                        if ($maxAddCountKW > 0) {
                            $addCount = 0;

                            $keywords = RefKeywordCard::model()->findAllByAttributes(array('cardid' => $card->cardid));
                            foreach ($keywords as $i) {
                                $i->delete();
                            }
                            foreach ($data['keywords'] as $i) {
                                $keywordId = Keywords::model()->getIdByValue($i);
                                if (!$keywordId) {
                                    $newKW = new Keywords();
                                    $newKW->value = $i;
                                    $newKW->save();
                                    $keywordId = $newKW->keywordid;
                                }
                                $keyword = new RefKeywordCard();
                                $keyword->cardid = $card->cardid;
                                $keyword->keywordid = $keywordId;
                                $keyword->save();

                                $addCount++;
                                if ($addCount >= $maxAddCountKW)
                                    break;
                            }
                        }

                        //photos
                        $photos = CardPhotos::model()->findAllByAttributes(array('cardid' => $card->cardid));
                        foreach ($photos as $i) {
                            $delete = true;
                            foreach ($data['photos'] as $p) {
                                if ($p['id'] == $i->cardphotoid) {
                                    $delete = false;
                                }
                            }
                            if ($delete) {
                                $i->delete();
                            }
                        }
                        for ($i = 0; $i < count($data['uploadphotos']); $i++) {
                            $photo = new CardPhotos();

                            ////
                            $info = pathinfo($_FILES['uploadphoto-' . $i]['name']);
                            $ext = $info['extension']; // get the extension of the file

                            $newname = $this->guid() . '.' . $ext;
                            $fullPath = Yii::app()->params['orgPhotosPath'] . $newname;

                            $allowedExts = array("jpeg", "jpg", "png");

                            $fileType = $_FILES['uploadphoto-' . $i]["type"];

                            if ((($fileType == "image/gif")
                                || ($fileType == "image/jpeg")
                                || ($fileType == "image/jpg")
                                || ($fileType == "image/pjpeg")
                                || ($fileType == "image/x-png")
                                || ($fileType == "image/png"))
                                && ($_FILES['uploadphoto-' . $i]["size"] < 2880 * 1800) // 200 кб
                                && in_array($ext, $allowedExts)
                            ) {
                                move_uploaded_file($_FILES['uploadphoto-' . $i]["tmp_name"], $fullPath);
                            }
                            ////

                            $photo->cardid = $card->cardid;
                            $photo->createdate = new CDbExpression('NOW()');
                            $photo->photo = $newname;
                            $photo->save(false);

                        }

                        //proposals
                        $proposals = Proposals::model()->findAllByAttributes(array('cardid' => $card->cardid));
                        foreach ($proposals as $i) {
                            $delete = true;
                            foreach ($data['proposals'] as $p) {
                                if ($p['id'] == $i->proposalsid) {
                                    $delete = false;
                                }
                            }
                            if ($delete === true) {
                                $i->delete();
                            }
                        }
                        for ($i = 0; $i < count($data['proposals']); $i++) {
                            $proposal = Proposals::model()->findByPk($data['proposals'][$i]['id']);
                            if ($proposal == null) {
                                $proposal = new Proposals();
                                $proposal->createdate = new CDbExpression('NOW()');
                            }
                            if (isset($_FILES['proposalphoto-' . $i])) {
                                $info = pathinfo($_FILES['proposalphoto-' . $i]['name']);
                                $ext = $info['extension']; // get the extension of the file

                                $newname = $this->guid() . '.' . $ext;
                                $fullPath = Yii::app()->params['proposalsPath'] . $newname;

                                $allowedExts = array("jpeg", "jpg", "png");

                                $fileType = $_FILES['proposalphoto-' . $i]["type"];

                                if ((($fileType == "image/gif")
                                    || ($fileType == "image/jpeg")
                                    || ($fileType == "image/jpg")
                                    || ($fileType == "image/pjpeg")
                                    || ($fileType == "image/x-png")
                                    || ($fileType == "image/png"))
                                    && ($_FILES['proposalphoto-' . $i]["size"] < 2880 * 1800) // 200 кб
                                    && in_array($ext, $allowedExts)
                                ) {
                                    move_uploaded_file($_FILES['proposalphoto-' . $i]["tmp_name"], $fullPath);

                                    $proposal->image = $newname;
                                }
                            }
                            ////

                            $proposal->cardid = $card->cardid;
                            $proposal->publicationstatus = 1;
                            if (isset($data['proposals'][$i]['name'])) $proposal->name = $data['proposals'][$i]['name'];
                            if (isset($data['proposals'][$i]['content'])) $proposal->content = $data['proposals'][$i]['content'];
                            if (isset($data['proposals'][$i]['startpublic'])) $proposal->startpublic = $data['proposals'][$i]['startpublic'];
                            if (isset($data['proposals'][$i]['endpublic'])) $proposal->endpublic = $data['proposals'][$i]['endpublic'];
                            $proposal->version = 0;
                            if (isset($data['proposals'][$i]['rubricid'])) $proposal->rubricid = $data['proposals'][$i]['rubricid'];
                            $proposal->fresh = false;
                            $proposal->updatedate = new CDbExpression('NOW()');
                            $proposal->save(false);

                        }

                        //vacancies
                        $vacancies = Vacancies::model()->findAllByAttributes(array('cardid' => $card->cardid));
                        foreach ($vacancies as $i) {
                            $delete = true;
                            foreach ($data['vacancies'] as $p) {
                                if ($p['id'] === $i->vacancyid) {
                                    $delete = false;
                                    break;
                                }
                            }
                            if ($delete === true) {
                                $i->delete();
                            }
                        }
                        for ($i = 0; $i < count($data['vacancies']); $i++) {
                            $vacancy = Vacancies::model()->findByPk($data['vacancies'][$i]['id']);
                            if ($vacancy == null) {
                                $vacancy = new Vacancies();
                                $vacancy->createdate = new CDbExpression('NOW()');
                            }

                            $vacancy->cardid = $card->cardid;
                            $vacancy->publicationstatusid = 1;
                            if (isset($data['vacancies'][$i]['name'])) $vacancy->name = $data['vacancies'][$i]['name'];
                            if (isset($data['vacancies'][$i]['content'])) $vacancy->content = $data['vacancies'][$i]['content'];
                            if (isset($data['vacancies'][$i]['startpublic'])) $vacancy->startpublic = $data['vacancies'][$i]['startpublic'];
                            if (isset($data['vacancies'][$i]['endpublic'])) $vacancy->endpublic = $data['vacancies'][$i]['endpublic'];
                            $vacancy->fresh = false;
                            $vacancy->updatedate = new CDbExpression('NOW()');
                            $vacancy->save(false);

                        }

                    } else {
                        $transaction->rollback();
                    }
                } else {
                    $err = $card->getErrors();
                    $this->sendErrorResponse('error validate new card');
                    $transaction->rollback();
                }

                //сохранить лого
                if (isset($_FILES["logo"])) {
                    $info = pathinfo($_FILES['logo']['name']);
                    $ext = $info['extension']; // get the extension of the file

                    $fileName = $this->guid() . '.' . $ext;
                    ;
                    $card->logo = $fileName;
                    $fullPath = Yii::app()->params['orgLogosPath'] . $fileName;

                    $allowedExts = array("jpeg", "jpg", "png");

                    $fileType = $_FILES["logo"]["type"];

                    if ((($fileType == "image/gif")
                        || ($fileType == "image/jpeg")
                        || ($fileType == "image/jpg")
                        || ($fileType == "image/pjpeg")
                        || ($fileType == "image/x-png")
                        || ($fileType == "image/png"))
                        && ($_FILES["logo"]["size"] < 1024 * 200) // 200 кб
                        && in_array($ext, $allowedExts)
                    ) {
                        move_uploaded_file($_FILES["logo"]["tmp_name"], $fullPath);
                    } else {
                        $transaction->rollback();
                        $this->sendErrorResponse('file invalid');
                    }
                    $card->save();
                }

                //order
                $newOrder = new Orders();
                $newOrder->setStatusAdd();
                $newOrder->createdate = new CDbExpression('NOW()');
                $newOrder->cardid = $card->cardid;

                $oldAcOrder = ActiveOrders::model()->findByAttributes(array('cardid' => $card->cardid));
                if ($oldAcOrder) {
                    $orderdatestart = new DateTime();
                    $orderdatefinish = new DateTime($oldAcOrder->order->publicationdate);
                    $orderdatefinish->add(new DateInterval('P' . ($oldAcOrder->order->months ? $oldAcOrder->order->months : '1') . 'M'));
                    $interval = $orderdatestart->diff($orderdatefinish);
                    if ($interval->days > 0) {
                        $newOrder->publicationdate = new CDbExpression('NOW()');
                    }
                }
                if ($newOrder->save()) {
                    //базовый пакет
                    if ($data['basepackage']) {
                        $newOrDetails1 = new OrderDetails();
                        $newOrDetails1->orderid = $newOrder->orderid;
                        $mainrubric = Rubrics::model()->findByPk($card->rubricid);
                        $mainpaidservicedetail = PaidServiceDetails::model()->with('paidservice')->findByPk($mainrubric->paidgroupid);
                        $newOrDetails1->paidservicedetaild = $mainpaidservicedetail->paidservicedetaild;
                        $newOrDetails1->months = $data['months'];
                        if (!$newOrDetails1->save()) {
                            $transaction->rollback();
                            $this->sendErrorResponse('error in save newOrDetails1');
                        }
                    }

                    //ключевые слова цена
                    if ($data['keywordspackage']) {
                        if ($data['keywordcountid'] != 70) { //если это не бесплатный пакет
                            $newOrDetails2 = new OrderDetails();
                            $newOrDetails2->orderid = $newOrder->orderid;
                            $newOrDetails2->paidservicedetaild = $data['keywordcountid'];
                            $newOrDetails2->months = $data['months'];
                            if (!$newOrDetails2->save()) {
                                $transaction->rollback();
                                $this->sendErrorResponse('error in save newOrDetails2');
                            }
                        }
                    }

                    //рубрики
                    if ($data['rubricspackage']) {
                        if ($data['rubriccountid'] != 11) {
                            $newOrDetails3 = new OrderDetails();
                            $newOrDetails3->orderid = $newOrder->orderid;
                            $newOrDetails3->paidservicedetaild = $data['rubriccountid'];
                            $newOrDetails3->months = $data['months'];
                            if (!$newOrDetails3->save()) {
                                $transaction->rollback();
                                $this->sendErrorResponse('error in save newOrDetails3');
                            }
                        }
                    }
                    //топ
                    if ($data['top'] == true) {
                        $newOrDetails4 = new OrderDetails();
                        $newOrDetails4->orderid = $newOrder->orderid;
                        $newOrDetails4->paidservicedetaild = 21;
                        $newOrDetails4->months = $data['topmonths'];
                        if (!$newOrDetails4->save()) {
                            $transaction->rollback();
                            $this->sendErrorResponse('error in save newOrDetails4');
                        }
                    }
                    //в новости
                    if ($data['main'] == true) {
                        $newOrDetails7 = new OrderDetails();
                        $newOrDetails7->orderid = $newOrder->orderid;
                        $newOrDetails7->paidservicedetaild = 60;
                        $newOrDetails7->months = $data['months'];
                        if (!$newOrDetails7->save()) {
                            $transaction->rollback();
                            $this->sendErrorResponse('error in save newOrDetails7');
                        }
                    }
                    //предложения
                    if ($data['proposalspackage']) {
                        if (count($data['proposals']) > 0) {
                            $newOrDetails5 = new OrderDetails();
                            $newOrDetails5->orderid = $newOrder->orderid;
                            $newOrDetails5->paidservicedetaild = 31;
                            $newOrDetails5->months = $data['months'];
                            if (!$newOrDetails5->save()) {
                                $transaction->rollback();
                                $this->sendErrorResponse('error in save newOrDetails5');
                            }
                        }
                    }
                    //картинки
                    if ($data['photospackage']) {
                        if ($data['photoscountid'] != 50) {
                            $newOrDetails6 = new OrderDetails();
                            $newOrDetails6->orderid = $newOrder->orderid;
                            $newOrDetails6->paidservicedetaild = $data['photoscountid'];
                            $newOrDetails6->months = $data['months'];
                            if (!$newOrDetails6->save()) {
                                $transaction->rollback();
                                $this->sendErrorResponse('error in save newOrDetails6');
                            }
                        }
                    }

                } else {
                    $transaction->rollback();
                    $this->sendErrorResponse('error save in new order');
                }


                $newActiveOrder = new ActiveOrders();
                $newActiveOrder->orderid = $newOrder->orderid;
                $newActiveOrder->cardid = $card->cardid;

                $oldAcOrders = ActiveOrders::model()->findAllByAttributes(array('cardid' => $card->cardid));
                foreach ($oldAcOrders as $oldAcOrder) {
                    $oldAcOrder->delete();
                }
                if (!$newActiveOrder->save()) {
                    $transaction->rollback();
                    $this->sendErrorResponse('error save in new active order');
                }

                $transaction->commit();
                $this->sendSuccessResponse('ok');

            } else {
                $this->sendErrorResponse('error validate data');
            }
        } catch (Exception $ex) {
            $transaction->rollback();
            //print_r($ex);
            $this->sendErrorResponse('main error');
        }

    }

    public function actionSaveNewCard()
    {
        $data = CJSON::decode($_POST['data']);
        if ($data) {
            $newUser = new Users();
            $transaction = $newUser->getDbConnection()->beginTransaction();


            $newUser->email = $newUser->nickname = $data['step1']['email'];
            $newUser->userroleid = 3; //client
            $newUser->password = trim($data['step1']['password1']);
            //$newUser->createdate = new CDbExpression('NOW()');

            if ($newUser->validate()) {
                if ($newUser->save(false)) {
                    //формируем карточку
                    $newCard = new Cards();

                        if ($newUser->email == 'test@in4smart.kz')
                        {
                            $newCard->setScenario('hasTestUser');
                        }
                        else
                        {
                            $newCard->setScenario('hasNormalUser');
                        }

                    $newCard->userid = $newUser->userid;

                    if (isset($data['step2']['parentorgid'])) {
                        $c = Cards::model()->findByPk($data['step2']['parentorgid']);
                        if (isset($c))
                            $newCard->parentcardid = $data['step2']['parentorgid'];
                    }

                    $newCard->orgtypeid = $data['step2']['ownership'];
                    $newCard->keywordcountid = $data['step2']['keywordcountid'];
                    $newCard->rubriccountid = $data['step2']['rubriccountid'];
                    $newCard->regionid = $data['step2']['regionid'];
                    $newCard->cityid = $data['step2']['cityid'];
                    $newCard->cardstatusid = 3; // на модерации
                    $newCard->orgname = $data['step2']['companyName'];
                    $newCard->description = $data['step2']['description'];
                    $newCard->street = $data['step2']['street'];
                    $newCard->housenumber = $data['step2']['house'];
                    $newCard->office = $data['step2']['office'];
                    $newCard->websiteurl = $data['step2']['site'];
                    $newCard->ispublic = 0; // копия не публичная. когда одобрят создадим с признаком 1
                    $newCard->createdate = new CDbExpression('NOW()');
                    $newCard->juridicaladdress = $data['step3']['uAddress'];
                    $newCard->iinbin = $data['step3']['iinBin'];
                    $newCard->bik = $data['step3']['bik'];
                    $newCard->iik = $data['step3']['iik'];
                    $newCard->kbe = $data['step3']['kbe'];
                    $newCard->bankname = $data['step3']['bankName'];
                    $newCard->orgindex = $data['step2']['orgindex'];
                    $newCard->rubricid = $data['step2']['rubricid'];
                    $newCard->months = $data['step2']['months'];
                    $newCard->topmonths = $data['step2']['topmonths'];

                    $newCard->updatecontactname = $data['step2']['updatecontact'];
                    if ($data['step2']['updatecontactphone'])
                        $newCard->updatecontactphone = $data['step2']['updatecontactphone'];
                    $newCard->accountantname = $data['step2']['accountant'];
                    if ($data['step2']['accountantphone'])
                        $newCard->accountantphone = $data['step2']['accountantphone'];
                    $newCard->workschedule = $data['step2']['workschedule'];
                    $newCard->busnums = $data['step2']['busnums'];

                    //сохраняем карту
                    if ($newCard->validate()) {
                        if ($newCard->save(false)) {
                            //map cordinate
                            if (isset($data['step2']['coords'])) {
                                $mapCor = new MapCordinates();
                                $mapCor->cardid = $newCard->cardid;
                                $mapCor->valuex = $data['step2']['coords'][0];
                                $mapCor->valuey = $data['step2']['coords'][1];
                                $mapCor->save();
                            }


                            //emails
                            foreach ($data['step2']['emails'] as $i) {
                                $newEmail = new Emails();
                                $newEmail->cardid = $newCard->cardid;
                                $newEmail->value = $i;
                                $newEmail->save();
                            }

                            //telephones

                            foreach ($data['step2']['telephones'] as $i) {
                                $newTel = new Telephones();
                                $newTel->cardid = $newCard->cardid;
                               // $newTel->value = $i;
                                $newTel->value = $i['value'];
                                $newTel->note = $i['note'];
                                $newTel->phonetypeid = $i['phoneTypeId'];
                                $newTel->save();
                            }

                            //messengers

                            foreach ($data['step2']['messengers'] as $i) {
                                $newMess = new Messengers();
                                $newMess->cardid = $newCard->cardid;
                                $newMess->value = $i['value'];
                                $newMess->imessengertypeid = $i['mesTypeId'];
                                $newMess->save();
                            }

                            //rubrics
                            $serviceRubric = PaidServiceDetails::model()->findByPk($data['step2']['rubriccountid']);
                            $maxAddCountSR = $serviceRubric->count;

                            $rubric = new RefRubricsCards();

                            $rubric->cardid = $newCard->cardid;
                            $rubric->rubricid = $newCard->rubricid;
                            $rubric->save();
                            $addCount = 0;
                            $addCount++;
                            if ($maxAddCountSR > 0) {
                                foreach ($data['step2']['subrubricids'] as $i) {
                                    $subRubrics = new RefRubricsCards();
                                    $subRubrics->cardid = $newCard->cardid;
                                    $subRubrics->rubricid = $i;
                                    $subRubrics->save();

                                    $addCount++;
                                    if ($addCount >= $maxAddCountSR)
                                        break;
                                }
                            }

                            //keywords
                            $service = PaidServiceDetails::model()->findByPk($data['step2']['keywordcountid']);
                            $maxAddCountKW = $service->count;

                            if ($maxAddCountKW > 0) {
                                $addCount = 0;
                                foreach ($data['step2']['keywords'] as $i) {
                                    $keywordId = Keywords::model()->getIdByValue($i);
                                    if (!$keywordId) {
                                        $newKW = new Keywords();
                                        $newKW->value = $i;
                                        $newKW->save();
                                        $keywordId = $newKW->keywordid;
                                    }

                                    $cardKeyWord = new RefKeywordCard();
                                    $cardKeyWord->cardid = $newCard->cardid;
                                    $cardKeyWord->keywordid = $keywordId;
                                    $cardKeyWord->save();

                                    $addCount++;
                                    if ($addCount >= $maxAddCountKW)
                                        break;
                                }
                            }


                        } else {
                            $transaction->rollback();
                        }
                    } else {
                        $err = $newCard->getErrors();
                        $this->sendErrorResponse(CJSON::encode($err));
                        $transaction->rollback();
                    }

                    //сохраниить лого
                    if (isset($_FILES["uploadFile-0"])) {
                        $info = pathinfo($_FILES['uploadFile-0']['name']);
                        $ext = $info['extension']; // get the extension of the file

                        $fileName = $newCard->cardid . '_' . date("YmdHis") . '.' . $ext;; //
                        $newCard->logo = $fileName;
                        $fullPath = Yii::app()->params['orgLogosPath'] . $fileName;

                        $allowedExts = array("jpeg", "jpg", "png");

                        $fileType = $_FILES["uploadFile-0"]["type"];

                        if ((($fileType == "image/gif")
                                || ($fileType == "image/jpeg")
                                || ($fileType == "image/jpg")
                                || ($fileType == "image/pjpeg")
                                || ($fileType == "image/x-png")
                                || ($fileType == "image/png"))
                            && ($_FILES["uploadFile-0"]["size"] < 1024 * 200) // 200 кб
                            && in_array($ext, $allowedExts)
                        ) {
                            move_uploaded_file($_FILES["uploadFile-0"]["tmp_name"], $fullPath);
                        } else {
                            $transaction->rollback();
                            $this->sendErrorResponse('file invalid');
                        }
                        $newCard->save();
                    }
                    //order
                    $newOrder = new Orders();
                    $newOrder->setStatusAdd();
                    $newOrder->createdate = new CDbExpression('NOW()');
                    $newOrder->cardid = $newCard->cardid;

                    $oldAcOrder = ActiveOrders::model()->findByAttributes(array('cardid' => $newCard->cardid));
                    if ($oldAcOrder) {
                        $orderdatestart = new DateTime();
                        $orderdatefinish = new DateTime($oldAcOrder->order->publicationdate);
                        $orderdatefinish->add(new DateInterval('P' . ($oldAcOrder->order->months ? $oldAcOrder->order->months : '1') . 'M'));
                        $interval = $orderdatestart->diff($orderdatefinish);
                        if ($interval->days > 0) {
                            $newOrder->publicationdate = new CDbExpression('NOW()');
                        }
                    }

                    if ($newOrder->save()) {

                        //базовый пакет
                        if ($data['step2']['basepackage']) {
                            $newOrDetails1 = new OrderDetails();
                            $newOrDetails1->orderid = $newOrder->orderid;
                            $mainrubric = Rubrics::model()->findByPk($newCard->rubricid);
                            $mainpaidservicedetail = PaidServiceDetails::model()->with('paidservice')->findByPk($mainrubric->paidgroupid);
                            $newOrDetails1->paidservicedetaild = $mainpaidservicedetail->paidservicedetaild;
                            $newOrDetails1->months = $data['step2']['months'];
                            if (!$newOrDetails1->save()) {
                                $transaction->rollback();
                                $this->sendErrorResponse('error in save newOrDetails1');
                            }
                        }

                        //ключевые слова цена
                        if ($data['step2']['keywordspackage']) {
                            if ($data['step2']['keywordcountid'] != 70) { //если это не бесплатный пакет
                                $newOrDetails2 = new OrderDetails();
                                $newOrDetails2->orderid = $newOrder->orderid;
                                $newOrDetails2->paidservicedetaild = $data['step2']['keywordcountid'];
                                $newOrDetails2->months = $data['step2']['months'];
                                if (!$newOrDetails2->save()) {
                                    $transaction->rollback();
                                    $this->sendErrorResponse('error in save newOrDetails2');
                                }
                            }
                        }

                        //рубрики

                        if ($data['step2']['rubricspackage']) {
                            if ($data['step2']['rubriccountid'] != 11) {
                                $newOrDetails3 = new OrderDetails();
                                $newOrDetails3->orderid = $newOrder->orderid;
                                $newOrDetails3->paidservicedetaild = $data['rubriccountid'];
                                $newOrDetails3->months = $data['step2']['months'];
                                if (!$newOrDetails3->save()) {
                                    $transaction->rollback();
                                    $this->sendErrorResponse('error in save newOrDetails3');
                                }
                            }
                        }
                        //топ
                        if ($data['step2']['top'] == true) {
                            $newOrDetails4 = new OrderDetails();
                            $newOrDetails4->orderid = $newOrder->orderid;
                            $newOrDetails4->paidservicedetaild = 21;
                            $newOrDetails4->months = $data['step2']['topmonths'];
                            if (!$newOrDetails4->save()) {
                                $transaction->rollback();
                                $this->sendErrorResponse('error in save newOrDetails4');
                            }
                        }
                        //в новости
//                            if ($data['step2']['main'] == true) {
//                                $newOrDetails7 = new OrderDetails();
//                                $newOrDetails7->orderid = $newOrder->orderid;
//                                $newOrDetails7->paidservicedetaild = 60;
//                                $newOrDetails7->months = $data['step2']['months'];
//                                if (!$newOrDetails7->save()) {
//                                    $transaction->rollback();
//                                    $this->sendErrorResponse('error in save newOrDetails7');
//                                }
//                            }
//                            //предложения
//                            if($data['proposalspackage']) {
//                                if (count($data['proposals']) > 0) {
//                                    $newOrDetails5 = new OrderDetails();
//                                    $newOrDetails5->orderid = $newOrder->orderid;
//                                    $newOrDetails5->paidservicedetaild = 31;
//                                    $newOrDetails5->months = $data['months'];
//                                    if (!$newOrDetails5->save()) {
//                                        $transaction->rollback();
//                                        $this->sendErrorResponse('error in save newOrDetails5');
//                                    }
//                                }
//                            }
//                            //картинки
//                            if($data['photospackage']) {
//                                if ($data['photoscountid'] != 50) {
//                                    $newOrDetails6 = new OrderDetails();
//                                    $newOrDetails6->orderid = $newOrder->orderid;
//                                    $newOrDetails6->paidservicedetaild = $data['photoscountid'];
//                                    $newOrDetails6->months = $data['months'];
//                                    if (!$newOrDetails6->save()) {
//                                        $transaction->rollback();
//                                        $this->sendErrorResponse('error in save newOrDetails6');
//                                    }
//                                }
//                            }
                    } else {
                        $transaction->rollback();
                        $this->sendErrorResponse('error save in new order');
                    }


                    $newActiveOrder = new ActiveOrders();
                    $newActiveOrder->orderid = $newOrder->orderid;
                    $newActiveOrder->cardid = $newCard->cardid;

                    $oldAcOrders = ActiveOrders::model()->findAllByAttributes(array('cardid' => $newCard->cardid));
                    foreach ($oldAcOrders as $oldAcOrder) {
                        $oldAcOrder->delete();
                    }
                    if (!$newActiveOrder->save()) {
                        $transaction->rollback();
                        $this->sendErrorResponse('error save in new active order');
                    }

                    //отправить уведомление на активацию
                    $newUser->sendMailActivation();
                    $transaction->commit();
                    $this->sendSuccessResponse('ok', array('email' => $data['step1']['email']));

                } else {
                    $transaction->rollback();
                    $this->sendErrorResponse('error in save new user');
                }
            } else {
                $transaction->rollback();
                $err = $newUser->getErrors();
                print_r($err);
                $this->sendErrorResponse('error validate newUser');
            }

        } else {
            $this->sendErrorResponse('error validate data');
        }
    }

    public function actionActivateUser()
    {
        if (isset($_GET['user'])) {
            $u = Users::model()->findByAttributes(array('email' => $_GET['user']));
            if ($u) {
                $u->activateddate = new CDbExpression('NOW()');
                $u->save();
                $this->render('personalactivate');
            }
        } else {
            //$u = Users::model();
            //$u->sendMailActivation();
        }
    }

    public function actionCheckCardId()
    {
        $id = $_POST['cardid'];
        if (isset($id)) {
            $card = Cards::model()->findByPk($id);
            if ($card)
                if ($card->ispublic == 1)
                    $this->sendSuccessResponse('true');
        }
        $this->sendSuccessResponse('false');
    }

    public function actionSendRemind()
    {
        $sql_cards_data = 'SELECT  t.email AS email, DATE_ADD(u.publicationdate,INTERVAL u.months MONTH) AS OrderEndDate FROM orders u JOIN cards p ON p.cardid = u.cardid JOIN users t ON t.userid = p.userid WHERE (TO_DAYS(DATE_ADD(u.publicationdate,INTERVAL u.months MONTH)) - TO_DAYS(CURRENT_DATE)) = 7';
        $cards_data = Yii::app()->db->createCommand($sql_cards_data)->queryAll(true);


        for ($i = 0; $i < count($cards_data); $i++):

            $this->sendMailReminder($cards_data[$i]['email'], $cards_data[$i]['OrderEndDate']);

        endfor;

    }

    public function sendMailReminder($email, $end_date)
    {
        Yii::import('ext.YiiMailer.YiiMailer');
        $host = Yii::app()->params['host'];
        $content = "
		Уважаемый клиент!<br>
		Уведомляем Вас о том, что срок размещения на
        электронном и мобильном справочник Info Smart заканчивается " . $end_date . ".<br>
        Если вы желаете продлить размещение на нашем сайте, просим Вас сделать это в Личном кабинете!<br>
        Благодарим за сотрудничество и рады будем видеть Вас снова в числе наших
        клиентов!<br>
        <h4>С Уважением, команда Info Smart!</h4>";

        $mail = new YiiMailer('contact', array('message' => $content, 'name' => '', 'description' => 'Напоминание о продлении'));

        $mail->setFrom(Yii::app()->params['mailingAddress']);
        $mail->setSubject('Электронный и мобильный справочник Info Smart. Срок размещения заканчивается');
        $mail->setTo($email);

        if (!$mail->send()) {
            throw new EmailException('dont send error');
        }
    }



}