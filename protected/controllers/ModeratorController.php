<?php

class ModeratorController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/admin';

    /**
     * @return array action filters
     */

    public function rules()
    {
        return array(
            array('verifyCode', 'captcha', 'allowEmpty' => !extension_loaded('gd'))
        );
    }

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
                'testLimit' => '1'
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            //'page'=>array(
            //	'class'=>'CViewAction',
            //),
        );
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'expression' => 'Yii::app()->user->role==="moderator"'
            ),
            array(
                'deny',
                'users' => array('*'),
            )
        );
    }

    /*
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }


    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }*/

    public function actionRegistration()
    {
        Rubrics::model()->findAll();
//		$this->render('registration', array('tags' => Keywords::model()->findAll()));
        $this->redirect('site/registration');
    }


    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Cards;

        // Uncomment the following line if AJAX validation is needed
        //$this->performAjaxValidation($model);

        if (isset($_POST['Cards'])) {
            $model->attributes = $_POST['Cards'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->cardid));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Cards'])) {
            $model->attributes = $_POST['Cards'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->cardid));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $m = $this->loadModel($id);
        $publicCardId = $m->getPublicCardId();
        $m->delete();
        Cards::model()->deleteByPk($publicCardId);
        Users::model()->deleteByPk($m->user->userid);
        $m->deleteFiles();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Cards');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionCards()
    {
        $model = new Cards('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Cards'])) {
            $model->attributes = $_GET['Cards'];
        }
        $model->ispublic = 0; //модератор видит только приватные

        $this->render('cards', array(
            'model' => $model,
        ));
    }


    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Cards the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Cards::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Cards $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'cards-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /*	private static $rubricTree = array();

        public static function getRubricTree() {
            if (empty(self::$rubricTree)) {
                $rows = Rubrics::model()->findAll('parent_rubricid IS NULL');
                foreach ($rows as $item) {
                    self::$rubricTree[] = self::getRubricItems($item);
                }
            }
            return self::$rubricTree;
        }

        private static function getRubricItems($modelRow) {

            if (!$modelRow)
                return;

            if (isset($modelRow->rubrics)) {
                $chump = self::getRubricItems($modelRow->rubrics);
                if ($chump != null)
                    $res = array('label' => $modelRow->name, 'items' => $chump, 'id' => $modelRow->rubricid);
                else
                    $res = array('label' => $modelRow->name, 'id' => $modelRow->rubricid);
                return $res;
            } else {
                if (is_array($modelRow)) {
                    $arr = array();
                    foreach ($modelRow as $leaves) {
                        $arr[] = self::getRubricItems($leaves);
                    }
                    return $arr;
                } else {
                    return array('label' => ($modelRow->name), 'id' => $modelRow->rubricid);
                }
            }
        }*/

    public function actionSendToModeration()
    {
        $data = json_decode($_POST['data']);
        $privateCard = Cards::model()->findByPk($data->cardid);
        $transaction = $privateCard->getDbConnection()->beginTransaction();

        try {
            if ($data && $privateCard) {
                if ($data->action == 'approve') //одобрено
                {

                    if ($data->cardStatus == 'published' && $data->orderStatus == 'paid') // опалчено- опубликовано
                    {
                        if ($data->payment) {
                            $activeOrderId = $privateCard->getActiveOrderId();
                            $activeOrder = Orders::model()->findByPk($activeOrderId);
                            $activeInvoiceId = $activeOrder->getActiveInvoiceId();

                            if (!$activeInvoiceId) {
                                $transaction->rollback();
                                $this->sendErrorResponse('activeInvoice not set');
                            }

                            $activeOrder->setStatusPaid();
                            $activeOrder->save();
                            //есть ли активный заказ до этого?
                            $orders = Orders::model()->findAllByAttributes(array('cardid'=>$activeOrder->cardid),array('order'=>'orderid desc'));
                            $newActiveOrder = Orders::model()->findByPk($activeOrder->orderid);
                            $datetime = new DateTime($newActiveOrder->publicationdate);
                            foreach($newActiveOrder->orderDetails as $thisod) {//этот код полный отстой :( TODO fix it!
                                foreach ($orders as $order) {
                                    foreach ($order->orderDetails as $od) {
                                        if($od->paidservicedetaild == $thisod->paidservicedetaild) {
                                            $orderdatestart = new DateTime($od->publicationdate);
                                            $orderdatefinish = $orderdatestart;
                                            $orderdatefinish->add(new DateInterval('P' . ($od->months ? $od->months : '1') . 'M'));
                                            $interval = $datetime->diff($orderdatefinish);
                                            if ($interval->days > 0) {
                                                $thisod->publicationdate = $od->publicationdate;
                                                $thisod->save();
                                                break;
                                            }
                                        }
                                    }
                                }
                                if($thisod->publicationdate == null){
                                    $thisod->publicationdate = new CDbExpression('NOW()');
                                    $thisod->save();
                                }
                            }

                            $newPayment = new Payments();
                            $newPayment->checknumber = $data->payment->checkNumber;
                            $newPayment->sum = $data->payment->checkSum;
                            $newPayment->invoiceid = $activeInvoiceId;
                            $newPayment->setPaidOnSite(); //перевод на счет.

                            if (!$newPayment->save()) {
                                throw new Exception('error in newPayment');
                            }

                            if (isset($_FILES["uploadFile-0"])) {
                                $info = pathinfo($_FILES['uploadFile-0']['name']);
                                $ext = $info['extension']; // get the extension of the file

                                $fileName = $newPayment->paymentid . '_' . date("YmdHis") . '.' . $ext;
                                ; //
                                $newPayment->filename = $fileName;
                                $fullPath = Yii::app()->params['paymentsFilesPath'] . $fileName;

                                $allowedExts = array("pdf", "jpeg", "jpg", "png");

                                $fileType = $_FILES["uploadFile-0"]["type"];

                                if ((($fileType == "image/gif")
                                    || ($fileType == "image/jpeg")
                                    || ($fileType == "image/jpg")
                                    || ($fileType == "image/pjpeg")
                                    || ($fileType == "image/x-png")
                                    || ($fileType == "image/png")
                                    || ($fileType == "application/pdf"))
                                    && ($_FILES["uploadFile-0"]["size"] < 1024 * 1024 * 2) //2mb
                                    && in_array($ext, $allowedExts)
                                ) {
                                    move_uploaded_file($_FILES["uploadFile-0"]["tmp_name"], $fullPath);

                                } else {
                                    $transaction->rollback();
                                    $this->sendErrorResponse('file invalid');
                                }
                                $newPayment->save();
                            }

                            $privateCard->approveCard();
                            $publicCard = $privateCard->getPublicCard();
                            $publicCard->setStatusPublished();
                            $privateCard->setStatusPublished();
                            //отправить увдемоление
                            if ($data->copyToEmail) {
                                $user = $privateCard->user;
                                $user->sendNotification($data->mailContent, 'Карточка опубликована');
                            }

                            $transaction->commit();
                            $this->sendSuccessResponse('ok');
                        } else {
                            $transaction->rollback();
                            $this->sendErrorResponse('payment data is null');
                        }

                    } else if ($data->cardStatus == 'moderation' && $data->orderStatus == 'invoicing') //выставление счета на оплату - на модерации - ждет оплаты
                    {
                        //счет фактура
                        $newInvoice = new Invoices();
                        $activeOrderId = $privateCard->getActiveOrderId();
                        $newInvoice->orderid = $activeOrderId;
                        $activeOrder = Orders::model()->findByPk($activeOrderId);
                        $newInvoice->sum = $activeOrder->getSum();
                        if (!$newInvoice->save()) {
                            $transaction->rollback();
                            $this->sendErrorResponse('error');
                        }
                        $newActiveInvoice = new ActiveInvoices(); //установка активной счет фактуры
                        ActiveInvoices::model()->deleteByPk($activeOrderId);
                        $newActiveInvoice->orderid = $activeOrderId;
                        $newActiveInvoice->invoiceid = $newInvoice->invoiceid;
                        if (!$newActiveInvoice->save()) {
                            $transaction->rollback();
                            $this->sendErrorResponse('error');
                        }

                        if ($newInvoice->createPdf()) {
                            //выставление счета, отправка в емейл
                            $newInvoice->sendInvoice($privateCard->getUserEmail(), $privateCard->orgNameToString(), $data->mailContent);
                            $activeOrder->setStatusInvoicing();
                            $activeOrder->save();
                        } else {
                            $transaction->rollback();
                            $this->sendErrorResponse('не выставился счет на оплату');
                        }
                        $privateCard->setStatusModeration();

                        $transaction->commit();
                        $this->sendSuccessResponse('ok');
                    }

                } else if ($data->action == 'reject') //на корректировке
                {
                    if ($data->cardStatus == 'adjustments') // требует коректировок
                    {
                        $privateCard->setStatusAdjustmentReq();
                        $transaction->commit();
                        //отправить увдемоление
                     /*   if ($data->copyToEmail) {*/

                            $mail_content = "Здравствуйте, «" . $privateCard->orgNameToString() . "» !<br>

                        Благодарим, Вас за желание разместится на нашем

                        информационном справочнике «Info Smart». К сожалению, 

                        при регистрации была указана некорректная информация, 

                        (" . $data->mailContent . ").<br>

                        В связи, с чем в настоящий момент мы не можем подтвердить Вашу заявку. 

                        При устранении данной некорректной информации можете отправить заявку 

                        повторно.<br> 

                        Мы будем рады видеть Вас на нашем информационном справочнике, «Info Smart».<br>

                        Надеемся на плодотворное сотрудничество! <br>

						С уважением Info Smart! <br>";

                            $user = $privateCard->user;
                            $user->sendNotification($mail_content, 'Карточка требует корректировок', 'Неверные данные');

                 /*       }*/

                        $this->sendSuccessResponse('ok');
                    }
                }
            } else {
                $transaction->rollback();
                $this->sendErrorResponse('error');
            }
        } catch (Exception $ex) {
            $transaction->rollback();
            print_r($ex);
            $this->sendErrorResponse('error in sendtomoderation ');
        }

    }

    public function actionCardUnPublish()
    {
        $ids = $_POST['ids'];

        if (count($ids) > 0) {

            foreach ($ids as $id) {
                $card = Cards::model()->findByPk($id);
                if ($card) {
                    $card->setStatusUnPublished();
                } else {
                    $this->sendErrorResponse('card with send id does not exist');
                }
            }

            $this->sendSuccessResponse('success');
        } else {
            $this->sendErrorResponse('ids is null');
        }
    }

}
