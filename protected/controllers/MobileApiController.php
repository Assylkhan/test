<?php

class MobileApiController extends Controller
{

	public function actionFetchRubrics()
	{
		$data = Rubrics::model()->findAll();
		foreach ($data as &$v) {
			$v->filename = isset($v->filename) ? 'http://' . Yii::app()->params['host'] . '/images/rubrics/' . $v->filename : '';
		}
		echo CJSON::encode($data);
	}

	public function actionFetchCards()
	{
		$res = array();
		if (isset($_GET['Cards'])) {
			$model = new Cards();
			$model->unsetAttributes();
			$model->attributes = $_GET['Cards'];
			$data = $model->search('t.cardid, t.parentcardid, t.orgtypeid, t.regionid,  t.cityid, t.orgname, t.logo, t.street, t.housenumber, t.office, t.websiteurl, t.description');
		} else
			if (isset($_GET['lastid'])) {
				$lastid = $_GET['lastid'];
				$previd = 0;
				if (isset($_GET['previd'])) {
					$previd = $_GET['previd'];
				}
				$criteria = new CDbCriteria;
				$criteria->select = 't.cardid, t.parentcardid, t.orgtypeid, t.cardstatusid, t.regionid,  t.cityid, t.orgname, t.logo, t.street, t.housenumber, t.office, t.websiteurl, t.description';
				$criteria->condition = '(cardstatusid = 2 and cardid < '.$previd.') or (cardstatusid = 1 and ispublic = true and cardid > '.$lastid.')';
				$data = Cards::model()->findAll($criteria);
			}
			else
			{
				$data = Cards::model()->findAll(array('condition' => 'ispublic = 1 and cardstatusid = 1',
					'select'=>"t.cardid, t.parentcardid, t.orgtypeid, t.cardstatusid, t.regionid,  t.cityid, t.orgname, t.logo, t.street, t.housenumber, t.office, t.websiteurl, t.description"));
			}

		foreach ($data as $v) {
			$result = new stdClass();
			$result->cardid = $v->cardid;
			$result->parentcardid = $v->parentcardid;
			$result->orgtypeid = $v->orgtypeid;
			$result->regionid = $v->regionid;
			$result->cityid = $v->cityid;
			$result->orgname = $v->orgname;
			$result->logo = $v->logo ? 'http://' . Yii::app()->params['host'] . '/images/orglogos/' . $v->logo : '';
			$result->street = $v->street;
			$result->housenumber = $v->housenumber;
			$result->office = $v->office;
			$result->websiteurl = $v->websiteurl;
			$result->description = $v->description;
			$result->st = $v->cardstatusid;
			array_push($res, $result );

		}
		echo CJSON::encode($res);

	}

	public function actionFetchEmails()
	{
		echo CJSON::encode(Emails::model()->findAll());
	}

	public function actionFetchMapCoordinates()
	{
		echo CJSON::encode(MapCordinates::model()->findAll());
	}

	public function actionFetchMessengers()
	{
		echo CJSON::encode(Messengers::model()->findAll());
	}

	public function actionFetchIMessengersTypes()
	{
		echo CJSON::encode(ImessengerTypes::model()->findAll());
	}

	public function actionFetchRubricGroups()
	{
		echo CJSON::encode(RubricGroups::model()->findAll());
	}

	public function actionFetchRubricTypes()
	{
		echo CJSON::encode(RubricTypes::model()->findAll());
	}

	public function actionFetchSelectionTop()
	{
		//echo CJSON::encode(SelectionTop::model()->findAll(array("condition" => "date_stop >  now()")));
		$cards = Cards::model()->findAll("ispublic = true");
		shuffle($cards);
		$topcards = array();
		foreach($cards as $card){
			$p_card = $card->getPrivateCard();
			if ($p_card) {
				$check = $p_card->isBasePackageRun()&& $p_card->isPackageRun('3');
				if($check){
					array_push($topcards, $card);
				}
			}
		}
		$result = array();
		foreach($topcards as $it){
			array_push($result, $it->cardid);
		}

		echo CJSON::encode($result);
	}

	public function actionFetchTelephones()
	{
		echo CJSON::encode(Telephones::model()->findAll());
	}

	public function actionFetchCities()
	{
		$data = Cities::model()->findAll();
		foreach ($data as &$v) {
			$v->filename =  isset($v->filename) ? 'http://' . Yii::app()->params['host'] . '/images/cities/' . $v->filename : '';
		}
		echo CJSON::encode($data);
	}

	public function actionFetchOrgTypes()
	{
		echo CJSON::encode(OrgTypes::model()->findAll());
	}

	public function actionFetchRefRubricsCards()
	{
		echo CJSON::encode(RefRubricsCards::model()->findAll());
	}

	public function actionFetchRegions()
	{
		echo CJSON::encode(Regions::model()->findAll());
	}

	public function actionFetchKeywords()
	{
		echo CJSON::encode(Keywords::model()->findAll());
	}

	public function actionFetchRefKeywordCard()
	{
		echo CJSON::encode(RefKeywordCard::model()->findAll());
	}

	public function actionFetchCardPhotos()
	{
		$data = CardPhotos::model()->findAll();
		foreach ($data as &$v) {
			$v->photo = 'http://' . Yii::app()->params['host'] . '/images/orgphotos/' . $v->photo;
		}
		echo CJSON::encode($data);
	}

	public function actionFetchNews()
	{
		$data = News::model()->findAll();
		foreach ($data as &$v) {
			$v->filename = 'http://' . Yii::app()->params['host'] . '/images/news/' . $v->filename;
		}
		echo CJSON::encode($data);
	}

	public function actionFetchProposals()
	{
		$data = Proposals::model()->findAll(array("condition" => "endpublic >  now() and publicationstatusid = 1"));
		foreach ($data as $v) {
			$v->image = $v->image ? 'http://' . Yii::app()->params['host'] . '/images/proposals/' . $v->image: '';
		}
		echo CJSON::encode($data);
	}

	public function actionCardOpened()
    {
        $id = $_GET['cardid'];
        date_default_timezone_set('UTC');
        $date = new DateTime();
        if ($id) {
            $st = Statistics::model()->findByAttributes(array('cardid' => $id, 'date' => $date->format('d/m/Y'), 'type'=>2));
            if ($st == null) {
                $st = new Statistics();
                $st->cardid = $id;
                $st->date = $date->format('d/m/Y');
                $st->type = 2;//mobile
            }
            $st->count++;
            $st->save();
        }
    }
	
	public function actionCardCounter()
	{
		$post = Yii::app()->request->rawBody;
		if (!empty($post))
		{
			$data = CJSON::decode($post, false);
			foreach ($data as &$v) {
				$st = Statistics::model()->findByAttributes(array('cardid' => $v->id, 'date' => $v->date, 'type'=>2));
				if ($st == null) {
					$st = new Statistics();
					$st->cardid = $v->id;
					$st->date = $v->date;
					$st->type = 2;
				}
				$st->count = $st->count + $v->count;
				$st->save();
			}
		}
	}
	
	public function actionTrainTiming()
	{
		$post = Yii::app()->request->rawBody;
		if (!empty($post))
		{
			Yii::app()->db->createCommand()->truncateTable(TrainTiming::model()->tableName());
			date_default_timezone_set('UTC');
			$date = new DateTime();
			$xml = simplexml_load_string($post);
			foreach ($xml->RailwayStations as $v)
			{
				$arrival = '';
				if (!empty($v->Arrival)) {
					$arrival = substr($v->Arrival, 11);
				}
				$departure = '';
				if (!empty($v->Departure)) {
					$departure = substr($v->Departure, 11);
				}
				$st = new TrainTiming();
				$st->trainnumber = $v->TrainNumber;
				$st->direction = $v->Direction;
				$st->arrival = $arrival;
				$st->departure = $departure;
				$st->path = $v->Path;
				$st->tardiness = $v->Tardiness;
				$st->today = $date->format('d/m/Y');
				$st->save();
			}
		}
	}
	
	public function actionFetchTrainTiming()
	{
		echo CJSON::encode(TrainTiming::model()->findAll());
	}
	
	public function actionFetchUpdateSize()
	{
		$count = Rubrics::model()->count();
		$count = $count + Cards::model()->count("ispublic = true and cardstatusid = 1");
		$count = $count + Emails::model()->count();
		$count = $count + MapCordinates::model()->count();
		$count = $count + Messengers::model()->count();
		$count = $count + ImessengerTypes::model()->count();
		$count = $count + RubricGroups::model()->count();
		$count = $count + RubricTypes::model()->count();
		$count = $count + Telephones::model()->count();
		$count = $count + Cities::model()->count();
		$count = $count + OrgTypes::model()->count();
		$count = $count + RefRubricsCards::model()->count();
		$count = $count + Regions::model()->count();
		$count = $count + Keywords::model()->count();
		$count = $count + RefKeywordCard::model()->count();
		$count = $count + CardPhotos::model()->count();
		$count = $count + News::model()->count();
		$count = $count + Proposals::model()->count(array("condition" => "endpublic >  now() and publicationstatusid = 1"));
		$count = $count + TrainTiming::model()->count();
		echo CJSON::encode($count);
	}
}