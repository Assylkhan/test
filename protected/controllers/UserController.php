<?php

class UserController extends ProcessingController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main_clean';

	/**
	 * @return array action filters
	 */

	public function rules()
	{
		return array(
			array('verifyCode', 'captcha', 'allowEmpty' => !extension_loaded('gd'))
		);
	}

	public function actions()
	{
		return array(
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
				'testLimit' => '1'
			),
		);
	}

	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
            array('deny',
                'users'=>array('?'),
            ),
		);
	}

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionIndex()
    {
        if (Yii::app()->db->schema->getTable("balance",true)===null) {
            $command = Yii::app()->db->createCommand();
            $command->createTable("balance", array(
                'balanceid' => 'pk',
                'userid' => 'int(11) NOT NULL',
                'balance' => 'int(11) NOT NULL DEFAULT \'0\''
            ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2');
            $command->addForeignKey(
                "FK_fk_balance_users",
                "balance",
                "userid",
                "users",
                "userid",
                null
            );
        }
        if (Yii::app()->db->schema->getTable("recharge",true)===null) {
            $command->createTable("recharge", array(
                'rechargeid' => 'pk',
                'balanceid' => 'int(11) NOT NULL',
                'userid' => 'int(11) NOT NULL',
                'sum' => 'int(11) NOT NULL',
                'rechargedate' => 'datetime NOT NULL',
            ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2');
            $command->addForeignKey(
                "FK_fk_recharge_balance",
                "recharge",
                "balanceid",
                "balance",
                "balanceid",
                null
            );
            $command->addForeignKey(
                "FK_fk_recharge_users",
                "recharge",
                "userid",
                "users",
                "userid",
                null
            );
        }

        $user = Users::model()->findByPk(Yii::app()->user->id);
        $payStatus = '';

        if (Yii::app()->request->getParam('customerReference') != '') {
            $reference = Yii::app()->request->getParam('customerReference');
            if (isset(Yii::app()->session[$reference])) {
                try {
                    $params = json_decode(Yii::app()->session['payment'], true);
                    $operation = $params[0];
                    if ($operation == "payment") {
                        // Проводим все карточные махинации
                        $this->goCardPayment($params, $user->userid);
                    }

                    if ($operation == "balance") {
                        // Пополняем баланс текущего пользователя
                        $this->goBalanceRecharge($params, $user->userid);
                    }
                } catch (Exception $e) {
                    Yii::log("Exception during payment operation: " . $e);
                }
            } else {
                Yii::log("Not actual customer reference: " . $reference);
            }
        }


        $card = Cards::model()->findByAttributes(array('userid'=>$user->userid,'ispublic'=>0));
        $publiccard = Cards::model()->findByAttributes(array('userid'=>$user->userid,'ispublic'=>1));
        $orders = Orders::model()->findAllByAttributes(array('cardid'=>$card->cardid));
        $activeorder = ActiveOrders::model()->findByAttributes(array('cardid'=>$card->cardid));
        $balance = Balance::model()->findByAttributes(array('userid' => $user->userid));
        Yii::log("active order: " . $activeorder->orderid);

        $this->render('index', array(
            'user'=>$user,
            'card'=>$card,
            'publiccard'=>$publiccard,
            'orders'=>$orders,
            'activeorder'=>$activeorder,
            'payStatus' => $payStatus,
            'balance' => ($balance != null ? $balance->balance : 0)
        ));
    }

    // Обрабатываем операцию оплаты картой
    private function goCardPayment($params, $userId) {
        $cardId = intval($params[1]);
        $cost = intval($params[2]) / 100;

        $card = Cards::model()->findByAttributes(array(
            'cardid' => $cardId,
            'userid'=> $userId,
            'ispublic' => 0
        ));

        if ($card != null) {
            // @TODO: нужно ли записывать в сессию ID самого заказа при записанной карте?
//                            $command = Yii::app()->db->createCommand();
            $activeOrder = ActiveOrders::model()->findByAttributes( array('cardid' => $card->cardid) );
//            $activeInvoice = ActiveInvoices::model()->findByAttributes( array('orderid' => $activeOrder->orderid) );
            $order = Orders::model()->findByAttributes( array('orderid' => $activeOrder->orderid) );
            $orderDetails = OrderDetails::model()->findByAttributes( array('orderid' => $order->orderid) );
            if ($activeOrder != null && $order != null && $orderDetails != null) {
                $payInfo = PaidServiceDetails::model()->findByAttributes(array('paidservicedetaild' => $orderDetails->paidservicedetaild));
                $summ = $payInfo->cost * $payInfo->count;
                if ($cost == $summ) {
                    $payment = new Payments();
                    $payment->paymenttypeid = 1;
                    $payment->sum = $summ;
//                    $payment->invoiceid = $activeInvoice->invoiceid;
                    $payment->save();
                    $order->orderstatusid = 1;
                    $order->save();
                    $activeOrder->delete();
                    $card->cardstatusid = 1;
                    $card->save();
                    $payStatus = "OK";
                } else {
                    Yii::log("Summ " . $cost . " is not enough for payment");
                    $payStatus = "Summ " . $cost . " is not enough for payment";
                }
            } else {
                Yii::log("There is not such active order");
                $payStatus = "There is not such active order";
            }
        } else {
            Yii::log("There is not such card: " . $cardId);
            $payStatus = "There is not such card";
        }
    }

    // Обрабатываем операцию пополнения баланса
    private function goBalanceRecharge($params, $userId) {
        // Сумма пополнения баланса
        $sum = intval($params[1]) / 100;
        $balance = Balance::model()->findByAttributes(array('userid'
        => $userId));
        if ($balance == null) {
            $balance = new Balance();
            $balance->userid = $userId;
        }
        $balance->balance = $balance->balance + $sum;
        if ( $balance->save() ) {
            $recharge = new Recharge();
            $recharge->userid = $userId;
            $recharge->balanceid = $balance->balanceid;
            $recharge->sum = $sum;
            $recharge->rechargedate = date('Y-m-d H:i:s');
            $recharge->save();
            Yii::log("recharge saved");
        } else {
            Yii::log("Error while balance saving: " . $balance->errors);
        }
    }

    public function actionTopUp() {
        // Сюда соберем параметры оплаты
        $params = array();
        // Тип операции
        array_push($params, "balance");

        $summ = ((int) Yii::app()->request->getPost('summ')) * 100;
        Yii::log("summ - " . $summ);
        $user = Users::model()->findByPk(Yii::app()->user->id);

        if ($summ > 0) {
            // Вспомогательный класс
            include 'CNPMerchantWebServiceClient.php';
            // Сумма операции
            array_push($params, $summ);
            $basket = array();
            $goodsItem = new GoodsItem();
            $goodsItem->amount = $summ;
            $goodsItem->currencyCode = 398;
            $goodsItem->merchantsGoodsID = guid();
            $goodsItem->nameOfGoods = "Баланс";
            array_push($basket, $goodsItem);

            $pageURL = 'http';
            /*if ($_SERVER["HTTPS"] == "on") {
                $pageURL .= "s";
            }*/
            $pageURL .= "://";
            if ($_SERVER["SERVER_PORT"] != "80") {
                $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
            } else {
                $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
            }

            Yii::log("$pageURL - " . $pageURL);

            $client = new CNPMerchantWebServiceClient();
            $transactionDetails = new TransactionDetails();
            $transactionDetails->merchantId = "000000000000005";
            $transactionDetails->terminalId = "TEST TID";
            $transactionDetails->totalAmount = $summ; //*100;
            $transactionDetails->currencyCode = $basket[0]->currencyCode;
            $transactionDetails->description = "My first transaction";

            /* в конце url-а передаем '&customerReference=', для того чтоб использовать
             * автоматическую генерацию RRN, автоматически сгенерированный
             * customerReference будет дополняться в конце URL адреса в вашем returnURL. */
            $transactionDetails->returnURL = str_replace("topUp", "index", $pageURL) . "&customerReference=";
            Yii::log("$transactionDetails->returnURL - " . $transactionDetails->returnURL);

            $transactionDetails->goodsList = $basket;
            $transactionDetails->languageCode = "ru";
            $transactionDetails->merchantLocalDateTime = date("d.m.Y H:i:s");
            $transactionDetails->orderId = rand(1, 10000);
            $transactionDetails->purchaserName = $user->nickname;
            $transactionDetails->purchaserEmail = $user->email;

            $st = new startTransaction();
            $st->transaction = $transactionDetails;
            $startTransactionResult = $client->startTransaction($st);

            if ($startTransactionResult->return->success == true) {
                // на всякой случай сохраняем данные customerReference в сессию
//                $_SESSION["customerReference"] = $startTransactionResult->return->customerReference;
                // И запишем все в сессию
                Yii::app()->session[$startTransactionResult->return->customerReference] = json_encode($params);
                header("Location: " . $startTransactionResult->return->redirectURL);
            } else {
                Yii::log('Error: ' . $startTransactionResult->return->errorDescription);
            }
        }
    }

    /**
     * Начинаем оплату картой:
     * формируем данные и отправляем на сторону процессинга
     */
    public function actionCardPayment() {
        // Сюда соберем параметры оплаты
        $params = array();
        // Наименование операции
        array_push($params, "payment");

        $user = Users::model()->findByPk(Yii::app()->user->id);
        $card = Cards::model()->findByAttributes(array('userid'=>$user->userid,'ispublic'=>0));
        $activeorder = ActiveOrders::model()->findByAttributes(array('cardid'=>$card->cardid));
        // Запишем ID карты, по которой идет оплата ЕДИНСТВЕННОГО активного заказа
        array_push($params, $card->cardid);
        $details = OrderDetails::model()->findByAttributes(array('orderid' => $activeorder->orderid));
        $paidDetails = PaidServiceDetails::model()->findByAttributes(array('paidservicedetaild' => $details->paidservicedetaild));
        $cost = intval($paidDetails->cost) * intval($paidDetails->count) * 100;
        $paidService = PaidServices::model()->findByAttributes(array('paidserviceid' => $paidDetails->paidserviceid));
        $paymentName = $paidService->name;
        Yii::log("cost - " . $cost);
        // Сумма операции
        array_push($params, $cost);

        include 'CNPMerchantWebServiceClient.php';

        $basket = array();
        $goodsItem = new GoodsItem();
        $goodsItem->amount = $cost;
        $goodsItem->currencyCode = 398;
        $goodsItem->merchantsGoodsID = guid();
        $goodsItem->nameOfGoods = $paymentName;
        array_push($basket, $goodsItem);

        $pageURL = 'http';
        /*if ($_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }*/
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }

        Yii::log("$pageURL - " . $pageURL);

        $client = new CNPMerchantWebServiceClient();
        $transactionDetails = new TransactionDetails();
        $transactionDetails->merchantId = "000000000000005";
        $transactionDetails->terminalId = "TEST TID";
        $transactionDetails->totalAmount = $cost; //*100;
        $transactionDetails->currencyCode = $basket[0]->currencyCode;
        $transactionDetails->description = "Оплата заказа № " . $activeorder->orderid;

        /* в конце url-а передаем '&customerReference=', для того чтоб использовать
         * автоматическую генерацию RRN, автоматически сгенерированный
         * customerReference будет дополняться в конце URL адреса в вашем returnURL. */
        $transactionDetails->returnURL = str_replace("cardPayment", "index", $pageURL) . "&customerReference=";
        Yii::log("$transactionDetails->returnURL - " . $transactionDetails->returnURL);

        $transactionDetails->goodsList = $basket;
        $transactionDetails->languageCode = "ru";
        $transactionDetails->merchantLocalDateTime = date("d.m.Y H:i:s");
        $transactionDetails->orderId = rand(1, 10000);
        $transactionDetails->purchaserName = $user->nickname;
        $transactionDetails->purchaserEmail = $user->email;

        $st = new startTransaction();
        $st->transaction = $transactionDetails;
        $startTransactionResult = $client->startTransaction($st);

        if ($startTransactionResult->return->success == true) {
            // на всякой случай сохраняем данные customerReference в сессию
//            $_SESSION["customerReference"] = $startTransactionResult->return->customerReference;// Запишем стоимость, в жизни всякое бывает
            // И запишем все в сессию
            Yii::app()->session[$startTransactionResult->return->customerReference] = json_encode($params);
            header("Location: " . $startTransactionResult->return->redirectURL);
        } else {
            Yii::log('Error: ' . $startTransactionResult->return->errorDescription);
        }
    }

    /**
     * Осуществляем оплату с баланса
     */
    public function actionBalancePayment() {
        $card = Cards::model()->findByAttributes(array('userid' => Yii::app()->user->id,'ispublic'=>0));
        $activeOrder = ActiveOrders::model()->findByAttributes( array('cardid' => $card->cardid) );
//        $activeInvoice = ActiveInvoices::model()->findByAttributes( array('orderid' => $activeOrder->orderid) );
        $order = Orders::model()->findByAttributes( array('orderid' => $activeOrder->orderid) );
        $orderDetails = OrderDetails::model()->findByAttributes( array('orderid' => $order->orderid) );
        $balance = Balance::model()->findByAttributes(array('userid' => Yii::app()->user->id));

        if ($activeOrder != null && $order != null && $orderDetails != null) {
            $payInfo = PaidServiceDetails::model()->findByAttributes(array('paidservicedetaild' => $orderDetails->paidservicedetaild));
            $summ = $payInfo->cost * $payInfo->count;

            if ($balance->balance >= $summ) {
                $payment = new Payments();
                $payment->paymenttypeid = 3;
                $payment->sum = $summ;
                $payment->save();
                $order->orderstatusid = 1;
                $order->save();
                $activeOrder->delete();
                $card->cardstatusid = 1;
                $card->save();
                $payStatus = "OK";
                $balance->balance = $balance->balance - $summ;
                $balance->save();
                $this->redirect(array('users/index'));
            } else {
                Yii::log("Not enough money on the balance");
            }
        }
    }

    public function actionEdit()
    {
        $id = Yii::app()->user->id;
        $card = Cards::model()->findByAttributes(array('userid'=>$id,'ispublic' => 0));

        if(isset($_POST['Cards']))
        {
            $card->attributes=$_POST['Cards'];

            $rubricids = $_POST['Add']['rubrics'];
            foreach($rubricids as $rubricid){
                $rubric = new RefRubricsCards();
                $rubric->cardid = $card->id;
                $rubric->rubricid = $rubricid;
                $rubric->save();
            }

            if($card->save()){
                $this->redirect(array('index'));
            }
        }

        $this->render('edit', array(
            'card'=>$card
        ));
    }

    public function actionSaveEdit()
    {
        date_default_timezone_set('UTC');
        try {
            $data = CJSON::decode($_POST['data']);
            if ($data) {
                $id = Yii::app()->user->id;
                $card = Cards::model()->findByAttributes(array('userid'=>$id,'ispublic' => 0));

                $transaction = $card->getDbConnection()->beginTransaction();

                if (isset($data['parentcardid'])) {
                    $c = Cards::model()->findByPk($data['parentcardid']);
                    if (isset($c))
                        $card->parentcardid = $data['parentcardid'];
                }

                $card->orgtypeid = $data['orgtypeid'];

                if($data['keywordspackage']) {
                    $card->keywordcountid = $data['keywordcountid'];
                }
                if($data['rubricspackage']) {
                    $card->rubriccountid = $data['rubriccountid'];
                }
                if($data['photospackage']) {
                    $card->photoscountid = $data['photoscountid'];
                }
                $card->regionid = $data['regionid'];
               // $card->cityid = $data['cityid'];
                $card->cityid = 21;
                $card->cardstatusid = 3; // на модерации
                $card->orgname = $data['orgname'];
                $card->description = $data['description'];
                $card->street = $data['street'];
                $card->housenumber = $data['housenumber'];
                $card->office = $data['office'];
                $card->websiteurl = $data['websiteurl'];
                $card->juridicaladdress = $data['juridicaladdress'];
                $card->iinbin = $data['iinbin'];
                $card->bik = $data['bik'];
                $card->iik = $data['iik'];
                $card->kbe = $data['kbe'];
                $card->bankname = $data['bankname'];
                $card->orgindex = $data['orgindex'];
                $card->updatedate = new CDbExpression('NOW()');
                $card->workschedule = $data['workschedule'];
                $card->busnums = $data['busnums'];
                if($data['basepackage']) {
                    $card->months = $data['months'];
                }
                if ($data['top'] == true) {
                    $card->topmonths = $data['topmonths'];
                }

                if($data['basepackage']) {
                    $card->rubricid = $data['rubricid'];
                }

                $card->updatecontactname = $data['updatecontactname'];
                if($data['updatecontactphone'])
                    $card->updatecontactphone = $data['updatecontactphone'];

                $card->accountantname = $data['accountantname'];
                if($data['accountantphone'])
                    $card->accountantphone = $data['accountantphone'];

                if ($card->validate()) {
                    if ($card->save(false))
                    {
                        //map cordinates
                        if(isset($data['coordinates']))
                        {
                            $mapCor = MapCordinates::model()->findByAttributes(array('cardid'=>$card->cardid));
                            if($mapCor == null){
                                $mapCor = new MapCordinates();
                            }
                            $mapCor->cardid =  $card->cardid;
                            $mapCor->valuex = $data['coordinates'][0];
                            $mapCor->valuey = $data['coordinates'][1];
                            $mapCor->save();
                        }


                        //emails
                        $emails = Emails::model()->findAllByAttributes(array('cardid'=>$card->cardid));
                        foreach ($emails as $i) {
                            $i->delete();
                        }
                        foreach ($data['emails'] as $i) {
                            $email = new Emails();

                            $email->cardid = $card->cardid;
                            $email->value = $i;
                            $email->save();
                        }

                        //telephones
                        $telephones = Telephones::model()->findAllByAttributes(array('cardid'=>$card->cardid));
                        foreach ($telephones as $i) {
                            $i->delete();
                        }
                        foreach ($data['telephones'] as $i) {
                            $telephone = new Telephones();

                           /* $telephone->cardid = $card->cardid;
                            $telephone->value = $i;*/

                            $telephone->cardid = $card->cardid;
                            $telephone->value = $i['value'];
                            $telephone->note = $i['note'];
                            $telephone->phonetypeid = $i['PhoneTypeId'];

                            $telephone->save();
                        }

                        //messengers
                        $messengers = Messengers::model()->findAllByAttributes(array('cardid'=>$card->cardid));
                        foreach ($messengers as $i) {
                            $i->delete();
                        }
                        foreach ($data['messengers'] as $i) {
                            $messenger = new Messengers();

                            $messenger->cardid = $card->cardid;
                            $messenger->value = $i['value'];
                            $messenger->imessengertypeid = $i['typeid'];
                            $messenger->save();
                        }

                        //rubrics
                        if($data['rubricspackage']) {
                            $serviceRubric = PaidServiceDetails::model()->findByPk($data['rubriccountid']);
                            $maxAddCountSR = $serviceRubric->count;

                            if ($maxAddCountSR > 0) {
                                $addCount = 0;

                                $rubrics = RefRubricsCards::model()->findAllByAttributes(array('cardid' => $card->cardid));
                                foreach ($rubrics as $i) {
                                    $i->delete();
                                }
                                $rubric = new RefRubricsCards();

                                $rubric->cardid = $card->cardid;
                                $rubric->rubricid = $card->rubricid;
                                $rubric->save();

                                $addCount++;
                                foreach ($data['subrubricids'] as $i) {
                                    $rubric = new RefRubricsCards();

                                    $rubric->cardid = $card->cardid;
                                    $rubric->rubricid = $i;
                                    $rubric->save();

                                    $addCount++;
                                    if ($addCount >= $maxAddCountSR)
                                        break;
                                }
                            }
                        }

                        //keywords
                        if($data['keywordspackage']) {
                            $serviceKeyword = PaidServiceDetails::model()->findByPk($data['keywordcountid']);
                            $maxAddCountKW = $serviceKeyword->count;

                            if ($maxAddCountKW > 0) {
                                $addCount = 0;

                                $keywords = RefKeywordCard::model()->findAllByAttributes(array('cardid' => $card->cardid));
                                foreach ($keywords as $i) {
                                    $i->delete();
                                }
                                foreach ($data['keywords'] as $i) {
                                    $keywordId = Keywords::model()->getIdByValue($i);
                                    if (!$keywordId) {
                                        $newKW = new Keywords();
                                        $newKW->value = $i;
                                        $newKW->save();
                                        $keywordId = $newKW->keywordid;
                                    }
                                    $keyword = new RefKeywordCard();
                                    $keyword->cardid = $card->cardid;
                                    $keyword->keywordid = $keywordId;
                                    $keyword->save();

                                    $addCount++;
                                    if ($addCount >= $maxAddCountKW)
                                        break;
                                }
                            }
                        }

                        //photos
                        if($data['photospackage']) {
                            $photos = CardPhotos::model()->findAllByAttributes(array('cardid' => $card->cardid));
                            foreach ($photos as $i) {
                                $delete = true;
                                foreach ($data['photos'] as $p) {
                                    if ($p['id'] == $i->cardphotoid) {
                                        $delete = false;
                                    }
                                }
                                if ($delete) {
                                    $i->delete();
                                }
                            }
                            for ($i = 0; $i < count($data['uploadphotos']); $i++) {
                                $photo = new CardPhotos();

                                ////
                                $info = pathinfo($_FILES['uploadphoto-' . $i]['name']);
                                $ext = $info['extension']; // get the extension of the file

                                $newname = $this->guid() . '.' . $ext;
                                $fullPath = Yii::app()->params['orgPhotosPath'] . $newname;

                                $allowedExts = array("jpeg", "jpg", "png");

                                $fileType = $_FILES['uploadphoto-' . $i]["type"];

                                if ((($fileType == "image/gif")
                                        || ($fileType == "image/jpeg")
                                        || ($fileType == "image/jpg")
                                        || ($fileType == "image/pjpeg")
                                        || ($fileType == "image/x-png")
                                        || ($fileType == "image/png"))
                                    && ($_FILES['uploadphoto-' . $i]["size"] < 2880 * 1800) // 200 кб
                                    && in_array($ext, $allowedExts)
                                ) {
                                    move_uploaded_file($_FILES['uploadphoto-' . $i]["tmp_name"], $fullPath);
                                }
                                ////

                                $photo->cardid = $card->cardid;
                                $photo->createdate = new CDbExpression('NOW()');
                                $photo->photo = $newname;
                                $photo->save(false);

                            }
                        }

                        //proposals
                        if (count($data['proposals']) >= 0) {
                            $proposals = Proposals::model()->findAllByAttributes(array('cardid' => $card->cardid));
                            foreach ($proposals as $i) {
                                $delete = true;
                                foreach ($data['proposals'] as $p) {
                                    if ($p['id'] == $i->proposalsid) {
                                        $delete = false;
                                    }
                                }
                                if ($delete === true) {
                                    $i->delete();
                                }
                            }
                            for ($i = 0; $i < count($data['proposals']); $i++) {
                                $proposal = Proposals::model()->findByPk($data['proposals'][$i]['id']);
                                if ($proposal == null) {
                                    $proposal = new Proposals();
                                    $proposal->createdate = new CDbExpression('NOW()');
                                }
                                if (isset($_FILES['proposalphoto-' . $i])) {
                                    $info = pathinfo($_FILES['proposalphoto-' . $i]['name']);
                                    $ext = $info['extension']; // get the extension of the file

                                    $newname = $this->guid() . '.' . $ext;
                                    $fullPath = Yii::app()->params['proposalsPath'] . $newname;

                                    $allowedExts = array("jpeg", "jpg", "png");

                                    $fileType = $_FILES['proposalphoto-' . $i]["type"];

                                    if ((($fileType == "image/gif")
                                            || ($fileType == "image/jpeg")
                                            || ($fileType == "image/jpg")
                                            || ($fileType == "image/pjpeg")
                                            || ($fileType == "image/x-png")
                                            || ($fileType == "image/png"))
                                        && ($_FILES['proposalphoto-' . $i]["size"] < 2880 * 1800) // 200 кб
                                        && in_array($ext, $allowedExts)
                                    ) {
                                        move_uploaded_file($_FILES['proposalphoto-' . $i]["tmp_name"], $fullPath);

                                        $proposal->image = $newname;
                                    }
                                }
                                ////

                                $proposal->cardid = $card->cardid;
                                $proposal->publicationstatus = 1;
                                if (isset($data['proposals'][$i]['name'])) $proposal->name = $data['proposals'][$i]['name'];
                                if (isset($data['proposals'][$i]['content'])) $proposal->content = $data['proposals'][$i]['content'];
                                if (isset($data['proposals'][$i]['startpublic'])) $proposal->startpublic = $data['proposals'][$i]['startpublic'];
                                if (isset($data['proposals'][$i]['endpublic'])) $proposal->endpublic = $data['proposals'][$i]['endpublic'];
                                $proposal->version = 0;
                                if (isset($data['proposals'][$i]['rubricid'])) $proposal->rubricid = $data['proposals'][$i]['rubricid'];
                                $proposal->fresh = true;
                                $proposal->updatedate = new CDbExpression('NOW()');
                                $proposal->save(false);

                            }
                        }

                        //vacancies
                        $vacancies = Vacancies::model()->findAllByAttributes(array('cardid'=>$card->cardid));
                        foreach ($vacancies as $i) {
                            $delete = true;
                            foreach($data['vacancies'] as $p){
                                if($p['id'] === $i->vacancyid){
                                    $delete = false;
                                    break;
                                }
                            }
                            if($delete === true){
                                $i->delete();
                            }
                        }
                        for ($i = 0;$i<count($data['vacancies']);$i++) {
                            $vacancy = Vacancies::model()->findByPk($data['vacancies'][$i]['id']);
                            if($vacancy == null){
                                $vacancy = new Vacancies();
                                $vacancy->createdate = new CDbExpression('NOW()');
                            }

                            $vacancy->cardid = $card->cardid;
                            $vacancy->publicationstatusid = 1;
                            if(isset($data['vacancies'][$i]['name']))$vacancy->name = $data['vacancies'][$i]['name'];
                            if(isset($data['vacancies'][$i]['content']))$vacancy->content = $data['vacancies'][$i]['content'];
                            if(isset($data['vacancies'][$i]['startpublic']))$vacancy->startpublic = $data['vacancies'][$i]['startpublic'];
                            if(isset($data['vacancies'][$i]['endpublic']))$vacancy->endpublic = $data['vacancies'][$i]['endpublic'];
                            $vacancy->fresh = false;
                            $vacancy->updatedate = new CDbExpression('NOW()');
                            $vacancy->save(false);

                        }

                    } else {
                        $transaction->rollback();
                    }
                } else {
                    $err = $card->getErrors();
                    $this->sendErrorResponse('error validate new card');
                    $transaction->rollback();
                }

                //сохранить лого
                if(isset($_FILES["logo"]))
                {
                    $info = pathinfo($_FILES['logo']['name']);
                    $ext = $info['extension']; // get the extension of the file

                    $fileName = $this->guid().'.'.$ext; ;
                    $card->logo = $fileName;
                    $fullPath = Yii::app()->params['orgLogosPath'].$fileName;

                    $allowedExts = array("jpeg", "jpg", "png");

                    $fileType = $_FILES["logo"]["type"];

                    if ((($fileType == "image/gif")
                            || ($fileType == "image/jpeg")
                            || ($fileType == "image/jpg")
                            || ($fileType == "image/pjpeg")
                            || ($fileType == "image/x-png")
                            || ($fileType == "image/png"))
                        && ($_FILES["logo"]["size"] < 1024*200) // 200 кб
                        && in_array($ext, $allowedExts)) {
                        move_uploaded_file($_FILES["logo"]["tmp_name"], $fullPath);
                    }
                    else{
                        $transaction->rollback();
                        $this->sendErrorResponse('file invalid');
                    }
                    $card->save();
                }

                //order
                $newOrder = new Orders();
                $newOrder->setStatusAdd();
                $newOrder->createdate = new CDbExpression('NOW()');
                $newOrder->cardid = $card->cardid;

                $oldAcOrder = ActiveOrders::model()->findByAttributes(array('cardid'=>$card->cardid));
                if($oldAcOrder) {
                    $orderdatestart = new DateTime();
                    $orderdatefinish = new DateTime($oldAcOrder->order->publicationdate);
                    $orderdatefinish->add(new DateInterval('P' . ($oldAcOrder->order->months ? $oldAcOrder->order->months : '1') . 'M'));
                    $interval = $orderdatestart->diff($orderdatefinish);
                    if ($interval->days > 0) {
                        $newOrder->publicationdate = new CDbExpression('NOW()');
                    }
                }
                if ($newOrder->save()) {
                    //базовый пакет
                    if($data['basepackage']) {
                        $newOrDetails1 = new OrderDetails();
                        $newOrDetails1->orderid = $newOrder->orderid;
                        $mainrubric = Rubrics::model()->findByPk($card->rubricid);
                        $mainpaidservicedetail = PaidServiceDetails::model()->with('paidservice')->findByPk($mainrubric->paidgroupid);
                        $newOrDetails1->paidservicedetaild = $mainpaidservicedetail->paidservicedetaild;
                        $newOrDetails1->months = $data['months'];
                        if (!$newOrDetails1->save()) {
                            $transaction->rollback();
                            $this->sendErrorResponse('error in save newOrDetails1');
                        }
                    }

                    //ключевые слова цена
                    if($data['keywordspackage']) {
                        if ($data['keywordcountid'] != 70) { //если это не бесплатный пакет
                            $newOrDetails2 = new OrderDetails();
                            $newOrDetails2->orderid = $newOrder->orderid;
                            $newOrDetails2->paidservicedetaild = $data['keywordcountid'];
                            $newOrDetails2->months = $data['months'];
                            if (!$newOrDetails2->save()) {
                                $transaction->rollback();
                                $this->sendErrorResponse('error in save newOrDetails2');
                            }
                        }
                    }

                    //рубрики
                    if($data['rubricspackage']) {
                        if ($data['rubriccountid'] != 11) {
                            $newOrDetails3 = new OrderDetails();
                            $newOrDetails3->orderid = $newOrder->orderid;
                            $newOrDetails3->paidservicedetaild = $data['rubriccountid'];
                            $newOrDetails3->months = $data['months'];
                            if (!$newOrDetails3->save()) {
                                $transaction->rollback();
                                $this->sendErrorResponse('error in save newOrDetails3');
                            }
                        }
                    }
                    //топ
                    if ($data['top'] == true) {
                        $newOrDetails4 = new OrderDetails();
                        $newOrDetails4->orderid = $newOrder->orderid;
                        $newOrDetails4->paidservicedetaild = 21;


                        $startbase = null;
                        $finishbase = null;
                        if($data['basepackage'] == true){
                            $startbase = new DateTime();
                            $finishbase = $startbase->add(new DateInterval('P' . $data['months'] . 'M'));
                        } else {
                            $startbase = $card->getBasePackageStartDate();
                            $finishbase = $card->getBasePackageExpiredDate();
                        }
                        $now = new DateTime();
                        $starttop = new DateTime();
                        $finishtop = $starttop->add(new DateInterval('P' . $data['topmonths'] . 'M'));
                        $intervaltop = $finishtop->diff($finishbase);
                        $intervaltopreal = $now->diff($finishtop);
                        $ttmonths = 1;
                        if (!$intervaltop->invert) {
                            $ttmonths = $data['topmonths'];
                        } else {
                            $interval = $startbase->diff($finishbase);
                            $ttmonths = $data['topmonths'] * $interval->days / $intervaltopreal->days;
                        }

                        $newOrDetails4->months = $ttmonths;
                        if (!$newOrDetails4->save()) {
                            $transaction->rollback();
                            $this->sendErrorResponse('error in save newOrDetails4');
                        }
                    }
                    //в новости
//                    if ($data['main'] == true) {
//                        $newOrDetails7 = new OrderDetails();
//                        $newOrDetails7->orderid = $newOrder->orderid;
//                        $newOrDetails7->paidservicedetaild = 60;
//                        $newOrDetails7->months = $data['months'];
//                        if (!$newOrDetails7->save()) {
//                            $transaction->rollback();
//                            $this->sendErrorResponse('error in save newOrDetails7');
//                        }
//                    }
                    //предложения
//                    if($data['proposalspackage']) {
                        if (count($data['proposals']) > 0) {
                            $newOrDetails5 = new OrderDetails();
                            $newOrDetails5->orderid = $newOrder->orderid;
                            $newOrDetails5->paidservicedetaild = 31;
                            $newOrDetails5->months = $data['months'];
                            if (!$newOrDetails5->save()) {
                                $transaction->rollback();
                                $this->sendErrorResponse('error in save newOrDetails5');
                            }
//                        }
                    }
                    //картинки
                    if($data['photospackage']) {
                        if ($data['photoscountid'] != 50) {
                            $newOrDetails6 = new OrderDetails();
                            $newOrDetails6->orderid = $newOrder->orderid;
                            $newOrDetails6->paidservicedetaild = $data['photoscountid'];
                            $newOrDetails6->months = $data['months'];
                            if (!$newOrDetails6->save()) {
                                $transaction->rollback();
                                $this->sendErrorResponse('error in save newOrDetails6');
                            }
                        }
                    }

                }
                else
                {
                    $transaction->rollback();
                    $this->sendErrorResponse('error save in new order');
                }


                $newActiveOrder = new ActiveOrders();
                $newActiveOrder->orderid = $newOrder->orderid;
                $newActiveOrder->cardid = $card->cardid;

                $oldAcOrders = ActiveOrders::model()->findAllByAttributes(array('cardid'=>$card->cardid));
                foreach($oldAcOrders as $oldAcOrder) {
                    $oldAcOrder->delete();
                }
                if (!$newActiveOrder->save()) {
                    $transaction->rollback();
                    $this->sendErrorResponse('error save in new active order');
                }

                $transaction->commit();
                $this->sendSuccessResponse('ok');

            } else {
                $this->sendErrorResponse('error validate data');
            }
        }
        catch (Exception $ex) {
            $transaction->rollback();
            print_r($ex);
            $this->sendErrorResponse('main error');
        }

    }

    public function actionCreateProposal(){

        $id = Yii::app()->user->id;
        if($id){
            $card = Cards::model()->findByAttributes(array('userid'=>$id,'ispublic' => 0));

            $proposal = new Proposals();
            $proposal->publicationstatusid = 1;
            $proposal->fresh = 1;
            $proposal->name = 'Новое предложение';
            $proposal->content = '';
            $proposal->image = '';
            $proposal->version = '';
            $proposal->endpublic = new CDbExpression('NOW()');
            $proposal->startpublic = new CDbExpression('NOW()');
            $proposal->createdate = new CDbExpression('NOW()');
            $proposal->updatedate = new CDbExpression('NOW()');
            $proposal->rubricid = Rubrics::model()->findByAttributes(array('rubrictypeid'=>2))->rubricid;
            $proposal->cardid = $card->cardid;
            if($proposal->save()){
                $this->sendData(Proposals::model()->findByPk($proposal->proposalsid));
            }
        }
        $this->sendErrorResponse('error');
    }
    public function disableProfilers()
    {
        if (Yii::app()->getComponent('log')) {
            foreach (Yii::app()->getComponent('log')->routes as $route) {
                if (in_array(get_class($route), array('CProfileLogRoute', 'CWebLogRoute', 'YiiDebugToolbarRoute','DbProfileLogRoute'))) {
                    $route->enabled = false;
                }
            }
        }
    }
    public function actionGetInvoice(){
        $id = Yii::app()->user->id;
        if($id) {
            $user = Users::model()->findByPk($id);
            $card = Cards::model()->findByAttributes(array('userid'=>$user->userid,'ispublic'=>0));
            $publiccard = Cards::model()->findByAttributes(array('userid'=>$user->userid,'ispublic'=>1));
            $orders = Orders::model()->findAllByAttributes(array('cardid'=>$card->cardid));
            $activeorder = ActiveOrders::model()->findByAttributes(array('cardid'=>$card->cardid));
            $filename = $activeorder->order->activeInvoices->invoice->invoiceid.'.pdf';
            //отключить профайлеры
//            $this->disableProfilers();
            $file = Yii::getPathOfAlias('webroot').'/storage/invoices/'. $filename;
            // отдаем файл
            Yii::app()->request->sendFile(basename($file), file_get_contents($file));
        }
    }

    public function actionCreateVacancy(){

        $id = Yii::app()->user->id;
        if($id){
            $card = Cards::model()->findByAttributes(array('userid'=>$id,'ispublic' => 0));

            $vacancy = new Vacancies();
            $vacancy->publicationstatusid = 1;
            $vacancy->fresh = 1;
            $vacancy->name = 'Новая вакансия';
            $vacancy->content = '';
            $vacancy->endpublic = new CDbExpression('NOW()');
            $vacancy->startpublic = new CDbExpression('NOW()');
            $vacancy->createdate = new CDbExpression('NOW()');
            $vacancy->updatedate = new CDbExpression('NOW()');
            $vacancy->cardid = $card->cardid;

            if($vacancy->save()){
                $this->sendData(Vacancies::model()->findByPk($vacancy->vacancyid));
            }
        }
        $this->sendErrorResponse('error');
    }

    function guid(){
        if (function_exists('uuid_create')){
            $res = uuid_create();
            str_replace('}', '', $res);
            str_replace('{', '', $res);
            return $res;
        }else{
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = // "{"
                substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12);// "}"
            return $uuid;
        }
    }
}
