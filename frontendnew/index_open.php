<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <script src="js/jquery-2.1.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.liquidcarousel.js"></script>
    <script src="js/cookie.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="css/template.css">
    <link rel="stylesheet" href="css/modules.css">
    <link rel="stylesheet" href="css/screen.css">
</head>
<body>
<div id="content-block">
<div class="row header center-content">
    <div class="span12">
        <div class="span6 p-relative">
            <div class="logo">
                <a href="/">
                    <img src="images/logo.png" alt="INFOSMART" title="INFOSMART">
                </a>
            </div>
            <div class="citywidget">
                <div class="cityname">
                    <span class="city-arrow"></span>
                    <span class="current-city">Астана</span>
                </div>
                <div class="city-list">
                    <span class="city-list-arrow"></span>
                    <label for="city-ul">Выберите город:</label>
                    <ul class="city-ul" id="city-ul">
                        <li>Все города</li>
                        <li>Алмата</li>
                        <li>Астана</li>
                        <li>Длинное название города</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="span6">
            <ul class="mainmenu">
                <li>
                    <a class="deals" href="deals.php">
                        <span>предложения</span>
                    </a>
                </li>
                <li>
                    <a class="map" href="map.php">
                        <span>карта</span>
                    </a>
                </li>
                <li>
                    <a class="news" href="#">
                        <span>новости</span>
                    </a>
                </li>
                <li>
                    <a class="personal-area" href="#">
                        <span>личный кабинет</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="row news">
    <div class="span12 position-relatives">
        <div class="newsblock open-block">
            <a href="#">
                <div class="newscontent">
                    <img src="images/banner1.png">
                    <div class="ptext">
                        Зарина Дияс вошла в топ-40 лучших теннисисто...
                    </div>
                </div>
            </a>

            <a href="#">
                <div class="newscontent">
                    <img src="images/banner2.png">
                    <div class="ptext">
                        Ресторан японской кухни Angry Fish: скидка 50%...
                    </div>
                </div>
            </a>

            <a href="#">
                <div class="newscontent">
                    <img src="images/banner3.png">
                    <div class="ptext">
                        D-Fitness - борьба со стрессом выгодными ценой!
                    </div>
                </div>
            </a>

            <a href="#">
                <div class="newscontent">
                    <img src="images/banner4.png">
                    <div class="ptext">
                        Тариф «Выгодный смартфон» от Актив!
                    </div>
                </div>
            </a>

            <a href="#">
                <div class="newscontent">
                    <img src="images/banner5.png">
                    <div class="ptext">
                        Новая услуга проверки авто введена а Астане
                    </div>
                </div>
            </a>

            <a href="#">
                <div class="newscontent">
                    <img src="images/banner6.png">
                    <div class="ptext">
                        Ресторан японской кухни Angry Fish: скидка 50%...
                    </div>
                </div>
            </a>

            <a href="#">
                <div class="newscontent">
                    <img src="images/banner1.png">
                    <div class="ptext">
                        Зарина Дияс вошла в топ-40 лучших теннисисто...
                    </div>
                </div>
            </a>

            <a href="#">
                <div class="newscontent">
                    <img src="images/banner2.png">
                    <div class="ptext">
                        Ресторан японской кухни Angry Fish: скидка 50%...
                    </div>
                </div>
            </a>

            <a href="#">
                <div class="newscontent">
                    <img src="images/banner3.png">
                    <div class="ptext">
                        D-Fitness - борьба со стрессом выгодными ценой!
                    </div>
                </div>
            </a>
            <div class="news-share-buttons">
                <a href="#" class="deals-button">предложения</a>
                <a href="#" class="news-button">новости</a>
                <div class="clr"></div>
            </div>
        </div>
        <div class="clr"></div>
        <span class="more-less-news" id="moreLessNews"></span>
    </div>
</div>
<div class="row search center-content">
    <div class="span12 small-span">
        <div class="searchform">
            <form action="#">
                <input type="text" name="search" placeholder="Введите ключевое слово" class="searchinput">
                <input type="submit" name="button" class="searchbutton">

                <div class="checkbox-div">
                    <div class="search-rubric all-rubrics">
                        <span class="allrubrics-bg"></span> Все рубрики
                    </div>
                </div>
                <div class="checkbox-div">
                    Популярные:
                    <div class="search-rubric single-rubric">
                        <span class="car-bg"></span>Автомойки
                    </div>
                    <div class="search-rubric single-rubric">
                        <span class="bag-bg"></span>Хан шатыр
                    </div>
                    <div class="search-rubric single-rubric">
                        <span class="bag-bg"></span>Продуктовые магазины
                    </div>
                    <div class="search-rubric single-rubric">
                        <span class="car-bg"></span>СТО, техобслуживание
                    </div>
                </div>
            </form>
            <div class="clr"></div>
        </div>
    </div>
</div>

<div class="row center-content infosmart-adv">
    <div class="span12 small-span">
        <div class="span6 p-relative"><img src="images/mobile_app.png" alt="INFOSMART" title="INFOSMART"></div>
        <div class="span6">
            <h1>INFOSMART всегда под рукой</h1>
            <p class="adv-info">Ищете место для отдыха, хотите устроить шопинг, требуются различные услуги?</p>
            <p class="adv-info"><strong>Все что Вы искали в вашем городе, в при&shy;ложении для мобильного телефона INFOSMART</strong></p>
            <div class="button-row">
                <a href="#" title="App Store" class="appstore-link"></a>
                <a href="#" title="Google play" class="googleplay-link"></a>
            </div>
        </div>
    </div>
</div>

</div>
<div class="row footer">
    <ul class="footermenu">
        <li>
            <a href="#">
                <span>О сервисе</span>
            </a>
        </li>
        <li>
            <a href="#">
                <span>О нас</span>
            </a>
        </li>
        <li>
            <a href="#">
                <span>Пользовательское соглашение</span>
            </a>
        </li>
        <li>
            <a href="#">
                <span>Мобильное приложение</span>
            </a>
        </li>
    </ul>
    <span class="footer-copirights">&copy;2014 ТОО "Smart Selling Group". Контакты для вопросов размещения на сайте: </span>
    <span class="footer-email">email: zhiga_@mail.ru, </span>
    <span class="footer-phone">тел: +7 778 152 24 33, +7 7172 522 466</span>
</div>
<script>
    /* Запускаем сразу после загрузки страницы */
    $(document).ready(function () {
//    newsBlocks();
        // Читаем из куков текущий город
        var currentCity = readCookie("currentCity");
        currentCity = (currentCity != null) ? currentCity : $(".city-ul li").eq(0).text();
        $(".current-city").text(currentCity);

        $(".citywidget").click(function () {
            rotateBG();
            if ($(".city-list").css('display') == "block") {
                $(".city-list").slideUp();
                $(".citywidget").removeClass('active');
            } else {
                $(".citywidget").addClass('active');
                $(".city-list").slideDown();
            }

        });

        $(".city-ul li").click(function () {
            var selectedCity = $(this).text();
            $(".current-city").text(selectedCity);
            // Записываем в куки текущий город
            writeCookie("currentCity", selectedCity, 7);
            rotateBG();
            $(".city-list").slideUp();
            $(".citywidget").removeClass('active');
        });
        $('.newsblock.carousel-block').liquidcarousel({height:252});

        $(document).click(function(e) {
            if ($(e.target).parents().filter('.citywidget').length == 0 && $(e.target).attr("class") != "citywidget") {
                $(".citywidget").removeClass('active');
                $(".city-list").slideUp();
            }
        });

        $("#moreLessNews").click(function () {
            if ($(".more-less-news.active").length > 0) {
                $(this).removeClass('active');
            } else {
                $(this).addClass('active');
            }
        });
    });

    function rotateBG() {
        if ($(".city-arrow").is(".rotate-bg")) {
            $(".city-arrow").removeClass("rotate-bg");
        } else {
            $(".city-arrow").addClass("rotate-bg");
        }
    }
</script>
</body>
</html>