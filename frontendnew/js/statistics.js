
var desktop_source = [];
var mobile_source = [];
var definition = {
    "decimal": ",",
    "thousands": "\xa0",
    "grouping": [3],
    "currency": ["", " руб."],
    "dateTime": "%A, %e %B %Y г. %X",
    "date": "%d.%m.%Y",
    "time": "%H:%M:%S",
    "periods": ["AM", "PM"],
    "days": ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"],
    "shortDays": ["вс", "пн", "вт", "ср", "чт", "пт", "сб"],
    "months": ["январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"],
    "shortMonths": ["янв", "фев", "мар", "апр", "май", "июн", "июл", "авг", "сен", "окт", "ноя", "дек"]
};
var format = d3.locale(definition);
d3.time.format = format.timeFormat;
$.ajax(
    {
        url: '/cards/GetStatistics',
        type: 'POST',
        dataType: 'json',
        data: {
            cardId: $("#statistic").attr("data-id")
        },
        success: function (result) {
            console.log(result)
            desktop_source = result.desktop;
            mobile_source = result.mobile;

            refreshStatistics();
        }
    });

(function(){
    $('#site-stat-period').on('change', function (e) {
        console.log(e.target.value)
        createChart('#site-stat', redate(+e.target.value, desktop_source), +e.target.value);
    });
    $('#mobile-stat-period').on('change', function (e) {
        createChart('#mobile-stat', redate(+e.target.value, mobile_source), +e.target.value);
    });
})();


function refreshStatistics() {
    createChart('#site-stat', redate(2, desktop_source), 2);
    createChart('#mobile-stat', redate(2, mobile_source), 2);
}


//TODO: bug with multi select
function createChart(selector, data, type) {
    console.log(data);
    $(selector).empty();
    var svg = dimple.newSvg(selector, $(selector).width(), $(selector).height());
    var myChart = new dimple.chart(svg, data);
    myChart.setBounds(60, 30, $(selector).width() - 85, $(selector).height() - 85);
    switch (type) {
        case 1:
        {
            var x = myChart.addTimeAxis("x", "key", "%B", "%B");//
            x.title = 'дата'
            x.timePeriod = d3.time.months;//
            x.timeInterval = 2;

            var y = myChart.addMeasureAxis("y", "value");
            y.title = "количество просмотров";
            y.tickFormat = "d";

            break;
        }
        case 2:
        {
            var x = myChart.addTimeAxis("x", "key", "%d/%m/%Y", "%d/%m/%Y");//
            x.title = 'дата'
            x.timePeriod = d3.time.week;//
            x.timeInterval = 1;

            var y = myChart.addMeasureAxis("y", "value");
            y.title = "количество просмотров";
            y.tickFormat = "d";

            break;
        }
        case 3:
        {
            var x = myChart.addTimeAxis("x", "key", "%d/%m/%Y", "%d/%m/%Y");//
            x.title = 'дата'
            x.timePeriod = d3.time.day;//
            x.timeInterval = 1;

            var y = myChart.addMeasureAxis("y", "value");
            y.title = "количество просмотров";
            y.tickFormat = "d";

            break;
        }
    }

    var result = {
        chart: myChart,
        type: type
    };
    var s = myChart.addSeries(null, dimple.plot.line);
    s.getTooltipText = tooltip.bind(result);
    myChart.draw();
    return result;
}

function tooltip(e) {
    var format = '';
    switch (this.type) {
        case 1:
        {
            format = '%B';
            break;
        }
        case 2:
        {
            format = '%d %b%'
            break;
        }
        case 3:
        {
            format = '%d %b %A';
            break;
        }
    }
    return [
        "Дата: " + d3.time.format(format)(new Date(e.x)),//
        "Количество посещений: " + e.yValue
    ];
};

function redate(type, source){
    var now = new Date();
    var start = new Date();
    switch(type)
    {
        case 1:
            start.setFullYear(start.getFullYear()-1);
            break;
        case 2:
            start.setFullYear(start.getFullYear(), start.getMonth()-1);
            break;
        case 3:
            start.setFullYear(start.getFullYear(), start.getMonth(), start.getDate()-7);
            break;
    }
    console.log(start)
    var res = [];
    var range = type == 1?d3.time.month.range(start, now, 1):d3.time.day.range(start, now, 1);
    console.log(range.length)
    for (var i = 0; i < range.length; i++) {
        if(type == 1){
            res.push({
                key: d3.time.format('%B')(range[i]),
                value: getMonthDataFromSource(source, d3.time.format('%m/%Y')(range[i]))
            });
        } else {
            res.push({
                key: d3.time.format('%d/%m/%Y')(range[i]),
                value: getDataFromSource(source, d3.time.format('%d/%m/%Y')(range[i]))
            });
        }
    }
    return res;
}

function getDataFromSource(source, date){
    var value = 0;

    for(var i = 0;i<source.length;i++){
        if(source[i].date == date){
            return source[i].count;
        }
    }
    return value;
}
function getMonthDataFromSource(source, date){
    var value = 0;
    var sum = 0;
    for(var i = 0;i<source.length;i++){
        if(d3.time.format('%m/%Y')(d3.time.format('%d/%m/%Y').parse(source[i].date)) == date){
            sum += +source[i].count;
            console.log(sum);
        }
    }
    return sum;
}
