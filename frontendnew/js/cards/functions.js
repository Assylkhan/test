function ajaxmodal(page, cardid) {
    $("#loadingDiv").show();
    $.ajax(
        {
            url: '/cards/GetCard',
            type: 'POST',
            dataType: 'json',
            data: {
                pageId: page,
                cardId: cardid
            },
            success: function (data) {
                //console.log(data);
                openModal("general_modal", data.header, data.body);
                $('#loadingDiv').hide();

                if(typeof init == 'function'){
                    init();
                }

                $("#zh_payment").on("change", function(){
                    if($(this).val()){
                        $("#form-pay").css("display", "block");
                        $("#form-check").css("display", "block");
                    } else {
                        $("#form-pay").css("display", "block");
                        $("#form-check").css("display", "none");
                    }
                });
                $("#zh_payment2").on("change", function(){
                    if($(this).val()){
                        $("#form-pay").css("display", "block");
                        $("#form-check").css("display", "none");
                    } else {
                        $("#form-pay").css("display", "block");
                        $("#form-check").css("display", "block");
                    }
                });
                $("#zh_payment_none").on("change", function(){
                    if($(this).val()){
                        $("#form-pay").css("display", "none");
                    } else {
                        $("#form-pay").css("display", "block");
                    }
                });

            }
        });
}

function openModal(id, header, body) {
    //console.log(body);
    $("#" + id).html(body);
    //console.log($("#"+id));
    $('#' + id).modal('show');

}


function setNotPublished()
{
    var ids = $.fn.yiiGridView.getChecked('cards-grid','selectedItems');
    console.log(ids);
    $("#loadingDiv").show();
    $.ajax(
        {
            url: '/moderator/CardUnPublish',
            type: 'POST',
            dataType: 'json',
            data: {
                ids: ids
            },
            success: function (data) {
                console.log(data);
                if(data.message == 'success')
                {
                    location.reload();

                }
                else
                alert('Произошла ошибка');
                $('#loadingDiv').hide();
            }
        });
}



$(document).ready(function () {

    $('body').on('dblclick', '#cards-grid tbody tr', function(event)
    {
        console.log('asas');
        var id = $.fn.yiiGridView.getKey(
            'cards-grid',
            $(this).prevAll().length
        );
        ajaxmodal('general', id);
    });


});