function ajaxmodal(page, cardid) {
    $("#loadingDiv").show();
    $.ajax(
        {
            url: '/cards/GetCard',
            type: 'POST',
            dataType: 'json',
            data: {
                pageId: page,
                cardId: cardid
            },
            success: function (data) {
                console.log(data);
                openModal("general_modal", data.header, data.body);
                $('#loadingDiv').hide();
                init();
            }
        });
}
function ajaxmodaluser(page, cardid) {
    $("#loadingDiv").show();
    var data = getCardForSave();
    $.ajax(
        {
            url: '/cards/GetTempCard',
            type: 'POST',
            dataType: 'json',
            data: {
                pageId: page,
                cardId: cardid,
                card: data
            },
            success: function (data) {
                console.log(data);
                openModal("general_modal", data.header, data.body);
                $('#loadingDiv').hide();
                init();
            }
        });
}

function openModal(id, header, body) {
    //console.log(body);
    $("#" + id).html(body);
    //console.log($("#"+id));
    $('#' + id).modal('show');

}

$(document).ready(function () {
    $("#cards a.name").click(function (e) {
        var id = $(e.currentTarget).attr('data-id');
        ajaxmodal('general', id);
        return false;
    });
    $(".chargeable-unit a.name").click(function (e) {
        var id = $(e.currentTarget).attr('data-id');
        ajaxmodal('general', id);
        return false;
    });
    $(".preview-button").click(function (e) {
        var id = $(e.currentTarget).attr('data-id');
        ajaxmodal('general', id);
        return false;
    });
});