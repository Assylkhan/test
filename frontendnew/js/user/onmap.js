(function() {
    ymaps.ready(init);

    function init() {
        var adress = 'астана достык 13';
        var position = null;
        var active = false;
        var myMap;
        var myPlacemark;
        myMap = new ymaps.Map('map', {
            // При инициализации карты обязательно нужно указать
            // её центр и коэффициент масштабирования.
            center: [51.1379457, 71.4503897], // Astana
            zoom: 10
        });

        if (adress) {

            ymaps.geocode(adress, {
                /**
                 * Опции запроса
                 * @see http://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/geocode.xml
                 */
                // boundedBy: myMap.getBounds(), // Сортировка результатов от центра окна карты
                // strictBounds: true, // Вместе с опцией boundedBy будет искать строго внутри области, указанной в boundedBy
                results: 1 // Если нужен только один результат, экономим трафик пользователей
            }).then(function (res) {
                // Выбираем первый результат геокодирования.
                var firstGeoObject = res.geoObjects.get(0),
                // Координаты геообъекта.
                    coords = firstGeoObject.geometry.getCoordinates(),
                // Область видимости геообъекта.
                    bounds = firstGeoObject.properties.get('boundedBy');

                // Добавляем первый найденный геообъект на карту.
                //myMap.geoObjects.add(firstGeoObject);
                // Масштабируем карту на область видимости геообъекта.
                myMap.setBounds(bounds, {
                    checkZoomRange: true // проверяем наличие тайлов на данном масштабе.
                });

                /**
                 * Если нужно добавить по найденным геокодером координатам метку со своими стилями и контентом балуна, создаем новую метку по координатам найденной и добавляем ее на карту вместо найденной.
                 */

                myPlacemark = new ymaps.Placemark(coords, {
                    iconContent: adress,
                    balloonContent: adress
                }, {
                    preset: 'islands#violetStretchyIcon',
                    draggable: true
                });
                myPlacemark.events.add("dragend", function (event) {
                    position = myPlacemark.geometry.getCoordinates();
                    console.log(position);
                    getAddress(myPlacemark.geometry.getCoordinates());
                });

                myMap.geoObjects.add(myPlacemark);

            });
        }

        // Слушаем клик на карте
        myMap.events.add('click', function (e) {
            console.log(active)
            console.log(e.get('coords'))
            coords = e.get('coords');

            // Если метка уже создана – просто передвигаем ее
            if (myPlacemark) {
                myPlacemark.geometry.setCoordinates(coords);
            }
            // Если нет – создаем.
            else {
                myPlacemark = createPlacemark(coords);
                myMap.geoObjects.add(myPlacemark);
                // Слушаем событие окончания перетаскивания на метке.
                myPlacemark.events.add('dragend', function () {
                    position = myPlacemark.geometry.getCoordinates();
                    console.log(position);
                    getAddress(myPlacemark.geometry.getCoordinates());
                });
            }
            getAddress(coords);
            position = coords;
        });

        function createPlacemark(coords) {
            return new ymaps.Placemark(coords, {
                iconContent: 'поиск...'
            }, {
                preset: 'islands#violetStretchyIcon',
                draggable: true
            });
        }

        function getAddress(coords) {
            myPlacemark.properties.set('iconContent', 'поиск...');
            ymaps.geocode(coords).then(function (res) {
                var firstGeoObject = res.geoObjects.get(0);

                myPlacemark.properties
                    .set({
                        iconContent: firstGeoObject.properties.get('name'),
                        balloonContent: firstGeoObject.properties.get('text')
                    });
            });
        }
    }
})