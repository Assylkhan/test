var dataForSaveModeration =
{
    action: '',
    cardStatus: -1,
    orderStatus: -1,
    payment: {
        checkNumber: '',
        checkSum: 0
    },
    copyToEmail : false,
    mailContent: '',
    cardid: -1,
    file: {}
}

function copyEmail()
{
    if($('#zh_mail').attr('checked')){
        dataForSaveModeration.copyToEmail = true;
    }
    else
        dataForSaveModeration.copyToEmail = false;
    dataForSaveModeration.mailContent = $('#zh_message').val();
}

function reject()
{
    dataForSaveModeration.action = 'reject';
    dataForSaveModeration.cardStatus = 'adjustments';
    dataForSaveModeration.orderStatus = '';
    copyEmail();
    sendToModeration();
}

function approve()
{
    dataForSaveModeration.action = 'approve';
    if($('#zh_payment').attr('checked')){
        dataForSaveModeration.cardStatus = 'published';
        dataForSaveModeration.orderStatus = 'paid';

        var checkNum  = $('#checkNumber').val();

        if(!checkNum || checkNum.length ==0)
        {
            alert('Укажите номер чека');
            return;
        }

        var sum = $('#checkSum').val();
        if(!sum || sum < 0)
        {
            alert('Укажите сумму');
            return;
        }
        dataForSaveModeration.payment.checkNumber = checkNum;
        dataForSaveModeration.payment.checkSum = sum;
    } else if($('#zh_payment2').attr('checked')){
        dataForSaveModeration.cardStatus = 'published';
        dataForSaveModeration.orderStatus = 'paid';

        var checkNum  = $('#checkNumber').val();

        if(!checkNum || checkNum.length == 0)
        {
            alert('Укажите номер платежа');
            return;
        }

        var sum = $('#checkSum').val();
        if(!sum || sum < 0)
        {
            alert('Укажите сумму');
            return;
        }
        dataForSaveModeration.payment.checkNumber = checkNum;
        dataForSaveModeration.payment.checkSum = sum;
    } else
    {
        dataForSaveModeration.cardStatus = 'moderation';
        dataForSaveModeration.orderStatus = 'invoicing';
    }
    copyEmail();
    sendToModeration();
}

function sendToModeration()
{
    $("#loadingDiv").show();
    dataForSaveModeration.cardid = $('#cardId').text();

    var formData = new FormData();
    jQuery.each($('#fileImportData')[0].files, function(i, file) {
        formData.append('uploadFile-'+i, file);
    });

    formData.append('data',  JSON.stringify(dataForSaveModeration));

    jQuery.ajax({
        url: '/moderator/SendToModeration',
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        success: function(data, textStatus, jqXHR){

            data = jQuery.parseJSON(data);
            if(data.status == 'success')
            {
                $('#general_modal').modal('hide');
                alert('Сохранено');
                location.reload(true);
            }
            else if(data.status == 'error')
            {
                if(data.message == 'file invalid')
                {
                    alert('Не удается сохранить: прикрепленный файл не соответсвует требованиям');
                }
                else if(data.message == 'activeInvoice not set')
                {
                    alert('Еще не выставлен счет на оплату');
                }
                else
                {
                    alert('Не удается сохранить. Ошибка! Обратитесь к разработчику');
                }
            }

            $('#loadingDiv').hide();

        }
    });
}



