function showLoginWindow()
{
    $('#auth-error').hide();
    $('#general_modallogin').modal('show');
}


function auth(form, data, hasError)
{
    if (!hasError){
        var str = $("#login-form").serialize() + "&ajax=login-form";

        console.log('ajax send');
        $.ajax({
            type: "POST",
            url: '/site/AjaxLogin',
            data: str,
            dataType: "json",
            beforeSend : function() {
                $("#login").attr("disabled",true);
            },
            success: function(data, status) {
                //console.log('ajax callback');
                console.log(data);
                if(data.authenticated)
                {
                    window.location = data.redirectUrl;
                }
                else
                {

                    $.each(data, function(key, value) {
                        var div = "#"+key+"_em_";
                        $(div).text(value);
                        $(div).show();
                    });
                    $('#auth-error').show();
                    $("#login").attr("disabled",false);
                }
            }
        });
        return false;
    }
}

$(document).ready(function () {

    //console.log('*'+$('#showlogin').text()+'*');
    if ($('#showlogin').text() == "true") {
        //console.log('open');
        $('#general_modallogin').modal('show');
    }
});
