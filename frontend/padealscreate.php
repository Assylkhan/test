<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <script src="js/jquery-2.1.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.liquidcarousel.js"></script>
    <script src="js/cookie.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="css/template.css">
    <link rel="stylesheet" href="css/modules.css">
    <link rel="stylesheet" href="css/screen.css">
    <script>
        /* Запускаем сразу после загрузки страницы */
        $( document ).ready(function() {
            // Читаем из куков текущий город
            var currentCity = readCookie("currentCity");
            currentCity = (currentCity != null) ? currentCity : $(".city-ul li").eq(0).text();
            $(".current-city").text(currentCity);

            $('.upbutton').click(function(){
                $('html, body').animate({scrollTop:0}, 'slow');
            });

            $(".selectcity").click(function(){
                rotateBG();
                $(".city-ul").slideToggle();
            });

            $(".city-ul li").click(function(){
                var selectedCity = $( this).text();
                $(".current-city").text(selectedCity);
                // Записываем в куки текущий город
                writeCookie("currentCity", selectedCity, 7);
                rotateBG();
                $(".city-ul").slideToggle();
            });
        });

        function rotateBG() {
            if ( $(".city-arrow").is(".rotate-bg") ) {
                $(".city-arrow").removeClass("rotate-bg");
            } else {
                $(".city-arrow").addClass("rotate-bg");
            }
        }
    </script>
</head>
<body>
<div id="content-block">
    <div class="row header center-content">
        <div class="span12">
            <div class="span6 p-relative lessspan">
                <div class="logo">
                    <a href="/">
                        <img src="images/logo.png" alt="INFOSMART" title="INFOSMART">
                    </a>
                </div>
                <div class="citywidget">
                    <div class="cityname">
                        <span class="city-arrow"></span>
                        <span class="current-city">Астана</span>
                    </div>
                    <span class="selectcity">выберите город</span>
                    <ul class="city-ul">
                        <li>Астана</li>
                        <li>Москва</li>
                        <li>Лондон</li>
                        <li>Деревня Лопухи</li>
                        <li>Бишкек</li>
                        <li>Астраханка</li>
                        <li>Вашингтон</li>
                        <li>Иваново</li>
                        <li>Прага</li>
                        <li>Степногорск</li>
                        <li>Павлодар</li>
                    </ul>
                </div>
                <div class="infowidget">
                    <span class="weather-day">+18&deg;<span class="dotes">...</span>+20&deg;</span>
                    <span class="weather-night">+4&deg;<span class="dotes">...</span>+6&deg;</span>
                    <span class="dollar">185.5<span class="depreciation"></span></span>
                    <span class="euro">250.2<span class="growth-rate"></span></span>
                    <span class="rub">5.4<span class="depreciation"></span></span>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="span6 morespan">
                <ul class="mainmenu">
                    <li>
                        <a class="deals hover-green" href="deals.php">предложения</a>
                    </li>
                    <li>
                        <a class="map hover-green" href="#">карта</a>
                    </li>
                    <li>
                        <a class="news hover-green" href="#">новости</a>
                    </li>
                    <li>
                        <a class="personal-area hover-green" href="#">личный кабинет</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row registration center-content">
        <div class="span12">
            <h1>Личный кабинет - <span>Название предприятия</span></h1>
            <button class="preview-button hover-green">просмотр карточки</button>
        </div>
        <div class="span12 personal-menu">
            <ul>
                <li><a href="generalinformation.php" id="general">Общая информация</a></li>
                <li><a href="onmap.php" id="onmap">На карте</a></li>
                <li><a href="padeals.php" id="deals" class="active">Предложения</a></li>
                <li><a href="#" id="foto">Фото</a></li>
                <li><a href="#" id="job">Вакансии</a></li>
            </ul>
        </div>
        <div class="span12 registration2">
            <div class="span6 morespan">
                <div class="add-block">
                    <span class="add-text">
                        Информация для пользователя (например, стоимость 1 предложения,
                        предложение от дизайнера компании)
                    </span>
                    <button class="add-button hover-green">добавить предложение</button>
                </div>
                <div class="create-header">
                    <h2>Предложение №458965874233</h2>
                    <button class="public-remove hover-green">снять с публикации</button>
                </div>
                <table class="formtable">
                    <tbody>
                    <tr>
                        <td>
                            <label for="dealName">Название предложения<span class="required">*</span>:</label>
                            <span class="explanation">пояснение к пункту</span>
                        </td>
                        <td>
                            <input name="dealName" id="dealName" class="registration-input">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="category">Категория:</label>
                            <span class="explanation">пояснение к пункту</span>
                        </td>
                        <td>
                            <select name="category" id="category" class="registration-select">
                                <option value="1">категори 1</option>
                                <option value="2">категори 2</option>
                                <option value="3">категори 3</option>
                                <option value="4">категори 4</option>
                                <option value="5">категори 5</option>
                                <option value="6">категори 6</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="date1">Дата публикации:</label>
                            <span class="explanation">пояснение к пункту</span>
                        </td>
                        <td>
                            <input name="date1" id="date1" class="registration-input-date">
                            <input name="date2" id="date2" class="registration-input-date">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="logo">Картинка<span class="required">*</span>:</label>
                            <span class="explanation">пояснение к пункту</span>
                        </td>
                        <td>
                            <input type="file" name="logo" id="logo">
                            <span class="delete-span">удалить изображение</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="description">Описание предложения:</label>
                            <span class="explanation">пояснение к пункту</span>
                        </td>
                        <td>
                            <textarea name="description" id="description" class="registration-textarea"></textarea>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="span6 lessspan">
                <table class="infotable table-striped">
                    <tbody>
                    <tr>
                        <td colspan="2" class="info-label">На аккаунте</td>
                    </tr>
                    <tr>
                        <td>карточка предприятия, включая 3 рубрики, 3 подрубрики, 50 ключевых слов</td>
                        <td>99 999тг</td>
                    </tr>
                    <tr>
                        <td>выделение карточки в топ</td>
                        <td>99 999тг</td>
                    </tr>
                    <tr>
                        <td>дополнительное количество подкатегорий - 14</td>
                        <td>99 999тг</td>
                    </tr>
                    <tr>
                        <td>увеличенное количество ключевых слов - 150</td>
                        <td>99 999тг</td>
                    </tr>
                    <tr>
                        <td>увеличенное</td>
                        <td>99 999тг</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="info-label">Корзина</td>
                    </tr>
                    <tr>
                        <td>пункт корзины</td>
                        <td>99 999тг</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="span12 registration-buttons">
            <button class="cancel-step hover-green">отмена</button>
            <button class="next-step hover-green">сохранить и отправить на модерацию</button>
        </div>
    </div>
    <div class="wrap"></div>
</div>
<div class="row footer">
    <span class="upbutton">наверх</span>
    <ul class="footermenu">
        <li>
            <a href="#">Lorem</a>
        </li>
        <li>
            <a href="#">Ipsum</a>
        </li>
        <li>
            <a href="#">Dolor sit</a>
        </li>
        <li>
            <a href="#">Amet</a>
        </li>
        <li>
            <a href="#">Consectetur</a>
        </li>
        <li>
            <a href="#">Consectetur adipisicing</a>
        </li>
    </ul>
    <span class="footer-copirights">&copy;2014 elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</span>
    <span class="footer-email">email: zhiga_@mail.ru</span>
    <span class="footer-phone">тел: +7 778 152 24 33, +7 7172 522 466</span>
</div>
</body>
</html>