<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <script src="js/jquery-2.1.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.liquidcarousel.js"></script>
    <script src="js/cookie.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="css/template.css">
    <link rel="stylesheet" href="css/modules.css">
    <link rel="stylesheet" href="css/screen.css">
    <script>
        /* Запускаем сразу после загрузки страницы */
        $( document ).ready(function() {
            newsBlocks();

            // Читаем из куков текущий город
            var currentCity = readCookie("currentCity");
            currentCity = (currentCity != null) ? currentCity : $(".city-ul li").eq(0).text();
            $(".current-city").text(currentCity);

            $('.upbutton').click(function(){
                $('html, body').animate({scrollTop:0}, 'slow');
            });

            $(".selectcity").click(function(){
                rotateBG();
                $(".city-ul").slideToggle();
            });

            $(".city-ul li").click(function(){
                var selectedCity = $( this).text();
                $(".current-city").text(selectedCity);
                // Записываем в куки текущий город
                writeCookie("currentCity", selectedCity, 7);
                rotateBG();
                $(".city-ul").slideToggle();
            });
        });

        function rotateBG() {
            if ( $(".city-arrow").is(".rotate-bg") ) {
                $(".city-arrow").removeClass("rotate-bg");
            } else {
                $(".city-arrow").addClass("rotate-bg");
            }
        }

        /* Используем при изменении ширины окна в режиме live */
        $( window ).resize(function() {
            newsBlocks();
        });

        /* Собственно, меняем отступы для крайних в строке картинок, чтобы все было хорошо, дети росли, старики не болели, закончилась война */
        function newsBlocks() {
            var blocks = $(".newsblock");
            var width = window.innerWidth;
            var blockCount = parseInt(width/320);
            // Высчитываем длину блока в процентах
            var newWidth = 100/blockCount;
            // Переводим проценты в пиксели
            var newWidthPX = 1920/100/newWidth;
            // Если длина блока в пикселях менее минимально необходимой,
            // уменьшаем кол-во блоков и пересчитываем проценты
            if (newWidthPX < 320) {
                blockCount--;
                newWidth = 100/blockCount;
            }
            // Задаем новую длину для блоков
            for (var i = 0; i < blockCount; i++) {
                blocks.eq(i).css("width", newWidth + "%").show();
            }
            // Скрываем лишние
            for (blockCount; blockCount < blocks.length; blockCount++) {
                blocks.eq(blockCount).hide();
            }
        }
    </script>
</head>
<body>
<div id="content-block">
    <div class="row header center-content">
        <div class="span12">
            <div class="span6 p-relative lessspan">
                <div class="logo">
                    <a href="/">
                        <img src="images/logo.png" alt="INFOSMART" title="INFOSMART">
                    </a>
                </div>
                <div class="citywidget">
                    <div class="cityname">
                        <span class="city-arrow"></span>
                        <span class="current-city">Астана</span>
                    </div>
                    <span class="selectcity">выберите город</span>
                    <ul class="city-ul">
                        <li>Астана</li>
                        <li>Москва</li>
                        <li>Лондон</li>
                        <li>Деревня Лопухи</li>
                        <li>Бишкек</li>
                        <li>Астраханка</li>
                        <li>Вашингтон</li>
                        <li>Иваново</li>
                        <li>Прага</li>
                        <li>Степногорск</li>
                        <li>Павлодар</li>
                    </ul>
                </div>
                <div class="infowidget">
                    <span class="weather-day">+18&deg;<span class="dotes">...</span>+20&deg;</span>
                    <span class="weather-night">+4&deg;<span class="dotes">...</span>+6&deg;</span>
                    <span class="dollar">185.5<span class="depreciation"></span></span>
                    <span class="euro">250.2<span class="growth-rate"></span></span>
                    <span class="rub">5.4<span class="depreciation"></span></span>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="span6 morespan">
                <ul class="mainmenu">
                    <li>
                        <a class="deals hover-green" href="deals.php">предложения</a>
                    </li>
                    <li>
                        <a class="map hover-green" href="#">карта</a>
                    </li>
                    <li>
                        <a class="news hover-green" href="#">новости</a>
                    </li>
                    <li>
                        <a class="personal-area hover-green" href="#">личный кабинет</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row search center-content">
        <div class="span12">
            <div class="span6 searchdiv">
                <div class="searchform">
                    <form action="#">
                        <input type="text" name="search" placeholder="Введите ключевое слово" class="searchinput">
                        <input type="submit" name="button" class="searchbutton">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="map"> карта
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="deals"> в предложениях
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="news"> в новостях
                            </label>
                        </div>
                    </form>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="span6 adddiv">
                <div class="addblock">
                    <a href="#" class="hover-green">Добавить компанию</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row news">
        <div class="newsblock">
            <div class="newscontent">
                <img src="images/banner1.png">
            </div>
        </div>
        <div class="newsblock">
            <div class="newscontent">
                <img src="images/banner2.png">
            </div>
            <div class="newscontent">
                <img src="images/banner3.png">
                <div class="ptext">
                    <span class="caption">Уголовное дело возбуждено в отношении "Гульнар тур"</span>
                    <p class="text">Предварительный ущерб составил около шести миллионов</p>
                </div>
            </div>
        </div>
        <div class="newsblock">
            <div class="newscontent">
                <img src="images/banner4.png">
            </div>
        </div>
        <div class="newsblock">
            <div class="newscontent">
                <img src="images/banner5.png">
            </div>
            <div class="newscontent">
                <img src="images/banner6.png">
            </div>
        </div>
        <div class="newsblock">
            <div class="newscontent">
                <img src="images/banner7.png">
            </div>
        </div>
        <div class="newsblock">
            <div class="newscontent">
                <img src="images/banner5.png">
            </div>
            <div class="newscontent">
                <img src="images/banner6.png">
            </div>
        </div>
        <div class="newsblock">
            <div class="newscontent">
                <img src="images/banner7.png">
            </div>
        </div>
        <div class="newsblock">
            <div class="newscontent">
                <img src="images/banner5.png">
            </div>
            <div class="newscontent">
                <img src="images/banner6.png">
            </div>
        </div>
        <div class="newsblock">
            <div class="newscontent">
                <img src="images/banner7.png">
            </div>
        </div>
        <div class="clr"></div>
    </div>
    <div class="row rubrics center-content">
        <div class="span12">
            <div class="header-block">
                <div class="span6">
                    <div class="breadcrumbs">
                        <a href="#">Главная</a>
                        <a href="#">Авто</a>
                    </div>
                    <h1>Автозапчасти</h1>
                </div>
                <div class="span6">
                    <div class="view-block">
                        <span>вид:</span>
                        <a href="#" class="list-view active"></a>
                        <a href="#" class="block-view hover-green"></a>
                    </div>
                    <div class="refresh-block">
                        <span>(56 предприятий)</span>
                        <a href="#" class="refresh-button hover-green">обновить</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="span12">
            <div class="chargeable-unit">
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
            </div>
        </div>
        <div class="span12">
            <div class="free-block">
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
            </div>
        </div>
    </div>
    <div class="wrap"></div>
</div>
<div class="row footer">
    <span class="upbutton">наверх</span>
    <ul class="footermenu">
        <li>
            <a href="#">Lorem</a>
        </li>
        <li>
            <a href="#">Ipsum</a>
        </li>
        <li>
            <a href="#">Dolor sit</a>
        </li>
        <li>
            <a href="#">Amet</a>
        </li>
        <li>
            <a href="#">Consectetur</a>
        </li>
        <li>
            <a href="#">Consectetur adipisicing</a>
        </li>
    </ul>
    <span class="footer-copirights">&copy;2014 elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</span>
    <span class="footer-email">email: zhiga_@mail.ru</span>
    <span class="footer-phone">тел: +7 778 152 24 33, +7 7172 522 466</span>
</div>
</body>
</html>