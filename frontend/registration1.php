<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <script src="js/jquery-2.1.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.liquidcarousel.js"></script>
    <script src="js/cookie.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="css/template.css">
    <link rel="stylesheet" href="css/modules.css">
    <link rel="stylesheet" href="css/screen.css">
    <script>
        /* Запускаем сразу после загрузки страницы */
        $( document ).ready(function() {
            // Читаем из куков текущий город
            var currentCity = readCookie("currentCity");
            currentCity = (currentCity != null) ? currentCity : $(".city-ul li").eq(0).text();
            $(".current-city").text(currentCity);

            $('.upbutton').click(function(){
                $('html, body').animate({scrollTop:0}, 'slow');
            });

            $(".selectcity").click(function(){
                rotateBG();
                $(".city-ul").slideToggle();
            });

            $(".city-ul li").click(function(){
                var selectedCity = $( this).text();
                $(".current-city").text(selectedCity);
                // Записываем в куки текущий город
                writeCookie("currentCity", selectedCity, 7);
                rotateBG();
                $(".city-ul").slideToggle();
            });
        });

        function rotateBG() {
            if ( $(".city-arrow").is(".rotate-bg") ) {
                $(".city-arrow").removeClass("rotate-bg");
            } else {
                $(".city-arrow").addClass("rotate-bg");
            }
        }
    </script>
</head>
<body>
<div id="content-block">
    <div class="row header center-content">
        <div class="span12">
            <div class="span6 p-relative lessspan">
                <div class="logo">
                    <a href="/">
                        <img src="images/logo.png" alt="INFOSMART" title="INFOSMART">
                    </a>
                </div>
                <div class="citywidget">
                    <div class="cityname">
                        <span class="city-arrow"></span>
                        <span class="current-city">Астана</span>
                    </div>
                    <span class="selectcity">выберите город</span>
                    <ul class="city-ul">
                        <li>Астана</li>
                        <li>Москва</li>
                        <li>Лондон</li>
                        <li>Деревня Лопухи</li>
                        <li>Бишкек</li>
                        <li>Астраханка</li>
                        <li>Вашингтон</li>
                        <li>Иваново</li>
                        <li>Прага</li>
                        <li>Степногорск</li>
                        <li>Павлодар</li>
                    </ul>
                </div>
                <div class="infowidget">
                    <span class="weather-day">+18&deg;<span class="dotes">...</span>+20&deg;</span>
                    <span class="weather-night">+4&deg;<span class="dotes">...</span>+6&deg;</span>
                    <span class="dollar">185.5<span class="depreciation"></span></span>
                    <span class="euro">250.2<span class="growth-rate"></span></span>
                    <span class="rub">5.4<span class="depreciation"></span></span>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="span6 morespan">
                <ul class="mainmenu">
                    <li>
                        <a class="deals hover-green" href="deals.php">предложения</a>
                    </li>
                    <li>
                        <a class="map hover-green" href="#">карта</a>
                    </li>
                    <li>
                        <a class="news hover-green" href="#">новости</a>
                    </li>
                    <li>
                        <a class="personal-area hover-green" href="#">личный кабинет</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row registration center-content">
        <div class="span12">
            <h1>Регистрация</h1>
        </div>
        <div class="span12 registration-steps center-content">
            <div class="steps">
                <div class="passed-step"><span class="step-number">1</span> шаг</div>
                <div class="passed-brench"></div>
                <div class="active-step"><span class="step-number">2</span> шаг</div>
                <div class="upcoming-brench"></div>
                <div class="upcoming-step"><span class="step-number">3</span> шаг</div>
            </div>
        </div>
        <div class="span12 registration2">
<!--            <div class="span6 morespan">-->
                <form action="#" method="post">
                    <div class="reg-form">
                        <div class="reg-row">
                            <label for="login">Логин (email)</label>
                            <input type="text" name="login" id="login">
                            <div class="description">
                                <span class="arrow"></span>
                                <span class="text">свободен</span>
                            </div>
                        </div>
                        <div class="reg-row">
                            <label for="password1">пароль</label>
                            <input type="password" name="password1" id="password1">
                            <div class="description">
                                <span class="arrow"></span>
                                <span class="text">только латиница и цифры</span>
                            </div>
                        </div>
                        <div class="reg-row">
                            <label for="password2">Пароль повторно</label>
                            <input type="password" name="password2" id="password2">
                            <div class="description">
                                <span class="arrow"></span>
                                <span class="text">пароли совпадают</span>
                            </div>
                        </div>
                        <div class="reg-row">
                            <div class="captcha-block">
                                <img src="images/captcha.png" alt="Captcha" title="Captcha">
                            </div>
                            <input type="text" name="captcha" id="captcha">
                            <div class="description">
                                <span class="arrow"></span>
                                <span class="text">верно</span>
                            </div>
                        </div>
                    </div>
                </form>
<!--            </div>-->
            <!--<div class="span6 lessspan">
                <table class="paytable table-striped">
                    <tbody>
                    <tr>
                        <td colspan="2">
                            <span class="pay-label">Сумма</span>
                            <span class="sum">999 999 тг</span>
                        </td>
                    </tr>
                    <tr>
                        <td>карточка предприятия, включая 3 рубрики, 3 подрубрики, 50 ключевых слов</td>
                        <td>99 999тг</td>
                    </tr>
                    <tr>
                        <td>выделение карточки в топ</td>
                        <td>99 999тг</td>
                    </tr>
                    <tr>
                        <td>дополнительное количество подкатегорий - 14</td>
                        <td>99 999тг</td>
                    </tr>
                    <tr>
                        <td>увеличенное количество ключевых слов - 150</td>
                        <td>99 999тг</td>
                    </tr>
                    <tr>
                        <td>увеличенное</td>
                        <td>99 999тг</td>
                    </tr>
                    <tr class="last-tr">
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>-->
        </div>
        <div class="span12 registration-buttons">
            <button class="cancel-step hover-green">отмена</button>
            <button class="next-step hover-green">следующий шаг</button>
        </div>
    </div>
    <div class="wrap"></div>
</div>
<div class="row footer">
    <span class="upbutton">наверх</span>
    <ul class="footermenu">
        <li>
            <a href="#">Lorem</a>
        </li>
        <li>
            <a href="#">Ipsum</a>
        </li>
        <li>
            <a href="#">Dolor sit</a>
        </li>
        <li>
            <a href="#">Amet</a>
        </li>
        <li>
            <a href="#">Consectetur</a>
        </li>
        <li>
            <a href="#">Consectetur adipisicing</a>
        </li>
    </ul>
    <span class="footer-copirights">&copy;2014 elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</span>
    <span class="footer-email">email: zhiga_@mail.ru</span>
    <span class="footer-phone">тел: +7 778 152 24 33, +7 7172 522 466</span>
</div>
</body>
</html>