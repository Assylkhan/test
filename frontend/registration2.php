<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <script src="js/jquery-2.1.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.liquidcarousel.js"></script>
    <script src="js/cookie.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="css/template.css">
    <link rel="stylesheet" href="css/modules.css">
    <link rel="stylesheet" href="css/screen.css">
    <script>
        /* Запускаем сразу после загрузки страницы */
        $( document ).ready(function() {
            // Читаем из куков текущий город
            var currentCity = readCookie("currentCity");
            currentCity = (currentCity != null) ? currentCity : $(".city-ul li").eq(0).text();
            $(".current-city").text(currentCity);

            $('.upbutton').click(function(){
                $('html, body').animate({scrollTop:0}, 'slow');
            });

            $(".selectcity").click(function(){
                rotateBG();
                $(".city-ul").slideToggle();
            });

            $(".city-ul li").click(function(){
                var selectedCity = $( this).text();
                $(".current-city").text(selectedCity);
                // Записываем в куки текущий город
                writeCookie("currentCity", selectedCity, 7);
                rotateBG();
                $(".city-ul").slideToggle();
            });

            // Меняем сумму в корзине при изменении кол-ва ключевых слов
            $("#keynum").change(function() {
                var num = $("#keynum option:selected").val();
                alert("Сумма заказа будет изменена на: много денег*" + num);
            });
        });

        function rotateBG() {
            if ( $(".city-arrow").is(".rotate-bg") ) {
                $(".city-arrow").removeClass("rotate-bg");
            } else {
                $(".city-arrow").addClass("rotate-bg");
            }
        }
    </script>
</head>
<body>
<div id="content-block">
    <div class="row header center-content">
        <div class="span12">
            <div class="span6 p-relative lessspan">
                <div class="logo">
                    <a href="/">
                        <img src="images/logo.png" alt="INFOSMART" title="INFOSMART">
                    </a>
                </div>
                <div class="citywidget">
                    <div class="cityname">
                        <span class="city-arrow"></span>
                        <span class="current-city">Астана</span>
                    </div>
                    <span class="selectcity">выберите город</span>
                    <ul class="city-ul">
                        <li>Астана</li>
                        <li>Москва</li>
                        <li>Лондон</li>
                        <li>Деревня Лопухи</li>
                        <li>Бишкек</li>
                        <li>Астраханка</li>
                        <li>Вашингтон</li>
                        <li>Иваново</li>
                        <li>Прага</li>
                        <li>Степногорск</li>
                        <li>Павлодар</li>
                    </ul>
                </div>
                <div class="infowidget">
                    <span class="weather-day">+18&deg;<span class="dotes">...</span>+20&deg;</span>
                    <span class="weather-night">+4&deg;<span class="dotes">...</span>+6&deg;</span>
                    <span class="dollar">185.5<span class="depreciation"></span></span>
                    <span class="euro">250.2<span class="growth-rate"></span></span>
                    <span class="rub">5.4<span class="depreciation"></span></span>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="span6 morespan">
                <ul class="mainmenu">
                    <li>
                        <a class="deals hover-green" href="deals.php">предложения</a>
                    </li>
                    <li>
                        <a class="map hover-green" href="#">карта</a>
                    </li>
                    <li>
                        <a class="news hover-green" href="#">новости</a>
                    </li>
                    <li>
                        <a class="personal-area hover-green" href="#">личный кабинет</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row registration center-content">
        <div class="span12">
            <h1>Регистрация</h1>
        </div>
        <div class="span12 registration-steps center-content">
            <div class="steps">
                <div class="passed-step"><span class="step-number">1</span> шаг</div>
                <div class="passed-brench"></div>
                <div class="active-step"><span class="step-number">2</span> шаг</div>
                <div class="upcoming-brench"></div>
                <div class="upcoming-step"><span class="step-number">3</span> шаг</div>
            </div>
        </div>
        <div class="span12 registration2">
            <div class="span6 morespan">
                <table class="formtable">
                    <tbody>
                        <tr>
                            <td>
                                <label for="companyName">Название организации<span class="required">*</span>:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <input name="companyName" id="companyName" class="registration-input">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="ownership">Вид (форма) собственности:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <select name="ownership" id="ownership" class="registration-select">
                                    <option value="1">ТОО</option>
                                    <option value="2">ИП</option>
                                    <option value="3">АО</option>
                                    <option value="4">ОАО</option>
                                    <option value="5">ЗАО</option>
                                    <option value="6">ООО</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="logo">Логотип организации:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <input type="file" name="logo" id="logo">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="description">Описание компании:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <textarea name="description" id="description" class="registration-textarea"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="rubric">Рубрика<span class="required">*</span>:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <select name="rubric" id="rubric" class="registration-select">
                                    <option value="1">Рубрика 1</option>
                                    <option value="2">Рубрика 2</option>
                                    <option value="3">Рубрика 3</option>
                                    <option value="4">Рубрика 4</option>
                                    <option value="5">Рубрика 5</option>
                                    <option value="6">Рубрика 6</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="subrubric">Подрубрика<span class="required">*</span>:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <select name="subrubric" id="subrubric" class="registration-select">
                                    <option value="1">Подрубрика 1</option>
                                    <option value="2">Подрубрика 2</option>
                                    <option value="3">Подрубрика 3</option>
                                    <option value="4">Подрубрика 4</option>
                                    <option value="5">Подрубрика 5</option>
                                    <option value="6">Подрубрика 6</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="keynum">Количество ключевых слов:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <select name="keynum" id="keynum" class="registration-select">
                                    <option value="50" selected>50</option>
                                    <option value="100">100</option>
                                    <option value="150">150</option>
                                    <option value="500">500</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="keywords">Ключевые слова:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <textarea name="keywords" id="keywords" class="registration-textarea"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="index">Индекс<span class="required">*</span>:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <input type="text" name="index" id="index" class="registration-input">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="region">Область<span class="required">*</span>:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <select name="region" id="region" class="registration-select">
                                    <option value="Астана">Астана</option>
                                    <option value="Алматы">Алматы</option>
                                    <option value="Байконыр">Байконыр</option>
                                    <option value="Кокшетау">Кокшетау</option>
                                    <option value="Актобе">Актобе</option>
                                    <option value="Талдыкорган">Талдыкорган</option>
                                    <option value="Атырау">Атырау</option>
                                    <option value="Усть-Каменогорск">Усть-Каменогорск</option>
                                    <option value="Тараз">Тараз</option>
                                    <option value="Уральск">Уральск</option>
                                    <option value="Караганда">Караганда</option>
                                    <option value="Костанай">Костанай</option>
                                    <option value="Кызылорда">Кызылорда</option>
                                    <option value="Актау">Актау</option>
                                    <option value="Павлодар">Павлодар</option>
                                    <option value="Петропавловск">Петропавловск</option>
                                    <option value="Шымкент">Шымкент</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="city">Город<span class="required">*</span>:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <input type="text" name="city" id="city" class="registration-input">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="street">Улица<span class="required">*</span>:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <input type="text" name="street" id="street" class="registration-input">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="house">Дом<span class="required">*</span>:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <input type="text" name="house" id="house" class="registration-input">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="office">Офис:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <input type="text" name="office" id="office" class="registration-input">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="office">Отметить на карте:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <div style="width: 370px; height: 150px; background: #ffffff; cursor: pointer;"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="phone">Телефон<span class="required">*</span>:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <input type="text" name="phone" id="phone" class="registration-input">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="email">Электронная почта<span class="required">*</span>:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <input type="text" name="email" id="email" class="registration-input">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="site">Сайт:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <input type="text" name="site" id="site" class="registration-input">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="messenger">Интернет мессенджер:</label>
                                <span class="explanation">пояснение к пункту</span>
                            </td>
                            <td>
                                <select name="messenger_type" id="messenger_type" class="registration-select">
                                    <option value="1">skype</option>
                                    <option value="2">icq</option>
                                    <option value="3">jabber</option>
                                    <option value="4">рации из стаканчиков</option>
                                </select>

                                <input type="text" name="messenger" id="messenger"  class="registration-input-small">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="add-info">
                    <span class="add-label">Дополнительные поля информации</span>
                    <button class="add-open hover-green">раскрыть</button>
                </div>
            </div>
            <div class="span6 lessspan">
                <table class="paytable table-striped">
                    <tbody>
                        <tr>
                            <td colspan="2">
                                <span class="pay-label">Сумма</span>
                                <span class="sum">999 999 тг</span>
                            </td>
                        </tr>
                        <tr>
                            <td>карточка предприятия, включая 3 рубрики, 3 подрубрики, 50 ключевых слов</td>
                            <td>99 999тг</td>
                        </tr>
                        <tr>
                            <td>выделение карточки в топ</td>
                            <td>99 999тг</td>
                        </tr>
                        <tr>
                            <td>дополнительное количество подкатегорий - 14</td>
                            <td>99 999тг</td>
                        </tr>
                        <tr>
                            <td>увеличенное количество ключевых слов - 150</td>
                            <td>99 999тг</td>
                        </tr>
                        <tr>
                            <td>увеличенное</td>
                            <td>99 999тг</td>
                        </tr>
                        <tr class="last-tr">
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="span12 registration-buttons">
            <button class="cancel-step hover-green">отмена</button>
            <button class="next-step hover-green">следующий шаг</button>
        </div>
    </div>
    <div class="wrap"></div>
</div>
<div class="row footer">
    <span class="upbutton">наверх</span>
    <ul class="footermenu">
        <li>
            <a href="#">Lorem</a>
        </li>
        <li>
            <a href="#">Ipsum</a>
        </li>
        <li>
            <a href="#">Dolor sit</a>
        </li>
        <li>
            <a href="#">Amet</a>
        </li>
        <li>
            <a href="#">Consectetur</a>
        </li>
        <li>
            <a href="#">Consectetur adipisicing</a>
        </li>
    </ul>
    <span class="footer-copirights">&copy;2014 elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</span>
    <span class="footer-email">email: zhiga_@mail.ru</span>
    <span class="footer-phone">тел: +7 778 152 24 33, +7 7172 522 466</span>
</div>
</body>
</html>