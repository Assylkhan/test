<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <script src="js/jquery-2.1.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.liquidcarousel.js"></script>
    <script src="js/crossfilter.min.js"></script>
    <script src="js/d3.min.js"></script>
    <script src="js/dimple.v2.1.0.min.js"></script>
    <script src="js/cookie.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="css/template.css">
    <link rel="stylesheet" href="css/modules.css">
    <link rel="stylesheet" href="css/screen.css">
    <script>
        /* Запускаем сразу после загрузки страницы */
        $( document ).ready(function() {
            // Читаем из куков текущий город
            var currentCity = readCookie("currentCity");
            currentCity = (currentCity != null) ? currentCity : $(".city-ul li").eq(0).text();
            $(".current-city").text(currentCity);

            $('.upbutton').click(function(){
                $('html, body').animate({scrollTop:0}, 'slow');
            });

            $(".selectcity").click(function(){
                rotateBG();
                $(".city-ul").slideToggle();
            });

            $(".city-ul li").click(function(){
                var selectedCity = $( this).text();
                $(".current-city").text(selectedCity);
                // Записываем в куки текущий город
                writeCookie("currentCity", selectedCity, 7);
                rotateBG();
                $(".city-ul").slideToggle();
            });
        });

        function rotateBG() {
            if ( $(".city-arrow").is(".rotate-bg") ) {
                $(".city-arrow").removeClass("rotate-bg");
            } else {
                $(".city-arrow").addClass("rotate-bg");
            }
        }
    </script>
</head>
<body>
<div id="content-block">
    <div class="row header center-content">
        <div class="span12">
            <div class="span6 p-relative lessspan">
                <div class="logo">
                    <a href="/">
                        <img src="images/logo.png" alt="INFOSMART" title="INFOSMART">
                    </a>
                </div>
                <div class="citywidget">
                    <div class="cityname">
                        <span class="city-arrow"></span>
                        <span class="current-city">Астана</span>
                    </div>
                    <span class="selectcity">выберите город</span>
                    <ul class="city-ul">
                        <li>Астана</li>
                        <li>Москва</li>
                        <li>Лондон</li>
                        <li>Деревня Лопухи</li>
                        <li>Бишкек</li>
                        <li>Астраханка</li>
                        <li>Вашингтон</li>
                        <li>Иваново</li>
                        <li>Прага</li>
                        <li>Степногорск</li>
                        <li>Павлодар</li>
                    </ul>
                </div>
                <div class="infowidget">
                    <span class="weather-day">+18&deg;<span class="dotes">...</span>+20&deg;</span>
                    <span class="weather-night">+4&deg;<span class="dotes">...</span>+6&deg;</span>
                    <span class="dollar">185.5<span class="depreciation"></span></span>
                    <span class="euro">250.2<span class="growth-rate"></span></span>
                    <span class="rub">5.4<span class="depreciation"></span></span>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="span6 morespan">
                <ul class="mainmenu">
                    <li>
                        <a class="deals hover-green" href="deals.php">предложения</a>
                    </li>
                    <li>
                        <a class="map hover-green" href="#">карта</a>
                    </li>
                    <li>
                        <a class="news hover-green" href="#">новости</a>
                    </li>
                    <li>
                        <a class="personal-area hover-green" href="#">личный кабинет</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row search center-content">
        <div class="span12">
            <div class="span6 searchdiv">
                <div class="searchform">
                    <form action="#">
                        <input type="text" name="search" placeholder="Введите ключевое слово" class="searchinput">
                        <input type="submit" name="button" class="searchbutton">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="map"> карта
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="deals"> в предложениях
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="news"> в новостях
                            </label>
                        </div>
                    </form>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="span6 adddiv">
                <div class="addblock">
                    <a href="#" class="hover-green">Добавить компанию</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row person-area center-content">
        <div class="span12">
            <div class="header-block">
                <h1>Личный кабинет - <span>Название предприятия</span></h1>
            </div>
        </div>
        <div class="span12">
            <div class="balance">
                <div class="content">
                    <span>Баланс</span> <span class="bold-content">99 999 тг</span>
                </div>
                <button class="recharge-button hover-green">пополнить счет</button>
            </div>
            <div class="orders-history">
                <div class="content">
                    <span>История заказов</span>
                </div>
                <span class="result bold-content">+1</span>
            </div>
        </div>
        <div class="span12 business-card">
            <h3>Карточка предприятия</h3>
            <table class="info-table">
                <tbody>
                    <tr>
                        <td class="name">
                            <span class="main">название предприятия</span>
                            <span class="decription">(код карточки: 9874562)</span>
                        </td>
                        <td class="card-info">
                            <div class="info-date">опубликовано <span>(12.12.12)</span></div>
                            <hr>
                            <div class="info-date">ожидание модерации <span>(13.12.12)</span></div>
                        </td>
                        <td class="publication-label">
                            <span class="main">срок публикации</span>
                        </td>
                        <td class="publication-date">
                            <span class="info-date">с 12.12.12</span>
                            <hr>
                            <span class="info-date">до 13.12.12</span>
                        </td>
                    </tr>
                </tbody>
            </table>
            <ul class="menu">
                <li><a href="#">редактировать</a></li>
                <li><a href="#">предложения</a></li>
                <li><a href="#">вакансии</a></li>
            </ul>
            <table class="info-table">
                <tbody>
                <tr>
                    <td class="name">
                        <span class="main">Выделение карточки в топ</span>
                    </td>
                    <td class="card-info"></td>
                    <td class="publication-label2">
                        <span class="main">срок выделения</span>
                    </td>
                    <td class="publication-date">
                        <span class="info-date">с 12.12.12</span>
                        <hr>
                        <span class="info-date">до 13.12.12</span>
                    </td>
                </tr>
                </tbody>
            </table>
            <button class="remove-assignment hover-green">снять выделение</button>
            <button class="prolong-assignment hover-green">продлить выделение</button>
        </div>
        <div class="span12 statistic">
            <div class="span12">
                <div class="statistic-block">
                    <h4>Показы на сайте за</h4>
                    <select id="site-stat-period" class="period" name="period">
                        <option value="1">год</option>
                        <option selected value="2">месяц</option>
                        <option value="3">неделя</option>
                    </select>
                    <div id="site-stat" class="schedule"></div>
                </div>
            </div>
            <div class="span12">
                <div class="statistic-block">
                    <h4>Показы на мобильном за</h4>
                    <select id="mobile-stat-period" class="period" name="period">
                        <option value="1">год</option>
                        <option selected value="2">месяц</option>
                        <option value="3">неделя</option>
                    </select>
                    <div id="mobile-stat" class="schedule"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrap"></div>
</div>
<div class="row footer">
    <span class="upbutton">наверх</span>
    <ul class="footermenu">
        <li>
            <a href="#">Lorem</a>
        </li>
        <li>
            <a href="#">Ipsum</a>
        </li>
        <li>
            <a href="#">Dolor sit</a>
        </li>
        <li>
            <a href="#">Amet</a>
        </li>
        <li>
            <a href="#">Consectetur</a>
        </li>
        <li>
            <a href="#">Consectetur adipisicing</a>
        </li>
    </ul>
    <span class="footer-copirights">&copy;2014 elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</span>
    <span class="footer-email">email: zhiga_@mail.ru</span>
    <span class="footer-phone">тел: +7 778 152 24 33, +7 7172 522 466</span>
</div>
<script src="js/statistics.js"></script>
</body>
</html>