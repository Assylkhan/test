<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <script src="../js/jquery-2.1.0.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/bootstrap-hover-dropdown.min.js"></script>
    <script src="../js/jquery.liquidcarousel.js"></script>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/template.css">
    <link rel="stylesheet" href="../css/modules.css">
    <link rel="stylesheet" href="../css/screen.css">
    <link rel="stylesheet" href="../css/forms.css">
    <script type="text/javascript">
        $(document).ready(function(){
            $('#general_modal').modal('show');
        });
    </script>
</head>
<body>
<div class="modal fade" id="general_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <!--                <h4 class="modal-title">Modal title</h4>-->
            </div>
            <div class="modal-body">
                <div class="modal-row">
                    <div class="span6">
                        <ul class="top-nav breadcrumbs">
                            <li><a href="#">Главная</a></li>
                            <li><a href="#">Рубрика</a></li>
                            <li><a href="#">Подрубрика</a></li>
                        </ul>
                        <h1>Название предприятия</h1>
                        <a href="#" class="button-style share">поделиться</a>
                        <ul class="main-menu">
                            <li><a href="general.php">общая информация</a></li>
                            <li><a href="onmap.php">на карте</a></li>
                            <li><a href="deals.php">предложения</a></li>
                            <li><a href="foto.php">фото</a></li>
                            <li><a href="job.php" class="active">вакансии</a></li>
                            <li><a href="details.php">реквизиты</a></li>
                        </ul>
                        <div class="header-block">
                            <h2>Вакансии</h2>
                        </div>
                    </div>
                </div>
            </div>
            <!--            <div class="modal-footer"></div>-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</body>
</html>