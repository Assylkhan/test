<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <script src="../js/jquery-2.1.0.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/bootstrap-hover-dropdown.min.js"></script>
    <script src="../js/jquery.liquidcarousel.js"></script>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/template.css">
    <link rel="stylesheet" href="../css/modules.css">
    <link rel="stylesheet" href="../css/screen.css">
    <link rel="stylesheet" href="../css/forms.css">
    <script type="text/javascript">
        $(document).ready(function(){
            $('#general_modal').modal('show');
        });
    </script>
</head>
<body>
<div class="modal fade" id="general_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <!--                <h4 class="modal-title">Modal title</h4>-->
            </div>
            <div class="modal-body">
                <div class="modal-row">
                    <div class="span6 marginbottom">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li><a href="#auth" role="tab" data-toggle="tab">Авторизация</a></li>
                            <li class="active"><a href="#registration" role="tab" data-toggle="tab">Регистрация</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane" id="auth">
                                <div class="auth-error">
                                    введенные логин и пароль не совпадают
                                </div>
                                <form action="#" method="post">
                                    <div class="reg-form">
                                        <div class="reg-row">
                                            <label for="login">Логин (email)</label>
                                            <input type="text" name="login" id="login">
                                        </div>
                                        <div class="reg-row">
                                            <label for="password">Пароль</label>
                                            <input type="password" name="password" id="password">
                                        </div>
                                        <div class="reg-row">
                                            <a href="#" class="remain-link">забыли пароль?</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane active" id="registration">
                                <div class="steps">
                                    <div class="active-step"><span class="step-number">1</span> шаг</div>
                                    <div class="upcoming-brench"></div>
                                    <div class="upcoming-step"><span class="step-number">2</span> шаг</div>
                                    <div class="upcoming-brench"></div>
                                    <div class="upcoming-step"><span class="step-number">3</span> шаг</div>
                                </div>
                                <form action="#" method="post">
                                    <div class="reg-form">
                                        <div class="reg-row">
                                            <label for="login">Логин (email)</label>
                                            <input type="text" name="login" id="login">
                                            <div class="description">
                                                <span class="arrow"></span>
                                                <span class="text">свободен</span>
                                            </div>
                                        </div>
                                        <div class="reg-row">
                                            <label for="password1">пароль</label>
                                            <input type="password" name="password1" id="password1">
                                            <div class="description">
                                                <span class="arrow"></span>
                                                <span class="text">только латиница и цифры</span>
                                            </div>
                                        </div>
                                        <div class="reg-row">
                                            <label for="password2">Пароль повторно</label>
                                            <input type="password" name="password2" id="password2">
                                            <div class="description">
                                                <span class="arrow"></span>
                                                <span class="text">пароли совпадают</span>
                                            </div>
                                        </div>
                                        <div class="reg-row">
                                            <div class="captcha-block">
                                                <img src="../images/captcha.png" alt="Captcha" title="Captcha">
                                            </div>
                                            <input type="text" name="captcha" id="captcha">
                                            <div class="description">
                                                <span class="arrow"></span>
                                                <span class="text">верно</span>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="big-next-button hover-green">следующий шаг</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--            <div class="modal-footer"></div>-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</body>
</html>