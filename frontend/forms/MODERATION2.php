<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <script src="../js/jquery-2.1.0.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/bootstrap-hover-dropdown.min.js"></script>
    <script src="../js/jquery.liquidcarousel.js"></script>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/template.css">
    <link rel="stylesheet" href="../css/modules.css">
    <link rel="stylesheet" href="../css/screen.css">
    <link rel="stylesheet" href="../css/forms.css">
    <script type="text/javascript">
        $(document).ready(function(){
            $('#general_modal').modal('show');

            var total = $(".container").length;
            // Now display this wherever you want
            var text = "1/" + total;
            $('#carousel-index').text(text);
        });
    </script>
</head>
<body>

<div class="modal fade" id="general_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <!--                <h4 class="modal-title">Modal title</h4>-->
            </div>
            <div class="modal-body">
                <div class="modal-row">
                    <div class="span6">
                        <ul class="top-nav breadcrumbs">
                            <li><a href="#">Главная</a></li>
                            <li><a href="#">Рубрика</a></li>
                            <li><a href="#">Подрубрика</a></li>
                        </ul>
                        <h1>Название предприятия</h1>
                        <a href="#" class="button-style share">поделиться</a>
                        <ul class="main-menu">
                            <li><a href="general.php">общая информация</a></li>
                            <li><a href="onmap.php">на карте</a></li>
                            <li><a href="deals.php">предложения</a></li>
                            <li><a href="foto.php">фото</a></li>
                            <li><a href="job.php">вакансии</a></li>
                            <li><a href="details.php">реквизиты</a></li>
                			<li><a href="#" id="moder" class="active">модерация</a></li>
                        </ul>
                        <div class="span12 moderation">
            <h2>Добавить сообщение </h2><br><br>
            <textarea class="zh_message-body" name="message" id="zh_message"></textarea>
            <br><br>
            <label for="mail">
                <input type="checkbox" name="mail" id="zh_mail">
                копия на почту
            </label>
            <br><br>
            <label for="payment">
                <input type="checkbox" name="payment" id="zh_payment">
                оплачено на месте, ввести номер чека
            </label>
            <input type="text" name="checkNumber" class="zh_checkNumber">
            <br><br>
            <div class="zh_button-row">
                <button class="zh_hover-green hover-green">положительно</button>
                <button class="zh_hover-green hover-green">отрицательно</button>
            </div>
        </div>
                    </div>
                </div>
            </div>
            <!--            <div class="modal-footer"></div>-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    // This triggers after each slide change
    $('#mySlider1').on('slid.bs.carousel', function () {
        var carouselData = $(this).data('bs.carousel');
        var currentIndex = carouselData.getActiveIndex();
        var total = carouselData.$items.length;

        // Now display this wherever you want
        var text = (currentIndex + 1) + "/" + total;
        $('#carousel-index').text(text);
    });
</script>
</body>
</html>