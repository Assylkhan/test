<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <script src="../js/jquery-2.1.0.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/bootstrap-hover-dropdown.min.js"></script>
    <script src="../js/jquery.liquidcarousel.js"></script>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/template.css">
    <link rel="stylesheet" href="../css/modules.css">
    <link rel="stylesheet" href="../css/screen.css">
    <link rel="stylesheet" href="../css/forms.css">
    <script type="text/javascript">
        $(document).ready(function(){
            $('#general_modal').modal('show');
        });
    </script>
</head>
<body>
<div class="modal fade" id="general_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <!--                <h4 class="modal-title">Modal title</h4>-->
            </div>
            <div class="modal-body">
                <div class="modal-row">
                    <div class="span6">
                        <h6>Личный кабинет - Название предприятия</h6>
                        <div class="header-block">
                            <h1>История заказов</h1>
                        </div>
                        <div class="orders-list">
                            <div class="order">
                                <div class="order-header">
                                    <span class="name">Заказ № 656415354</span> - <span class="status">оплачено</span>
                                    <button class="hover-green">кнопка</button>
                                </div>
                                <div class="order-info">
                                    <span class="date">12.12.12</span>
                                    <p class="discription">
                                        предложение в рубрику "Авто" на период 12.12.12 - 12.12.12
                                    </p>
                                </div>
                            </div>
                            <div class="order nobg">
                                <div class="order-header">
                                    <span class="name">Заказ № 656415354</span> - <span class="status">модерация пройдена, ожидает оплаты</span>
                                    <button class="hover-green">кнопка</button>
                                </div>
                                <div class="order-info">
                                    <span class="date">12.12.12</span>
                                    <p class="discription">
                                        предложение в рубрику "Авто" на период 12.12.12 - 12.12.12
                                    </p>
                                </div>
                            </div>
                            <div class="order">
                                <div class="order-header">
                                    <span class="name">Заказ № 656415354</span> - <span class="status">отправлен на модерацию</span>
                                    <button class="hover-green">кнопка</button>
                                </div>
                                <div class="order-info">
                                    <span class="date">12.12.12</span>
                                    <p class="discription">
                                        предложение в рубрику "Авто" на период 12.12.12 - 12.12.12
                                    </p>
                                </div>
                            </div>
                            <div class="order nobg">
                                <div class="order-header">
                                    <span class="name">Заказ № 656415354</span> - <span class="status">оплачено</span>
                                    <button class="hover-green">кнопка</button>
                                </div>
                                <div class="order-info">
                                    <span class="date">12.12.12</span>
                                    <p class="discription">
                                        предложение в рубрику "Авто" на период 12.12.12 - 12.12.12
                                    </p>
                                </div>
                            </div>
                            <div class="order">
                                <div class="order-header">
                                    <span class="name">Заказ № 656415354</span> - <span class="status">модерация пройдена, ожидает оплаты</span>
                                    <button class="hover-green">кнопка</button>
                                </div>
                                <div class="order-info">
                                    <span class="date">12.12.12</span>
                                    <p class="discription">
                                        предложение в рубрику "Авто" на период 12.12.12 - 12.12.12
                                    </p>
                                </div>
                            </div>
                            <div class="order nobg">
                                <div class="order-header">
                                    <span class="name">Заказ № 656415354</span> - <span class="status">отправлен на модерацию</span>
                                    <button class="hover-green">кнопка</button>
                                </div>
                                <div class="order-info">
                                    <span class="date">12.12.12</span>
                                    <p class="discription">
                                        предложение в рубрику "Авто" на период 12.12.12 - 12.12.12
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--            <div class="modal-footer"></div>-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</body>
</html>