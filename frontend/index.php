<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <script src="js/jquery-2.1.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.liquidcarousel.js"></script>
    <script src="js/cookie.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="css/template.css">
    <link rel="stylesheet" href="css/modules.css">
    <link rel="stylesheet" href="css/screen.css">
</head>
<body>
<div id="content-block">
<div class="row header center-content">
    <div class="span12">
        <div class="span6 p-relative lessspan">
            <div class="logo">
                <a href="/">
                    <img src="images/logo.png" alt="INFOSMART" title="INFOSMART">
                </a>
            </div>
            <div class="citywidget">
                <div class="cityname">
                    <span class="city-arrow"></span>
                    <span class="current-city">Астана</span>
                </div>
                <span class="selectcity">выберите город</span>
                <ul class="city-ul">
                    <li>Астана</li>
                    <li>Москва</li>
                    <li>Лондон</li>
                    <li>Деревня Лопухи</li>
                    <li>Бишкек</li>
                    <li>Астраханка</li>
                    <li>Вашингтон</li>
                    <li>Иваново</li>
                    <li>Прага</li>
                    <li>Степногорск</li>
                    <li>Павлодар</li>
                </ul>
            </div>
            <div class="infowidget">
                <span class="weather-day">+18&deg;<span class="dotes">...</span>+20&deg;</span>
                <span class="weather-night">+4&deg;<span class="dotes">...</span>+6&deg;</span>
                <span class="dollar">185.5<span class="depreciation"></span></span>
                <span class="euro">250.2<span class="growth-rate"></span></span>
                <span class="rub">5.4<span class="depreciation"></span></span>

                <div class="clr"></div>
            </div>
        </div>
        <div class="span6 morespan">
            <ul class="mainmenu">
                <li>
                    <a class="deals hover-green" href="deals.php">предложения</a>
                </li>
                <li>
                    <a class="map hover-green" href="map.php">карта</a>
                </li>
                <li>
                    <a class="news hover-green" href="#">новости</a>
                </li>
                <li>
                    <a class="personal-area hover-green" href="#">личный кабинет</a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="row search center-content">
    <div class="span12">
        <div class="span6 searchdiv">
            <div class="searchform">
                <form action="#">
                    <input type="text" name="search" placeholder="Введите ключевое слово" class="searchinput">
                    <input type="submit" name="button" class="searchbutton">

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="map"> карта
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="deals"> в предложениях
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="news"> в новостях
                        </label>
                    </div>
                </form>
                <div class="clr"></div>
            </div>
        </div>
        <div class="span6 adddiv">
            <div class="addblock">
                <a href="#" class="hover-green">Добавить компанию</a>
            </div>
        </div>
    </div>
</div>
<div class="row news">
    <div class="newsblock">
        <div class="newscontent">
            <img src="images/banner1.png">
        </div>
    </div>
    <div class="newsblock">
        <div class="newscontent">
            <img src="images/banner2.png">
        </div>
        <div class="newscontent">
            <img src="images/banner3.png">

            <div class="ptext">
                <span class="caption">Уголовное дело возбуждено в отношении "Гульнар тур"</span>

                <p class="text">Предварительный ущерб составил около шести миллионов</p>
            </div>
        </div>
    </div>
    <div class="clr"></div>
</div>
<div class="row headings center-content">
<div class="span12">
<div class="heading">
    <table class="headengtable table-striped">
        <thead>
        <tr>
            <th colspan="2">Lorem ipsum dolor</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="name">Sit amet, consectetur</td>
            <td class="count">26</td>
        </tr>
        <tr>
            <td class="name">Adipisicing elit, sed<span class="funtion">функция</span></td>
            <td class="count">71</td>
        </tr>
        <tr>
            <td class="name">Do eiusmod tempor incididunt</td>
            <td class="count">12</td>
        </tr>
        <tr>
            <td class="name">Ut labore et</td>
            <td class="count">210</td>
        </tr>
        </tbody>
    </table>
    <table class="headengtable table-striped">
        <thead>
        <tr>
            <th colspan="2">Dolore magna</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="name">Aliqua. Ut enim ad</td>
            <td class="count">114</td>
        </tr>
        <tr>
            <td class="name">Minim veniam, quis</td>
            <td class="count">69</td>
        </tr>
        <tr>
            <td class="name">Nostrud exercitation ullamco</td>
            <td class="count">42</td>
        </tr>
        <tr>
            <td class="name">Laboris nisi ut aliquip ex</td>
            <td class="count">93</td>
        </tr>
        <tr>
            <td class="name">Ea commodo consequat</td>
            <td class="count">12</td>
        </tr>
        <tr>
            <td class="name">Duis aute irure dolor in reprehenderit</td>
            <td class="count">26</td>
        </tr>
        <tr>
            <td class="name">In voluptate</td>
            <td class="count">12</td>
        </tr>
        </tbody>
    </table>
    <table class="headengtable table-striped">
        <thead>
        <tr>
            <th colspan="2">Perspiciatis</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="name">Unde omnis iste</td>
            <td class="count">6</td>
        </tr>
        <tr>
            <td class="name">Natus error</td>
            <td class="count">9</td>
        </tr>
        <tr>
            <td class="name">Sit voluptatem accusantium</td>
            <td class="count">24</td>
        </tr>
        <tr>
            <td class="name">Doloremque laudantium, totam</td>
            <td class="count">18</td>
        </tr>
        <tr>
            <td class="name">ab illo inventore veritatis et quasi</td>
            <td class="count">27</td>
        </tr>
        <tr>
            <td class="name">architecto beatae vitae dicta sunt explicab</td>
            <td class="count">2</td>
        </tr>
        <tr>
            <td class="name">Nemo enim ipsam voluptatem</td>
            <td class="count">12</td>
        </tr>
        <tr>
            <td class="name">quia voluptas</td>
            <td class="count">33</td>
        </tr>
        <tr>
            <td class="name">sit</td>
            <td class="count">4</td>
        </tr>
        </tbody>
    </table>
</div>
<div class="headingmargin"></div>
<div class="heading">
    <table class="headengtable table-striped">
        <thead>
        <tr>
            <th colspan="2">Velit</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="name">velit esse cillum dolore</td>
            <td class="count">13</td>
        </tr>
        <tr>
            <td class="name">eu fugiat</td>
            <td class="count">31</td>
        </tr>
        <tr>
            <td class="name">nulla pariatur. Excepteur sint occaecat</td>
            <td class="count">26</td>
        </tr>
        <tr>
            <td class="name">cupidatat non proident</td>
            <td class="count">21</td>
        </tr>
        <tr>
            <td class="name">sunt in culpa</td>
            <td class="count">13</td>
        </tr>
        <tr>
            <td class="name">qui officia deserunt</td>
            <td class="count">31</td>
        </tr>
        <tr>
            <td class="name">mollit anim</td>
            <td class="count">26</td>
        </tr>
        <tr>
            <td class="name">cupidatat non proident</td>
            <td class="count">21</td>
        </tr>
        <tr>
            <td class="name">id est laborum</td>
            <td class="count">26</td>
        </tr>
        <tr>
            <td class="name">Sed ut</td>
            <td class="count">21</td>
        </tr>
        </tbody>
    </table>
    <table class="headengtable table-striped">
        <thead>
        <tr>
            <th colspan="2">Aut odit</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="name">aut odit aut fugit</td>
            <td class="count">13</td>
        </tr>
        <tr>
            <td class="name">sed quia consequuntur</td>
            <td class="count">31</td>
        </tr>
        <tr>
            <td class="name">magni dolores eos</td>
            <td class="count">26</td>
        </tr>
        <tr>
            <td class="name">qui ratione voluptatem</td>
            <td class="count">21</td>
        </tr>
        <tr>
            <td class="name">sequi nesciunt. Neque</td>
            <td class="count">13</td>
        </tr>
        <tr>
            <td class="name">porro quisquam est</td>
            <td class="count">31</td>
        </tr>
        <tr>
            <td class="name">qui dolorem ipsum</td>
            <td class="count">26</td>
        </tr>
        <tr>
            <td class="name">cupidatat non proident</td>
            <td class="count">21</td>
        </tr>
        <tr>
            <td class="name">quia dolor sit amet</td>
            <td class="count">26</td>
        </tr>
        <tr>
            <td class="name">consectetur, adipisci velit</td>
            <td class="count">21</td>
        </tr>
        <tr>
            <td class="name">qui dolorem ipsum</td>
            <td class="count">26</td>
        </tr>
        <tr>
            <td class="name">sed quia non</td>
            <td class="count">21</td>
        </tr>
        <tr>
            <td class="name">numquam eius modi</td>
            <td class="count">26</td>
        </tr>
        <tr>
            <td class="name">tempora incidunt ut</td>
            <td class="count">21</td>
        </tr>
        </tbody>
    </table>
</div>
<div class="headingmargin"></div>
<div class="heading">
    <table class="headengtable table-striped">
        <thead>
        <tr>
            <th colspan="2">Perspiciatis</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="name">unde omnis iste</td>
            <td class="count">6</td>
        </tr>
        <tr>
            <td class="name">natus error</td>
            <td class="count">9</td>
        </tr>
        <tr>
            <td class="name">sit voluptatem accusantium</td>
            <td class="count">24</td>
        </tr>
        <tr>
            <td class="name">doloremque laudantium, totam</td>
            <td class="count">18</td>
        </tr>
        <tr>
            <td class="name">ab illo inventore veritatis et quasi</td>
            <td class="count">27</td>
        </tr>
        <tr>
            <td class="name">architecto beatae vitae dicta sunt explicab</td>
            <td class="count">2</td>
        </tr>
        <tr>
            <td class="name">Nemo enim ipsam voluptatem</td>
            <td class="count">12</td>
        </tr>
        <tr>
            <td class="name">quia voluptas</td>
            <td class="count">33</td>
        </tr>
        <tr>
            <td class="name">sit</td>
            <td class="count">4</td>
        </tr>
        </tbody>
    </table>
    <table class="headengtable table-striped">
        <thead>
        <tr>
            <th colspan="2">Lorem ipsum dolor</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="name">Sit amet, consectetur</td>
            <td class="count">26</td>
        </tr>
        <tr>
            <td class="name">Adipisicing elit, sed <span class="function">функция</span></td>
            <td class="count">71</td>
        </tr>
        <tr>
            <td class="name">do eiusmod tempor incididunt</td>
            <td class="count">12</td>
        </tr>
        <tr>
            <td class="name">ut labore et</td>
            <td class="count">210</td>
        </tr>
        </tbody>
    </table>
    <table class="headengtable table-striped">
        <thead>
        <tr>
            <th colspan="2">Dolore magna</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="name">aliqua. Ut enim ad</td>
            <td class="count">114</td>
        </tr>
        <tr>
            <td class="name">minim veniam, quis</td>
            <td class="count">69</td>
        </tr>
        <tr>
            <td class="name">nostrud exercitation ullamco</td>
            <td class="count">42</td>
        </tr>
        <tr>
            <td class="name">laboris nisi ut aliquip ex</td>
            <td class="count">93</td>
        </tr>
        <tr>
            <td class="name">ea commodo consequat</td>
            <td class="count">12</td>
        </tr>
        <tr>
            <td class="name">Duis aute irure dolor in reprehenderit</td>
            <td class="count">26</td>
        </tr>
        <tr>
            <td class="name">in voluptate</td>
            <td class="count">12</td>
        </tr>
        </tbody>
    </table>
</div>
<div class="headingmargin"></div>
<div class="heading">
    <table class="headengtable table-striped">
        <thead>
        <tr>
            <th colspan="2">Aut odit</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="name">aut odit aut fugit</td>
            <td class="count">13</td>
        </tr>
        <tr>
            <td class="name">sed quia consequuntur</td>
            <td class="count">31</td>
        </tr>
        <tr>
            <td class="name">magni dolores eos</td>
            <td class="count">26</td>
        </tr>
        <tr>
            <td class="name">qui ratione voluptatem</td>
            <td class="count">21</td>
        </tr>
        <tr>
            <td class="name">sequi nesciunt. Neque</td>
            <td class="count">13</td>
        </tr>
        <tr>
            <td class="name">porro quisquam est</td>
            <td class="count">31</td>
        </tr>
        <tr>
            <td class="name">qui dolorem ipsum</td>
            <td class="count">26</td>
        </tr>
        <tr>
            <td class="name">cupidatat non proident</td>
            <td class="count">21</td>
        </tr>
        <tr>
            <td class="name">quia dolor sit amet</td>
            <td class="count">26</td>
        </tr>
        <tr>
            <td class="name">consectetur, adipisci velit</td>
            <td class="count">21</td>
        </tr>
        <tr>
            <td class="name">qui dolorem ipsum</td>
            <td class="count">26</td>
        </tr>
        <tr>
            <td class="name">sed quia non</td>
            <td class="count">21</td>
        </tr>
        <tr>
            <td class="name">numquam eius modi</td>
            <td class="count">26</td>
        </tr>
        <tr>
            <td class="name">tempora incidunt ut</td>
            <td class="count">21</td>
        </tr>
        </tbody>
    </table>
    <table class="headengtable table-striped">
        <thead>
        <tr>
            <th colspan="2">Velit</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="name">velit esse cillum dolore</td>
            <td class="count">13</td>
        </tr>
        <tr>
            <td class="name">eu fugiat</td>
            <td class="count">31</td>
        </tr>
        <tr>
            <td class="name">nulla pariatur. Excepteur sint occaecat</td>
            <td class="count">26</td>
        </tr>
        <tr>
            <td class="name">cupidatat non proident</td>
            <td class="count">21</td>
        </tr>
        <tr>
            <td class="name">sunt in culpa</td>
            <td class="count">13</td>
        </tr>
        <tr>
            <td class="name">qui officia deserunt</td>
            <td class="count">31</td>
        </tr>
        <tr>
            <td class="name">mollit anim</td>
            <td class="count">26</td>
        </tr>
        <tr>
            <td class="name">cupidatat non proident</td>
            <td class="count">21</td>
        </tr>
        <tr>
            <td class="name">id est laborum</td>
            <td class="count">26</td>
        </tr>
        <tr>
            <td class="name">Sed ut</td>
            <td class="count">21</td>
        </tr>
        </tbody>
    </table>
</div>
</div>
</div>
<div class="wrap"></div>
</div>
<div class="row footer">
    <span class="upbutton">наверх</span>
    <ul class="footermenu">
        <li>
            <a href="#">Lorem</a>
        </li>
        <li>
            <a href="#">Ipsum</a>
        </li>
        <li>
            <a href="#">Dolor sit</a>
        </li>
        <li>
            <a href="#">Amet</a>
        </li>
        <li>
            <a href="#">Consectetur</a>
        </li>
        <li>
            <a href="#">Consectetur adipisicing</a>
        </li>
    </ul>
    <span
        class="footer-copirights">&copy;2014 elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</span>
    <span class="footer-email">email: zhiga_@mail.ru</span>
    <span class="footer-phone">тел: +7 778 152 24 33, +7 7172 522 466</span>
</div>
<script>
    /* Запускаем сразу после загрузки страницы */
    $(document).ready(function () {
        newsBlocks();
        // Читаем из куков текущий город
        var currentCity = readCookie("currentCity");
        currentCity = (currentCity != null) ? currentCity : $(".city-ul li").eq(0).text();
        $(".current-city").text(currentCity);

        $('.upbutton').click(function () {
            $('html, body').animate({scrollTop: 0}, 'slow');
        });

        $(".selectcity").click(function () {
            rotateBG();
            $(".city-ul").slideToggle();
        });

        $(".city-ul li").click(function () {
            var selectedCity = $(this).text();
            $(".current-city").text(selectedCity);
            // Записываем в куки текущий город
            writeCookie("currentCity", selectedCity, 7);
            rotateBG();
            $(".city-ul").slideToggle();
        });
    });

    function rotateBG() {
        if ($(".city-arrow").is(".rotate-bg")) {
            $(".city-arrow").removeClass("rotate-bg");
        } else {
            $(".city-arrow").addClass("rotate-bg");
        }
    }

    /* Используем при изменении ширины окна в режиме live */
    $(window).resize(function () {
        newsBlocks();
    });

    /* Собственно, меняем отступы для крайних в строке картинок, чтобы все было хорошо, дети росли, старики не болели, закончилась война */
    function newsBlocks() {
        var blocks = $(".newsblock");
        var width = window.innerWidth;
        var blockCount = parseInt(width / 320);
        // Высчитываем длину блока в процентах
        var newWidth = 100 / blockCount;
        // Переводим проценты в пиксели
        var newWidthPX = 1920 / 100 / newWidth;
        // Если длина блока в пикселях менее минимально необходимой,
        // уменьшаем кол-во блоков и пересчитываем проценты
        if (newWidthPX < 320) {
            blockCount--;
            newWidth = 100 / blockCount;
        }
        // Задаем новую длину для блоков
        for (var i = 0; i < blockCount; i++) {
            blocks.eq(i).css("width", newWidth + "%").show();
        }
        // Скрываем лишние
        for (blockCount; blockCount < blocks.length; blockCount++) {
            blocks.eq(blockCount).hide();
        }
    }
</script>
</body>
</html>