<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <script src="js/jquery-2.1.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.liquidcarousel.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="css/template.css">
    <link rel="stylesheet" href="css/modules.css">
    <link rel="stylesheet" href="css/screen.css">
    <link rel="stylesheet" href="css/admin.css">
</head>
<body>
    <div id="content-block">
        <div class="row header center-content">
            <div class="span12">
                <div class="span6 p-relative lessspan">
                    <div class="logo">
                        <a href="/">
                            <img src="images/logo.png" alt="INFOSMART" title="INFOSMART">
                        </a>
                    </div>
                </div>
                <div class="span6 morespan">
                    <ul class="admin-menu">
                        <li>
                            <a href="admin.php">пользователи</a>
                        </li>
                        <li>
                            <a href="cards.php">карточки</a>
                        </li>
                        <li>
                            <a href="news.php">новости</a>
                        </li>
                        <li>
                            <a href="#">статистика</a>
                        </li>
                        <li>
                            <a href="settings.php">настройки сайта</a>
                        </li>
                        <li>
                            <a href="#">выйти</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="span12">
                <div class="button-row">
                    <button class="hover-green">добавить модератора</button>
                    <button class="hover-green">добавить администратора</button>
                </div>
                <table class="table-bordered">
                    <thead>
                        <tr>
                            <th></th>
                            <th>№</th>
                            <th>Роль</th>
                            <th>Никнейм</th>
                            <th>Пароль</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><span class="delete"></span></td>
                            <td>1</td>
                            <td>Модератор</td>
                            <td>Zhiga</td>
                            <td>zxcasd</td>
                        </tr>
                        <tr>
                            <td><span class="delete"></span></td>
                            <td>2</td>
                            <td>Модератор</td>
                            <td>Zhalgas</td>
                            <td>zxcasd</td>
                        </tr>
                        <tr>
                            <td><span class="delete"></span></td>
                            <td>3</td>
                            <td>Модератор</td>
                            <td>Jon doe</td>
                            <td>zxcasd</td>
                        </tr>
                        <tr>
                            <td><span class="delete"></span></td>
                            <td>4</td>
                            <td>Администратор</td>
                            <td>Admin</td>
                            <td>zxcasd</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
<!--        <div class="wrap"></div>-->
    </div>
</body>
</html>