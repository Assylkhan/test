<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <script src="js/jquery-2.1.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.liquidcarousel.js"></script>
    <script src="js/cookie.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="css/template.css">
    <link rel="stylesheet" href="css/modules.css">
    <link rel="stylesheet" href="css/screen.css">
    <script>
        /* Запускаем сразу после загрузки страницы */
        $( document ).ready(function() {
            // Читаем из куков текущий город
            var currentCity = readCookie("currentCity");
            currentCity = (currentCity != null) ? currentCity : $(".city-ul li").eq(0).text();
            $(".current-city").text(currentCity);

            $('.upbutton').click(function(){
                $('html, body').animate({scrollTop:0}, 'slow');
            });

            $(".selectcity").click(function(){
                rotateBG();
                $(".city-ul").slideToggle();
            });

            $(".city-ul li").click(function(){
                var selectedCity = $( this).text();
                $(".current-city").text(selectedCity);
                // Записываем в куки текущий город
                writeCookie("currentCity", selectedCity, 7);
                rotateBG();
                $(".city-ul").slideToggle();
            });
        });

        function rotateBG() {
            if ( $(".city-arrow").is(".rotate-bg") ) {
                $(".city-arrow").removeClass("rotate-bg");
            } else {
                $(".city-arrow").addClass("rotate-bg");
            }
        }
    </script>
</head>
<body>
<div id="content-block">
    <div class="row header center-content">
        <div class="span12">
            <div class="span6 p-relative lessspan">
                <div class="logo">
                    <a href="/">
                        <img src="images/logo.png" alt="INFOSMART" title="INFOSMART">
                    </a>
                </div>
                <div class="citywidget">
                    <div class="cityname">
                        <span class="city-arrow"></span>
                        <span class="current-city">Астана</span>
                    </div>
                    <span class="selectcity">выберите город</span>
                    <ul class="city-ul">
                        <li>Астана</li>
                        <li>Москва</li>
                        <li>Лондон</li>
                        <li>Деревня Лопухи</li>
                        <li>Бишкек</li>
                        <li>Астраханка</li>
                        <li>Вашингтон</li>
                        <li>Иваново</li>
                        <li>Прага</li>
                        <li>Степногорск</li>
                        <li>Павлодар</li>
                    </ul>
                </div>
                <div class="infowidget">
                    <span class="weather-day">+18&deg;<span class="dotes">...</span>+20&deg;</span>
                    <span class="weather-night">+4&deg;<span class="dotes">...</span>+6&deg;</span>
                    <span class="dollar">185.5<span class="depreciation"></span></span>
                    <span class="euro">250.2<span class="growth-rate"></span></span>
                    <span class="rub">5.4<span class="depreciation"></span></span>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="span6 morespan">
                <ul class="mainmenu">
                    <li>
                        <a class="deals hover-green" href="deals.php">предложения</a>
                    </li>
                    <li>
                        <a class="map hover-green" href="#">карта</a>
                    </li>
                    <li>
                        <a class="news hover-green" href="#">новости</a>
                    </li>
                    <li>
                        <a class="personal-area hover-green" href="#">личный кабинет</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row search center-content">
        <div class="span12">
            <div class="span6 searchdiv">
                <div class="searchform">
                    <form action="#">
                        <input type="text" name="search" placeholder="Введите ключевое слово" class="searchinput">
                        <input type="submit" name="button" class="searchbutton">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="map"> карта
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="deals"> в предложениях
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="news"> в новостях
                            </label>
                        </div>
                        <div class="open-advanced">
                            раскрыть <span>расширенный поиск</span>
                        </div>
                    </form>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="span6 adddiv">
                <div class="addblock">
                    <a href="#" class="hover-green">Добавить компанию</a>
                </div>
            </div>
        </div>
        <div class="span12 advanced-search">
            <div class="span6">
                <label for="categories">Искать в категории:</label>
                <select id="categories">
                    <option value="">Все категории</option>
                    <option value="">Категория 1</option>
                    <option value="">Категория 2</option>
                    <option value="">Категория 3</option>
                    <option value="">Категория 4</option>
                    <option value="">Категория 5</option>
                </select>
                <div class="search-results">
                    <span class="result-label">Поиск:</span>
                    <span class="search-param">"New Serv"</span>
                    <span class="result-count">(предприятий:56)</span>
                </div>
            </div>
            <div class="span6 right-content">
                <label for="cities">Искать в городах:</label>
                <select id="cities">
                    <option value="">Все города</option>
                    <option value="">Город 1</option>
                    <option value="">Город 2</option>
                    <option value="">Город 3</option>
                    <option value="">Город 4</option>
                    <option value="">Город 5</option>
                </select>
                <div class="search-buttons">
                    <button class="search-button hover-green">кнопка</button>
                    <button class="search-button hover-green">кнопка</button>
                    <button class="search-button hover-green">кнопка</button>
                    <button class="search-button hover-green">кнопка</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row seasrch-result center-content">
        <div class="span12">
            <div class="chargeable-unit">
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
            </div>
        </div>
        <div class="span12">
            <div class="free-block">
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="category">Категория, к...</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="category">Категория, к...</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="category">Категория, к...</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="category">Категория, к...</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="category">Категория, к...</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="category">Категория, к...</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="category">Категория, к...</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="category">Категория, к...</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="category">Категория, к...</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="category">Категория, к...</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="category">Категория, к...</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="category">Категория, к...</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="category">Категория, к...</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="category">Категория, к...</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="category">Категория, к...</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="category">Категория, к...</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="category">Категория, к...</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
                <div class="unit">
                    <span class="name">New ServiceArt</span>
                    <span class="category">Категория, к...</span>
                    <span class="phones">+7 (778) 152 24 33, +7 (7172) 625 648</span>
                    <span class="address">ВП24, 13 Достык</span>
                </div>
            </div>
        </div>
        <div class="span12">
            <a href="#" class="more-result hover-green">Другие</a>
        </div>
    </div>
    <div class="wrap"></div>
</div>
<div class="row footer">
    <span class="upbutton">наверх</span>
    <ul class="footermenu">
        <li>
            <a href="#">Lorem</a>
        </li>
        <li>
            <a href="#">Ipsum</a>
        </li>
        <li>
            <a href="#">Dolor sit</a>
        </li>
        <li>
            <a href="#">Amet</a>
        </li>
        <li>
            <a href="#">Consectetur</a>
        </li>
        <li>
            <a href="#">Consectetur adipisicing</a>
        </li>
    </ul>
    <span class="footer-copirights">&copy;2014 elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</span>
    <span class="footer-email">email: zhiga_@mail.ru</span>
    <span class="footer-phone">тел: +7 778 152 24 33, +7 7172 522 466</span>
</div>
</body>
</html>