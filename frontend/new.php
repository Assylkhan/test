<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <script src="js/jquery-2.1.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.liquidcarousel.js"></script>
    <script src="js/cookie.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="css/template.css">
    <link rel="stylesheet" href="css/modules.css">
    <link rel="stylesheet" href="css/screen.css">
    <script>
        /* Запускаем сразу после загрузки страницы */
        $( document ).ready(function() {
            // Читаем из куков текущий город
            var currentCity = readCookie("currentCity");
            currentCity = (currentCity != null) ? currentCity : $(".city-ul li").eq(0).text();
            $(".current-city").text(currentCity);

            $('.upbutton').click(function(){
                $('html, body').animate({scrollTop:0}, 'slow');
            });

            $(".selectcity").click(function(){
                rotateBG();
                $(".city-ul").slideToggle();
            });

            $(".city-ul li").click(function(){
                var selectedCity = $( this).text();
                $(".current-city").text(selectedCity);
                // Записываем в куки текущий город
                writeCookie("currentCity", selectedCity, 7);
                rotateBG();
                $(".city-ul").slideToggle();
            });

            function rotateBG() {
                if ( $(".city-arrow").is(".rotate-bg") ) {
                    $(".city-arrow").removeClass("rotate-bg");
                } else {
                    $(".city-arrow").addClass("rotate-bg");
                }
            }
        });
    </script>
</head>
<body>
<div id="content-block">
    <div class="row header center-content">
        <div class="span12">
            <div class="span6 p-relative lessspan">
                <div class="logo">
                    <a href="/">
                        <img src="images/logo.png" alt="INFOSMART" title="INFOSMART">
                    </a>
                </div>
                <div class="citywidget">
                    <div class="cityname">
                        <span class="city-arrow"></span>
                        <span class="current-city">Астана</span>
                    </div>
                    <span class="selectcity">выберите город</span>
                    <ul class="city-ul">
                        <li>Астана</li>
                        <li>Москва</li>
                        <li>Лондон</li>
                        <li>Деревня Лопухи</li>
                        <li>Бишкек</li>
                        <li>Астраханка</li>
                        <li>Вашингтон</li>
                        <li>Иваново</li>
                        <li>Прага</li>
                        <li>Степногорск</li>
                        <li>Павлодар</li>
                    </ul>
                </div>
                <div class="infowidget">
                    <span class="weather-day">+18&deg;<span class="dotes">...</span>+20&deg;</span>
                    <span class="weather-night">+4&deg;<span class="dotes">...</span>+6&deg;</span>
                    <span class="dollar">185.5<span class="depreciation"></span></span>
                    <span class="euro">250.2<span class="growth-rate"></span></span>
                    <span class="rub">5.4<span class="depreciation"></span></span>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="span6 morespan">
                <ul class="mainmenu">
                    <li>
                        <a class="deals hover-green" href="deals.php">предложения</a>
                    </li>
                    <li>
                        <a class="map hover-green" href="#">карта</a>
                    </li>
                    <li>
                        <a class="news hover-green" href="#">новости</a>
                    </li>
                    <li>
                        <a class="personal-area hover-green" href="#">личный кабинет</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row search center-content">
        <div class="span12">
            <div class="span6 searchdiv">
                <div class="searchform">
                    <form action="#">
                        <input type="text" name="search" placeholder="Введите ключевое слово" class="searchinput">
                        <input type="submit" name="button" class="searchbutton">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="map"> карта
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="deals"> в предложениях
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="news"> в новостях
                            </label>
                        </div>
                        <div class="open-advanced">
                            раскрыть <span>расширенный поиск</span>
                        </div>
                    </form>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="span6 adddiv">
                <div class="addblock">
                    <a href="#" class="hover-green">Добавить компанию</a>
                </div>
            </div>
        </div>
        <div class="span12 new">
            <div class="header-row">
                <h1>Название новости</h1>
                <span class="public-date">12.12.12</span>
            </div>
            <div class="new-content">
                <div class="img-block">
                    <img src="images/new_img.png" alt="New" title="New">
                    <span class="source">источник</span>
                </div>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ac massa commodo, feugiat felis id,
                    elementum dolor. Etiam dolor purus, congue a nulla nec, tincidunt cursus ipsum. Class aptent taciti
                    sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum gravida orci non
                    odio auctor convallis. Duis consequat erat quis odio euismod volutpat. Pellentesque ac faucibus ipsum,
                    a volutpat augue. Etiam ornare metus vitae elit fermentum, in tempus tortor eleifend. Donec ut fermentum
                    dolor. Morbi dapibus sapien sit amet nisi iaculis semper. Sed tempor elit at erat suscipit suscipit.
                    Aliquam non ante tellus. Curabitur tincidunt et mauris ut ultricies. Etiam congue metus et pretium porta.
                    Mauris mauris dolor, luctus non imperdiet ut, hendrerit vel sapien. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ac massa commodo, feugiat felis id,
                    elementum dolor. Etiam dolor purus, congue a nulla nec, tincidunt cursus ipsum. Class aptent taciti
                    sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum gravida orci non
                    odio auctor convallis. Duis consequat erat quis odio euismod volutpat. Pellentesque ac faucibus ipsum,
                    a volutpat augue. Etiam ornare metus vitae elit fermentum, in tempus tortor eleifend. Donec ut fermentum
                    dolor. Morbi dapibus sapien sit amet nisi iaculis semper. Sed tempor elit at erat suscipit suscipit.
                    Aliquam non ante tellus. Curabitur tincidunt et mauris ut ultricies. Etiam congue metus et pretium porta.
                    Mauris mauris dolor, luctus non imperdiet ut, hendrerit vel sapien.
                </p>
                <p>
                    Curabitur blandit commodo ante quis varius. Etiam porta dui et odio consequat ornare. Aliquam sit amet
                    erat ac augue fermentum pharetra a eget lectus. Duis convallis enim id augue ultricies porttitor. Donec
                    id lacus vel leo sodales luctus nec ut ante. Nullam vel dictum lacus, vel gravida dolor. Ut pharetra
                    consequat magna et pellentesque. Ut lobortis neque ac dui condimentum tristique. Maecenas dignissim turpis
                    enim, ut condimentum justo hendrerit sed. Phasellus tincidunt, leo sit amet ullamcorper tincidunt, libero
                    nibh dignissim est, vitae porttitor ante justo et mi. Nullam adipiscing tortor elit, vel mattis felis
                    suscipit consequat. Nam et tristique metus. Donec eget metus vitae ante vehicula mollis. Maecenas congue
                    mattis turpis, sed ultrices arcu ullamcorper eget. Curabitur blandit commodo ante quis varius. Etiam porta dui et odio consequat ornare. Aliquam sit amet
                    erat ac augue fermentum pharetra a eget lectus. Duis convallis enim id augue ultricies porttitor. Donec
                    id lacus vel leo sodales luctus nec ut ante. Nullam vel dictum lacus, vel gravida dolor. Ut pharetra
                    consequat magna et pellentesque. Ut lobortis neque ac dui condimentum tristique. Maecenas dignissim turpis
                    enim, ut condimentum justo hendrerit sed. Phasellus tincidunt, leo sit amet ullamcorper tincidunt, libero
                    nibh dignissim est, vitae porttitor ante justo et mi. Nullam adipiscing tortor elit, vel mattis felis
                    suscipit consequat. Nam et tristique metus. Donec eget metus vitae ante vehicula mollis. Maecenas congue
                    mattis turpis, sed ultrices arcu ullamcorper eget. Curabitur blandit commodo ante quis varius. Etiam porta dui et odio consequat ornare. Aliquam sit amet
                    erat ac augue fermentum pharetra a eget lectus. Duis convallis enim id augue ultricies porttitor. Donec
                    id lacus vel leo sodales luctus nec ut ante. Nullam vel dictum lacus, vel gravida dolor. Ut pharetra
                    consequat magna et pellentesque. Ut lobortis neque ac dui condimentum tristique. Maecenas dignissim turpis
                    enim, ut condimentum justo hendrerit sed. Phasellus tincidunt, leo sit amet ullamcorper tincidunt, libero
                    nibh dignissim est, vitae porttitor ante justo et mi. Nullam adipiscing tortor elit, vel mattis felis
                    suscipit consequat. Nam et tristique metus. Donec eget metus vitae ante vehicula mollis. Maecenas congue
                    mattis turpis, sed ultrices arcu ullamcorper eget.
                </p>
                <p>
                    Sed sed erat et nunc iaculis tristique. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nulla
                    dictum lectus feugiat, congue felis vel, tincidunt lectus. Aenean non posuere nisl, vel vestibulum nisi.
                    Etiam nisi nunc, eleifend at dui et, facilisis tempor mi. Aenean eu scelerisque ante, eu pellentesque ipsum.
                    Maecenas at enim commodo, dignissim lorem ac, hendrerit leo. Cras condimentum, mauris ac dapibus tempus, nulla
                    sem pellentesque tortor, eu blandit augue enim id elit. Mauris lacinia aliquet mauris, ac tempor ligula porta
                    eget. Nulla facilisi. Phasellus commodo id odio tincidunt ultrices. Donec tristique lectus tellus. Praesent ac
                    ultrices odio. Curabitur auctor egestas leo a pharetra. Suspendisse vitae lacus malesuada, sollicitudin nulla
                    id, bibendum velit. Proin consectetur, orci sit amet ornare ultrices, lorem turpis sollicitudin dolor, quis
                    hendrerit massa enim vitae nisi. Sed sed erat et nunc iaculis tristique. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nulla
                    dictum lectus feugiat, congue felis vel, tincidunt lectus. Aenean non posuere nisl, vel vestibulum nisi.
                    Etiam nisi nunc, eleifend at dui et, facilisis tempor mi. Aenean eu scelerisque ante, eu pellentesque ipsum.
                    Maecenas at enim commodo, dignissim lorem ac, hendrerit leo. Cras condimentum, mauris ac dapibus tempus, nulla
                    sem pellentesque tortor, eu blandit augue enim id elit. Mauris lacinia aliquet mauris, ac tempor ligula porta
                    eget. Nulla facilisi. Phasellus commodo id odio tincidunt ultrices. Donec tristique lectus tellus. Praesent ac
                    ultrices odio.
                </p>
                <div class="img-block">
                    <img src="images/new_img.png" alt="New" title="New">
                    <span class="source">источник</span>
                </div>
                <p>
                    <strong>Integer dictum scelerisque gravida. Curabitur lobortis orci velit, quis varius ipsum consequat sit amet.
                    Praesent egestas tortor nec neque imperdiet facilisis. Donec commodo, lacus in hendrerit auctor, tortor nulla
                    consequat diam, vitae sagittis felis enim ut nulla.</strong> Ut semper sem at quam tristique cursus. Lorem ipsum dolor
                    sit amet, consectetur adipiscing elit. Suspendisse id nisl eu ante fermentum egestas. Suspendisse non vulputate
                    nunc. Morbi suscipit tortor quam, quis rhoncus nibh iaculis quis. Duis et vestibulum nibh. Morbi ut magna dolor.
                    Vestibulum sollicitudin non quam eu ullamcorper. Praesent molestie mauris at mauris rutrum, quis pellentesque
                    lectus lacinia. Nam facilisis orci leo, condimentum consequat mauris eleifend sed. Ut semper sem at quam tristique cursus. Lorem ipsum dolor
                    sit amet, consectetur adipiscing elit. Suspendisse id nisl eu ante fermentum egestas. Suspendisse non vulputate
                    nunc. Morbi suscipit tortor quam, quis rhoncus nibh iaculis quis. Duis et vestibulum nibh. Morbi ut magna dolor.
                    Vestibulum sollicitudin non quam eu ullamcorper. Praesent molestie mauris at mauris rutrum, quis pellentesque
                    lectus lacinia. Nam facilisis orci leo, condimentum consequat mauris eleifend sed. Suspendisse non vulputate
                    nunc. Morbi suscipit tortor quam, quis rhoncus nibh iaculis quis. Duis et vestibulum nibh. Morbi ut magna dolor.
                    Vestibulum sollicitudin non quam eu ullamcorper. Praesent molestie mauris at mauris rutrum, quis pellentesque
                    lectus lacinia. Nam facilisis orci leo, condimentum consequat mauris eleifend sed.
                </p>
                <p>
                    Etiam felis elit, ultrices ut dolor a, ullamcorper consequat nulla. Nam posuere purus vel neque tincidunt aliquam.
                    Curabitur quis justo sem. Morbi ac purus nec dolor tempor tincidunt. Maecenas iaculis risus sed odio tristique
                    porttitor. Pellentesque semper sit amet diam ut volutpat. Curabitur mi mi, tincidunt sit amet eleifend non, rhoncus
                    at enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus felis
                    augue, suscipit sollicitudin consequat vitae, vestibulum ac ligula. In ut aliquet lectus, posuere volutpat turpis.
                    Morbi rhoncus lobortis sem, quis vehicula mi tincidunt eu. Etiam felis elit, ultrices ut dolor a, ullamcorper consequat nulla. Nam posuere purus vel neque tincidunt aliquam.
                    Curabitur quis justo sem. Morbi ac purus nec dolor tempor tincidunt. Maecenas iaculis risus sed odio tristique
                    porttitor. Pellentesque semper sit amet diam ut volutpat. Curabitur mi mi, tincidunt sit amet eleifend non, rhoncus
                    at enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus felis
                    augue, suscipit sollicitudin consequat vitae, vestibulum ac ligula. In ut aliquet lectus, posuere volutpat turpis.
                    Morbi rhoncus lobortis sem, quis vehicula mi tincidunt eu. Etiam felis elit, ultrices ut dolor a, ullamcorper consequat nulla. Nam posuere purus vel neque tincidunt aliquam.
                    Curabitur quis justo sem. Morbi ac purus nec dolor tempor tincidunt. Maecenas iaculis risus sed odio tristique
                    porttitor. Pellentesque semper sit amet diam ut volutpat. Curabitur mi mi, tincidunt sit amet eleifend non, rhoncus
                    at enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus felis
                    augue, suscipit sollicitudin consequat vitae, vestibulum ac ligula. In ut aliquet lectus, posuere volutpat turpis.
                    Morbi rhoncus lobortis sem, quis vehicula mi tincidunt eu. Etiam felis elit, ultrices ut dolor a, ullamcorper consequat nulla. Nam posuere purus vel neque tincidunt aliquam.
                    Curabitur quis justo sem. Morbi ac purus nec dolor tempor tincidunt. Maecenas iaculis risus sed odio tristique
                    porttitor. Pellentesque semper sit amet diam ut volutpat. Curabitur mi mi, tincidunt sit amet eleifend non, rhoncus
                    at enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus felis
                    augue, suscipit sollicitudin consequat vitae, vestibulum ac ligula. In ut aliquet lectus, posuere volutpat turpis.
                    Morbi rhoncus lobortis sem, quis vehicula mi tincidunt eu.
                </p>
                <span class="new-author">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                </span>
            </div>
        </div>
    </div>
    <div class="wrap"></div>
</div>
<div class="row footer">
    <span class="upbutton">наверх</span>
    <ul class="footermenu">
        <li>
            <a href="#">Lorem</a>
        </li>
        <li>
            <a href="#">Ipsum</a>
        </li>
        <li>
            <a href="#">Dolor sit</a>
        </li>
        <li>
            <a href="#">Amet</a>
        </li>
        <li>
            <a href="#">Consectetur</a>
        </li>
        <li>
            <a href="#">Consectetur adipisicing</a>
        </li>
    </ul>
    <span class="footer-copirights">&copy;2014 elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</span>
    <span class="footer-email">email: zhiga_@mail.ru</span>
    <span class="footer-phone">тел: +7 778 152 24 33, +7 7172 522 466</span>
</div>
</body>
</html>