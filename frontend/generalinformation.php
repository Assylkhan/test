<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <script src="js/jquery-2.1.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.liquidcarousel.js"></script>
    <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="js/cookie.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="css/template.css">
    <link rel="stylesheet" href="css/modules.css">
    <link rel="stylesheet" href="css/screen.css">
    <script>
        /* Запускаем сразу после загрузки страницы */
        $( document ).ready(function() {
            marginBlocks();

            // Читаем из куков текущий город
            var currentCity = readCookie("currentCity");
            currentCity = (currentCity != null) ? currentCity : $(".city-ul li").eq(0).text();
            $(".current-city").text(currentCity);

            $('.upbutton').click(function(){
                $('html, body').animate({scrollTop:0}, 'slow');
            });

            $(".selectcity").click(function(){
                rotateBG();
                $(".city-ul").slideToggle();
            });

            $(".city-ul li").click(function(){
                var selectedCity = $( this).text();
                $(".current-city").text(selectedCity);
                // Записываем в куки текущий город
                writeCookie("currentCity", selectedCity, 7);
                rotateBG();
                $(".city-ul").slideToggle();
            });

            // Показываем лист контактов, которые можно добавить
            $(".contact-button").click(function() {
                  $("#contactList").show();
            });

            // Добавляем поля для контакта, выбранного из списка
            $("#contactList").change(function() {
                var type = parseInt($("#contactList option:selected").val());
                $("#contactList").hide();

                var contactName = getName(type);
                var contactLabel = getLabel(type);
                var tr = document.createElement("tr");

                var td1 = document.createElement("td");
                var label = document.createElement("label");
                label.innerHTML = contactLabel;
                var span = document.createElement("span");
                span.setAttribute("class", "explanation");
                span.innerHTML = "пояснение к пункту";
                td1.appendChild(label);
                td1.appendChild(span);

                var td2 = document.createElement("td");
                var input = document.createElement("input");
                input.setAttribute("type", "text");
                input.setAttribute("name", contactName + "[]");
                input.setAttribute("class", "registration-input");
                td2.appendChild(input);

                tr.appendChild(td1);
                tr.appendChild(td2);
                $(".formtable tbody").append(tr);
            });

            // Добавляем рубрику / подрубрику
            $(".add-rubric").click(function() {
                var rubric = $(".rubric-tr").eq(0).clone();
                var last = $(".subrubric-tr").length - 1;
                $(rubric).insertAfter($(".subrubric-tr").eq(last));
                var subrubric = $(".subrubric-tr").eq(0).clone();
                var slast = $(".rubric-tr").length - 1;
                $(subrubric).insertAfter($(".rubric-tr").eq(slast));
            });
        });

        function rotateBG() {
            if ( $(".city-arrow").is(".rotate-bg") ) {
                $(".city-arrow").removeClass("rotate-bg");
            } else {
                $(".city-arrow").addClass("rotate-bg");
            }
        }

        function getName(type) {
            var name = "";

            switch(type) {
                case 1: name = "phone"; break;
                case 2: name = "email"; break;
                case 3: name = "site"; break;
                case 4: name = "messenger"; break;
            }

            return name;
        }

        function getLabel(type) {
            var label = "";

            switch(type) {
                case 1: label = "Телефон:"; break;
                case 2: label = "Электронная почта:"; break;
                case 3: label = "Сайт:"; break;
                case 4: label = "Интернет мессенджер:"; break;
            }

            return label;
        }

        function createDeal() {
            if ($('#create_form').is(":visible")) {
                $('#create_form').slideUp('slow');
            } else {
                $('#create_form').slideDown('slow');
            }
        }

        function editDeal(event) {
            var param = event.target.getAttribute("param");
            if ($("#form_" + param).is(":visible")) {
                $("#form_" + param).slideUp('slow');
            } else {
                $("#form_" + param).slideDown('slow');
            }
        }

        /* Используем при изменении ширины окна в режиме live */
        $( window ).resize(function() {
            $(".img-block").removeClass("no-right")
            marginBlocks()
        });

        /* Собственно, меняем отступы для крайних в строке картинок, чтобы все было хорошо, дети росли, старики не болели, закончилась война */
        function marginBlocks() {
            var blocks = $(".img-block");
            var length = blocks.length;
            var width = window.innerWidth;
            var num = width < 1440 ? 4 : 6;
            if (length > 0) {
                for (var i = num; i < length; i += num + 1) {
                    if (blocks.eq(i)) {
                        blocks.eq(i).addClass("no-right");
                    }
                }
            }
        }
    </script>
</head>
<body>
<div id="content-block">
    <div class="row header center-content">
        <div class="span12">
            <div class="span6 p-relative lessspan">
                <div class="logo">
                    <a href="/">
                        <img src="images/logo.png" alt="INFOSMART" title="INFOSMART">
                    </a>
                </div>
                <div class="citywidget">
                    <div class="cityname">
                        <span class="city-arrow"></span>
                        <span class="current-city">Астана</span>
                    </div>
                    <span class="selectcity">выберите город</span>
                    <ul class="city-ul">
                        <li>Астана</li>
                        <li>Москва</li>
                        <li>Лондон</li>
                        <li>Деревня Лопухи</li>
                        <li>Бишкек</li>
                        <li>Астраханка</li>
                        <li>Вашингтон</li>
                        <li>Иваново</li>
                        <li>Прага</li>
                        <li>Степногорск</li>
                        <li>Павлодар</li>
                    </ul>
                </div>
                <div class="infowidget">
                    <span class="weather-day">+18&deg;<span class="dotes">...</span>+20&deg;</span>
                    <span class="weather-night">+4&deg;<span class="dotes">...</span>+6&deg;</span>
                    <span class="dollar">185.5<span class="depreciation"></span></span>
                    <span class="euro">250.2<span class="growth-rate"></span></span>
                    <span class="rub">5.4<span class="depreciation"></span></span>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="span6 morespan">
                <ul class="mainmenu">
                    <li>
                        <a class="deals hover-green" href="deals.php">предложения</a>
                    </li>
                    <li>
                        <a class="map hover-green" href="map.php">карта</a>
                    </li>
                    <li>
                        <a class="news hover-green" href="#">новости</a>
                    </li>
                    <li>
                        <a class="personal-area hover-green" href="#">личный кабинет</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row registration center-content">
        <div class="span12">
            <h1>Личный кабинет - <span>Название предприятия</span></h1>
            <button class="preview-button hover-green">просмотр карточки</button>
        </div>

        <div class="span12 personal-menu">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-general" id="general" role="tab" data-toggle="tab">Общая информация</a></li>
                <li><a href="#tab-onmap" id="onmap" role="tab" data-toggle="tab">На карте</a></li>
                <li><a href="#tab-padeals" id="deals" role="tab" data-toggle="tab">Предложения</a></li>
                <li><a href="#tab-foto" id="foto" role="tab" data-toggle="tab">Фото</a></li>
                <li><a href="#tab-job" id="job" role="tab" data-toggle="tab">Вакансии</a></li>
            </ul>
        </div>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
                <div class="span12 registration2">
                    <div class="span6 morespan">
                        <table class="formtable">
                            <tbody>
                                <tr>
                                    <td>
                                        <label for="companyName">Название организации<span class="required">*</span>:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <input name="companyName" id="companyName" class="registration-input">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="ownership">Вид (форма) собственности:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <select name="ownership" id="ownership" class="registration-select">
                                            <option value="1">ТОО</option>
                                            <option value="2">ИП</option>
                                            <option value="3">АО</option>
                                            <option value="4">ОАО</option>
                                            <option value="5">ЗАО</option>
                                            <option value="6">ООО</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="logo">Логотип организации:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <input type="file" name="logo" id="logo">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="description">Описание компании:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <textarea name="description" id="description" class="registration-textarea"></textarea>
                                    </td>
                                </tr>
                                <tr class="rubric-tr">
                                    <td>
                                        <label for="">Рубрика<span class="required">*</span>:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <select name="rubric[]" class="registration-select">
                                            <option value="1">Рубрика 1</option>
                                            <option value="2">Рубрика 2</option>
                                            <option value="3">Рубрика 3</option>
                                            <option value="4">Рубрика 4</option>
                                            <option value="5">Рубрика 5</option>
                                            <option value="6">Рубрика 6</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr class="subrubric-tr">
                                    <td>
                                        <label for="">Подрубрика<span class="required">*</span>:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <select name="subrubric[]" class="registration-select">
                                            <option value="1">Подрубрика 1</option>
                                            <option value="2">Подрубрика 2</option>
                                            <option value="3">Подрубрика 3</option>
                                            <option value="4">Подрубрика 4</option>
                                            <option value="5">Подрубрика 5</option>
                                            <option value="6">Подрубрика 6</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <button class="hover-green add-rubric">добавить рубрику</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="keynum">Количество ключевых слов:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <select name="keynum" id="keynum" class="registration-select">
                                            <option value="50" selected>50</option>
                                            <option value="100">100</option>
                                            <option value="150">150</option>
                                            <option value="500">500</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="keywords">Ключевые слова:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <textarea name="keywords" id="keywords" class="registration-textarea"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="index">Индекс<span class="required">*</span>:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <input type="text" name="index" id="index" class="registration-input">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="region">Область<span class="required">*</span>:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <select name="region" id="region" class="registration-select">
                                            <option value="Астана">Астана</option>
                                            <option value="Алматы">Алматы</option>
                                            <option value="Байконыр">Байконыр</option>
                                            <option value="Кокшетау">Кокшетау</option>
                                            <option value="Актобе">Актобе</option>
                                            <option value="Талдыкорган">Талдыкорган</option>
                                            <option value="Атырау">Атырау</option>
                                            <option value="Усть-Каменогорск">Усть-Каменогорск</option>
                                            <option value="Тараз">Тараз</option>
                                            <option value="Уральск">Уральск</option>
                                            <option value="Караганда">Караганда</option>
                                            <option value="Костанай">Костанай</option>
                                            <option value="Кызылорда">Кызылорда</option>
                                            <option value="Актау">Актау</option>
                                            <option value="Павлодар">Павлодар</option>
                                            <option value="Петропавловск">Петропавловск</option>
                                            <option value="Шымкент">Шымкент</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="city">Город<span class="required">*</span>:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <input type="text" name="city" id="city" class="registration-input">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="street">Улица<span class="required">*</span>:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <input type="text" name="street" id="street" class="registration-input">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="house">Дом<span class="required">*</span>:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <input type="text" name="house" id="house" class="registration-input">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="office">Офис:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <input type="text" name="office" id="office" class="registration-input">
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <button class="hover-green contact-button">добавить контакт</button>
                                        <select name="contactList" id="contactList" class="registration-select">
                                            <option value="3">сайт</option>
                                            <option value="1">телефон</option>
                                            <option value="2">электронная почта</option>
                                            <option value="4">интернет мессенджер</option>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="span6 lessspan">
                        <table class="infotable table-striped">
                            <tbody>
                            <tr>
                                <td colspan="2" class="info-label">На аккаунте</td>
                            </tr>
                            <tr>
                                <td>карточка предприятия, включая 3 рубрики, 3 подрубрики, 50 ключевых слов</td>
                                <td>99 999тг</td>
                            </tr>
                            <tr>
                                <td>выделение карточки в топ</td>
                                <td>99 999тг</td>
                            </tr>
                            <tr>
                                <td>дополнительное количество подкатегорий - 14</td>
                                <td>99 999тг</td>
                            </tr>
                            <tr>
                                <td>увеличенное количество ключевых слов - 150</td>
                                <td>99 999тг</td>
                            </tr>
                            <tr>
                                <td>увеличенное</td>
                                <td>99 999тг</td>
                            </tr>
                            <tr>
                                <td class="info-label">Корзина</td>
                                <td>99 999тг</td>
                            </tr>
                            <tr>
                                <td>пункт корзины</td>
                                <td>99 999тг</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab-onmap">
                <div class="span12 onmap">
                    <div class="span6 lessspan">
                        <span class="maptip">Для отметки компании на карте перенесите маркер на карту с помощью мышки</span>
                        <span class="main-marker">основное местоположение</span>
                        <span class="branch-marker">Филиал 1</span>
                    </div>
                    <div class="span6 morespan">
                        <div id="map"></div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab-padeals">
                <div class="span12 registration2">
                    <div class="span6 morespan">
                        <div class="add-block">
                                <span class="add-text">
                                    Информация для ользователя (например, стоимость 1 предложения,
                                    предложение от дизайнера компании)
                                </span>
                            <button class="add-button hover-green" onclick="createDeal();">добавить предложение</button>
                        </div>
                        <div class="add-form" id="create_form">
                            <div class="create-header">
                                <h2>Предложение №458965874233</h2>
                                <button class="public-remove hover-green">снять с публикации</button>
                            </div>
                            <table class="formtable">
                                <tbody>
                                <tr>
                                    <td>
                                        <label for="dealName">Название предложения<span class="required">*</span>:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <input name="dealName" id="dealName" class="registration-input">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="category">Категория:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <select name="category" id="category" class="registration-select">
                                            <option value="1">категори 1</option>
                                            <option value="2">категори 2</option>
                                            <option value="3">категори 3</option>
                                            <option value="4">категори 4</option>
                                            <option value="5">категори 5</option>
                                            <option value="6">категори 6</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="date1">Дата публикации:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <input name="date1" id="date1" class="registration-input-date">
                                        <input name="date2" id="date2" class="registration-input-date">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="logo">Картинка<span class="required">*</span>:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <input type="file" name="logo" id="logo">
                                        <span class="delete-span">удалить изображение</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="description">Описание предложения:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <textarea name="description" id="description" class="registration-textarea"></textarea>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="one-deal" onclick="editDeal(event);" param="1">
                            Название предложения
                        </div>
                        <div class="add-form" id="form_1">
                            <div class="create-header">
                                <h2>Предложение №458965874233</h2>
                                <button class="public-remove hover-green">снять с публикации</button>
                            </div>
                            <table class="formtable">
                                <tbody>
                                <tr>
                                    <td>
                                        <label for="dealName">Название предложения<span class="required">*</span>:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <input name="dealName" id="dealName" class="registration-input">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="category">Категория:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <select name="category" id="category" class="registration-select">
                                            <option value="1">категори 1</option>
                                            <option value="2">категори 2</option>
                                            <option value="3">категори 3</option>
                                            <option value="4">категори 4</option>
                                            <option value="5">категори 5</option>
                                            <option value="6">категори 6</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="date1">Дата публикации:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <input name="date1" id="date1" class="registration-input-date">
                                        <input name="date2" id="date2" class="registration-input-date">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="logo">Картинка<span class="required">*</span>:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <input type="file" name="logo" id="logo">
                                        <span class="delete-span">удалить изображение</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="description">Описание предложения:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <textarea name="description" id="description" class="registration-textarea"></textarea>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="one-deal" onclick="editDeal(event);" param="2">
                            Название предложения
                        </div>
                        <div class="add-form" id="form_2">
                            <div class="create-header">
                                <h2>Предложение №458965874233</h2>
                                <button class="public-remove hover-green">снять с публикации</button>
                            </div>
                            <table class="formtable">
                                <tbody>
                                <tr>
                                    <td>
                                        <label for="dealName">Название предложения<span class="required">*</span>:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <input name="dealName" id="dealName" class="registration-input">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="category">Категория:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <select name="category" id="category" class="registration-select">
                                            <option value="1">категори 1</option>
                                            <option value="2">категори 2</option>
                                            <option value="3">категори 3</option>
                                            <option value="4">категори 4</option>
                                            <option value="5">категори 5</option>
                                            <option value="6">категори 6</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="date1">Дата публикации:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <input name="date1" id="date1" class="registration-input-date">
                                        <input name="date2" id="date2" class="registration-input-date">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="logo">Картинка<span class="required">*</span>:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <input type="file" name="logo" id="logo">
                                        <span class="delete-span">удалить изображение</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="description">Описание предложения:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <textarea name="description" id="description" class="registration-textarea"></textarea>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="one-deal" onclick="editDeal(event);" param="3">
                            Название предложения
                        </div>
                        <div class="add-form" id="form_3">
                            <div class="create-header">
                                <h2>Предложение №458965874233</h2>
                                <button class="public-remove hover-green">снять с публикации</button>
                            </div>
                            <table class="formtable">
                                <tbody>
                                <tr>
                                    <td>
                                        <label for="dealName">Название предложения<span class="required">*</span>:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <input name="dealName" id="dealName" class="registration-input">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="category">Категория:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <select name="category" id="category" class="registration-select">
                                            <option value="1">категори 1</option>
                                            <option value="2">категори 2</option>
                                            <option value="3">категори 3</option>
                                            <option value="4">категори 4</option>
                                            <option value="5">категори 5</option>
                                            <option value="6">категори 6</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="date1">Дата публикации:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <input name="date1" id="date1" class="registration-input-date">
                                        <input name="date2" id="date2" class="registration-input-date">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="logo">Картинка<span class="required">*</span>:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <input type="file" name="logo" id="logo">
                                        <span class="delete-span">удалить изображение</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="description">Описание предложения:</label>
                                        <span class="explanation">пояснение к пункту</span>
                                    </td>
                                    <td>
                                        <textarea name="description" id="description" class="registration-textarea"></textarea>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="span6 lessspan">
                        <table class="infotable table-striped">
                            <tbody>
                                <tr>
                                    <td colspan="2" class="info-label">На аккаунте</td>
                                </tr>
                                <tr>
                                    <td>карточка предприятия, включая 3 рубрики, 3 подрубрики, 50 ключевых слов</td>
                                    <td>99 999тг</td>
                                </tr>
                                <tr>
                                    <td>выделение карточки в топ</td>
                                    <td>99 999тг</td>
                                </tr>
                                <tr>
                                    <td>дополнительное количество подкатегорий - 14</td>
                                    <td>99 999тг</td>
                                </tr>
                                <tr>
                                    <td>увеличенное количество ключевых слов - 150</td>
                                    <td>99 999тг</td>
                                </tr>
                                <tr>
                                    <td>увеличенное</td>
                                    <td>99 999тг</td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="info-label">Корзина</td>
                                </tr>
                                <tr>
                                    <td>пункт корзины</td>
                                    <td>99 999тг</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab-foto">
                <div class="span12 foto">
                    <div class="img-block">
                        <img src="images/foto1.png">
                        <span class="delete-span">удалить изображение</span>
                    </div>
                    <div class="img-block">
                        <img src="images/foto2.png">
                        <span class="delete-span">удалить изображение</span>
                    </div>
                    <div class="img-block">
                        <img src="images/foto3.png">
                        <span class="delete-span">удалить изображение</span>
                    </div>
                    <div class="img-block">
                        <img src="images/foto4.png">
                        <span class="delete-span">удалить изображение</span>
                    </div>
                    <div class="img-block">
                        <img src="images/foto1.png">
                        <span class="delete-span">удалить изображение</span>
                    </div>
                    <div class="img-block">
                        <img src="images/foto2.png">
                        <span class="delete-span">удалить изображение</span>
                    </div>
                    <div class="img-block">
                        <img src="images/foto3.png">
                        <span class="delete-span">удалить изображение</span>
                    </div>
                    <div class="img-block">
                        <img src="images/foto4.png">
                        <span class="delete-span">удалить изображение</span>
                    </div>
                    <div class="img-block add-img hover-green">загрузить изображение</div>
                </div>
            </div>
            <div class="tab-pane" id="tab-job">
                <div class="span12 job">
                    <div class="button-row">
                        <button class="job-buttons hover-green">создать</button>
                        <button class="job-buttons hover-green">отменить</button>
                        <button class="job-buttons hover-green">сохранить</button>
                        <button class="job-buttons hover-green">удалить</button>
                    </div>
                    <table class="job-table table-striped">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>Название вакансии</td>
                            <td>Дата размещения</td>
                            <td>Статус</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>12345678</td>
                            <td>Название вакансии</td>
                            <td>12.12.12</td>
                            <td>активная</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <script src="js/onmap.js"></script>


        <div class="span12 registration-buttons">
            <button class="cancel-step hover-green">отмена</button>
            <button class="next-step hover-green">сохранить и отправить на модерацию</button>
        </div>
    </div>
    <div class="wrap"></div>
</div>
<div class="row footer">
    <span class="upbutton">наверх</span>
    <ul class="footermenu">
        <li>
            <a href="#">Lorem</a>
        </li>
        <li>
            <a href="#">Ipsum</a>
        </li>
        <li>
            <a href="#">Dolor sit</a>
        </li>
        <li>
            <a href="#">Amet</a>
        </li>
        <li>
            <a href="#">Consectetur</a>
        </li>
        <li>
            <a href="#">Consectetur adipisicing</a>
        </li>
    </ul>
    <span class="footer-copirights">&copy;2014 elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</span>
    <span class="footer-email">email: zhiga_@mail.ru</span>
    <span class="footer-phone">тел: +7 778 152 24 33, +7 7172 522 466</span>
</div>
</body>
</html>