<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <script src="js/jquery-2.1.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.liquidcarousel.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="css/template.css">
    <link rel="stylesheet" href="css/modules.css">
    <link rel="stylesheet" href="css/screen.css">
    <link rel="stylesheet" href="css/admin.css">
</head>
<body>
<div id="content-block">
    <div class="row header center-content">
        <div class="span12">
            <div class="span6 p-relative lessspan">
                <div class="logo">
                    <a href="/">
                        <img src="images/logo.png" alt="INFOSMART" title="INFOSMART">
                    </a>
                </div>
            </div>
            <div class="span6 morespan">
                <ul class="admin-menu">
                    <li>
                        <a href="admin.php">пользователи</a>
                    </li>
                    <li>
                        <a href="cards.php">карточки</a>
                    </li>
                    <li>
                        <a href="news.php">новости</a>
                    </li>
                    <li>
                        <a href="#">статистика</a>
                    </li>
                    <li>
                        <a href="settings.php">настройки сайта</a>
                    </li>
                    <li>
                        <a href="#">выйти</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="span12">
            <div class="main-row">
                <h1 class="adminh">Новости</h1>
                <div class="search-input">
                    <input name="search" id="search">
                </div>
                <div class="pages">
                    <ul>
                        <li><a href="#">1-25</a></li>
                        <li><a href="#">26-50</a></li>
                        <li><a href="#">51-75</a></li>
                        <li>...</li>
                        <li><a href="#">1826-1850</a></li>
                        <li><a href="#">1851-1875</a></li>
                        <li><a href="#">1876-1896</a></li>
                    </ul>
                </div>
            </div>
            <div class="button-row">
                <a href="addnew.php" class="button-style hover-green">новая</a>
                <button class="not-active hover-green">снять с публикации</button>
                <select class="results" name="results">
                    <option value="5">показано 5 из 1896</option>
                    <option value="10">показано 10 из 1896</option>
                    <option value="15">показано 15 из 1896</option>
                    <option value="20">показано 20 из 1896</option>
                    <option value="25" selected>показано 25 из 1896</option>
                    <option value="50">показано 50 из 1896</option>
                    <option value="100">показано 100 из 1896</option>
                </select>
            </div>
            <table class="table-bordered full-table">
                <thead>
                <tr>
                    <th class="wfc"></th>
                    <th>№</th>
                    <th>Код</th>
                    <th>Название новости</th>
                    <th>Публикация</th>
                    <th>Дата публикации</th>
                    <th>Вводный текст</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><input type="checkbox" value="1" class="row"></td>
                    <td>1</td>
                    <td>453243568</td>
                    <td>Zhiga</td>
                    <td>опубликовано</td>
                    <td>12.12.12</td>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</td>
                </tr>
                <tr>
                    <td><input type="checkbox" value="1" class="row"></td>
                    <td>1</td>
                    <td>453243568</td>
                    <td>Zhiga</td>
                    <td>опубликовано</td>
                    <td>12.12.12</td>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</td>
                </tr>
                <tr>
                    <td><input type="checkbox" value="1" class="row"></td>
                    <td>1</td>
                    <td>453243568</td>
                    <td>Zhiga</td>
                    <td>опубликовано</td>
                    <td>12.12.12</td>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</td>
                </tr>
                <tr>
                    <td><input type="checkbox" value="1" class="row"></td>
                    <td>1</td>
                    <td>453243568</td>
                    <td>Zhiga</td>
                    <td>опубликовано</td>
                    <td>12.12.12</td>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</td>
                </tr>
                <tr>
                    <td><input type="checkbox" value="1" class="row"></td>
                    <td>1</td>
                    <td>453243568</td>
                    <td>Zhiga</td>
                    <td>опубликовано</td>
                    <td>12.12.12</td>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
<!--    <div class="wrap"></div>-->
</div>
</body>
</html>