
var tempDataArray = {
    keywordcount: 0,
    rubriccount: 0,
    photoscount: 0
};

var saveDataArray = {
    keywordcountid: -1,
    rubriccountid: -1,
    selectiontopid: -1,
    subrubricids: [],
    keywords: [],
    telephones: [],
    emails: [],
    messengers: [],
    proposals: [],
    vacancies: [],
    photos: [],
    uploadphotos: []
};

var order = {
    services: []
};

var map = {
    name: '',
    adress: '',
    position: null,
    active: false,
    map: null,
    placemark: null
};

function refreshRecalc(){
    recalcPackage(777, "#basepackage", ".basepackage", $("#rubricid").val());
    recalcPackage(1, "#keywordspackage", ".keywords", $("#keywordcountid").val());
    recalcPackage(2, "#rubricspackage", ".rubrics", $("#rubriccountid").val());
    recalcPackage(3, "#top", ".top", 21);
    recalcPackage(6, "#photospackage", ".photos", $("#photoscountid").val());
    recalcPackage(4, '', '', 31);
}

function recalcPackage(id, checker, selector, paid, tabselector){
    tabselector = tabselector || selector+"-tab";
    if ($(checker).attr('checked')||(id == '4'&&saveDataArray.proposals.length > 0)) {
        if(id == 777) {
            getBasisPaidServiceDetails(paid);
        } else {
            getPaidServiceDetails(paid)
        }
        $(selector).css('display', 'table-row');
        $(tabselector).css('display', 'block');
    } else {
        removeOrderId(id);
        $('.order-detail-' + id).remove();
        console.log('remove')
        console.log($('.order-detail-' + id))
        console.log(order.services)
        updateOrders();
        updateOrderSumma();
        $(selector).css('display', 'none');
        $(tabselector).css('display', 'none');
    }
}

(function(){
    tempDataArray.keywordcount = keywords_count;
    tempDataArray.rubriccount = rubrics_count;
    for(var i = 0;i<first_rubrics.length;i++){
        console.log(first_rubrics)
        var val = first_rubrics[i].id;
        var name = first_rubrics[i].name;
        saveDataArray.subrubricids.push(first_rubrics[i].id);
        var divEntry = $('#card-rubrics');
        divEntry.append("<div class='optionContent optionForDelete' id='add-sub-rub-" + val + "'> <span class='optionItem'>" + name + "</span>" +
            "<span onclick='deleteCardRubric(" + val + ", add-sub-rub-" + val + ");false;'><img src='/images/delete.png' width='16' height='16' alt=''/></span> </div>");

    }
    for(var i = 0;i<first_telephones.length;i++){
        console.log(first_telephones)
        var val = first_telephones[i].value;
        saveDataArray.telephones.push(first_telephones[i].value);
        var divEntry = $('#card-telephones');
        divEntry.append("<div class='optionContent optionForDelete' id='tel_" + val + "'> <span class='optionItem'>" + val + "</span>" +
            "<span onclick='deleteCardTelephone(" + val + ", tel_" + val + ");false;'><img src='/images/delete.png' width='16' height='16' alt=''/></span> </div>");

        }
    for(var i = 0;i<first_emails.length;i++){
        console.log(first_emails)
        var val = first_emails[i].value;
        var id = val;

        id = id.replace('@', '');
        id = id.replace('.', '');

        saveDataArray.emails.push(first_emails[i].value);
        var divEntry = $('#card-emails');
        divEntry.append("<div class='optionContent optionForDelete' id='email_" + id + "'> <span class='optionItem'>" + val + "</span>" +
            "<span onclick=\"deleteCardEmail('" + val + "', email_" + id + ");false;\"><img src='/images/delete.png' width='16' height='16' alt=''/></span> </div>");

    }
    for(var i = 0;i<first_messengers.length;i++){
        console.log(first_messengers)
        var val = {typeid:first_messengers[i].typeid,typename:first_messengers[i].typename,value:first_messengers[i].value};
        var type = val.typeid;
        var sudid = val.value;
        var data = val
        sudid = sudid.replace(/\s/g, '');//удялем пробелы
        sudid = sudid.replace('(', '');
        sudid = sudid.replace(')', '');
        sudid = sudid.replace('@', '');
        sudid = sudid.replace('.', '');
        saveDataArray.messengers.push(val);
        var divEntry = $('#card-messengers');
        divEntry.append("<div class='optionContent optionForDelete' id='mess" + type + sudid + "'> <span class='optionItem'>" + $("#imessengertypeid option[value='" + type + "']").text() + "-" + val.value + "</span>" +
            "<span onclick=\"deleteCardMessenger("  + data.typeid + ",'" + data.typename + "','" + data.value +  "', mess" + type + sudid + ");false;\"><img src='/images/delete.png' width='16' height='16' alt=''/></span> </div>");

    }

    for(var i = 0;i<first_keywords.length;i++){
        console.log(first_keywords)
        var val = first_keywords[i].value;
        saveDataArray.keywords.push(first_keywords[i].value);
    }


    $("#ajax_no_initial_tag_handler").tagHandler({
        assignedTags: saveDataArray.keywords,
        onAdd: function (tag) {

            if (tempDataArray.keywordcount < 0) {
                alert('Выберите количество ключевых слов');
                return;
            }

            if (tempDataArray.keywordcount > saveDataArray.keywords.length)
                saveDataArray.keywords.push(tag)
            else {
                alert('Вы достигли максимального количества ключевых слов');
                return false;
            }

        },
        onDelete: function (tag) {
            var index = saveDataArray.keywords.indexOf(tag);
            saveDataArray.keywords.splice(index, 1);
            return true;
        },
        availableTags: []

    });
    //proposals
    for(var i = 0;i<first_proposals.length;i++){
        var item = {
            id:first_proposals[i].id,
            name:first_proposals[i].name,
            fresh:first_proposals[i].fresh,
            image:first_proposals[i].image,
            content:first_proposals[i].content,
            rubricid:first_proposals[i].rubricid,
            endpublic:first_proposals[i].endpublic,
            startpublic:first_proposals[i].startpublic
        };
        saveDataArray.proposals.push(item);
    }
    for(var i = 0;i<saveDataArray.proposals.length;i++){
        appendProposal(saveDataArray.proposals[i]);
    }
    //vacancies
    for(var i = 0;i<first_vacancies.length;i++){
        var item = {
            id:first_vacancies[i].id,
            name:first_vacancies[i].name,
            content:first_vacancies[i].content,
            endpublic:first_vacancies[i].endpublic,
            startpublic:first_vacancies[i].startpublic
        };
        saveDataArray.vacancies.push(item);
    }
    for(var i = 0;i<saveDataArray.vacancies.length;i++){
        appendVacancy(saveDataArray.vacancies[i]);
    }
    //photos
    for(var i = 0;i<first_photos.length;i++){
        var item = {
            id:first_photos[i].id,
            photo:first_photos[i].photo
        };
        saveDataArray.photos.push(item);
    }
    for(var i = 0;i<saveDataArray.photos.length;i++){
        appendPhoto(saveDataArray.photos[i]);
    }

    $(".add-rubric").click(function(e){
        addCardRubric();
        return false;
    });
    $(".add-telephone").click(function(e){
        addCardTelephone();
        return false;
    });
    $(".add-email").click(function(e){
        addCardEmail();
        return false;
    });
    $(".add-messenger").click(function(e){
        addCardMessenger();
        return false;
    });

    $("#months").change(function () {
        if($("#months").val()){
            for (var i = 0; i < order.services.length; i++) {
                if (order.services[i].typeid != 3&&order.services[i].typeid != 7) {
                    var qualifier = $("#months").val() || 1;
                    order.services[i].months = qualifier;
                }
            }
            updateOrders();
            updateOrderSumma();
        }
    });

    $("#topmonths").change(function () {
        if($("#topmonths").val()){
            for (var i = 0; i < order.services.length; i++) {
                if (order.services[i].typeid == 3) {
                    var qualifier = $("#topmonths").val() || 1;
                    order.services[i].months = qualifier;
                    break;
                }
            }
            updateOrders();
            updateOrderSumma();
        }
    });
    //$("#mainmonths").change(function () {
    //    if($("#mainmonths").val()){
    //        for (var i = 0; i < order.services.length; i++) {
    //            if (order.services[i].typeid == 7) {
    //                var qualifier = $("#mainmonths").val() || 1;
    //                order.services[i].months = qualifier;
    //                break;
    //            }
    //        }
    //        updateOrders();
    //        updateOrderSumma();
    //    }
    //});


    $("#rubrics-selector").change(function () {
        if ($(this).val()) {
            getCardSubRubrics($(this).val());
        }
    });

    $("#main-rubrics-selector").change(function () {
        if ($(this).val()) {
            getMainCardSubRubrics($(this).val());
        }
    });
    //if ($("#main-rubrics-selector").val()) {
    //    getMainCardSubRubrics($("#main-rubrics-selector").val());
    //}

    $(".basepackage").css('display', 'none');
    $("#basepackage").change(function () {
        //recalcPackage(777, "#basepackage", ".basepackage", $("#rubricid").val());
        refreshRecalc();
    });
    $("#rubricid").change(function () {
        //recalcPackage(777, "#basepackage", ".basepackage", $("#rubricid").val());
        refreshRecalc();
    });

    $("#months").change(function () {
        //recalcPackage(777, "#basepackage", ".basepackage", $("#rubricid").val());
        refreshRecalc();
    });

    $(".proposal-startpublic").change(function () {
        //recalcPackage(777, "#basepackage", ".basepackage", $("#rubricid").val());
        refreshRecalc();
    });
    $(".proposal-endpublic").change(function () {
        //recalcPackage(777, "#basepackage", ".basepackage", $("#rubricid").val());
        refreshRecalc();
    });

    $(".rubrics").css('display', 'none');
    $("#rubricspackage").change(function () {
        recalcPackage(2, "#rubricspackage", ".rubrics", $("#rubriccountid").val());
    });
    $("#rubriccountid").change(function () {
        recalcPackage(2, "#rubricspackage", ".rubrics", $("#rubriccountid").val());
    });

    $(".keywords").css('display', 'none');
    $("#keywordspackage").change(function () {
        recalcPackage(1, "#keywordspackage", ".keywords", $("#keywordcountid").val());
    });
    $("#keywordcountid").change(function () {
        console.log('key chng')
        console.log(order.services)
        recalcPackage(1, "#keywordspackage", ".keywords", $("#keywordcountid").val());
    });

    $(".photos").css('display', 'none');
    $(".photos-tab").css('display', 'none');
    $("#photospackage").change(function () {
        recalcPackage(6, "#photospackage", ".photos", $("#photoscountid").val());
    });
    $("#photoscountid").change(function () {
        recalcPackage(6, "#photospackage", ".photos", $("#photoscountid").val());
    });

    $(".top").css('display', 'none');
    $("#top").change(function () {
        recalcPackage(3, "#top", ".top", 21);
    });
    $("#topmonths").change(function () {
        recalcPackage(3, "#top", ".top", 21);
    });

    $(".save-edit").on("click", function(){
        sendToModeration();
    });

    $(".cancel-edit").on("click", function(){
        cancelEditCard();
    });

    $("#reset-map").on('click', function(){
        map.position = null;
        init();
    });

    $("#deals").on("click", function(){
        getProposalsRubrics();
    })

    $('.months-editor').on('change', function(e){
        if(!checkDates()){
            alert('Выбрана неправильная дата вывода в топ или базового пакета!')
        }
    })

    $("#onmap").on('click', function(){
        map.name = $("#orgname").val();
        map.adress = $("#cityid option[value='" + $("#cityid").val() + "']").text() + " " + $("#street").val() + " " + $("#housenumber").val();
        console.log(map);
        if(!map.map){
            ymaps.ready(function(){

                map.map = new ymaps.Map('map', {
                    center: [51.1379457, 71.4503897], // Astana
                    zoom: 10
                });

                // Слушаем клик на карте
                map.map.events.add('click', function (e) {
                    map.position = e.get('coords');

                    // Если метка уже создана – просто передвигаем ее
                    if (map.placemark) {
                        map.placemark.geometry.setCoordinates(map.position);
                    }
                    // Если нет – создаем.
                    else {
                        map.placemark = createPlacemark(map.position);
                        map.map.geoObjects.add(map.placemark);
                        // Слушаем событие окончания перетаскивания на метке.
                        map.placemark.events.add('dragend', function () {
                            map.position = map.placemark.geometry.getCoordinates();
                            getAddress(map.placemark.geometry.getCoordinates());
                        });
                    }
                    getAddress(map.position);
                });

            });
        }
        init();
    });
    refreshRecalc();
})();

function checkDates() {
    if ($("#top").attr('checked')) {
        $baseexpired = null;
        if ($("#basepackage").attr('checked') || !expired.base) {
            $baseexpired = new Date();
            $baseexpired.setMonth($baseexpired.getMonth() + parseInt($("#months").val()));
        } else {
            $baseexpired = expired.base;
        }
        $topexpired = new Date();
        $topexpired.setMonth($topexpired.getMonth() + parseInt($("#topmonths").val()));

        var timeDiff = ($baseexpired.getTime() - $topexpired.getTime());
        if (timeDiff < 0) {
            return false;
        }
        //var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    }
    return true;
}
function daysBetween(first, second) {

    // Copy date parts of the timestamps, discarding the time parts.
    var one = new Date(first.getFullYear(), first.getMonth(), first.getDate());
    var two = new Date(second.getFullYear(), second.getMonth(), second.getDate());

    // Do the math.
    var millisecondsPerDay = 1000 * 60 * 60 * 24;
    var millisBetween = two.getTime() - one.getTime();
    var days = millisBetween / millisecondsPerDay;

    // Round down.
    return Math.floor(days);
}
function getProposalCost(cost) {
    var sum = 0;
    if (saveDataArray.proposals.length > 0) {
        for(var i=0;i<saveDataArray.proposals.length;i++){
            var item = saveDataArray.proposals[i];
            var store = $("#proposal-"+item.id);
            if(store){
                var startpublic = $("#proposal-"+item.id + " .proposal-startpublic").val();
                var endpublic = $("#proposal-"+item.id + " .proposal-endpublic").val();
                if(startpublic && endpublic && startpublic != '' && endpublic != '')
                var start = new Date(startpublic);
                var end = new Date(endpublic);
                var timeDiff = daysBetween(start, end);
                sum += timeDiff * Math.floor(cost/30);
            }
        }

    }
    return sum;
}

function editProposal(event) {
    var param = event.target.getAttribute("param");
    if ($("#form_" + param).is(":visible")) {
        $("#form_" + param).slideUp('slow');
    } else {
        $("#form_" + param).slideDown('slow');
    }
}

function getCardSubRubrics(rubricId) {
    $.ajax(
        {
            url: '/site/GetSubRubrics',
            type: 'POST',
            dataType: 'json',
            data: {
                rubricId: rubricId
            },
            success: function (result) {
                var select = $('#subrubrics-selector');
                select.empty();
                $.each(result, function () {
                    select.append($("<option />").val(this.rubricid).text(this.name));
                });

            }
        });
}

function getMainCardSubRubrics(rubricId) {
    $.ajax(
        {
            url: '/site/GetSubRubrics',
            type: 'POST',
            dataType: 'json',
            data: {
                rubricId: rubricId
            },
            success: function (result) {
                var select = $('#rubricid');
                select.empty();
                select.append($("<option />").val(-1).text('Выберите'));
                $.each(result, function () {
                    select.append($("<option />").val(this.rubricid).text(this.name));
                });

            }
        });
}

function addCardRubric() {
    console.log(tempDataArray)
    if (tempDataArray.rubriccount == 0) {
        alert('Выберите количество рубрик');
        return;
    }

    if (tempDataArray.rubriccount > saveDataArray.subrubricids.length) {
        var select = $('#subrubrics-selector');
        if (select.val() && select.val() != -1) {
            var val = select.val();
            console.log(val);
            if ($.inArray(val, saveDataArray.subrubricids) == -1) {
                saveDataArray.subrubricids.push(val);

                var divEntry = $('#card-rubrics');
                divEntry.append("<div class='optionContent optionForDelete' id='add-sub-rub-" + val + "'> <span class='optionItem'>" + $("#subrubrics-selector option[value='" + val + "']").text() + "</span>" +
                    "<span onclick='deleteCardRubric(" + val + ", add-sub-rub-" + val + ");false;'><img src='/images/delete.png' width='16' height='16' alt=''/></span> </div>");

            }
        }
        select.val(-1);
    }
    else {
        alert('Вы достигли максимального количества рубрик');
    }
}

function deleteCardRubric(val, divId) {
    saveDataArray.subrubricids.splice(saveDataArray.subrubricids.indexOf(val.toString()), 1);
    divId.remove();
}

function addCardEmail() {
    console.log(tempDataArray)
        var input = $('#email');
    console.log(input.val())
        if (input.val() && input.val() != "") {
            var val = input.val();
            var id = input.val();

            id = id.replace('@', '');
            id = id.replace('.', '');

            console.log(val);
            if ($.inArray(val, saveDataArray.emails) == -1) {
                saveDataArray.emails.push(val);

                var divEntry = $('#card-emails');
                divEntry.append("<div class='optionContent optionForDelete' id='email_" + id + "'> <span class='optionItem'>" + val + "</span>" +
                    "<span onclick=\"deleteCardEmail('" + val + "', email_" + id + ");false;\"><img src='/images/delete.png' width='16' height='16' alt=''/></span> </div>");

            }
        }
        input.val("");
}

function deleteCardEmail(val, divId) {
    saveDataArray.emails.splice(saveDataArray.emails.indexOf(val.toString()), 1);
    divId.remove();
}

function addCardTelephone() {
    console.log(tempDataArray)
        var input = $('#telephone');
    console.log(input.val())
        if (input.val() && input.val() != "") {
            var val = input.val().trim();
            val = val.replace(/\s/g, '');//удялем пробелы
            val = val.replace('(', '');
            val = val.replace(')', '');
            console.log(val);
            if ($.inArray(val, saveDataArray.telephones) == -1) {
                saveDataArray.telephones.push(val);

                var divEntry = $('#card-telephones');
                divEntry.append("<div class='optionContent optionForDelete' id='tel_" + val + "'> <span class='optionItem'>" + val + "</span>" +
                    "<span onclick='deleteCardTelephone(" + val + ", tel_" + val + ");false;'><img src='/images/delete.png' width='16' height='16' alt=''/></span> </div>");

            }
        }
        input.val("");
}

function deleteCardTelephone(val, divId) {
    saveDataArray.telephones.splice(saveDataArray.telephones.indexOf(val.toString()), 1);
    divId.remove();
}

function addCardMessenger() {
    console.log(tempDataArray)
        var select = $('#imessengertypeid');
        var input = $('#messenger');
        if (select.val() && select.val() != -1) {
            if (input.val() && input.val() != "") {
                var type = select.val();
                var val = input.val();
                var data = {
                    typeid: type,
                    typename: $("#imessengertypeid option[value='" + type + "']").text(),
                    value: val
                };
                var sudid = val;
                sudid = sudid.replace(/\s/g, '');//удялем пробелы
                sudid = sudid.replace('(', '');
                sudid = sudid.replace(')', '');
                sudid = sudid.replace('@', '');
                sudid = sudid.replace('.', '');
                console.log(val);
                console.log(sudid);
                if ($.inArray(data, saveDataArray.messengers) == -1) {
                    saveDataArray.messengers.push(data);

                    var divEntry = $('#card-messengers');
                    divEntry.append("<div class='optionContent optionForDelete' id='mess" + type + sudid + "'> <span class='optionItem'>" + $("#imessengertypeid option[value='" + type + "']").text() + "-" + val + "</span>" +
                        "<span onclick='deleteCardMessenger(" + data.typeid + ",'" + data.typename + "','" + data.value + "', mess" + type + sudid + ");false;'><img src='/images/delete.png' width='16' height='16' alt=''/></span> </div>");

                }
            }
        }
        select.val(-1);
        input.val("");
}

function deleteCardMessenger(id, name, value, divId) {
    var val = {
        typeid: id,
        typename: name,
        value: value
    }
    saveDataArray.messengers.splice(saveDataArray.messengers.indexOf(val.toString()), 1);
    divId.remove();
}

function getPaidServiceDetails(id) {
    //actionGetBalancePaidServiceDetails

    $.ajax(
        {
            url: '/site/GetPaidServiceDetails',
            type: 'POST',
            dataType: 'json',
            data: {
                paidservicedetaild: id
            },
            error: function(result){
                console.log(result);
            },
            success: function (result) {
                if (result) {
                    var source = result.jsonDataSource;

                    $.ajax(
                        {
                            url: '/site/GetBalancePaidServiceDetails',
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                paidservicedetaild: source.attributes.paidserviceid
                            },
                            success: function (balanceres) {
                                var month = $("#months").val() || 1;
                                if (source.attributes.paidserviceid == '3') {
                                    month = $("#topmonths").val() || 1;
                                }
                                if (source.attributes.paidserviceid == '7') {
                                    month = $("#mainmonths").val() || 1;
                                }
                                if(source.attributes.paidserviceid == '4'){
                                    var cost = getProposalCost(source.attributes.cost);
                                    var service = {
                                        id: source.attributes.paidservicedetaild,
                                        typeid: source.attributes.paidserviceid,
                                        typename: source.relations.paidservice.name,
                                        cost: cost,
                                        months: month,
                                        balance: balanceres.balance ? balanceres.balance : 0,
                                        count: source.attributes.count
                                    };
                                    addOrderItem(service);
                                    updateOrders();
                                } else {
                                    $.ajax(
                                        {
                                            url: '/site/GetCostPaidServiceDetails',
                                            type: 'POST',
                                            dataType: 'json',
                                            data: {
                                                paidservicedetailid: id,
                                                count: source.attributes.count,
                                                months: month,
                                                fullcost: $("#basepackage").attr('checked') ? 1 : 0,
                                                topmonths: $('#topmonths').val() || 1
                                            },
                                            success: function (costres) {
                                                if (balanceres && costres) {
                                                    var month = $("#months").val() || 1;
                                                    if (source.attributes.paidserviceid == '3') {
                                                        month = $("#topmonths").val() || 1;
                                                    }
                                                    if (source.attributes.paidserviceid == '7') {
                                                        month = $("#mainmonths").val() || 1;
                                                    }
                                                    var service = {
                                                        id: source.attributes.paidservicedetaild,
                                                        typeid: source.attributes.paidserviceid,
                                                        typename: source.relations.paidservice.name,
                                                        cost: costres.cost || source.attributes.cost,
                                                        months: month,
                                                        balance: balanceres.balance ? balanceres.balance : 0,
                                                        count: source.attributes.count
                                                    };
                                                    addOrderItem(service);
                                                    updateOrders();
                                                }
                                            }
                                        });
                                }
                            }
                        });

                }
                else
                    console.log('error');

            }
        });
}

function getBasisPaidServiceDetails(id) {
    $.ajax(
        {
            url: '/site/GetBasisPaidServiceDetails',
            type: 'POST',
            dataType: 'json',
            data: {
                rubricid: id
            },
            error: function(result){
                console.log(result);
            },
            success: function (result) {
                if (result) {
                    var source = result.jsonDataSource;

                    $.ajax(
                        {
                            url: '/site/GetBalancePaidServiceDetails',
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                paidservicedetaild: source.attributes.paidserviceid
                            },
                            success: function (balanceres) {
                                var month = $("#months").val() || 1;
                                if (source.attributes.paidserviceid == '3') {
                                    month = $("#topmonths").val() || 1;
                                }
                                if (source.attributes.paidserviceid == '7') {
                                    month = $("#mainmonths").val() || 1;
                                }
                                $.ajax(
                                    {

                                        //getCalcPackageCost($months, $count, $paidservicedetailid, $fullcost = null){
                                        url: '/site/GetCostPaidServiceDetails',
                                        type: 'POST',
                                        dataType: 'json',
                                        data: {
                                            paidservicedetailid: source.attributes.paidservicedetaild,
                                            count: source.attributes.count,
                                            months: month,
                                            fullcost: $("#basepackage").attr('checked') ? 1 : 0,
                                            topmonths: $('#topmonths').val() || 1
                                        },
                                        success: function (costres) {
                                            if (balanceres && costres) {
                                                var month = $("#months").val() || 1;
                                                if (source.attributes.paidserviceid == '3') {
                                                    month = $("#topmonths").val() || 1;
                                                }
                                                if (source.attributes.paidserviceid == '7') {
                                                    month = $("#mainmonths").val() || 1;
                                                }
                                                var service = {
                                                    id: source.attributes.paidservicedetaild,
                                                    typeid: source.attributes.paidserviceid,
                                                    typename: source.relations.paidservice.name,
                                                    cost: costres.cost || source.attributes.cost,
                                                    months: month,
                                                    balance: balanceres.balance ? balanceres.balance : 0,
                                                    count: source.attributes.count
                                                };
                                                addOrderItem(service);
                                                updateOrders();
                                            }
                                        }
                                    });
                            }
                        });

                }
                else
                    console.log('error');

            }
        });
}

function addOrderItem(service) {
    for (var i = 0; i < order.services.length; i++) {
        if (order.services[i].typeid == service.typeid) {
            order.services.splice(order.services.indexOf(service.typeid), 1);
        }
    }
    order.services.push(service);
}
function removeOrderItem(service) {
    for (var i = 0; i < order.services.length; i++) {
        if (order.services[i].typeid == service.typeid) {
            order.services.splice(order.services.indexOf(service.typeid, 1));
        }
    }
}
function removeOrderId(service) {
    for (var i = 0; i < order.services.length; i++) {
        if (order.services[i].typeid == service) {
            order.services.splice(order.services.indexOf(service), 1);
        }
    }
}

function addPaymentDetail(type, months, cost, balance, serviceTypeId) {
    $('.order-detail-' + serviceTypeId).remove();
    if(parseInt(cost) - parseInt(balance)>0) {
        $('.order-details tbody').append('<tr class = "order-detail-' + serviceTypeId + ' orderItem"><td name = "type">' + type + '</td><td>за ' + months + ' мес.</td><td name = "cost"> ' + cost + 'тг</td><td> ' + (balance > 0 ? '-' : '') + balance + 'тг</td></tr>');
    }
    updateOrderSumma();
}

function updateOrders(){
    for(var i=0;i<order.services.length;i++) {
        var item = order.services[i];
        switch (item.typeid) {
            case "1":
                tempDataArray.keywordcount = item.count;
                saveDataArray.keywordcountid = item.paidservicedetaild;
                addPaymentDetail(item.typename + ' ' + item.count + ' шт. ', item.months, parseInt(item.cost), item.balance, item.typeid);
                break;
            case "2":
                tempDataArray.rubriccount = item.count;
                saveDataArray.rubriccountid = item.paidservicedetaild;
                addPaymentDetail(item.typename + ' ' + item.count + ' шт. ', item.months, parseInt(item.cost), item.balance, item.typeid);
                break;
            case "3":
                addPaymentDetail(item.typename, item.months, parseInt(item.cost), item.balance, item.typeid);
                break;
            case "4":
                if (saveDataArray.proposals.length > 0) {
                    addPaymentDetail(item.typename + " " + saveDataArray.proposals.length + ' шт.', item.months, parseInt(item.cost), item.balance, item.typeid);
                }
                break;
            case "6":
                tempDataArray.photoscount = item.count;
                saveDataArray.photoscountid = item.paidservicedetaild;
                addPaymentDetail(item.typename + ' ' + item.count + ' шт. ', item.months, parseInt(item.cost), item.balance, item.typeid);
                break;
            case "7":
                addPaymentDetail(item.typename, item.months, parseInt(item.cost), item.balance, item.typeid);
                break;
            case "777":
                addPaymentDetail(item.typename + " №" + item.id + " <ul><li>1 рубрика</li><li>25 слов</li><li>Контакты и Размещение на карте</li><li>Лого или картинка - 1 шт.</li><li>Размещение на мобильном приложении и на веб сайте</li></ul>", item.months, parseInt(item.cost), item.balance, item.typeid);
                break;
        }
    }
}

function updateOrderSumma() {
    var sum = 0;
    for (var i = 0; i < order.services.length; i++) {
        var addsum = parseInt(order.services[i].cost) - parseInt(order.services[i].balance);
        sum = sum + (addsum > 0 ? addsum : 0);
    }
    var qualifier = $("#months").val() || 1;
    var isDiscount = qualifier == 12 ? true : false;
    var res = sum * (isDiscount ? 0.85 : 1);
    var textDiscount = isDiscount?" со скидкой 15%":"";
    $('.orderSum').text(Math.ceil(res) + ' тг' + textDiscount);
}

function addTelephoneForSave() {

    var textField = $('#phone');
    if (textField.val() && textField.val().length == 15) {
        var val = textField.val().trim();
        val = val.replace(/\s/g, '');//удялем пробелы
        val = val.replace('(', '');
        val = val.replace(')', '');

        if ($.inArray(val, saveDataArray.telephones) == -1) {
            saveDataArray.telephones.push(val);

            var divEntry = $('#telephonesAddItems');
            divEntry.append("<div class='optionContent optionForDelete' id='addTel" + val + "'> <span class='optionItem'>" + val + "</span>" +
                "<span onclick='deleteTelephone(" + val + ", addTel" + val + ");false;'><img src='/images/delete.png' width='16' height='16' alt=''/></span> </div>");

        }
        textField.val('');
    }
    else
        alert('Заполните номер правильно');

}

function deleteTelephone(val, divId) {
    saveDataArray.telephones.splice(saveDataArray.telephones.indexOf(val.toString()), 1);
    divId.remove();
    console.log(saveDataArray.telephones);
}

function addEmailForSave() {
    var textField = $('#email');
    if(textField.val().length == 0)
    {
        alert('Введите значение');
        return;
    }
    if (validateEmail(textField.val())) {
        var val = textField.val().trim();

        if ($.inArray(val, saveDataArray.emails) == -1) {
            saveDataArray.emails.push(val);

            var divEntry = $('#emailsAddItems');
            var id = val.replace('@', '');
            id = id.replace('.', '');

            divEntry.append("<div class='optionContent optionForDelete' id='addEmail" + id + "'> <span class='optionItem'>" + val + "</span>" +
                "<span onclick='deleteEmail(\"" + val.toString() + "\", addEmail" + id + ");false;'><img src='/images/delete.png' width='16' height='16' alt=''/></span> </div>");

        }
        textField.val('');
    }
    else
        alert('Не правильный формат е-мейл адреса. Пример: test@test.kz');

}

function deleteEmail(val, divId) {
    saveDataArray.emails.splice(saveDataArray.emails.indexOf(val.toString()), 1);
    divId.remove();
}

function addMessengerForSave() {
    var selectMesType = $('#messenger_type'), valueFiled = $('#messenger');
    var val = valueFiled.val(), mesTypeId = selectMesType.val();

    if (mesTypeId && val) {
        var data = {
            mesTypeId: mesTypeId,
            value: val
        };

        var isAdd = false;
        for (var i = 0; i < saveDataArray.messengers.length; i++) {
            var item = saveDataArray.messengers[i];
            if (item.value == val && item.mesTypeId == mesTypeId) {
                isAdd = true;
            }
        }

        if (!isAdd) {
            saveDataArray.messengers.push(data);
            console.log(saveDataArray.messengers);

            var divEntry = $('#messengersAddItems'), unId = mesTypeId + getRandomInt(5000, 10000);
            console.log(unId);

            divEntry.append("<div class='optionContent optionForDelete' id='addMessenger" + unId + "'> <span class='optionItem'>" + $("#messenger_type option[value='" + mesTypeId + "']").text() + ' - ' + val + "</span>" +
                "<span onclick='deleteMessengers(\"" + val + "\", " + mesTypeId + ", addMessenger" + unId + ");false;'><img src='/images/delete.png' width='16' height='16' alt=''/></span> </div>");


        }
        valueFiled.val('');
    }
    else
        alert('Заполните поле месенджера');
}

function deleteMessengers(val, mesTypeId, divId) {

    for (var i = 0; i < saveDataArray.messengers.length; i++) {
        var item = saveDataArray.messengers[i];
        if (item.value == val && item.mesTypeId == mesTypeId) {
            saveDataArray.messengers.splice(i, 1);
        }
    }
    divId.remove();
    console.log(saveDataArray.messengers);
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


///// step 3 /////////////////////

function step3Validate() {
    result = true;

    if (!$('#uAddress').val()) {
        $('#uAddress').addClass('errorClass');
        result = false;
    }
    else
        $('#uAddress').removeClass('errorClass');


    if (!$('#iinBin').val()) {
        $('#iinBin').addClass('errorClass');
        result = false;
    }
    else {

        if ($('#iinBin').val().length != 12) {
            $('#iinBin').addClass('errorClass');
            alert('Длина ИИН/БИН не правильная');
            result = false;
        }
        else if ($('#iinBin').val().length == 12)
            $('#iinBin').removeClass('errorClass');
    }


    if (!$('#bik').val()) {
        $('#bik').addClass('errorClass');
        result = false;
    }
    else
        $('#bik').removeClass('errorClass');

    if (!$('#iik').val()) {
        $('#iik').addClass('errorClass');
        result = false;
    }
    else
        $('#iik').removeClass('errorClass');

    if (!$('#kbe').val()) {
        $('#kbe').addClass('errorClass');
        result = false;
    }
    else
        $('#kbe').removeClass('errorClass');

    if (!$('#bankName').val()) {
        $('#bankName').addClass('errorClass');
        result = false;
    }
    else
        $('#bankName').removeClass('errorClass');

    return result;
}

function performOrder() {
    //var div = $('#finishOrder');

    var summa = 0;

    for (var i = 0; i < $('.orderItem').length; i++) {
        var item = $('.orderItem')[i];
        var trId = item.id;
        console.log('id:' + trId);
        var tdElCost = $('#' + trId + ' td[name = cost]');
        ;
        var tdElType = $('#' + trId + ' td[name = type]');
        ;
        var sum = tdElCost[0].innerText;
        var type = tdElType[0].innerText;
        var sum2 = parseInt(sum.replace('тг', ''));
        summa += sum2;
        console.log(sum);
        console.log(type);

        var idNum = trId.replace('order-detail-', '');
        $('.finish-order tr:last').before('<tr id = "finish-order-detail-' + idNum + '">' +
            '<td><button class="exclude hover-green" onclick="deleteOrderItem(' + idNum + ', finish-order-detail-' + idNum + '); false;">исключить</button>' + type + '</td>' +
            '<td>' + sum + '</td> </tr>');

    }
    updateOrders();
    updateOrderSumma();

}

function deleteOrderItem(typeid, elId) {

    for (var i = 0; i < order.services.length; i++) {
        if (order.services[i].typeid == typeid) {
            order.services.splice(order.services.indexOf(typeid, 1));
            if(typeid == 1)
            {
                saveDataArray.keywordcountid = 1;// paidservicedetailid // если удалили , то скидываем на поумаолчанию
            } else if (typeid == 2)
                saveDataArray.rubriccountid = 11;// paidservicedetailid

        }
    }

    elId.remove();
    updateOrders();
    updateOrderSumma();
}

function cancelEditCard(){
    window.location = "/user/index";
}

function sendToModeration() {

    if(!checkDates()){
        alert('Выбрана неправильная дата вывода в топ или базового пакета!');
        return;
    }

    if($("#basepackage").attr('checked') && !($("#rubricid").val() > 0)){
        alert('Выберите рубрику базового пакета!');
        return;
    }

    var data = getCardForSave();
    console.log(data);
    $.ajax(
        {
            url: '/user/saveedit',
            type: 'POST',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            error: function(result){
                console.log(result);
            },
            success: function (result) {
                console.log(result);
                result = jQuery.parseJSON(result);

                if(result.status == 'success')
                {
                    $('#emailResp').text(result.data.email);
                    $('#success_modal').modal('show');
                }
                else if(result.status == 'error')
                {
                    if(result.message == 'emailFiledToSend')
                    {
                        $('#errormsg').text('Не удалось отправить письмо на указаный емейл адрес');
                    } else if(result.message == 'file invalid')
                    {
                        $('#errormsg').text('Загружаемый лого не соответствует требованиям');
                    }


                    $('#error_modal').modal('show');
                }

                $('#loadingDiv').hide();
            }
        });
}

function getNormalTelephoneVal(v) {
    v = v.replace('(', '');
    v = v.replace(')', '');
    v = v.replace(/\s/g, '');
    return v;
}

function getCardForSave() {
    function getObj() {
        var obj = {};
        var inputs = $('.card-editor');
        $.each(inputs, function (i, input) {
            if($(input).attr('type') == 'checkbox'){
                obj[input.id] = $(input).attr('checked')?true:false;
            } else
            {
                obj[input.id] = input.value;
            }
        });
        return obj;
    }

    var data = getObj();
    console.log(data);
    var updatecontactphone = getNormalTelephoneVal($('#updatecontactphone').val());
    var accountantphone = getNormalTelephoneVal($('#accountantphone').val());

    data.rubriccountid = $("#rubricspackage").attr('checked') ? data.rubriccountid : '11';
    data.subrubricids = saveDataArray.subrubricids;
//    data.keywordcountid = saveDataArray.keywordcountid;
    data.keywords = saveDataArray.keywords;
    data.telephones = saveDataArray.telephones;
    data.emails = saveDataArray.emails;
    data.messengers = saveDataArray.messengers;
    data.updatecontactphone = updatecontactphone;
    data.accountantphone = accountantphone;
    data.coordinates = map.position;

    prepareDeals();
    prepareVacancies();
    preparePhotos();
    data.proposals = saveDataArray.proposals;
    data.vacancies = saveDataArray.vacancies;
    data.photos = saveDataArray.photos;
    data.uploadphotos = saveDataArray.uploadphotos;
    if($("#photospackage").attr('checked'))//$("#subrubric option[value='" + val + "']").text(
        if(data.photos.length + data.uploadphotos.length > parseInt($("#photoscountid option[value='"+$("#photoscountid").val()+"']").text())){
            alert('Уберите лишние фото!');
            return null;
        }


    var formData = new FormData();
    var logo = $('#logo')[0].files[0];
    formData.append('logo', logo);

    for(var i = 0;i<saveDataArray.uploadphotos.length;i++){
        formData.append('uploadphoto-'+i, $('.add-photo')[0].files[i]);
    }
    for(var i=0;i<saveDataArray.proposals.length;i++){
        var item = saveDataArray.proposals[i];
        var store = $("#proposal-"+item.id);
        if($("#proposal-"+item.id + " .proposal-image")[0])
        if($("#proposal-"+item.id + " .proposal-image")[0]['files'])
        if($("#proposal-"+item.id + " .proposal-image")[0].files)
        if($("#proposal-"+item.id + " .proposal-image")[0].files[0]){
            formData.append('proposalphoto-'+i, $("#proposal-"+item.id + " .proposal-image")[0].files[0]);
        }
    }

    console.log(data);

    formData.append('data',  JSON.stringify(data));

    return formData;
}
//map
function init() {
    if(map.position){
        // Если метка уже создана – просто передвигаем ее
        if (map.placemark) {
            map.placemark.geometry.setCoordinates(map.position);
        }
        // Если нет – создаем.
        else {
            map.placemark = createPlacemark(map.position);
            map.map.geoObjects.add(map.placemark);
            // Слушаем событие окончания перетаскивания на метке.
            map.placemark.events.add('dragend', function () {
                map.position = map.placemark.geometry.getCoordinates();
                getAddress(map.placemark.geometry.getCoordinates());
            });
        }
    } else {
        if (map.adress) {

            ymaps.geocode(map.adress, {
                /**
                 * Опции запроса
                 * @see http://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/geocode.xml
                 */
                // boundedBy: myMap.getBounds(), // Сортировка результатов от центра окна карты
                // strictBounds: true, // Вместе с опцией boundedBy будет искать строго внутри области, указанной в boundedBy
                results: 1 // Если нужен только один результат, экономим трафик пользователей
            }).then(function (res) {
                // Выбираем первый результат геокодирования.
                var firstGeoObject = res.geoObjects.get(0),
                // Координаты геообъекта.
                    coords = firstGeoObject.geometry.getCoordinates(),
                // Область видимости геообъекта.
                    bounds = firstGeoObject.properties.get('boundedBy');

                // Масштабируем карту на область видимости геообъекта.
                map.map.setBounds(bounds, {
                    checkZoomRange: true // проверяем наличие тайлов на данном масштабе.
                });

                if(map.placemark){
                    map.placemark.geometry.setCoordinates(coords);
                    map.placemark.properties
                        .set({
                            iconContent: map.name,
                            balloonContent: map.name
                        });
                } else {
                    // Добавляем первый найденный геообъект на карту.
                    //myMap.geoObjects.add(firstGeoObject);
                    // Масштабируем карту на область видимости геообъекта.
                    map.map.setBounds(bounds, {
                        checkZoomRange: true // проверяем наличие тайлов на данном масштабе.
                    });

                    /**
                     * Если нужно добавить по найденным геокодером координатам метку со своими стилями и контентом балуна, создаем новую метку по координатам найденной и добавляем ее на карту вместо найденной.
                     */

                    map.placemark = new ymaps.Placemark(coords, {
                        iconContent: map.name,
                        balloonContent: map.name
                    }, {
                        preset: 'islands#violetStretchyIcon',
                        draggable: true
                    });
                    map.placemark.events.add("dragend", function (event) {
                        map.position = map.placemark.geometry.getCoordinates();
                        getAddress(map.placemark.geometry.getCoordinates());
                    });

                    map.map.geoObjects.add(map.placemark);
                }
            });
        }
    }
}

function createPlacemark(coords) {
    return new ymaps.Placemark(coords, {
        iconContent: 'поиск...'
    }, {
        preset: 'islands#violetStretchyIcon',
        draggable: true
    });
}

function getAddress(coords) {
    map.placemark.properties.set('iconContent', 'поиск...');
    ymaps.geocode(coords).then(function (res) {
        var firstGeoObject = res.geoObjects.get(0);

        map.placemark.properties
            .set({
                iconContent: map.name,
                balloonContent: map.name
            });
    });
}

//proposals

function getProposalsRubrics() {
    $.ajax(
        {
            url: '/site/GetManySubRubrics',
            type: 'POST',
            dataType: 'json',
            data: {
                rubricsIds: saveDataArray.subrubricids
            },
            success: function (result) {
                var select = $('.proposal-rubricid');
                select.empty();
                for(var i = 0;i<saveDataArray.proposals.length;i++){
                    var item = saveDataArray.proposals[i];
                    console.log(item.id)
                    $.each(result, function () {
                        var sel = false;
                        if(item.rubricid == this.rubricid){
                            sel = true;
                        }
                        $("#proposal-"+item.id + " .proposal-rubricid").append($("<option "+(sel?"selected":"")+"/>").val(this.rubricid).text(this.name));
                    });
                }

            }
        });
}

function createDeal() {

    $.ajax(
        {
            url: '/user/createproposal',
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false,
            error: function(result){
                console.log(result);
            },
            success: function (result) {
                console.log(result);
                result = jQuery.parseJSON(result);
                var item = saveDataArray.proposals.push({
                    id:result.proposalsid,
                    name: result.name,
                    fresh: result.fresh,
                    image:result.image,
                    content:result.content,
                    rubricid:result.rubricid,
                    endpublic:result.endpublic,
                    startpublic:result.startpublic
                });
                appendProposal(saveDataArray.proposals[item - 1]);
            }
        });
}

function editDeal(event) {
    var param = event.target.getAttribute("param");
    console.log(param)
    if ($("#proposal-form-" + param).is(":visible")) {
        $("#proposal-form-" + param).slideUp('slow');
    } else {
        $("#proposal-form-" + param).slideDown('slow');
    }
}

function deleteDeal(event){
    var param = event.target.getAttribute("param");
    for(var i = 0;i<saveDataArray.proposals.length;i++){
        if(saveDataArray.proposals[i].id == param){
            saveDataArray.proposals.splice(i, 1);
            $('#proposal-'+param).remove();
        }
    }
    //recalcPackage(4, "#proposalspackage", ".proposals", 31);
    getPaidServiceDetails(31);
}

function prepareDeals(){
    for(var i=0;i<saveDataArray.proposals.length;i++){
        var item = saveDataArray.proposals[i];
        var store = $("#proposal-"+item.id);
        if(store){
            item.name = $("#proposal-"+item.id + " .proposal-name").val();
            item.content = $("#proposal-"+item.id + " .proposal-content").val();
            item.rubricid = $("#proposal-"+item.id + " .proposal-rubricid").val();
            item.startpublic = $("#proposal-"+item.id + " .proposal-startpublic").val();
            item.endpublic = $("#proposal-"+item.id + " .proposal-endpublic").val();
            item.image = saveDataArray.proposals[i].image;
        }
    }
}

function appendProposal(item){
    console.log(item)
    var divEntry = $('#card-proposals');
    divEntry.append(''+
    '<div data-id="'+item.id+'" id="proposal-'+item.id+'">'+
    '<div class="one-deal" onclick="editDeal(event);" param="'+item.id+'">'+
    item.name +
    '</div>'+
    '<div class="add-form" id="proposal-form-'+item.id+'">'+
    '<div class="create-header">'+
    '<h2>Предложение #'+item.id+'</h2>'+
    '<button class="public-remove hover-green" onclick="deleteDeal(event);" param="'+item.id+'">снять с публикации</button>'+
    '</div>'+
    '<table class="formtable">'+
    '<tbody>'+
    '<tr>'+
    '<td>'+
    '<label for="dealName">Название предложения:</label>'+
    '</td>'+
    '<td>'+
    '<input value="'+item.name+'" name="dealName" class="proposal-name registration-input">'+
    '</td>'+
    '</tr>'+
    //'<tr>'+
    //'<td>'+
    //'<label for="category">Категория:</label>'+
    //'</td>'+
    //'<td>'+
    //'<select name="category" class="proposal-rubricid registration-select">'+
    //'<option value="1">категория 1</option>'+
    //'</select>'+
    //'</td>'+
    //'</tr>'+
    '<tr>'+
    '<td>'+
    '<label for="date1">Дата начала публикации:</label>'+
    '</td>'+
    '<td>'+
    '<input value="'+item.startpublic+'" name="date1" type="date" class="proposal-startpublic registration-input-date">'+
    '</td>'+
    '</tr>'+
    '<tr>'+
    '<td>'+
    '<label for="date2">Дата конца публикации:</label>'+
    '</td>'+
    '<td>'+
    '<input value="'+item.endpublic+'" name="date2" type="date" class="proposal-endpublic registration-input-date">'+
    '</td>'+
    '</tr>'+
    '<tr>'+
    '<td>'+
    '<label for="logo">Картинка:</label>'+
    '</td>'+
    '<td>'+
    '<input class="proposal-image" type="file" name="logo">'+
    '</td>'+
    '</tr>'+
    '<tr>'+
    '<td>'+
    '<label for="description">Описание предложения:</label>'+
    '</td>'+
    '<td>'+
    '<textarea name="description" class="proposal-content registration-textarea">'+item.content+'</textarea>'+
    '</td>'+
    '</tr>'+
    '</tbody>'+
    '</table>'+
    '</div>'+
    '</div>');
    //recalcPackage(4, "#proposalspackage", ".proposals", 31);
    getPaidServiceDetails(31);
}

//vacancies
function createVacancies() {

    $.ajax(
        {
            url: '/user/createvacancy',
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false,
            error: function(result){
                console.log(result);
            },
            success: function (result) {
                console.log(result);
                result = jQuery.parseJSON(result);
                var item = saveDataArray.vacancies.push({
                    id:result.vacancyid,
                    name: result.name,
                    content:result.content,
                    endpublic:result.endpublic,
                    startpublic:result.startpublic
                });
                appendVacancy(saveDataArray.vacancies[item - 1]);
            }
        });
}

function editVacancies(event) {
    var param = event.target.getAttribute("param");
    console.log(param)
    if ($("#vacancy-form-" + param).is(":visible")) {
        $("#vacancy-form-" + param).slideUp('slow');
    } else {
        $("#vacancy-form-" + param).slideDown('slow');
    }
}

function deleteVacancies(event){
    var param = event.target.getAttribute("param");
    for(var i = 0;i<saveDataArray.vacancies.length;i++){
        if(saveDataArray.vacancies[i].id == param){
            saveDataArray.vacancies.splice(i, 1);
            $('#vacancy-'+param).remove();
        }
    }
}

function prepareVacancies(){
    for(var i=0;i<saveDataArray.vacancies.length;i++){
        var item = saveDataArray.vacancies[i];
        var store = $("#vacancy-"+item.id);
        if(store){
            item.name = $("#vacancy-"+item.id + " .vacancy-name").val();
            item.content = $("#vacancy-"+item.id + " .vacancy-content").val();
            item.startpublic = $("#vacancy-"+item.id + " .vacancy-startpublic").val();
            item.endpublic = $("#vacancy-"+item.id + " .vacancy-endpublic").val();
        }
    }
}

function appendVacancy(item){
    console.log(item)
    var divEntry = $('#card-vacancies');
    divEntry.append(''+
        '<div id="vacancy-'+item.id+'">'+
            '<div class="one-deal" onclick="editVacancies(event);" param="'+item.id+'">'+
                item.name +
            '</div>'+
            '<div class="add-form" id="vacancy-form-'+item.id+'">'+
                '<div class="create-header">'+
                    '<h2>Вакансия #'+item.id+'</h2>'+
                    '<button class="public-remove hover-green" onclick="deleteVacancies(event);" param="'+item.id+'">снять с публикации</button>'+
                '</div>'+
                '<table class="formtable">'+
                    '<tbody>'+
                        '<tr>'+
                            '<td>'+
                                '<label for="dealName">Название вакансии:</label>'+
                            '</td>'+
                            '<td>'+
                                '<input value="'+item.name+'" name="dealName" class="vacancy-name registration-input">'+
                            '</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>'+
                                '<label for="date1">Дата начала публикации:</label>'+
                            '</td>'+
                            '<td>'+
                                '<input value="'+item.startpublic+'" name="date1" type="date" class="vacancy-startpublic registration-input-date">'+
                            '</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>'+
                                '<label for="date2">Дата конца публикации:</label>'+
                            '</td>'+
                            '<td>'+
                                '<input value="'+item.endpublic+'" name="date2" type="date" class="vacancy-endpublic registration-input-date">'+
                            '</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>'+
                                '<label for="description">Описание вакансии:</label>'+
                            '</td>'+
                            '<td>'+
                                '<textarea name="description" class="vacancy-content registration-textarea">'+item.content+'</textarea>'+
                            '</td>'+
                        '</tr>'+
                    '</tbody>'+
                '</table>'+
            '</div>'+
        '</div>');
    $(".proposal-startpublic").change(function () {
        //recalcPackage(777, "#basepackage", ".basepackage", $("#rubricid").val());
        refreshRecalc();
    });
    $(".proposal-endpublic").change(function () {
        //recalcPackage(777, "#basepackage", ".basepackage", $("#rubricid").val());
        refreshRecalc();
    });
}
/*

 <div class="img-block">
 <img src="images/foto4.png">
 <span class="delete-span">удалить изображение</span>
 </div>
 */

//photos

function deletePhotos(event){
    var param = event.target.getAttribute("param");
    for(var i = 0;i<saveDataArray.photos.length;i++){
        if(saveDataArray.photos[i].id == param){
            saveDataArray.photos.splice(i, 1);
            $('#photo-'+param).remove();
        }
    }
}

function preparePhotos(){
    saveDataArray.uploadphotos = [];
    var files = $(".add-photo")[0].files;
    for(var i = 0;i<files.length;i++){
        saveDataArray.uploadphotos.push({
            id:'',
            name:files[i].name
        });
    }
}

function appendPhoto(item){
    console.log(item)
    var divEntry = $('#card-photos');
    divEntry.append(''+
            '<div class="img-block" id="photo-'+item.id+'">'+
                '<img src="/images/orgphotos/'+item.photo+'">'+
                '<span class="delete-span" onclick="deletePhotos(event)" param="'+item.id+'">удалить изображение</span>'+
            '</div>');
}