var firstStepValidates = {
    email: false,
    password: false
};

var tempDataArray = {
    keywordcount: 0,
    rubriccount: 0
};

var saveDataArray = {
    keywordcountid: -1,
    rubriccountid: -1,
    selectiontopid: -1,
    subrubricids: [],
    keywords: [],
    telephones: [],
    emails: [],
    messengers: []
};

var order = {
    services: []
};

var map = {
    name: '',
    adress: '',
    position: null,
    active: false,
    map: null,
    placemark: null
};

$(document).ready(function () {

    $('#searchDiv').hide();

    $("#ajax_no_initial_tag_handler").tagHandler({
        //getData: { id: 'user123', type: 'user' },
        //getURL: '/site/GetTags',
        onAdd: function (tag) {

            if (tempDataArray.keywordcount < 0) {
                alert('Выберите количество ключевых слов');
                return;
            }

            if (tempDataArray.keywordcount > saveDataArray.keywords.length)
                saveDataArray.keywords.push(tag)
            else {
                setTimeout(function () {
                    alert('Вы достигли максимального количества ключевых слов');
                }, 500);
                return false;
            }

        },
        onDelete: function (tag) {
            var index = saveDataArray.keywords.indexOf(tag);
            saveDataArray.keywords.splice(index, 1);
            return true;
        },
        availableTags: [ ]
        //autocomplete: true,
        //initLoad: false,

    });

    refreshCaptcha();

    $("#login").focusout(function () {
        verifyEmail();
    });

    $("#password2").focusout(function () {
        verifyPassword();
    });

    $("#password1").focusout(function () {
        verifyPassword();
    });

    $("#rubric").change(function () {
        if ($(this).val()) {
            getSubRubrics($(this).val());
        }
    });

    $("#keywordnum").change(function () {
        if ($(this).val()) {
            getPaidServiceDetails($(this).val());
        }
    });

    $("#rubriccount").change(function () {
        if ($(this).val()) {
            getPaidServiceDetails($(this).val());
        }
    });

    $("#selectiontopday").change(function () {
        if ($(this).val()) {
            getPaidServiceDetails($(this).val());
        }
    });

    $("#main-rubrics-selector").change(function () {
        if ($(this).val()) {
            getMainCardSubRubrics($(this).val());
        }
    });
    if ($("#main-rubrics-selector").val()) {
        getMainCardSubRubrics($("#main-rubrics-selector").val());
    }
    $("#rubricid").change(function () {
        if ($(this).val()) {
            getBasisPaidServiceDetails($(this).val());
        }
    });


    $("#companyName").attr('maxlength', '100');
    $("#orgindex").attr('maxlength', '50');
    $("#street").attr('maxlength', '50');
    $("#house").attr('maxlength', '20');
    $("#office").attr('maxlength', '20');
    $("#site").attr('maxlength', '25');
    $("#updatecontact").attr('maxlength', '50');
    $("#updatecontactphone").attr('maxlength', '16');
    $("#email").attr('maxlength', '40');
    $("#messenger").attr('maxlength', '30');

    $("#accountant").attr('maxlength', '50');
    $("#accountantphone").attr('maxlength', '13');


    $("#uAddress").attr('maxlength', '50');
    $("#iinBin").attr('maxlength', '12');
    $("#iinBin").keyup(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });

    $("#phone").attr('maxlength', '16');
    $("#phone").attr('minlength', '3');
   /* $("#phone").keyup(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });*/

    $("#parentorgid").keyup(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });

    $("#bik").attr('maxlength', '20');
    $("#iik").attr('maxlength', '20');
    $("#kbe").attr('maxlength', '4');
    $("#kbe").keyup(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });
    $("#bankName").attr('maxlength', '50');

    $("#workschedule").attr('maxlength', '1000');
    $("#busnums").attr('maxlength', '50');
    $("#login").attr('maxlength', '50');

    $("#region").change(function () {
        if ($(this).val()) {
            var region = $('#region :selected').text();
            initWithAddress(region);
        }
    });

    $("#phone_type").change(function () {
        if ($(this).val() != '3')
        {
            $("#phone").mask("+9(9999)99-99-99");
        }
        else
        {
            $("#phone").mask("+9(999)9999999");
        }

    });

    $("#city").change(function () {
        if ($(this).val()) {
            var city = $('#city :selected').text();
            initWithAddress('Город '+city);
        }
    });

    $("#street").focusout(function () {
        if ($(this).val()) {
            var city = $('#city :selected').text();
            var street = $('#street').val();
            initWithAddress('Город '+city + ' ' + street);
        }
    });

    $("#house").focusout(function () {
        if ($(this).val()) {
            var city = $('#city :selected').text();
            var street = $('#street').val();
            var house = $('#house').val();

            initWithAddress('Город '+city + ' ' + street + ' ' + house);
        }
    });

    $("#top").change(function () {
        if ($("#top").attr('checked')) {
            getPaidServiceDetails(21);
        } else {
            removeOrderId('3');
            $('.order-detail-' + '3').remove();
            updateOrders();
            updateOrderSumma();
        }
    });
    //
    //$("#main").change(function () {
    //    if($(this).attr('checked')){
    //        getPaidServiceDetails(4);
    //    } else {
    //        removeOrderId('4');
    //        $('.order-detail-' + '4').remove();
    //        updateOrders();
    //        updateOrderSumma();
    //    }
    //});


    $("#months").change(function () {
        if ($("#months").val()) {
            for (var i = 0; i < order.services.length; i++) {
                if (order.services[i].typeid != 3 && order.services[i].typeid != 7) {
                    var qualifier = $("#months").val() || 1;
                    order.services[i].months = qualifier;
                }
            }
            getBasisPaidServiceDetails($('#rubricid').val());
        }
    });

    $("#topmonths").change(function () {
        if ($("#topmonths").val()) {
            for (var i = 0; i < order.services.length; i++) {
                if (order.services[i].typeid == 3) {
                    var qualifier = $("#topmonths").val() || 1;
                    order.services[i].months = qualifier;
                    break;
                }
            }
            getBasisPaidServiceDetails($('#rubricid').val());
        }
    });

    $('.months-editor').on('change', function(e){
        if(!checkDates()){
            alert('Выбрана неправильная дата вывода в топ или базового пакета!')
        }
    })
    //
    //function refreshRecalc(){
    //    recalcPackage(777, "#basepackage", ".basepackage", $("#rubricid").val());
    //    recalcPackage(1, "#keywordspackage", ".keywords", $("#keywordcountid").val());
    //    recalcPackage(2, "#rubricspackage", ".rubrics", $("#rubriccountid").val());
    //    recalcPackage(3, "#top", ".top", 21);
    //    getPaidServiceDetails(31);
    //}

    //
    //$("#mainmonths").change(function () {
    //    if($("#mainmonths").val()){
    //        for (var i = 0; i < order.services.length; i++) {
    //            if (order.services[i].typeid == 3) {
    //                var qualifier = $("#topmonths").val() || 1;
    //                order.services[i].months = qualifier;
    //                break;
    //            } else {
    //                if (order.services[i].typeid == 7) {
    //                    var qualifier = $("#mainmonths").val() || 1;
    //                    order.services[i].months = qualifier;
    //                    break;
    //                }
    //            }
    //        }
    //        updateOrders();
    //        updateOrderSumma();
    //    }
    //});

});

function checkDates() {
    if ($("#top").attr('checked')) {
        $baseexpired = null;
//        if ($("#basepackage").attr('checked')) {
            $baseexpired = new Date();
            $baseexpired.setMonth($baseexpired.getMonth() + parseInt($("#months").val()));
//        }
        $topexpired = new Date();
        $topexpired.setMonth($topexpired.getMonth() + parseInt($("#topmonths").val()));

        var timeDiff = ($baseexpired.getTime() - $topexpired.getTime());
        if (timeDiff < 0) {
            return false;
        }
        //var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    }
    return true;
}

function go2Step() {
    if (step1Validate()) {
        $('#registrationform1').hide();
        $('#registrationform2').show();
    }
}

function go3Step() {
    if (step2Validate()) {
        $('#registrationform2').hide();
        $('#registrationform3').show();
        performOrder();
    }
}

function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (!emailReg.test($email)) {
        return false;
    } else {
        return true;
    }
}

function validatePassword(passw) {
    var pReg = /^[a-zA-Z0-9]+$/;
    if (!pReg.test(passw)) {
        return false;
    } else {
        return true;
    }
}

function verifyEmail() {
    var email = $('#login').val();
    if (email) {
        if (validateEmail(email)) {
            $.ajax(
                {
                    url: '/site/EmailIsFree',
                    type: 'POST',
                    data: {
                        'email': email
                    },
                    dataType: 'json',
                    success: function (result) {
                        if (result.status == 'success' && result.message == 'no') {
                            $("#loginstatus").text('свободен');
                            $('#login').removeClass('errorClass');
                            firstStepValidates.email = true;
                        }
                        else if (result.status == 'success' && result.message == 'yes') {
                            $("#loginstatus").text('занято');
                            $('#login').addClass('errorClass');

                            firstStepValidates.email = false;
                        }
                    }
                });
        }
        else {
            $("#loginstatus").text('не правильный формат');
            $('#login').addClass('errorClass');
        }

    }
    else
        console.log('no');

}

function verifyPassword() {
    var password1 = $("#password1").val();
    if (password1) {
        if (validatePassword(password1)) {
            if (validatePassword($('#password2').val())) {
                if (password1 == $('#password2').val()) {
                    $("#passwordstatus").text('совпадают');
                    $('#password2').removeClass('errorClass');
                    firstStepValidates.password = true;
                }
                else {
                    $("#passwordstatus").text('несовпадают');
                    $('#password2').addClass('errorClass');
                    firstStepValidates.password = false;
                }
            }
            else {
                firstStepValidates.password = false;
                $("#passwordstatus").text('только латиница и цифры');
                $('#password2').addClass('errorClass');
            }

        }
        else {
            firstStepValidates.password = false;
            $("#passwordstatus").text('только латиница и цифры');
            $('#password2').addClass('errorClass');
        }
    }
}

function step1Validate() {
    if (!firstStepValidates.email)
        verifyEmail();
    if (!firstStepValidates.password)
        verifyPassword();

    var res = false;
    if (firstStepValidates.email)//email
        if (firstStepValidates.password)//passw
        {
            if (verifyCaptcha()) {
                res = true;
            }
        }
    return res;
}

function verifyCaptcha() {
    var enteredVal = $('#captcha').val(), res = false;
    $.ajax(
        {
            url: '/site/VerifyCaptcha',
            type: 'POST',
            async: false,
            data: {
                'input': enteredVal
            },
            dataType: 'json',
            success: function (result) {
                if (result.status == 'success' && result.message == 'true') {
                    $("#captchastatus").text('верно');
                    $('#captcha').removeClass('errorClass');
                    res = true;
                }
                else if (result.status == 'success' && result.message == 'false') {
                    $("#captchastatus").text('неверно');
                    $('#captcha').addClass('errorClass');
                    res = false;
                }
                else {
                    $("#captchastatus").text('неверно');
                    $('#captcha').addClass('errorClass');
                    res = false;
                }
            }
        });
    return res;
}

function refreshCaptcha() {
    $.ajax(
        {
            url: '/site/captcha/refresh/1',
            type: 'POST',
            dataType: 'json',
            cache: false,
            success: function (data) {
                jQuery('#yw0').attr('src', data['url']);
                jQuery('body').data('captcha.hash', [data['hash1'], data['hash2']]);
            }
        });
}

///// step 2//////////////////////////

function step2Validate() {
    var result = true;

    //companyName
    if (!$('#companyName').val()) {
        $('#companyName').addClass('errorClass');
        result = false;
    }
    else
        $('#companyName').removeClass('errorClass');

    ////rubriccount
    //if (!$('#rubriccount').val()) {
    //    $('#rubriccount').addClass('errorClass');
    //    result = false;
    //}
    //else
    //    $('#rubriccount').removeClass('errorClass');
    //
    ////subrubrics
    //
    //if (saveDataArray.subrubricids.length == 0) {
    //    $('#rubric').addClass('errorClass');
    //    $('#subrubric').addClass('errorClass');
    //    result = false;
    //}
    //else {
    //    $('#rubric').removeClass('errorClass');
    //    $('#subrubric').removeClass('errorClass');
    //}

    //keywordcount
    if (!$('#keywordnum').val()) {
        $('#keywordnum').addClass('errorClass');
        result = false;
    }
    else
        $('#keywordnum').removeClass('errorClass');

    //orgindex
    if (!$('#orgindex').val()) {
        $('#orgindex').addClass('errorClass');
        result = false;
    }
    else
        $('#orgindex').removeClass('errorClass');


    if ($('#login').val() != 'test@in4smart.kz')
    {

    //region
    if (!$('#region').val()) {
        $('#region').addClass('errorClass');
        result = false;
    }
    else
        $('#region').removeClass('errorClass');

/*
    //city
    if (!$('#city').val()) {
        $('#city').addClass('errorClass');
        result = false;
    }
    else
        $('#city').removeClass('errorClass');*/

    //street
    if (!$('#street').val()) {
        $('#street').addClass('errorClass');
        result = false;
    }
    else
        $('#street').removeClass('errorClass');

    //house
    if (!$('#house').val()) {
        $('#house').addClass('errorClass');
        result = false;
    }
    else
        $('#house').removeClass('errorClass');

    // telephone
    if (saveDataArray.telephones.length == 0) {
        $('#phone').addClass('errorClass');
        result = false;
    }
    else {
        $('#phone').removeClass('errorClass');
    }

    // email
    if (saveDataArray.emails.length == 0) {
        $('#email').addClass('errorClass');
        result = false;
    }
    else {
        $('#email').removeClass('errorClass');
    }
    }

    //city
    if (!$('#city').val()) {
        $('#city').addClass('errorClass');
        result = false;
    }
    else
        $('#city').removeClass('errorClass');

    if ($('#parentorgid').val()) {

        if (checkCardId($('#parentorgid').val())) {
            $('#parentorgid').removeClass('errorClass');
        }
        else {
            $('#parentorgid').addClass('errorClass');
            result = false;
        }

    }
    else
        $('#parentorgid').removeClass('errorClass');

    if(!($("#rubricid").val() > 0)){
        alert('Выберите рубрику базового пакета!');
        return;
    }
    if(!checkDates()){
        alert('Выбрана неправильная дата вывода в топ или базового пакета!')
        result = false;
    }
    return result;
}

function checkCardId(cardid) {
    var res = false;
    $.ajax(
        {
            url: '/site/CheckCardId',
            type: 'POST',
            dataType: 'json',
            async: false,
            data: {
                cardid: cardid
            },
            success: function (result) {

                if (result.message == 'true') {
                    res = true;
                    $('#parentorgiderror').text('');
                }
                else {
                    res = false;
                    $('#parentorgiderror').text('Такого ID не существует');
                }


            }
        });

    return res;
}


function getSubRubrics(rubricId) {
    $.ajax(
        {
            url: '/site/GetSubRubrics',
            type: 'POST',
            dataType: 'json',
            data: {
                rubricId: rubricId
            },
            success: function (result) {
                //console.log(result);

                var select = $('#subrubric');
                select.empty();
                select.append($("<option />").val(-1).text('Выберите'));
                $.each(result, function () {
                    select.append($("<option />").val(this.rubricid).text(this.name));
                });

            }
        });
}

function addSubRubricForSave() {
    if (tempDataArray.rubriccount == 0) {
        alert('Выберите количество рубрик');
        return;
    }

    if (tempDataArray.rubriccount > saveDataArray.subrubricids.length) {
        var select = $('#subrubric');
        if (select.val() && select.val() != -1) {
            var val = select.val();
            if ($.inArray(val, saveDataArray.subrubricids) == -1) {
                saveDataArray.subrubricids.push(val);

                var divEntry = $('#subrubricAddItems');
                divEntry.append("<div class='optionContent optionForDelete' id='addSubRub" + val + "'> <span class='optionItem'>" + $("#subrubric option[value='" + val + "']").text() + "</span>" +
                    "<span onclick='deleteSubrubrics(" + val + ", addSubRub" + val + ");false;'><img src='/images/delete.png' width='16' height='16' alt=''/></span> </div>");

            }
        }
        select.val(-1);
    }
    else {
        alert('Вы достигли максимального количества рубрик');
    }
}

function deleteSubrubrics(val, divId) {
    //console.log( saveDataArray.subrubricids);
    //console.log(divId);
    saveDataArray.subrubricids.splice(saveDataArray.subrubricids.indexOf(val.toString()), 1);
    divId.remove();
    //console.log( saveDataArray.subrubricids);
}
function addOrderItem(service) {
    for (var i = 0; i < order.services.length; i++) {
        console.log(order.services[i].typeid);
        if (order.services[i].typeid == service.typeid) {
            order.services.splice(i, 1);
        }
    }
    order.services.push(service);
}

function updateOrderSumma() {
    var sum = 0;
    for (var i = 0; i < order.services.length; i++) {
        if (order.services[i].typeid == 4) {
            sum = sum + parseInt(order.services[i].cost) * parseInt(order.services[i].months) * saveDataArray.proposals.length - (order.services[i].balance);
        } else {
            sum = sum + parseInt(order.services[i].cost) * parseInt(order.services[i].months) - (order.services[i].balance);
        }
    }
    var qualifier = $("#months").val() || 1;
    var isDiscount = qualifier == 12 ? true : false;
    var res = sum * (isDiscount ? 0.85 : 1);
    var textDiscount = isDiscount ? " со скидкой 15%" : "";
    //$('.orderSum').text('за ' + qualifier + " месяцев" + textDiscount);
    $('.orderSum').text(Math.ceil(res) + ' тг' + textDiscount);
}

/*
function addTelephoneForSave() {

    var textField = $('#phone');

    //   console.log(textField.val());
    if (textField.val() && (textField.val().length <= 11 && textField.val().length >= 3)) {
        var val = textField.val().trim();
        val = val.replace(/\s/g, '');//удялем пробелы
        val = val.replace('(', '');
        val = val.replace(')', '');

        if ($.inArray(val, saveDataArray.telephones) == -1) {
            saveDataArray.telephones.push(val);

            var divEntry = $('#telephonesAddItems');
            divEntry.append("<div class='optionContent optionForDelete' id='addTel" + val + "'> <span class='optionItem'>" + val + "</span>" +
                "<span onclick='deleteTelephone(" + val + ", addTel" + val + ");false;'><img src='/images/delete.png' width='16' height='16' alt=''/></span> </div>");

        }
        textField.val('');
    }
    else
        alert('Заполните номер правильно');

}*/

function addTelephoneForSave() {

    var selectPhoneType = $('#phone_type'), valueFiled = $('#phone'), noteField = $('#note');
    var val = valueFiled.val(), phoneTypeId = selectPhoneType.val(), note = noteField.val();


    if (phoneTypeId && val) {
        var data = {
            phoneTypeId: phoneTypeId,
            value: val,
            note: note
        };

        var isAdd = false;
        for (var i = 0; i < saveDataArray.telephones.length; i++) {
            var item = saveDataArray.telephones[i];
            if (item.value == val && item.phoneTypeId == phoneTypeId) {
                isAdd = true;
            }
        }

        if (!isAdd) {
            saveDataArray.telephones.push(data);
            console.log(saveDataArray.telephones);

            var divEntry = $('#telephonesAddItems'), unId = phoneTypeId + getRandomInt(5000, 10000);
            console.log(unId);

            divEntry.append("<div class='optionContent optionForDelete' id='addTel" + unId + "'> <span class='optionItem'>" + $("#phone_type option[value='" + phoneTypeId + "']").text() + ' - ' + val + ' - ' + note + "</span>" +
                "<span onclick='deleteTelephone(\"" + val + "\", " + phoneTypeId + ", addTel" + unId + ");false;'><img src='/images/delete.png' width='16' height='16' alt=''/></span> </div>");


        }
        valueFiled.val('');
        noteField.val('');
    }
    else
        alert('Заполните номер правильно');
}

function deleteTelephone(val, phoneTypeId, divId) {

    for (var i = 0; i < saveDataArray.telephones.length; i++) {
        var item = saveDataArray.telephones[i];
        if (item.value == val && item.phoneTypeId == phoneTypeId) {
            saveDataArray.telephones.splice(i, 1);
        }
    }
    divId.remove();
    console.log(saveDataArray.telephones);
}

/*
function deleteTelephone(val, divId) {
    saveDataArray.telephones.splice(saveDataArray.telephones.indexOf(val.toString()), 1);
    divId.remove();
    // console.log(saveDataArray.telephones);
}*/

function addEmailForSave() {
    var textField = $('#email');
    if (textField.val().length == 0) {
        alert('Введите значение');
        return;
    }
    if (validateEmail(textField.val())) {
        var val = textField.val().trim();

        if ($.inArray(val, saveDataArray.emails) == -1) {
            saveDataArray.emails.push(val);

            var divEntry = $('#emailsAddItems');
            var id = val.replace('@', '');
            id = id.replace('.', '');

            divEntry.append("<div class='optionContent optionForDelete' id='addEmail" + id + "'> <span class='optionItem'>" + val + "</span>" +
                "<span onclick='deleteEmail(\"" + val.toString() + "\", addEmail" + id + ");false;'><img src='/images/delete.png' width='16' height='16' alt=''/></span> </div>");

        }
        textField.val('');
    }
    else
        alert('Не правильный формат е-мейл адреса. Пример: test@test.kz');

}

function deleteEmail(val, divId) {
    saveDataArray.emails.splice(saveDataArray.emails.indexOf(val.toString()), 1);
    divId.remove();
}

function addMessengerForSave() {
    var selectMesType = $('#messenger_type'), valueFiled = $('#messenger');
    var val = valueFiled.val(), mesTypeId = selectMesType.val();

    if (mesTypeId && val) {
        var data = {
            mesTypeId: mesTypeId,
            value: val
        };

        var isAdd = false;
        for (var i = 0; i < saveDataArray.messengers.length; i++) {
            var item = saveDataArray.messengers[i];
            if (item.value == val && item.mesTypeId == mesTypeId) {
                isAdd = true;
            }
        }

        if (!isAdd) {
            saveDataArray.messengers.push(data);
            console.log(saveDataArray.messengers);

            var divEntry = $('#messengersAddItems'), unId = mesTypeId + getRandomInt(5000, 10000);
            console.log(unId);

            divEntry.append("<div class='optionContent optionForDelete' id='addMessenger" + unId + "'> <span class='optionItem'>" + $("#messenger_type option[value='" + mesTypeId + "']").text() + ' - ' + val + "</span>" +
                "<span onclick='deleteMessengers(\"" + val + "\", " + mesTypeId + ", addMessenger" + unId + ");false;'><img src='/images/delete.png' width='16' height='16' alt=''/></span> </div>");


        }
        valueFiled.val('');
    }
    else
        alert('Заполните поле месенджера');
}

function deleteMessengers(val, mesTypeId, divId) {

    for (var i = 0; i < saveDataArray.messengers.length; i++) {
        var item = saveDataArray.messengers[i];
        if (item.value == val && item.mesTypeId == mesTypeId) {
            saveDataArray.messengers.splice(i, 1);
        }
    }
    divId.remove();
    console.log(saveDataArray.messengers);
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


///// step 3 /////////////////////

function step3Validate() {
    result = true;

    if ($('#login').val() != 'test@in4smart.kz')
    {

    if (!$('#uAddress').val()) {
        $('#uAddress').addClass('errorClass');
        result = false;
    }
    else
        $('#uAddress').removeClass('errorClass');


    if (!$('#iinBin').val()) {
        $('#iinBin').addClass('errorClass');
        result = false;
    }
    else {

        if ($('#iinBin').val().length != 12) {
            $('#iinBin').addClass('errorClass');
            alert('Длина ИИН/БИН не правильная');
            result = false;
        }
        else if ($('#iinBin').val().length == 12)
            $('#iinBin').removeClass('errorClass');
    }


    if (!$('#bik').val()) {
        $('#bik').addClass('errorClass');
        result = false;
    }
    else
        $('#bik').removeClass('errorClass');

    if (!$('#iik').val()) {
        $('#iik').addClass('errorClass');
        result = false;
    }
    else
        $('#iik').removeClass('errorClass');

    if (!$('#kbe').val()) {
        $('#kbe').addClass('errorClass');
        result = false;
    }
    else
        $('#kbe').removeClass('errorClass');

    if (!$('#bankName').val()) {
        $('#bankName').addClass('errorClass');
        result = false;
    }
    else
        $('#bankName').removeClass('errorClass');
      }

    return result;
}

function performOrder() {
    for (var i = 0; i < order.services.length; i++) {
        var item = order.services[i];
        addFinishPaymentDetail(item.typename, item.months, item.cost, 0, item.typeid);

    }
    updateOrderSumma();
}

function deleteOrderItem(typeid, elId) {

    for (var i = 0; i < order.services.length; i++) {
        if (order.services[i].typeid == typeid) {
            order.services.splice(i, 1);
            if (typeid == 1) {
                saveDataArray.keywordcountid = 1;// paidservicedetailid // если удалили , то скидываем на поумаолчанию
            } else if (typeid == 2)
                saveDataArray.rubriccountid = 11;// paidservicedetailid

        }
    }

    $("." + elId).remove();
    updateOrderSumma();
}

function sendToModeration() {

    var data = getCardForSave();
    //console.log(data);
    $("#loadingDiv").show();
    if (step3Validate()) {

        $.ajax(
            {
                url: '/site/SaveNewCard',
                type: 'POST',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                /* data: {
                 'data': JSON.stringify(data)
                 },*/
                //dataType: 'json',
                success: function (result) {
                    //console.log(result);
                    result = jQuery.parseJSON(result);

                    if (result.status == 'success') {
                        $('#emailResp').text(result.data.email);
                        $('#general_modal').modal('show');
                    }
                    else if (result.status == 'error') {
                        if (result.message == 'emailFiledToSend') {
                            $('#errormsg').text('Не удалось отправить письмо на указаный емейл адрес');
                        } else if (result.message == 'file invalid') {
                            $('#errormsg').text('Загружаемый лого не соответствует требованиям');
                        }


                        $('#general_modal2').modal('show');
                        // console.log(result.message);
                    }

                    $('#loadingDiv').hide();
                }
            });
    }
    else
        $('#loadingDiv').hide();
}

function getNormalTelephoneVal(v) {
   /* v = v.replace('(', '');
    v = v.replace(')', '');*/
    v = v.replace(/\s/g, '');
    return v;
}

function getCardForSave() {

    var updatecontactphone = getNormalTelephoneVal($('#updatecontactphone').val());
    var accountantphone = getNormalTelephoneVal($('#accountantphone').val());

    var data = {
        step1: {
            email: $('#login').val(),
            password1: $('#password1').val(),
            password2: $('#password2').val()
        },
        step2: {
            companyName: $('#companyName').val(),
            ownership: $('#ownership').val(),
            description: $('#description').val(),
            rubriccountid: $("#rubricspackage").attr('checked') ? $("#rubriccount").val() : '11',
            subrubricids: saveDataArray.subrubricids,
            keywordcountid: $("#keywordnum").val(),
            keywords: saveDataArray.keywords,
            regionid: $('#region').val(),
            cityid: $('#city').val(),
            street: $('#street').val(),
            house: $('#house').val(),
            office: $('#office').val(),
            telephones: saveDataArray.telephones,
            emails: saveDataArray.emails,
            site: $('#site').val(),
            messengers: saveDataArray.messengers,
            updatecontact: $('#updatecontact').val(),
            updatecontactphone: updatecontactphone,
            accountant: $('#accountant').val(),
            accountantphone: accountantphone,
            workschedule: $('#workschedule').val(),
            busnums: $('#busnums').val(),
            orgindex: $('#orgindex').val(),
            coords: map.position,
            parentorgid: $('#parentorgid').val(),
            //7
            basepackage: true,
            months: $("#months").val(),
            topmonths: $("#topmonths").val(),
            rubricid: $("#rubricid").val(),
            top: $("#top").attr('checked') ? true : false,
            main: $("#main").attr('checked') ? true : false,
            rubricspackage: $("#rubricspackage").attr('checked') ? true : false,
            keywordspackage: true

        },
        step3: {
            uAddress: $('#uAddress').val(),
            iinBin: !$('#iinBin').val() ? null : $('#iinBin').val(),
            bik: $('#bik').val(),
            iik: $('#iik').val(),
            kbe: !$('#kbe').val() ? null : $('#kbe').val(),
            bankName: $('#bankName').val()
        }

    }

    var formData = new FormData();
    jQuery.each($('#logo')[0].files, function (i, file) {
        formData.append('uploadFile-' + i, file);
    });

    //console.log(data);

    formData.append('data', JSON.stringify(data));

    return formData;
}

function getPaidServiceDetails(id) {
    //actionGetBalancePaidServiceDetails

    $.ajax(
        {
            url: '/site/GetPaidServiceDetails',
            type: 'POST',
            dataType: 'json',
            data: {
                paidservicedetaild: id
            },
            error: function (result) {
                console.log(result);
            },
            success: function (result) {
                if (result) {
                    var source = result.jsonDataSource;

                    var month = $("#months").val() || 1;
                    if (source.attributes.paidserviceid == '3') {
                        month = $("#topmonths").val() || 1;
                    }
                    if (source.attributes.paidserviceid == '7') {
                        month = $("#mainmonths").val() || 1;
                    }
                    $.ajax(
                        {
                            url: '/site/GetCostPaidServiceDetails',
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                paidservicedetailid: source.attributes.paidservicedetaild,
                                count: source.attributes.count,
                                months: month,
                                fullcost: 1,
                                topmonths: $('#topmonths').val() || 1
                            },
                            success: function (costres) {
                                if (costres) {
                                    var service = {
                                        id: source.attributes.paidservicedetaild,
                                        typeid: source.attributes.paidserviceid,
                                        typename: source.relations.paidservice.name,
                                        cost: costres.cost || source.attributes.cost,
                                        months: month,
                                        balance: 0,
                                        count: source.attributes.count
                                    };
                                    addOrderItem(service);
                                    updateOrders();


                                }
                                else
                                    console.log('error');

                            }
                        });
                }
            }
        });
}

function getBasisPaidServiceDetails(id) {
    $.ajax(
        {
            url: '/site/GetBasisPaidServiceDetails',
            type: 'POST',
            dataType: 'json',
            data: {
                rubricid: id
            },
            error: function (result) {
                console.log(result);
            },
            success: function (result) {
                if (result) {
                    var source = result.jsonDataSource;
                    var month = $("#months").val() || 1;
                    if (source.attributes.paidserviceid == '3') {
                        month = $("#topmonths").val() || 1;
                    }
                    if (source.attributes.paidserviceid == '7') {
                        month = $("#mainmonths").val() || 1;
                    }
                    $.ajax(
                        {
                            url: '/site/GetCostPaidServiceDetails',
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                paidservicedetailid: source.attributes.paidservicedetaild,
                                count: source.attributes.count,
                                months: month,
                                fullcost: 1,
                                topmonths: $('#topmonths').val() || 1
                            },
                            success: function (costres) {
                                if (costres) {

                                    var service = {
                                        id: source.attributes.paidservicedetaild,
                                        typeid: source.attributes.paidserviceid,
                                        typename: source.relations.paidservice.name,
                                        cost: costres.cost || source.attributes.cost,
                                        months: month,
                                        balance: 0,
                                        count: source.attributes.count
                                    };
                                    addOrderItem(service);
                                    updateOrders();


                                }
                                else
                                    console.log('error');

                            }
                        });
                }
            }
        });
}

function getCardSubRubrics(rubricId) {
    $.ajax(
        {
            url: '/site/GetSubRubrics',
            type: 'POST',
            dataType: 'json',
            data: {
                rubricId: rubricId
            },
            success: function (result) {
                var select = $('#subrubrics-selector');
                select.empty();
                $.each(result, function () {
                    select.append($("<option />").val(this.rubricid).text(this.name));
                });

            }
        });
}

function getMainCardSubRubrics(rubricId) {
    $.ajax(
        {
            url: '/site/GetSubRubrics',
            type: 'POST',
            dataType: 'json',
            data: {
                rubricId: rubricId
            },
            success: function (result) {
                var select = $('#rubricid');
                select.empty();
                select.append($("<option />").val(-1).text('Выберите'));
                $.each(result, function () {
                    select.append($("<option />").val(this.rubricid).text(this.name));
                });

            }
        });
}

function addPaymentDetail(type, months, cost, balance, serviceTypeId) {
    $('.order-detail-' + serviceTypeId).remove();
    if(parseInt(cost) - parseInt(balance)>0) {
        $('.order-details tbody').append('<tr class = "order-detail-' + serviceTypeId + ' orderItem"><td name = "type">' + type + '</td><td>за ' + months + ' мес.</td><td name = "cost"> ' + cost + 'тг</td><td> ' + (balance > 0 ? '-' : '') + balance + 'тг</td></tr>');
    }
    updateOrderSumma();
}
function updateOrders(){
    for(var i=0;i<order.services.length;i++) {
        var item = order.services[i];
        switch (item.typeid) {
            case "1":
                tempDataArray.keywordcount = item.count;
                saveDataArray.keywordcountid = item.paidservicedetaild;
                addPaymentDetail(item.typename + ' ' + item.count + ' шт. ', item.months, parseInt(item.cost), item.balance, item.typeid);
                break;
            case "2":
                tempDataArray.rubriccount = item.count;
                saveDataArray.rubriccountid = item.paidservicedetaild;
                addPaymentDetail(item.typename + ' ' + item.count + ' шт. ', item.months, parseInt(item.cost), item.balance, item.typeid);
                break;
            case "3":
                addPaymentDetail(item.typename, item.months, parseInt(item.cost), item.balance, item.typeid);
                break;
            case "4":
                if (saveDataArray.proposals.length > 0) {
                    addPaymentDetail(item.typename + " " + saveDataArray.proposals.length + ' шт.', item.months, parseInt(item.cost), item.balance, item.typeid);
                }
                break;
            case "6":
                tempDataArray.photoscount = item.count;
                saveDataArray.photoscountid = item.paidservicedetaild;
                addPaymentDetail(item.typename + ' ' + item.count + ' шт. ', item.months, parseInt(item.cost), item.balance, item.typeid);
                break;
            case "7":
                addPaymentDetail(item.typename, item.months, parseInt(item.cost), item.balance, item.typeid);
                break;
            case "777":
                addPaymentDetail(item.typename + " №" + item.id + " <ul><li>1 рубрика</li><li>25 слов</li><li>Контакты и Размещение на карте</li><li>Лого или картинка - 1 шт.</li><li>Размещение на мобильном приложении и на веб сайте</li></ul>", item.months, parseInt(item.cost), item.balance, item.typeid);
                break;
        }
    }
}

function updateOrderSumma() {
    var sum = 0;
    for (var i = 0; i < order.services.length; i++) {
        sum = sum + parseInt(order.services[i].cost);
    }
    var qualifier = $("#months").val() || 1;
    var isDiscount = qualifier == 12 ? true : false;
    var res = sum * (isDiscount ? 0.85 : 1);
    var textDiscount = isDiscount?" со скидкой 15%":"";
    $('.orderSum').text(Math.ceil(res) + ' тг' + textDiscount);
}

function addFinishPaymentDetail(type, months, cost, balance, serviceTypeId) {
    $('.order-detail-' + serviceTypeId).remove();
    $('#finishOrder tbody').append('<tr class = "order-detail-' + serviceTypeId + ' orderItem"><td name = "type">' + type + '</td><td>за ' + months + ' мес.</td><td name = "cost"> ' + cost + 'тг</td><td>' + (serviceTypeId != '777' ? '<button class="exclude hover-green" onclick=\"deleteOrderItem(' + serviceTypeId + ', \'order-detail-' + serviceTypeId + '\'); false;\">исключить</button>' : '') + '</td></tr>');

    updateOrderSumma();
}

function removeOrderId(service) {
    for (var i = 0; i < order.services.length; i++) {
        if (order.services[i].typeid == service) {
            order.services.splice(order.services.indexOf(service), 1);
        }
    }
}


