var map = {
    name: '',
    adress: '',
    position: null,
    active: false,
    map: null,
    placemark: null
};

function createMap(pcoords) {

    console.log(pcoords);
    if (!map.map) {
        map.map = new ymaps.Map('map2', {
            // При инициализации карты обязательно нужно указать
            // её центр и коэффициент масштабирования.
            center: [pcoords[0], pcoords[1]],
            zoom: 12
        });
    }

    map.position = pcoords;

    // Слушаем клик на карте
    map.map.events.add('click', function (e) {
        //console.log(map.active)
        //console.log(e.get('coords'))
        var coords = e.get('coords');

        // Если метка уже создана – просто передвигаем ее
        if (map.placemark) {
            map.placemark.geometry.setCoordinates(coords);
        }
        // Если нет – создаем.
        else {
            map.placemark = createPlacemark(coords);
            map.map.geoObjects.add(map.placemark);
            // Слушаем событие окончания перетаскивания на метке.
            map.placemark.events.add('dragend', function () {
                map.position = map.placemark.geometry.getCoordinates();
                // console.log(position);
                getAddress(map.placemark.geometry.getCoordinates());
            });
        }
        getAddress(map.position);
    });
}

function createPlacemark(coords) {
    return new ymaps.Placemark(coords, {
        iconContent: 'поиск...'
    }, {
        preset: 'islands#violetStretchyIcon',
        draggable: true
    });
}

function getAddress(coords) {
    map.placemark.properties.set('iconContent', 'поиск...');
    ymaps.geocode(coords).then(function (res) {
        map.placemark.properties
            .set({
                iconContent: map.name,
                balloonContent: map.name
            });
    });
}

function setPosition(pcoords) {

    if (map.position) {
        // Если метка уже создана – просто передвигаем ее
        if (map.placemark) {

            map.placemark.geometry.setCoordinates(pcoords);
            map.position = pcoords;
            getAddress(map.position);
        }
        // Если нет – создаем.
        else {
            map.placemark = createPlacemark(pcoords);//map.position
            map.map.geoObjects.add(map.placemark);
            // Слушаем событие окончания перетаскивания на метке.
            map.placemark.events.add('dragend', function () {
                map.position = map.placemark.geometry.getCoordinates();
            });
            getAddress(map.position);
        }
    } else {
        if (map.adress) {

            ymaps.geocode(map.adress, {
                /**
                 * Опции запроса
                 * @see http://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/geocode.xml
                 */
                // boundedBy: myMap.getBounds(), // Сортировка результатов от центра окна карты
                // strictBounds: true, // Вместе с опцией boundedBy будет искать строго внутри области, указанной в boundedBy
                results: 1 // Если нужен только один результат, экономим трафик пользователей
            }).then(function (res) {
                    // Выбираем первый результат геокодирования.
                    var firstGeoObject = res.geoObjects.get(0),
                    // Координаты геообъекта.
                        coords = firstGeoObject.geometry.getCoordinates(),
                    // Область видимости геообъекта.
                        bounds = firstGeoObject.properties.get('boundedBy');

                    // Масштабируем карту на область видимости геообъекта.
                    map.map.setBounds(bounds, {
                        checkZoomRange: true // проверяем наличие тайлов на данном масштабе.
                    });

                    if (map.placemark) {
                        map.placemark.geometry.setCoordinates(coords);
                        map.placemark.properties
                            .set({
                                iconContent: map.name,
                                balloonContent: map.name
                            });
                    } else {
                        // Добавляем первый найденный геообъект на карту.
                        //myMap.geoObjects.add(firstGeoObject);
                        // Масштабируем карту на область видимости геообъекта.
                        map.map.setBounds(bounds, {
                            checkZoomRange: true // проверяем наличие тайлов на данном масштабе.
                        });

                        /**
                         * Если нужно добавить по найденным геокодером координатам метку со своими стилями и контентом балуна, создаем новую метку по координатам найденной и добавляем ее на карту вместо найденной.
                         */

                        map.placemark = new ymaps.Placemark(coords, {
                            iconContent: map.name,
                            balloonContent: map.name
                        }, {
                            preset: 'islands#violetStretchyIcon',
                            draggable: true
                        });
                        map.placemark.events.add("dragend", function (event) {
                            map.position = map.placemark.geometry.getCoordinates();
                            getAddress(map.placemark.geometry.getCoordinates());
                        });

                        map.map.geoObjects.add(map.placemark);
                    }
                });
        }
    }
}

function initWithAddress(address) {

    var myGeocoder = ymaps.geocode(address);
    myGeocoder.then(
        function (res) {
            var corrss = res.geoObjects.get(0).geometry.getCoordinates();

            var firstGeoObject = res.geoObjects.get(0),
                bounds = firstGeoObject.properties.get('boundedBy');

            if (map.map) {
                map.map.setBounds(bounds, {
                    checkZoomRange: true // проверяем наличие тайлов на данном масштабе.
                });
                map.name = address;
            }

            createMap(corrss);
            setPosition(corrss);
        },
        function (err) {
            alert('Ошибка');
        }
    );
}



