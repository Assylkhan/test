(function() {
    ymaps.ready(init);
    function init() {
        var adress = 'астана достык 13';
        var position = null;
        var myMap;
        var myPlacemark;
        myMap = new ymaps.Map('bigMap', {
            // При инициализации карты обязательно нужно указать
            // её центр и коэффициент масштабирования.
            center: [51.1379457, 71.4503897], // Astana
            zoom: 10
        });

        if(position){
            myPlacemark = createPlacemark(position);
            myMap.geoObjects.add(myPlacemark);
            getAddress(position);
        } else if (adress) {
            ymaps.geocode(adress, {
                /**
                 * Опции запроса
                 * @see http://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/geocode.xml
                 */
                // boundedBy: myMap.getBounds(), // Сортировка результатов от центра окна карты
                // strictBounds: true, // Вместе с опцией boundedBy будет искать строго внутри области, указанной в boundedBy
                results: 1 // Если нужен только один результат, экономим трафик пользователей
            }).then(function (res) {
                // Выбираем первый результат геокодирования.
                var firstGeoObject = res.geoObjects.get(0),
                // Координаты геообъекта.
                    coords = firstGeoObject.geometry.getCoordinates(),
                // Область видимости геообъекта.
                    bounds = firstGeoObject.properties.get('boundedBy');
console.log(coords)
                // Добавляем первый найденный геообъект на карту.
                //myMap.geoObjects.add(firstGeoObject);
                // Масштабируем карту на область видимости геообъекта.
                myMap.setBounds(bounds, {
                    checkZoomRange: true // проверяем наличие тайлов на данном масштабе.
                });

                myPlacemark = new ymaps.Placemark(coords, {
                    iconContent: adress,
                    balloonContent: adress
                }, {
                    preset: 'islands#violetStretchyIcon'
                });

                myMap.geoObjects.add(myPlacemark);

            });
        }

        function createPlacemark(coords) {
            return new ymaps.Placemark(coords, {
                iconContent: 'поиск...'
            }, {
                preset: 'islands#violetStretchyIcon'
            });
        }

        function getAddress(coords) {
            myPlacemark.properties.set('iconContent', 'поиск...');
            ymaps.geocode(coords).then(function (res) {
                var firstGeoObject = res.geoObjects.get(0);

                // Область видимости геообъекта.
                var bounds = firstGeoObject.properties.get('boundedBy');

                // Добавляем первый найденный геообъект на карту.
                //myMap.geoObjects.add(firstGeoObject);
                // Масштабируем карту на область видимости геообъекта.
                myMap.setBounds(bounds, {
                    checkZoomRange: true // проверяем наличие тайлов на данном масштабе.
                });

                myPlacemark.properties
                    .set({
                        iconContent: firstGeoObject.properties.get('name'),
                        balloonContent: firstGeoObject.properties.get('text')
                    });
            });
        }
    }
})()