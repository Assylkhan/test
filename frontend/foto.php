<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <script src="js/jquery-2.1.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-hover-dropdown.min.js"></script>
    <script src="js/jquery.liquidcarousel.js"></script>
    <script src="js/cookie.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="css/template.css">
    <link rel="stylesheet" href="css/modules.css">
    <link rel="stylesheet" href="css/screen.css">
    <script>
        /* Запускаем сразу после загрузки страницы */
        $( document ).ready(function() {
            marginBlocks();

            // Читаем из куков текущий город
            var currentCity = readCookie("currentCity");
            currentCity = (currentCity != null) ? currentCity : $(".city-ul li").eq(0).text();
            $(".current-city").text(currentCity);

            $('.upbutton').click(function(){
                $('html, body').animate({scrollTop:0}, 'slow');
            });

            $(".selectcity").click(function(){
                rotateBG();
                $(".city-ul").slideToggle();
            });

            $(".city-ul li").click(function(){
                var selectedCity = $( this).text();
                $(".current-city").text(selectedCity);
                // Записываем в куки текущий город
                writeCookie("currentCity", selectedCity, 7);
                rotateBG();
                $(".city-ul").slideToggle();
            });
        });

        /* Используем при изменении ширины окна в режиме live */
        $( window ).resize(function() {
            $(".img-block").removeClass("no-right")
            marginBlocks()
        });

        /* Собственно, меняем отступы для крайних в строке картинок, чтобы все было хорошо, дети росли, старики не болели, закончилась война */
        function marginBlocks() {
            var blocks = $(".img-block");
            var length = blocks.length;
            var width = window.innerWidth;
            var num = width < 1440 ? 4 : 6;
            if (length > 0) {
                for (var i = num; i < length; i += num + 1) {
                    if (blocks.eq(i)) {
                        blocks.eq(i).addClass("no-right");
                    }
                }
            }
        }

        function rotateBG() {
            if ( $(".city-arrow").is(".rotate-bg") ) {
                $(".city-arrow").removeClass("rotate-bg");
            } else {
                $(".city-arrow").addClass("rotate-bg");
            }
        }
    </script>
</head>
<body>
<div id="content-block">
    <div class="row header center-content">
        <div class="span12">
            <div class="span6 p-relative lessspan">
                <div class="logo">
                    <a href="/">
                        <img src="images/logo.png" alt="INFOSMART" title="INFOSMART">
                    </a>
                </div>
                <div class="citywidget">
                    <div class="cityname">
                        <span class="city-arrow"></span>
                        <span class="current-city">Астана</span>
                    </div>
                    <span class="selectcity">выберите город</span>
                    <ul class="city-ul">
                        <li>Астана</li>
                        <li>Москва</li>
                        <li>Лондон</li>
                        <li>Деревня Лопухи</li>
                        <li>Бишкек</li>
                        <li>Астраханка</li>
                        <li>Вашингтон</li>
                        <li>Иваново</li>
                        <li>Прага</li>
                        <li>Степногорск</li>
                        <li>Павлодар</li>
                    </ul>
                </div>
                <div class="infowidget">
                    <span class="weather-day">+18&deg;<span class="dotes">...</span>+20&deg;</span>
                    <span class="weather-night">+4&deg;<span class="dotes">...</span>+6&deg;</span>
                    <span class="dollar">185.5<span class="depreciation"></span></span>
                    <span class="euro">250.2<span class="growth-rate"></span></span>
                    <span class="rub">5.4<span class="depreciation"></span></span>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="span6 morespan">
                <ul class="mainmenu">
                    <li>
                        <a class="deals hover-green" href="deals.php">предложения</a>
                    </li>
                    <li>
                        <a class="map hover-green" href="map.php">карта</a>
                    </li>
                    <li>
                        <a class="news hover-green" href="#">новости</a>
                    </li>
                    <li>
                        <a class="personal-area hover-green" href="#">личный кабинет</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row registration center-content">
        <div class="span12">
            <h1>Личный кабинет - <span>Название предприятия</span></h1>
            <button class="preview-button hover-green">просмотр карточки</button>
        </div>
        <div class="span12 personal-menu">
            <ul>
                <li><a href="generalinformation.php" id="general">Общая информация</a></li>
                <li><a href="onmap.php" id="onmap">На карте</a></li>
                <li><a href="padeals.php" id="deals">Предложения</a></li>
                <li><a href="foto.php" id="foto" class="active">Фото</a></li>
                <li><a href="job.php" id="job">Вакансии</a></li>
            </ul>
        </div>
        <div class="span12 foto">
            <div class="img-block">
                <img src="images/foto1.png">
                <span class="delete-span">удалить изображение</span>
            </div>
            <div class="img-block">
                <img src="images/foto2.png">
                <span class="delete-span">удалить изображение</span>
            </div>
            <div class="img-block">
                <img src="images/foto3.png">
                <span class="delete-span">удалить изображение</span>
            </div>
            <div class="img-block">
                <img src="images/foto4.png">
                <span class="delete-span">удалить изображение</span>
            </div>
            <div class="img-block">
                <img src="images/foto1.png">
                <span class="delete-span">удалить изображение</span>
            </div>
            <div class="img-block">
                <img src="images/foto2.png">
                <span class="delete-span">удалить изображение</span>
            </div>
            <div class="img-block">
                <img src="images/foto3.png">
                <span class="delete-span">удалить изображение</span>
            </div>
            <div class="img-block">
                <img src="images/foto4.png">
                <span class="delete-span">удалить изображение</span>
            </div>
            <div class="img-block add-img hover-green">загрузить изображение</div>
        </div>
        <div class="span12 registration-buttons">
            <button class="cancel-step hover-green">отмена</button>
            <button class="next-step hover-green">сохранить и отправить на модерацию</button>
        </div>
    </div>
    <div class="wrap"></div>
</div>
<div class="row footer">
    <span class="upbutton">наверх</span>
    <ul class="footermenu">
        <li>
            <a href="#">Lorem</a>
        </li>
        <li>
            <a href="#">Ipsum</a>
        </li>
        <li>
            <a href="#">Dolor sit</a>
        </li>
        <li>
            <a href="#">Amet</a>
        </li>
        <li>
            <a href="#">Consectetur</a>
        </li>
        <li>
            <a href="#">Consectetur adipisicing</a>
        </li>
    </ul>
    <span class="footer-copirights">&copy;2014 elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</span>
    <span class="footer-email">email: zhiga_@mail.ru</span>
    <span class="footer-phone">тел: +7 778 152 24 33, +7 7172 522 466</span>
</div>
</body>
</html>