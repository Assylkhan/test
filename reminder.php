<?php

// change the following paths if necessary
$yii = dirname(__FILE__) . '/libs/yii/framework/yii.php';
$config = dirname(__FILE__) . '/protected/config/main.php';

require_once($yii);
Yii::createWebApplication($config);


$sql_cards_data = 'SELECT  t.email AS email, DATE_ADD(u.publicationdate,INTERVAL u.months MONTH) AS OrderEndDate FROM orders u JOIN cards p ON p.cardid = u.cardid JOIN users t ON t.userid = p.userid WHERE (TO_DAYS(DATE_ADD(u.publicationdate,INTERVAL u.months MONTH)) - TO_DAYS(CURRENT_DATE)) = 2';
$cards_data = Yii::app()->db->createCommand($sql_cards_data)->queryAll(true);

print_r($cards_data);

for ($i = 0; $i < count($cards_data); $i++):
    echo $cards_data[$i]['email'] . '<br>';

    sendMailReminder($cards_data[$i]['email'], $cards_data[$i]['OrderEndDate']);

endfor;


function sendMailReminder($email, $end_date)
{
    Yii::import('ext.YiiMailer.YiiMailer');
    $host = Yii::app()->params['host'];
    $content = "
		Уважаемый клиент!<br>
		Уведомляем Вас о том, что срок размещения на
        электронном и мобильном справочник Info Smart заканчивается " . $end_date . ".<br>
        Если вы желаете продлить размещение на нашем сайте, просим Вас сделать это в Личном кабинете!<br>
        Благодарим за сотрудничество и рады будем видеть Вас снова в числе наших
        клиентов!<br>
        <h4>С Уважением, команда Info Smart!</h4>";

    $mail = new YiiMailer('contact', array('message' => $content, 'name' => '', 'description' => 'Напоминание о продлении'));

    $mail->setFrom(Yii::app()->params['mailingAddress']);
    $mail->setSubject('Электронный и мобильный справочник Info Smart. Срок размещения заканчивается');
    $mail->setTo($email);

    if (!$mail->send()) {
        throw new EmailException('dont send error');
    }
}
