<?php

// change the following paths if necessary
$yii = dirname(__FILE__) . '/libs/yii/framework/yii.php';
$config = dirname(__FILE__) . '/protected/config/main.php';

require_once($yii);
Yii::createWebApplication($config);

sendMailReminder();

function sendMailReminder()
{
    $email = 'kohakoshkin@mail.ru';
    Yii::import('ext.YiiMailer.YiiMailer');
    $host = Yii::app()->params['host'];
    $content = "
		Уважаемый клиент!<br>
		Уведомляем Вас о том, что срок размещения на
        электронном и мобильном справочник Info Smart заканчивается.<br>
        Если вы желаете продлить размещение на нашем сайте, просим Вас сделать это в Личном кабинете!<br>
        Благодарим за сотрудничество и рады будем видеть Вас снова в числе наших
        клиентов!<br>
        <h4>С Уважением, команда Info Smart!</h4>";

    $mail = new YiiMailer('contact', array('message' => $content, 'name' => '', 'description' => 'Напоминание о продлении'));

    $mail->setFrom(Yii::app()->params['mailingAddress']);
    $mail->setSubject('Электронный и мобильный справочник Info Smart. Срок размещения заканчивается');
    $mail->setTo($email);

    if (!$mail->send()) {
        throw new EmailException('dont send error');
    }
}
