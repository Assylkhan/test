<?php

/*********************************************************************************
 * LiveZilla config.inc.php
 *
 * Copyright 2013 LiveZilla GmbH
 * All rights reserved.
 * LiveZilla is a registered trademark.
 *
 * Improper changes in this file may cause critical errors.
 * To modify LiveZilla Server settings it is strongly recommended to use
 * LiveZilla Server Admin application and desist from editing this file directly.
 *
 ********************************************************************************/

// ALL VALUES ARE BASE64 ENCODED (encoder/decoder can be found on the net)

$_CONFIG["gl_lzid"] = "NjBhMzJlNzA=";
$_CONFIG["gl_lzst"] = "MA==";
$_CONFIG["gl_pr_csp"] = "MmYyMDRiN2Q5YWEzYTYyNjc3NTg4ODUwNDc1NTYyZDY=";
$_CONFIG["gl_pr_nbl"] = "NWI2ZjQ3OTgxMjBmOWFmZGEyNmI4NjM2MmM3Yzg4MTk=";
$_CONFIG["gl_pr_ngl"] = "NjZkYmQzOWVkMzZmMjJjMWU3OWFiZjQ3NjcxNzZlNGM=";
$_CONFIG["gl_pr_str"] = "NmE0Njc3Zjg2OTA2NDMxOTM0NTY3ZTEwNTBjNmNiMmU=";
$_CONFIG["gl_pr_st"] = "MF8wXzA=";
$_CONFIG["gl_pr_cr"] = "MTQxMTAxODAxNw==";
$_CONFIG["gl_crc3"] = "TVRReE1UQXhPREF4Tnl3d0xEQXNNQ3d3TERBc05qWmtZbVF6T1dWa016Wm1NakpqTVdVM09XRmlaalEzTmpjeE56WmxOR009";
$_CONFIG["gl_licl"][0] = "WVRveU9udHBPakE3Y3pvME5Eb2lUVlJOTlU5RVVYbGFhbU40VFcxUk1scEVZekpPUjA1c1RqSlZNRTVIV21oYVZGVTFUa1JDYkZsdFZUMGlPMms2TVR0ek9qZzZJbFpHU2twUlZYYzlJanQ5";

// Database settings pets
$_CONFIG[0]["gl_db_host"] = "YXAtY2Rici1henVyZS1lYXN0LWMuY2xvdWRhcHAubmV0";
$_CONFIG[0]["gl_db_user"] = "YjlkOWNkZDc2N2M1MDI=";
$_CONFIG[0]["gl_db_ext"] = "bXlzcWw=";
$_CONFIG[0]["gl_db_pass"] = "NTIxYjNiNmM=";
$_CONFIG[0]["gl_db_name"] = "Y2RiX2MyMjRiNDU5Yjc=";
$_CONFIG[0]["gl_db_prefix"] = "bHpf";
$_CONFIG[0]["gl_host"] = "aW40c21hcnQuYXp1cmV3ZWJzaXRlcy5uZXQ=";
$_CONFIG[0]["gl_root"] = "MQ==";
$_CONFIG[0]["gl_datprov"] = "MQ==";


// Installer login pets
$_CONFIG[0]["gl_insu"] = "YWRtaW4=";
$_CONFIG[0]["gl_insp"] = "NTYxZmQxZjc4NjE3NmU5ZThlZjgyOWIxMzUyNjhkZmM=";


?>